FROM node:alpine as web

LABEL maintainer="matthias.hortenhuber@ki.se"

 # Project Files and Settings
 ARG PROJECT=dcc
 ARG PROJECT_DIR=/var/www/

 # Install Python and Package Libraries
RUN apk add --no-cache\
    bash\
    python\
    apache2-mod-wsgi\
    python-dev\
    py-pip\
    py-cffi\
    libffi-dev\
    gcc\
    make\
    g++\
    git\
    jpeg-dev\
    libpng-dev\
    freetype-dev\
    musl-dev\
    postgresql-dev\
    fftw-dev \
    openblas-dev \
    py-cairo\
    py-numpy

RUN mkdir -p $PROJECT_DIR
COPY requirements.txt $PROJECT_DIR/requirements.txt
COPY package.json yarn.lock $PROJECT_DIR/
WORKDIR $PROJECT_DIR
RUN pip install -r requirements.txt
RUN yarn install --silent --no-lockfile
RUN yarn global add jsdoc --silent

COPY . $PROJECT_DIR
