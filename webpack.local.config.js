const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const config = require('./webpack.base.config.js');

// config.entry = [
//   // 'webpack-hot-middleware/client?http://localhost:3000',
//   './dcc/static/js/react'
// ];

config.output.publicPath = 'http://localhost:3000/static/js/bundles/';
config.output.filename= "[name]-dev.js";
console.log("+++++++local environment++++++++")
config.plugins = config.plugins.concat([
  // new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new BundleTracker({ filename: './webpack-local-stats.json' }),
  new webpack.SourceMapDevToolPlugin({ filename: '[file].map' }),
  // removes a lot of debugging code in React
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('development')
    }
  }),

  // // keeps hashes consistent between compilations
  // new webpack.optimize.OccurrenceOrderPlugin(),
  //
  // // minifies your code
  // new webpack.optimize.UglifyJsPlugin({
  //     compressor: {
  //         warnings: false
  //     }
  // }),

  // new PrepackWebpackPlugin({mathRandomSeed:'seedvalue'}),
  new WebpackCleanupPlugin()

]);

// Add a loader for JSX files
config.module.rules.push(
  {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    query: {
      //specify that we will be dealing with React code
      presets: ['env', 'react']
    }
  },
  {
    test: /\.css$/,
    loader: 'style-loader!css-loader?root=.'
  },
  { test: /\.scss$/, loader: 'style!css!postcss-loader!sass' }
);
module.exports = config; //important to _not_ export it as {config}
