const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');
const environments = require('gulp-environments');
const PluginError = require('plugin-error');
const cache = require('gulp-cached');
const log = require('fancy-log');
const postcss = require('gulp-postcss');
const purify = require('gulp-purifycss');
const atImport = require("postcss-import")
const sass = require('gulp-sass');
const calc = require("postcss-calc")

const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const webpackConfig = require(process.env.NODE_ENV === 'development' ?
  './webpack.local.config' :
  './webpack.prod.config');
const browsersync = require('browser-sync');
const {
  server,
  browser
} = require('./server');
const spawn = require('child_process')
  .spawn;

const development = environments.development;
const production = environments.production;

gulp.task('django-runserver', function() {
  var runserver = spawn('python', ['./dcc/manage.py', 'runserver_plus'], {
    stdio: 'inherit'
  });

  runserver.on('close', function(code) {
    if (code !== 0) {
      console.error('Django runserver exited with error code: ' + code);
      process.exit(-1);
    } else {
      console.log('Django runserver exited normally.');
    }
  });
});
gulp.task('fonts', function() {
  return gulp
    .src("node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid*")
    .pipe(gulp.dest("dcc/dcc/static/webfonts/"));
})
gulp.task('webpack-prod', function(callback) {
  webpack(webpackConfig, function(err, stats) {
    if (err) throw new PluginError('webpack', err);
    log(
      '[webpack]',
      stats.toString({
        assets: true,
        chunks: false,
        chunkModules: false,
        colors: true,
        hash: false,
        timings: false,
        mode: 'production',
        version: false,
        displayModels: true
      })
    );
    callback();
  });
});
gulp.task('css', function() {
  const plugins = [
    atImport,
    calc(),
    // purify({
    //   content: ['dcc/dcc/static/js/*.{js,jsx}',
    //     'dcc/modeldcc/templates/**/*.html'
    //   ],
    //   whitelist: [
    //     'seriesHeaderTitle',
    //     'biosampleHeaderTitle',
    //     'assayHeaderTitle',
    //     'sequencingHeaderTitle',
    //     'dataHeaderTitle'
    //   ]
    // }),
    autoprefixer(),
    cssnano()
  ]
  return gulp
    .src(['dcc/dcc/static/css/*.scss'])
    // .pipe(cache('styling'))
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(rename({
      extname: '.css'
    }))
    .pipe(purify(['dcc/dcc/static/js/*.{js,jsx}',
     'dcc/modeldcc/templates/**/*.html'],
     { whitelist: [
       'seriesHeaderTitle',
        'biosampleHeaderTitle',
        'assayHeaderTitle',
        'sequencingHeaderTitle',
        'dataHeaderTitle']
      }
    ))
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write())
    .pipe(
      gulp.dest(function(file) {
        return file.base;
      })
    )
    .pipe(browser.stream());
});
gulp.task('js', function() {
  return gulp
    .src(['dcc/dcc/static/js/[^_]*.js', '!dcc/dcc/static/js/*.min.js'])
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(production(uglify()))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sourcemaps.write())
    .pipe(
      gulp.dest(function(file) {
        return file.base;
      })
    )
    .pipe(browser.stream());
});

function reload(done) {
  browser.reload();
  done();
}

function stream(done) {
  browser.reload({
    stream: true
  });
  done();
}
const watch = () => {
  gulp.watch(
    ['dcc/dcc/static/js/*.{js,jsx}', '!dcc/dcc/static/js/*.min.js'],
    gulp.series('js', reload)
  );
  gulp.watch(
    ['dcc/modeldcc/templates/**/*.html', 'dcc/modeldcc/*.py'],
    gulp.series(reload)
  );
  gulp.watch(['dcc/dcc/static/css/*.scss'], gulp.series('css'));
};
export const dev = gulp.series(
  gulp.parallel('js', 'css'),
  gulp.parallel(server, watch, 'django-runserver')
);
export const servers = gulp.series(
  gulp.parallel(server, 'django-runserver')
);
export const prod = gulp.parallel('css', 'js', 'fonts', 'webpack-prod');
