#!/bin/sh

python dcc/manage.py collectstatic --noinput --verbosity 0 && \
gunicorn --chdir dcc dcc.production_wsgi:application --bind 0.0.0.0:8000
