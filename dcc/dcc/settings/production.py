"""
Django production settings for dcc project.
"""

import os
import logging

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY =  os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
COMPRESS_ENABLED = False
COMPRESS_OUTPUT_DIR= 'compressor'
COMPRESS_CSS_FILTERS = ["compressor.filters.cssmin.CSSMinFilter"]
COMPRESS_JS_FILTERS = ["compressor.filters.jsmin.JSMinFilter"]
GZIP_CONTENT_TYPES = (
    'text/css',
    'application/javascript',
    'application/x-javascript',
    'text/javascript'
)
FILE_UPLOAD_PERMISSIONS = 0644  # user r+w group r others r
ALLOWED_DOWNLOADS = ['BIGNARROWPEAK', 'BIGWIG', 'BIGBED']
SETTINGS_EXPORT = [
    'DEBUG',
    'ALLOWED_DOWNLOADS',
]
GROUP = os.environ.get('GROUP')
ALLOWED_HOSTS = [
    'web', '0.0.0.0'
]
# Application definition

INSTALLED_APPS = (
    'admin_tools',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'modeldcc',
    'django_tables2',
    'django_extensions',
    'bootstrap3',
    'crispy_forms',
    'django.contrib.admin',
    'webpack_loader',
    'rest_framework',
    'cachalot',
    'compressor',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.views.decorators.csrf._EnsureCsrfCookie',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

ROOT_URLCONF = 'dcc.urls'

CRISPY_TEMPLATE_PACK = 'bootstrap3'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django_settings_export.settings_export',
                ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader',
            ],
        },
    },
]

WSGI_APPLICATION = 'dcc.wsgi.application'

LOGIN_REDIRECT_URL = '/dcc/'
LOGIN_URL = '/users/login/'

UPLOAD_PATH = '/var/www/non_annotated_files/'

#User registration settings
ACCOUNT_ACTIVATION_DAYS = 1 # One-day activation window;

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('SQL_ENGINE'),
        'NAME': os.environ.get('SQL_DATABASE'),
        'USER': os.environ.get('SQL_USER'),
        'PASSWORD': os.environ.get('SQL_PASSWORD'),
        'HOST': os.environ.get('SQL_HOST'),
        'PORT': os.environ.get('SQL_PORT')
    }
}
FILE_UPLOAD_HANDLERS = (
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'US/Pacific'

USE_I18N = True

USE_L10N = True

USE_TZ = True

#SECURE_SSL_REDIRECT = True
#SESSION_COOKIE_SECURE = True
#CSRF_COOKIE_SECURE = True


STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')
STATICFILES_DIRS = ('/var/www/dcc/dcc/static/',)

MEDIA_URL = '/files/'
MEDIA_ROOT = '/var/www/dcc/modeldcc/media_root/'
MEDIA_ROOT_ANNOT = '/var/www/annotated_files'
MEDIA_ROOT_DCC = '/var/www'
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'js/bundles/',
        'STATS_FILE': '/var/www/webpack-stats-prod.json',
    }
}


ADMINS = (
  ('admin', os.environ.get('ADMIN_EMAIL')),
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(MEDIA_ROOT,'tmp'),
    }
}
EMAIL_HOST = os.environ.get('HOST_EMAIL')
EMAIL_HOST_USER = os.environ.get('HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('HOST_PASSWD')
EMAIL_PORT = 587
EMAIL_USE_TLS = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SHORT_DATETIME_FORMAT = 'Y-m-d'
CONN_MAX_AGE = 120
