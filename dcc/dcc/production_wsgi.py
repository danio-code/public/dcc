"""
WSGI config for dcc project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys
import site

#site-packages of virtualenv

# Add the app's directory to the PYTHONPATH
sys.path.append('/var/www/html/dcc')
sys.path.append('/var/www/html/dcc/dcc')
# sys.path.append('/opt/rh/httpd24/root/var/www/html/dcc/modeldcc')


# Activate your virtual env
# execfile(activate_env, dict(__file__=activate_env))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dcc.settings.production")

import django
django.setup()
from django.contrib.auth.handlers.modwsgi import check_password

from django.core.handlers.wsgi import WSGIHandler
application = WSGIHandler()
