import React from 'react';
import ReactDOM from 'react-dom';
import SearchInput, {
  createFilter
} from 'react-search-input';
import crossfilter from 'crossfilter';
import classnames from 'classnames';
import Collapsible from 'react-collapsible';
import TruncatedSpan from './truncated';
import LoadingAnimation from './loadingAnimation';
import HeatMap from './heatmap.jsx';
import createReactClass from 'create-react-class';
import ReactPaginate from 'react-paginate';
// import InfiniteAnyHeight from 'react-infinite-any-height';
import prettyBytes from 'pretty-bytes';
import FileSaver from 'file-saver';
import flatten from 'flat';
import {
  CSVLink
} from 'react-csv';
import uniqBy from 'lodash.uniqby';
import {
  Map
} from 'immutable';
// import { Transition, TransitionGroup } from 'react-transition-group';
import ClampLines from 'react-clamp-lines';
import reductio from 'reductio';

const csrftoken = Cookies.get('csrftoken');


/**
 * Fields for the facet sidebar. The first level, e.g. "Series", are the headers
 * for each section in the sidebar. Each section has a number of key-value pairs,
 * which correspond to the individual facets. The key for each pair is the key
 * for the data in the dataExport.json. The value is the displayed name for it
 * in the sidebar.
 *
 */
const sideBarContent = {
  headers: [{
      Series: {
        series_type: 'series type',
        series_status: 'series status'
      }
    },
    {
      Biosample: {
        biosample_type: 'biosample type',
        background: 'gen. background',
        anatomical_term: 'anat. term',
        stage: 'stage',
        post_fertilization: 'tpf',
        treatment: 'treatment',
        mutation_description: 'mut. description',
        biosample_lab: 'biosample lab'
      }
    },
    {
      Assay: {
        assay_type: 'assay type',
        assay_target: 'target',
        library_prep: 'library preparation',
        assay_lab: 'assay lab'
      }
    },
    {
      Sequencing: {
        platform: 'platform',
        instrument: 'instrument',
        paired_end: 'single/paired end',
        sequencing_lab: 'sequencing lab'
      }
    },
    {
      Data: {
        file_type: 'file type',
        mapped_genome: 'genome version',
        version: 'freeze version',
        command: 'pipeline'
      }
    }
  ]
};

/**
 * Create the list of select options used for the y-axis of the
 * heat map based on the key-value pairs in sideBarContent
 *
 *
 */
let heatMapOptions = sideBarContent.headers.reduce(function(result, current) {
  return Object.assign(result, current);
}, {});
let heatMapOptions_tmp = []
Object.keys(heatMapOptions)
  .map(key => heatMapOptions_tmp.push(heatMapOptions[key]))
heatMapOptions_tmp = heatMapOptions_tmp.reduce(function(result, current) {
  return Object.assign(result, current);
}, {});
heatMapOptions = []
for (let [key, value] of Object.entries(heatMapOptions_tmp)) {
  heatMapOptions.push({
    label: value,
    value: key
  })
}


/**
 * Set up csrf authentication for all ajax calls
 *
 */
function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
}
$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    xhr.setRequestHeader('X-CSRFToken', csrftoken);
  }
});


/**
 * Sort an array of objects by a specific key
 *
 */
function sortByKey(array, key) {
  return array.sort(function(a, b) {
    var x = a[key];
    var y = b[key];
    return x < y ? 1 : x > y ? -1 : 0;
  });
}


/**
 * Sort an array of objects by a two keys, one nested inside
 * the other
 *
 * @param  {array} array a nested array
 * @param  {string} keys  two nested keys given as a string separated by a "."

 */
function sortByNestedKey(array, keys) {
  let key = keys.split('.')
  return array.sort(function(a, b) {
    var x = a[key[0]][key[1]];
    var y = b[key[0]][key[1]];
    return x < y ? 1 : x > y ? -1 : 0;
  });
}


/**
 * The overall parent component rendering the data export table view
 */
var FileTable = createReactClass({
      displayName: 'Filetable',
      getInitialState() {
        return {
          data: [], // the initial data
          filter: '',
          filters: [],
          isFiltering: false,
          isLoading: false,
          filteredData: [], // the filtered data
          selected: Map(),
          selectedData: [],
          selectedFacet: {},
          selectedHeatMapOption: "assay_type",
          open: false,
          clearFilterLoading: false,
          elementsPerPage: 10,
          pageCount: 1,
          currentPage: 0,
          selectedView: this.props.view || 'table',
          seriesElements: {},
          loadingError: true,
          showTitles: [],
          cf: []
        };
      },
      componentDidMount() {
        this.setState({
          isLoading: false
        });
        this.loadTableFromServer();
      },

      /**
       * Initial data load from the server and save it in the state
       * (can take a long time)
       */
      loadTableFromServer() {
        $.ajax({
          url: this.props.url,
          datatype: 'json',
          cache: false,
          success: function(data) {
            console.log('data loaded succesfully')
            this.setState({
              data: data.data,
              filteredData: data.data,
              filterKeys: data.filterKeys
            });
            this.updatePageCount(data.data);
          }.bind(this),
          error: function(response) {
            this.setState({
              loadingError: true
            });
          }
        });
      },

      /**
       * Update the pagination page count
       *
       */
      updatePageCount(filteredData = this.state.filteredData) {
        filteredData = uniqBy(filteredData, 'series_id');// remove duplicate series entries
        this.setState({
          pageCount: Math.ceil(filteredData.length / this.state.elementsPerPage)
        });
      },

      /**
       * Update the state for currentPage when the pagination
       * page changes
       *
       */
      handlePageChange(page) {
        this.setState({
          currentPage: page.selected
        });
      },

      /**
       * Create the SeriesElement components
       *
       * @return {array}  an array of divs, one div per Series Element
       */
      createSeriesElements() {
        if (this.state.filteredData.length > 0) {//check if the filtering did /
          //not remove all data, i.e. the filtered term didn't exist
          if (this.state.filter === '') {
            var filteredData = this.state.filteredData;
          } else { // filter the data if a search filter has been set
            var filteredData = this.state.filteredData.filter(
              createFilter(this.state.filter, this.state.filterKeys)
            );
          }
          filteredData = sortByKey(filteredData, 'series_id');
          let cf = crossfilter(filteredData); //generate crossfilter object
          var bySeriesId = cf.dimension(d => d.series_id); //group by series_id
          var seriesElements = uniqBy(filteredData, 'series_id')
            .slice(
              this.state.currentPage * this.state.elementsPerPage,
              (this.state.currentPage + 1) * this.state.elementsPerPage
            );// create pagination slice
          seriesElements = seriesElements.map(
              function(series, i, elements) {
                var filteredBySeriesId = bySeriesId
                  .filter(series.series_id)
                  .top(Infinity);
                filteredBySeriesId = sortByKey(filteredBySeriesId, 'biosample_id');
                bySeriesId.filterAll();
                return <div key = {
                  series.series_id
                }
                id = {
                    series.series_id
                  } >
                  <div className = "series" >
                    <span className = "series_title" >
                      <span className = "series_id" > {series.series_id}
                        </span> {
                    series.series_title
                  } &ensp;
                ( <a className = "detail_link"
                  href = {
                    "dcc/detailSeries/" + parseInt(series.series_id.slice(3, 9), 10)
                  } >details </a>)
                    </span>
                  <div className = "row m-0 " >
                  <div className = "col-xl-10 col-lg-9 col-12" >
                  <dl className = "single_column m-0" >
                  <dt> description </dt>
                  <dd>
                  <ClampLines text = {
                    series.series_description
                  }
                  lines = "1"
                  moreText = "more"
                  lessText = "less"
                  className = "description seriesdescription" />
                  </dd>
                </dl> </div> {
                    i <elements.length - 1 ? <div className = "p-0 col-xl-2 col-lg-3 col-12 " > {
                        " "
                      } <a href = {
                        "#" + elements[i + 1].series_id
                      }
                    className = "btn btn-danger m-0 next-series" > {
                        "Jump to next series"
                      } </a> </div> : null} </div> <Biosamples biosample = {
                        filteredBySeriesId
                      }
                    drawer = {
                      this.updateDrawer.bind
                    }
                    toggle = {
                      this.onChildToggle
                    }
                    delete = {
                      this.deleteItem
                    }
                    selection = {
                      this.state.selected
                    }
                    showTitles = {
                      this.state.showTitles
                    }
                    onMouseEnter = {
                      this.showTitleOn
                    }
                    onMouseLeave = {
                      this.showTitleOff
                    }
                    /> </div> </div>;
                  }.bind(this)
                );
                return seriesElements;
              }
            },
            toggleDrawer() {
              this.setState({
                open: !this.state.open
              });
            },
            updateDrawer(data) {
              this.setState({
                open: true
              });
              let selected = this.state.selectedData;
              selected.push(flatten(data));
              this.setState({
                selectedData: selected
              });
            },
            closeDrawer() {
              this.setState({
                open: false,
                selected: Map(),
                selectedData: []
              });
            },

            /**
             * Remove all or specific filter terms
             *
             * @param  {string} category    if no category is specified all
             * filters will be removed
             */
            clearFilter(category = '') {
              let clearedSelectedFacet = this.state.selectedFacet;
              let clearedFilteredData = this.state.data;
              if (category.length > 0) {
                clearedSelectedFacet[category] = false;
                for (let key in clearedSelectedFacet) {
                  if (clearedSelectedFacet[key]) {
                    let keys = key.split('-');
                    let facetValue = keys[0];
                    let facetKeys = keys[1];
                    if (facetValue.trim()
                      .indexOf(' ') > 0) {
                      //hotfix for not working exact match if facetValue contain multiple words
                      clearedFilteredData = clearedFilteredData.filter(
                        createFilter(facetValue, facetKeys)
                      );
                    } else {
                      clearedFilteredData = clearedFilteredData.filter(
                        createFilter(facetValue, facetKeys, {
                          exactMatch: true
                        })
                      );
                    }
                  }
                  clearedFilteredData = clearedFilteredData.filter(
                    createFilter(this.state.filter, this.state.filterKeys)
                  );
                }
              } else {
                for (let key in clearedSelectedFacet) {
                  clearedSelectedFacet[key] = false;
                }
              }
              this.setState({
                filter: '',
                filteredData: clearedFilteredData,
                selectedFacet: clearedSelectedFacet,
                isFiltering: false,
                clearFilterLoading: !this.state.clearFilterLoading
              });
              this.updatePageCount(this.state.data);
            },

            /**
             * Filter based on search input field (after more
             * than three characters have been entered)
             *
             * @param  {string} term the entered search term

             */
            searchUpdated(term) {
              if (term.length > 3) {
                this.setState({
                  filter: term,
                  isFiltering: true
                });
                this.filterData();
              }
            },

            /**
             * Remove selected data item from selection
             *

             */
            deleteItem(item) {
              let newSelectedData = this.state.selectedData.filter(val => val !== item);
              let newSelected = this.state.selected;
              newSelected['DA' + item.data_id] = false;
              this.setState({
                selectedData: newSelectedData,
                selected: newSelected
              });
              if (newSelectedData.length === 0) {
                this.closeDrawer();
              }
            },

            /**
             * Open drawer after data item was selected
             *
             */
            afterSelect() {
              this.setState({
                open: true
              });
            },

            /**
             * Select the attribute for y-axis in the heatmap
             *
             */
            selectHeatMapOption(value) {
              this.setState({
                selectedHeatMapOption: value
              });
            },

            /**
             * Filter the data based on selected facets and/or
             * entered search terms
             *
             */
            filterData() {
              let filteredData = this.state.data.filter(
                createFilter(this.state.filter, this.state.filterKeys)
              );
              this.setState({
                filteredData: filteredData,
                isFiltering: true
              });
              this.updatePageCount(filteredData);
            },

            /**
             *              *
             */

            /**
             * Initiate filtering based on selected facets
             *
             * @param {object}  selectedFacets An object, where each key correspond to
             * a key in the dataExport.json and the value to a specific
             * value for the given key in the dataExport.json
             */
            filterFacet(selectedFacets) {
              for (let facetKeys in selectedFacets) {
                let facetValue = selectedFacets[facetKeys]
                if (!this.state.selectedFacet[facetValue + '-' + facetKeys]) {
                  if (facetValue.trim()
                    .indexOf(' ') + facetValue.trim()
                    .indexOf('(') > 0) {
                    //hotfix for not working exact match if facetValue contain multiple words
                    facetValue = facetValue.replace('(', '\\(')
                      .replace(')', '\\)');
                    var filteredData = this.state.filteredData.filter(
                      createFilter(facetValue, facetKeys)
                    );
                  } else {
                    var filteredData = this.state.filteredData.filter(
                      createFilter(facetValue, facetKeys, {
                        exactMatch: true
                      })
                    );
                  }
                  let selected = this.state.selectedFacet;
                  selected[facetValue + '-' + facetKeys] = !this.state.selectedFacet[
                    facetValue + '-' + facetKeys
                  ];
                  this.setState({
                    filteredData: filteredData,
                    isFiltering: true,
                    selectedFacet: selected
                  });
                  this.updatePageCount(filteredData);
                } else {
                  this.clearFilter(facetValue + '-' + facetKeys);
                }
              }
            },

            /**
             * filter data based on selection of a tile in the
             * heatmap
             *
             */
            heatMap2SideBar(x, y) {
              let selectedData = this.state.filteredData;
              for (let [xKey, xValue] of x) {
                selectedData = selectedData.filter(createFilter(xValue, xKey));
                for (let [yKey, yValue] of y) {
                  selectedData = selectedData.filter(createFilter(yValue, yKey));
                }
              }
            },
            onChildToggle(data, isSelected) {
              let selections = this.state.selected;
              let selectionsData = this.state.selectedData;
              if (!isSelected) {
                selectionsData = selectionsData.filter(val => {
                  return val !== data;
                });
              } else {
                this.setState({
                  open: true
                });
                selectionsData.push(flatten(data));
              }
              selections = selections.set('DA' + data.data_id, isSelected);
              this.setState({
                selected: selections,
                selectedData: uniqBy(selectionsData, 'data_id')
              });
              if (selectionsData.length === 0) {
                this.closeDrawer();
              }
            },

            /**
             * toggle between table and heatmap view
             *
             */
            toggleView(view) {
              this.setState({
                selectedView: view
              });
            },

            /**
             * send a file request to the server to recieve an tar
             * zipped archive for the selected data items
             *
             */
            downloadZip() {
              let filepaths = [];
              let selected = this.state.selectedData;
              selected.map(function(item) {
                filepaths.push(item['primary_file_url'].replace("https://danio-code.zfin.org", ""));
                if (item['secondary_file_url'] !== 'no value') {
                  filepaths.push(item['secondary_file_url'].replace("https://danio-code.zfin.org", ""));
                }
              });
              return (window.location =
                '/getZippedFiles/?filepaths=' + filepaths.toString());
            },

            /**
             * create a file called 'DCC-urls.txt' with the urls
             * to the selected data items (without contacting the server)
             *
             * @return {type}  description
             */
            downloadURL() {
              let filepaths = [];
              let selected = this.state.selectedData;
              selected.map(function(item) {
                filepaths.push(item['primary_file_url']);
                if (item['secondary_file_url'] !== 'no value') {
                  filepaths.push(item['secondary_file_url']);
                }
              });
              filepaths = Object.values(filepaths)
                .join('\n')
              filepaths = new Blob([filepaths], {
                type: 'application/txt'
              });
              FileSaver.saveAs(filepaths, 'DCC-urls.txt');
            },

            /**
             * select all (filtered) data items of the table,
             * including those hidden because of pagination
             *
             * @return {type}  description
             */
            selectAll() {
              var selected = [];
              this.state.filteredData.map(item => {
                selected.push(flatten(item));
              });
              this.setState({
                open: true,
                selectedData: selected
              });
            },

            /**
             * display the ids as title for the items
             *
             */
            showTitleOn() {
              this.setState({
                showTitle: true
              })
            },
            /**
             * remove the ids as title for the items
             *
             */
            showTitleOff() {
              this.setState({
                showTitle: false
              })
            },
            render() {
              let filterBtnClasses = classnames('btn btn-outline-primary', {
                hidden: !this.state.isFiltering
              });

              return ( <div >
                 <div className = {
                  classnames('slide_content', {
                    slide_content_open: this.state.open
                  })
                } >
                <div className = "container-fluid" >
                <div className = "button_row row" >
                <div className = "btn-group col-3 d-flex justify-content-center" >
                <button className = {
                  classnames('btn btn-outline-primary', {
                    'btn-selected active': this.state.selectedView === 'table'
                  })
                }
                onClick = {
                  () => {
                    this.toggleView('table');
                  }
                } >
                <span >
                <i className = "fas fa-align-justify" /> &emsp; Table </span> </button> <button className = {
                  classnames('btn btn-outline-primary', {
                    'btn-selected active': this.state.selectedView === 'heatmap'
                  })
                }
                onClick = {
                  () => {
                    this.toggleView('heatmap');
                  }
                } >
                <span >
                <i className = "fas fa-th" /> &emsp; Heat Map </span> </button> </div> <div className = "search_row col-lg-8 col-md-12" >
                <button className = {
                  filterBtnClasses
                }
                onClick = {
                  () => this.clearFilter()
                } >
                Clear filters </button> <SearchInput className = "search-input col-9b"
                onChange = {
                  this.searchUpdated
                }
                value = {
                  this.state.filter
                }
                /> </div> <button className = "btn btn-outline-primary select-all"
                onClick = {
                  () => {
                    this.selectAll();
                  }
                } >
                <span > Select All </span> </button> </div> </div> <div className = "fileTable" >
                <SideBar onFacetChange = {
                  this.searchUpdated
                }
                filterFacet = {
                  this.filterFacet
                }
                selectedFacet = {
                  this.state.selectedFacet
                }
                filteredData = {
                  this.state.filteredData
                }
                crossfilterData = {
                  crossfilter(this.state.filteredData)
                }
                clearFilter = {
                  this.clearFilter
                }
                filter = {
                  this.state.filter
                }
                />{' '} {
                  this.state.selectedView === 'heatmap' ? //if heatmap is selected render the heatmap instead of the table
                  ( <div >
                    <HeatMapSelect selectHeatMapOption = {
                      this.selectHeatMapOption
                    }
                    selectedHeatMapOption = {
                      this.state.selectedHeatMapOption
                    }
                    /> <HeatMap data = {
                      this.state.filteredData
                    }
                    size = {
                      [950, 750]
                    }
                    margin = {
                      {
                        top: 10,
                        right: 10,
                        bottom: 150,
                        left: 100
                      }
                    }
                    filterFacet = {
                      this.filterFacet
                    }
                    toggleView = {
                      this.toggleView
                    }
                    selectedOption = {
                      this.state.selectedHeatMapOption
                    }
                    /> </div>
                  ) : this.state.filteredData.length > 0 ?
                  // only render the table if not all items were
                  // filtered away
                  ( <DataTable pageCount = {
                      this.state.pageCount
                    }
                    handlePageChange = {
                      this.handlePageChange
                    }
                    currentPage = {
                      this.state.currentPage
                    }
                    selected = {
                      this.state.selected
                    }
                    createSeriesElements = {
                      this.createSeriesElements
                    }
                    filteredData = {
                      this.state.filteredData
                    }
                    />
                  ) : this.state.filter.length > 0 ?
                  // if the filteredData objects is empty but a filter was
                  // given, inform about a unsuccessful search
                  ( <h2 className = "m-auto mt-4" >
                    No results
                    for {
                      this.state.filter
                    } </h2>
                  ) : ( <LoadingAnimation />
                  )
                } </div> </div> <div className = {
                  classnames('dock ', {
                    dock_open: this.state.open
                  })
                } >
                <button onClick = {
                  () => this.closeDrawer()
                }
                type = "button"
                className = "close w-100"
                aria-label = "Close" >close</button>
                <Collapsible trigger = {
                  'Show/Hide Files'
                }
                triggerClassName = {
                  'bg-light'
                }
                triggerOpenedClassName = {
                  'bg-light'
                }
                open = {
                  true
                } >
                <ul key = "dataList"
                className = "data_list list-group"
                id = "dataList " > {
                  this.state.selectedData.map(item => ( <li key = {
                        item.data_id
                      }
                      className = "list-group-item" >
                      <button onClick = {
                        () => this.deleteItem(item)
                      }
                      type = "button"
                      className = "close"
                      aria-label = "Close" >
                      <span aria-hidden = "true" > &times; </span> </button> <TruncatedSpan text = {
                        item['primary_file_name'].substring(
                          item['primary_file_name'].lastIndexOf('/') + 1
                        )
                      }
                      length = {
                        80
                      }
                      /> {
                        item['primary_file_name'].length > 0 ? <TruncatedSpan
                        text = {
                          item['primary_file_name'].substring(
                            item['primary_file_name'].lastIndexOf('/') + 1
                          )
                        }
                        length = {
                          80
                        }
                        /> : null} </li>
                      ))
                  } </ul> </Collapsible> <div className = "button_row" >
                  <div className = "btn-group-vertical" >
                  <button type = "button"
                  className = 'btn btn-outline-primary'
                  data-toggle = "tooltip"
                  data-placement = "top"
                  onClick = {
                    () => this.downloadURL()
                  } >
                  Get URLs </button> <CSVLink
                  target = "_self"
                  className = "btn btn-outline-primary"
                  filename = {
                    'DCC-annotations.csv'
                  }
                  headers = {
                    [
                      "series_id",
                      "series_title",
                      "series_status",
                      "series_type",
                      "series_description",
                      "publication",
                      "sra_geo_id",
                      "biosample_id",
                      "biosample_lab",
                      "biosample_type",
                      "background",
                      "anatomical_term",
                      "post_fertilization",
                      "stage",
                      "sequencing_lab",
                      "treatment",
                      "mutation_description",
                      "source",
                      "biosample_description",
                      "biosample_controlled_by",
                      "biosamplereplicate_id",
                      "assay_id",
                      "assay_type",
                      "assay_lab",
                      "assay_target",
                      "library_prep",
                      "assay_description",
                      "appliedassay_id",
                      "appliedassay_controlled_by",
                      "technicalreplicate_id",
                      "sequencing_id",
                      "platform",
                      "instrument",
                      "date",
                      "paired_end",
                      "strand_mode",
                      "max_read_length",
                      "data_id",
                      "file_type",
                      "primary_file_url",
                      "primary_file_name",
                      "primary_file_size",
                      "secondary_file_url",
                      "secondary_file_name",
                      "secondary_file_size",
                      "sha1",
                      "mapped_genome",
                      "version",
                      "command"
                    ]
                  }
                  separator = {
                    '\;'
                  }
                  data = {
                    this.state.selectedData
                  } >
                  Download Annotations Table </CSVLink> <button data-toggle = "tooltip"
                  data-placement = "top"
                  type = "button"
                  className = 'btn btn-outline-primary'
                  onClick = {
                    () => this.downloadZip()
                  } >
                  Get (zipped) Files </button> </div> </div> </div>  </div>
                );
              }
            });

        /**
         * The select field for the y-axis of the heat map
         */
        const HeatMapSelect = createReactClass({
              displayName: 'HeatMapSelect',
              getInitialState: function() {
                return {
                  options: heatMapOptions
                };
              },
              handleChange: function(e) {
                this.props.selectHeatMapOption(e.target.value)
              },
              render() {
                return ( <div className = "input-group w-25 m-auto"
                  key = "selectGroup" >
                  <div className = "input-group-prepend" >
                  <label className = "input-group-text"
                  htmlFor = "inputGroupSelect01" > y-axis </label> </div> <select value = {
                    this.props.selectedHeatMapOption
                  }
                  onChange = {
                    this.handleChange
                  }
                  className = "custom-select"
                  id = "inputGroupSelect01" > {
                    this.state.options.map(item => ( <option value = {
                        item.value
                      }
                      key = {
                        item.value
                      } > {
                        item.label
                      } </option>
                    ))
                  } </select> </div>)
                }
              })

              /**
               * Sidebar element, which is the parent to the facets
               */
              const SideBar = createReactClass({
              displayName: 'SideBar',
              getInitialState() {
                return sideBarContent
              },
              render() {
                let facets = this.state.headers.map(
                  function(header, i) {
                    var section_title = Object.keys(header)[0];
                    let classes = `headerTitle ${section_title.toLowerCase()}HeaderTitle`;
                    return ( <div key = {
                        section_title
                      }
                      className = {
                        `facet facetSection ${section_title.toLowerCase()}FacetSection`
                      } >
                      <Collapsible trigger = {
                        section_title
                      }
                      triggerClassName = {
                        classes
                      }
                      triggerOpenedClassName = {
                        classes
                      }
                      open = {
                        true
                      } >
                      <Facet facetText = {
                        header[section_title]
                      }
                      section = {
                        section_title
                      }
                      filteredData = {
                        this.props.filteredData
                      }
                      crossfilterData = {
                        this.props.crossfilterData
                      }
                      filterFacet = {
                        this.props.filterFacet
                      }
                      clearFilter = {
                        this.props.clearFilter
                      }
                      selectedFacet = {
                        this.props.selectedFacet
                      }
                      /> </Collapsible> </div>
                    );
                  }.bind(this)
                );
                return <div className = "sideBar " > {
                  facets
                } </div>;
              }
            });

            /**
             * The parent facet, which contains a list of facet elements
             */
            var Facet = createReactClass({
              displayName: 'Facet',
              getInitialState() {
                return {
                  selected: {}
                };
              },
              render() {
                var facetElement = [];
                if (this.props.crossfilterData) {
                  let filterFacet = this.props.filterFacet;
                  for (let key in this.props.facetText) {
                    let byFacetText = this.props.crossfilterData.dimension(d => d[key]);
                    let reducer = reductio()
                      .exception(d => {
                        return d[this.props.section.toLowerCase() + '_id']
                      })
                      .exceptionCount(true)
                    let groupedByFacetText = reducer(byFacetText.group())
                      .all();
                    sortByNestedKey(groupedByFacetText, 'value.exceptionCount');
                    facetElement.push( <div key = {
                        key
                      }
                      className = "facetList" >
                      <div className = "d-flex flex-row justify-content-between align-items-center w-100" >
                      <b > {
                        this.props.facetText[key]
                      } </b>
                      </div> <FacetElements data = {
                        groupedByFacetText
                      }
                      category = {
                        key
                      }
                      filterFacet = {
                        filterFacet
                      }
                      clearFilter = {
                        this.props.clearFilter
                      }
                      selectedFacet = {
                        this.props.selectedFacet
                      }
                      /> </div>
                    );
                  }
                }
                return <div > {
                  facetElement
                } </div>;
              }
            });


            /**
             * The individual elements inside a facet
             */
            var FacetElements = createReactClass({
              displayName: 'FacetElements',
              shouldComponentUpdate(nextProps, nextState) {
                let thisGroupNames = [];
                let nextGroupNames = [];
                for (var element in this.props.selectedFacet) {
                  thisGroupNames.push(
                    element.substring(element.lastIndexOf('-') + 1, element.length)
                  );
                }
                let cond1 = thisGroupNames.indexOf(this.props.category) > -1;

                for (var element in nextProps.selectedFacet) {
                  nextGroupNames.push(
                    element.substring(element.lastIndexOf('-') + 1, element.length)
                  );
                }
                let cond2 = nextGroupNames.indexOf(nextProps.category) > -1;
                return !nextGroupNames.some(val => thisGroupNames.indexOf(val) === -1);
              },

              getInitialState() {
                return {
                  more: false,
                  selected: this.props.selectedFacet
                };
              },

              /**
               * Toggle between shortened and full list
               */
              toggleMore() {
                this.setState({
                  more: this.state.more ? false : true
                });
              },

              /**
               * Filter by clicked facet element
               *
               * @param  {string} key      name for the value of the facet element
               * in the dataExport.json
               * @param  {string} category key for the facet element in the
               * dataExport.json

               */
              handleClick(key, category) {
                this.props.filterFacet({
                  [category]: key
                });
              },
              render() {
                let totalCount = 0;
                this.props.data.forEach(function(text) {
                  totalCount += text.value.exceptionCount;
                });
                let facetElement = this.props.data.map(
                  function(text, i) {
                    let percentage = Math.ceil(text.value.exceptionCount / totalCount * 100);
                    if (i > 5 && !this.state.more) {
                      return false;
                    }
                    return ( <li key = {
                        text.key
                      }
                      className = {
                        classnames('facetOption', {
                          selectedFacet: this.state.selected[
                            text.key + '-' + this.props.category
                          ]
                        })
                      } >
                      <div onClick = {
                        () => this.handleClick(text.key, this.props.category)
                      } >
                      <TruncatedSpan text = {
                        text.key ? text.key : "no value"
                      }
                      length = {
                        60
                      }
                      /> &ensp;
                      ({
                        text.value.exceptionCount
                      }) <span className = "bar"
                      style = {
                        {
                          width: percentage + '%'
                        }
                      }
                      /> </div> </li>
                    );
                  }.bind(this)
                );
                let facetElements = [];
                let moreLess = '';
                if (!this.state.more) {
                  moreLess = 'more';
                  facetElements = facetElement.slice(0, 5);
                } else {
                  moreLess = 'less';
                  facetElements = facetElement;
                }
                return ( <div >
                  <ul > {
                    facetElements
                  } {
                    facetElement.length > 5 ? ( <span onClick = {
                        () => this.toggleMore()
                      }
                      className = "moreLess" > {
                        moreLess
                      } </span>
                    ) : null
                  } </ul> </div>
                );
              }
            });

            /**
             * Card-like element displaying the biosample information.
             * Nested inside it are similar card-like elements for biosample replicate,
             * applied assay, sequencing and data metadata. Uses a specially styled
             * `<dl>` to show small metadata keys and larger metadata values.
             */
            var Biosamples = createReactClass({
                  displayName: 'Biosamples',
                  render() {
                    var byBiosampleId = crossfilter(this.props.biosample)
                      .dimension(
                        d => d.biosample_id
                      );
                    var biosampleElements = uniqBy(this.props.biosample, 'biosample_id')
                      .map(
                        function(biosample, i) {
                          var filteredByBiosampleId = byBiosampleId
                            .filter(biosample.biosample_id)
                            .top(Infinity);

                          filteredByBiosampleId = sortByKey(
                            filteredByBiosampleId,
                            'biosamplereplicate_id'
                          );
                          byBiosampleId.filterAll();
                          return <div key = {
                            biosample.biosample_id
                          }
                          className = "card biosample" >
                            <span className = "title biosample_title" > {
                              biosample.biosample_id
                            } </span> <div className = "biosampleText" >
                            <dl >
                            <div className = "dict_wrapper" >
                            <dt > biosample type </dt> <dd > {
                              biosample.biosample_type
                            } </dd> </div> {
                              biosample.anatomical_term !== "no value" ? <div className = "dict_wrapper" >
                                <dt > anatomical term </dt> <dd > {
                                  biosample.anatomical_term
                                } </dd> </div> : null} <div className = "dict_wrapper" >
                                <dt > gen.background </dt> <dd > {
                                  biosample.background
                                } </dd> </div> <div className = "dict_wrapper" >
                                <dt > dev.stage </dt> <dd > {
                                  biosample.stage
                                } </dd> </div> {
                                  biosample.post_fertilization ? <div className = "dict_wrapper" >
                                    <dt > tpf </dt> <dd > {
                                      biosample.post_fertilization
                                    } </dd> </div> : null} {
                                      biosample.source ? <div className = "dict_wrapper" >
                                        <dt > source </dt> <dd > {
                                          biosample.source
                                        } </dd> </div> : null} {
                                          biosample.treatment ? <div className = "dict_wrapper" >
                                            <dt > treatment </dt> <dd > {
                                              biosample.treatment
                                            } </dd> </div> : null} {
                                              biosample.mutation_description ? <div className = "dict_wrapper" >
                                                <dt > mutation description </dt> <dd >
                                                <ClampLines text = {
                                                  biosample.mutation_description
                                                }
                                              lines = "3"
                                              moreText = "more"
                                              lessText = "less"
                                              className = "description " />
                                                </dd> </div> : null} {
                                                  biosample.biosample_description ? <div className = "dict_wrapper" >
                                                    <dt > biosample description </dt> <dd >
                                                    <ClampLines text = {
                                                      biosample.biosample_description
                                                    }
                                                  lines = "3"
                                                  moreText = "more"
                                                  lessText = "less"
                                                  className = "description " />
                                                    </dd> </div> : null} {
                                                      biosample.biosample_controlled_by !== "" ? <div className = "dict_wrapper" >
                                                        <dt > controlled by </dt> <dd > {
                                                          biosample.biosample_controlled_by
                                                        } </dd> </div> : null} </dl> {
                                                          /* <span>{biosample.bioreplicates}</span> */ } </div> <BiosampleReplicate biosampleReplicate = {
                                                          filteredByBiosampleId
                                                        }
                                                      drawer = {
                                                        this.props.drawer
                                                      }
                                                      delete = {
                                                        this.props.delete
                                                      }
                                                      toggle = {
                                                        this.props.toggle
                                                      }
                                                      selection = {
                                                        this.props.selection
                                                      }
                                                      /> </div>;
                                                    }.bind(this)
                                                );
                                              return <div className = "biosamples" > {
                                                biosampleElements
                                              } </div>;
                                            }
                                        });
                                  var BiosampleReplicate = createReactClass({
                                    displayName: 'BiosampleReplicate',
                                    render() {
                                      var byBiosampleReplicateId = crossfilter(
                                          this.props.biosampleReplicate
                                        )
                                        .dimension(d => d.biosamplereplicate_id);
                                      var biosampleReplicateElements = uniqBy(
                                          this.props.biosampleReplicate,
                                          'biosamplereplicate_id'
                                        )
                                        .map(
                                          function(biosampleReplicate) {
                                            var filteredByBiosampleReplicateId = byBiosampleReplicateId
                                              .filter(biosampleReplicate.biosamplereplicate_id)
                                              .top(Infinity);
                                            filteredByBiosampleReplicateId = sortByKey(
                                              filteredByBiosampleReplicateId,
                                              'appliedassay_id'
                                            );
                                            byBiosampleReplicateId.filterAll();
                                            return ( <div key = {
                                                biosampleReplicate.biosamplereplicate_id
                                              }
                                              className = "biosampleReplicate" >
                                              <AppliedAssay appliedAssay = {
                                                filteredByBiosampleReplicateId
                                              }
                                              drawer = {
                                                this.props.drawer
                                              }
                                              delete = {
                                                this.props.delete
                                              }
                                              toggle = {
                                                this.props.toggle
                                              }
                                              selection = {
                                                this.props.selection
                                              }
                                              /> </div>
                                            );
                                          }.bind(this)
                                        );
                                      return ( <div className = "biosampleReplicates d-flex flex-wrap" > {
                                          biosampleReplicateElements
                                        } </div>
                                      );
                                    }
                                  });
                                  var AppliedAssay = createReactClass({
                                        render() {
                                          var byAppliedAssayId = crossfilter(this.props.appliedAssay)
                                            .dimension(
                                              d => d.appliedassay_id
                                            );
                                          var appliedAssayElements = uniqBy(
                                              this.props.appliedAssay,
                                              'appliedassay_id'
                                            )
                                            .map(
                                              function(appliedAssay) {
                                                var filteredByAppliedAssayId = byAppliedAssayId
                                                  .filter(appliedAssay.appliedassay_id)
                                                  .top(Infinity);
                                                filteredByAppliedAssayId = sortByKey(
                                                  filteredByAppliedAssayId,
                                                  'technicalreplicate_id'
                                                );
                                                byAppliedAssayId.filterAll();
                                                return ( <div key = {
                                                    appliedAssay.appliedassay_id
                                                  }
                                                  className = "appliedAssay" >
                                                  <span className = "title appliedAssay_title" > {
                                                    appliedAssay.appliedassay_id
                                                  } </span> <dl >
                                                  <div className = "dict_wrapper" >
                                                  <dt > assay type </dt> <dd > {
                                                    appliedAssay.assay_type
                                                  } </dd> </div> {
                                                    appliedAssay.assay_target ? ( <div className = "dict_wrapper" >
                                                      <dt > target </dt> <dd > {
                                                        appliedAssay.assay_target
                                                      } </dd> </div>
                                                    ) : null
                                                  } {
                                                    appliedAssay.library_prep !== 'no value' ? ( <div className = "dict_wrapper" >
                                                      <dt > library preparation </dt> <dd > {
                                                        appliedAssay.library_prep
                                                      } </dd> </div>
                                                    ) : null
                                                  } <div className = "dict_wrapper" >
                                                  <dt > description </dt> <dd >
                                                  <ClampLines text = {
                                                    appliedAssay.assay_description
                                                  }
                                                  lines = "3"
                                                  moreText = "more"
                                                  lessText = "less"
                                                  className = "description " />
                                                  </dd> </div> {
                                                    appliedAssay.appliedassay_controlled_by !== "no value" ? <div className = "dict_wrapper" >
                                                      <dt > controlled by </dt> <dd > {
                                                        appliedAssay.appliedassay_controlled_by
                                                      } </dd> </div> : null} </dl> <TechnicalReplicate
                                                    technicalReplicate = {
                                                      filteredByAppliedAssayId
                                                    }
                                                    drawer = {
                                                      this.props.drawer
                                                    }
                                                    delete = {
                                                      this.props.delete
                                                    }
                                                    toggle = {
                                                      this.props.toggle
                                                    }
                                                    selection = {
                                                      this.props.selection
                                                    }
                                                    /> </div>
                                                  );
                                                }.bind(this)
                                              );
                                              return <div className = "appliedAssays" > {
                                                appliedAssayElements
                                              } </div>;
                                            }
                                        });
                                      var TechnicalReplicate = createReactClass({
                                        render() {
                                          var byTechnicalReplicateId = crossfilter(
                                              this.props.technicalReplicate
                                            )
                                            .dimension(d => d.technicalreplicate_id);
                                          var technicalReplicateElements = uniqBy(
                                              this.props.technicalReplicate,
                                              'technicalreplicate_id'
                                            )
                                            .map(
                                              function(technicalReplicate) {
                                                var filteredByTechnicalReplicateId = byTechnicalReplicateId
                                                  .filter(technicalReplicate.technicalreplicate_id)
                                                  .top(Infinity);
                                                filteredByTechnicalReplicateId = sortByKey(
                                                  filteredByTechnicalReplicateId,
                                                  'sequencing_id'
                                                );
                                                byTechnicalReplicateId.filterAll();
                                                return ( <div key = {
                                                    technicalReplicate.technicalreplicate_id
                                                  }
                                                  className = "technicalReplicate" >
                                                  <Sequencing sequencing = {
                                                    filteredByTechnicalReplicateId
                                                  }
                                                  drawer = {
                                                    this.props.drawer
                                                  }
                                                  delete = {
                                                    this.props.delete
                                                  }
                                                  toggle = {
                                                    this.props.toggle
                                                  }
                                                  selection = {
                                                    this.props.selection
                                                  }
                                                  /> </div>
                                                );
                                              }.bind(this)
                                            );
                                          return ( <div className = "technicalReplicates" > {
                                              technicalReplicateElements
                                            } </div>
                                          );
                                        }
                                      });
                                      var Sequencing = createReactClass({
                                        render() {
                                          var bySequencingId = crossfilter(this.props.sequencing)
                                            .dimension(
                                              d => d.sequencing_id
                                            );
                                          var sequencingElements = uniqBy(this.props.sequencing, 'sequencing_id')
                                            .map(
                                              function(sequencing) {
                                                var filteredBySequencingId = bySequencingId
                                                  .filter(sequencing.sequencing_id)
                                                  .top(Infinity);
                                                filteredBySequencingId = sortByKey(filteredBySequencingId, 'data_id');
                                                bySequencingId.filterAll();
                                                return ( <div key = {
                                                    sequencing.sequencing_id
                                                  }
                                                  className = "sequencing" >
                                                  <span className = "title sequencing_title" > {
                                                    sequencing.sequencing_id
                                                  } </span> <dl >
                                                  <div className = "dict_wrapper" >
                                                  <dt > platform </dt> <dd > {
                                                    sequencing.platform
                                                  } </dd> </div> <div className = "dict_wrapper" >
                                                  <dt > instrument </dt> <dd > {
                                                    sequencing.instrument
                                                  } </dd> </div> {
                                                    sequencing.strand_mode ? ( <div className = "dict_wrapper" >
                                                      <dt > strand mode </dt> <dd > {
                                                        sequencing.strand_mode
                                                      } </dd> </div>
                                                    ) : null
                                                  } <div className = "dict_wrapper" >
                                                  <dt > sequencing lab </dt> <dd > {
                                                    sequencing.sequencing_lab
                                                  } </dd> </div> {
                                                    sequencing.chemistry_version ? ( <div className = "dict_wrapper" >
                                                      <dt > chemistry version </dt> <dd > {
                                                        sequencing.chemistry_version
                                                      } </dd> </div>
                                                    ) : null
                                                  } {
                                                    sequencing.max_read_length ? ( <div className = "dict_wrapper" >
                                                      <dt > max.read length </dt> <dd > {
                                                        sequencing.max_read_length
                                                      } </dd> </div>
                                                    ) : null
                                                  } {
                                                    sequencing.date ? ( <div className = "dict_wrapper" >
                                                      <dt > sequencing date </dt> <dd className = "no_break" > {
                                                        sequencing.date
                                                      } </dd> </div>
                                                    ) : null
                                                  } </dl> <Data data = {
                                                    filteredBySequencingId
                                                  }
                                                  drawer = {
                                                    this.props.drawer
                                                  }
                                                  delete = {
                                                    this.props.delete
                                                  }
                                                  toggle = {
                                                    this.props.toggle
                                                  }
                                                  selection = {
                                                    this.props.selection
                                                  }
                                                  /> </div>
                                                );
                                              }.bind(this)
                                            );
                                          return <div > {
                                            sequencingElements
                                          } </div>;
                                        }
                                      });
                                      const Data = createReactClass({
                                        displayName: 'Data',
                                        render() {
                                          // var byDataId = crossfilter(this.props.data).dimension(d => d.data_id);
                                          var dataElements = uniqBy(this.props.data, 'data_id')
                                            .map(
                                              function(data) {
                                                if (data.file_type !== 'LOG' && data.file_type !== 'PDF') {
                                                  let classes = classnames('data', {
                                                    selected: this.props.selection.get('DA' + data.data_id)
                                                  });
                                                  return ( <DataElement selection = {
                                                      this.props.selection
                                                    }
                                                    id = {
                                                      data.data_id
                                                    }
                                                    data = {
                                                      data
                                                    }
                                                    toggle = {
                                                      this.props.toggle
                                                    }
                                                    />
                                                  );
                                                }
                                              }.bind(this)
                                            );
                                          var logElements = this.props.data.map(
                                            function(data) {
                                              if (data.file_type === 'LOG' || data.file_type === 'PDF') {
                                                return ( <DataElement selection = {
                                                    this.props.selection
                                                  }
                                                  id = {
                                                    data.data_id
                                                  }
                                                  data = {
                                                    data
                                                  }
                                                  toggle = {
                                                    this.props.toggle
                                                  }
                                                  />
                                                )
                                              }
                                            }.bind(this)
                                          );

                                          return ( <div > {
                                              dataElements
                                            }

                                            {
                                              logElements.length > 1 ? ( <Collapsible trigger = {
                                                  'QC/log-files'
                                                } >
                                                {
                                                  logElements
                                                } </Collapsible>
                                              ) : null
                                            }

                                            </div>
                                          );
                                        }
                                      });
                                      var DataElement = createReactClass({
                                        displayName: 'DataElement',
                                        handleSelection(data) {
                                          this.props.toggle(data, !this.props.selection.get('DA' + data.data_id));
                                        },
                                        render() {
                                          let classes = classnames('data', {
                                            selected: this.props.selection.get('DA' + this.props.id)
                                          });
                                          let label_classes = classnames('badge badge-pill ml-2 p-1', {
                                            'text-light border border-light': this.props.data.mapped_genome === "GRCz10/danRer10",
                                            'badge-light': this.props.data.mapped_genome === "GRCz11/danRer11"
                                          })
                                          if (this.props.data.file_type === 'LOG') {}
                                          return ( <div key = {
                                              this.props.id
                                            }
                                            id = {
                                              this.props.id
                                            }
                                            className = {
                                              classes
                                            }
                                            onClick = {
                                              () => this.handleSelection(this.props.data)
                                            } >
                                            {
                                              this.props.data.secondary_file_name !== 'no value' ?/*Show different elements if secondary files are included or not*/
                                              <div >
                                              <div className = "fas fa-copy" />
                                              <div className = "" > {
                                                this.props.data.primary_file_name.substring(
                                                  this.props.data.primary_file_name.lastIndexOf('/') + 1
                                                ) + ' + ' +
                                                this.props.data.secondary_file_name.substring(
                                                  this.props.data.secondary_file_name.lastIndexOf('/') + 1
                                                ) +
                                                ' (' +
                                                prettyBytes(this.props.data.primary_file_size) +
                                                ' + ' + prettyBytes(this.props.data.primary_file_size) +
                                                ')'
                                              } </div>

                                              </div>: <div>
                                              <div className = "" > {
                                                this.props.data.file_type === 'PDF' ? <i className = "fas fa-file-pdf" > </i> :<i className="fas fa-file"></i>
                                              } {
                                                this.props.data.mapped_genome !== "no value" ? <span className = {
                                                    label_classes
                                                  } > {
                                                    this.props.data.mapped_genome
                                                  } </span> :
                                                  null
                                              } </div> <div className = "file_name" > {
                                                this.props.data.primary_file_name.substring(
                                                  this.props.data.primary_file_name.lastIndexOf('/') + 1
                                                ) +
                                                ' (' +
                                                prettyBytes(this.props.data.primary_file_size) +
                                                ')'
                                              } </div> </div>
                                            } </div>
                                          );
                                        }
                                      });
                                      var DownloadButton = createReactClass({
                                            handleDownload: function(link) {
                                              location.href = link;
                                            },
                                            render() {
                                              let data = this.props.dataFile;
                                              return ( <div >
                                                <button type = "button"
                                                className = "btn btn-default btn-download"
                                                onClick = {
                                                  () => this.handleDownload(data.url)
                                                } >
                                                Download {
                                                  this.props.dataType
                                                }
                                                ( <span className = "file_size" > {
                                                    data.size
                                                  } </span>) </button> </div>
                                                );
                                              }
                                            });
                                          var DataTable = createReactClass({

                                            render() {
                                              let seriesElements = this.props.createSeriesElements();
                                              return ( <div className = "table" >
                                                <div > {
                                                  seriesElements
                                                }{

                                                } </div> <div className = "text-center" >
                                                <ReactPaginate containerClassName = {
                                                  'pagination justify-content-center'
                                                }
                                                pageClassName = {
                                                  'page-item'
                                                }
                                                pageLinkClassName = {
                                                  'page-link'
                                                }
                                                previousClassName = {
                                                  'page-item'
                                                }
                                                breakClassName = {
                                                  'page-link'
                                                }
                                                nextClassName = {
                                                  'page-item'
                                                }
                                                previousLinkClassName = {
                                                  'page-link'
                                                }
                                                nextLinkClassName = {
                                                  'page-link'
                                                }
                                                pageCount = {
                                                  this.props.pageCount
                                                }
                                                pageRangeDisplayed = {
                                                  10
                                                }
                                                marginPagesDisplayed = {
                                                  2
                                                }
                                                onPageChange = {
                                                  this.props.handlePageChange
                                                }
                                                activeClassName = {
                                                  'active'
                                                }
                                                hrefBuilder = {
                                                  () => {
                                                    return '#';
                                                  }
                                                }
                                                disabledClassName = {
                                                  'disabled'
                                                }
                                                /> </div> </div>
                                              );
                                            }
                                          });

                                          ReactDOM.render( <
                                            FileTable url = "/media/dataExport.json"
                                            view = {
                                              window.props
                                            }
                                            />,
                                            document.getElementById('container')
                                          );
