import React from 'react';
import createClass from 'create-react-class';

const TruncatedSpan = createClass({
  truncate(fullStr, strLen) {
    if (fullStr.length <= strLen) {
      return fullStr;
    }

    let separator = '...';

    var sepLen = separator.length,
        charsToShow = strLen - sepLen,
        frontChars = Math.ceil(charsToShow/2),
        backChars = Math.floor(charsToShow/2);

    return fullStr.substr(0, frontChars) +
           separator +
           fullStr.substr(fullStr.length - backChars);
  },
  render() {
    return (
      <span title={this.props.text}>{this.truncate(this.props.text,this.props.length)}</span>
    );
  }

});

export default TruncatedSpan;
