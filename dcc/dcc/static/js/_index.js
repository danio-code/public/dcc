import {selection, select, event as currentEvent, selectAll} from 'd3-selection';
import 'd3-transition';
import {scaleBand, scaleLinear, scaleOrdinal} from 'd3-scale';
import {keys} from 'd3-collection';
import {max} from 'd3-array';
import {axisBottom, axisLeft} from 'd3-axis';
import {stack} from 'd3-shape';
import Cookies from 'js-cookie';

document.addEventListener('DOMContentLoaded', function() {
  const csrftoken = Cookies.get('csrftoken');
    /* jshint undef:true */
    /*jshint devel:false */
    /*global d3, plot_data */
    "use strict";
    // var data_input = plot_data.sequences,
    //     unit = "sequencing runs"
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        xhr.setRequestHeader('X-CSRFToken', csrftoken);
      }
    });
    let plot_data ={};
    $.ajax({
    url: '/media/assay2labs.json',
    datatype: 'json',
    cache: true,
    beforeSend: function() { $('#loading').show(); },
    success: function(data) {
    $('#loading').hide();
    plot_data = data
    $(".button_row .btn.active").trigger("click");
    },
    error: function(response) {
    console.log(response.status + response.statusText);
    console.log(response);
    }
  })

    function bar_plot(data_input, unit) {
        var margin = {
                top: 20,
                right: 10,
                bottom: 100,
                left: 50
            },
            height = 450 - margin.top - margin.bottom,
            width = height*2.5 - margin.left - margin.right;

            let refTypes =[
              'ChIP-seq',
              'RNA-seq',
              'ATAC-seq',
              'Ribo-seq',
              'RRBS',
              '3P-seq',
              'mRNA-seq',
              'TAB-seq',
              'BS-seq',
              'miRNA-seq',
              'small-RNA-seq',
              'MethylCap-seq',
              'PAL-seq',
              '4C-seq',
              'MeDIP-seq',
              'MNase-seq',
              'SAPAS',
              'Hi-C',
              'DNAse-seq',
              'CAGE-seq',
              'MethylC-seq'
            ]
        var lab_names = keys(data_input[0]).filter(function(key) {
            return key !== "name";
        });
        lab_names = lab_names.filter(function(key) {
            return key !== "sum";
        }).sort();

        var m = data_input.length;
        var stacked = stack().keys(lab_names),
            layers = stacked(data_input),
            yGroupMax = max(layers, function(layer) {
                return max(layer, function(d) {
                    return d[1] - d[0];
                });
            }),
            yStackMax = max(layers, function(layer) {
                return max(layer, function(d) {
                    return d[1];
                });
            });

        layers.forEach(function(d, i) {
            d.forEach(function(dd) {
                dd.lab_name = lab_names[i];
            });
        });
        var x = scaleBand()
            .rangeRound([0, width])
            .padding(0.2);

        x.domain(data_input.map(function(d) {
            return d.name;
        }).sort(function (a, b) {
          return a.toLowerCase().localeCompare(b.toLowerCase());
        }));

        var y = scaleLinear()
            .domain([0, yStackMax]).nice()
            .range([height, 0]);

        var z = scaleBand().domain(lab_names).rangeRound([0, x.bandwidth()]);

        var color = scaleOrdinal()
            .range(['#e74c3c', '#e67e22', '#40c057', '#27ae60', '#1098ad', '#0b7285']);

        var svg = select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var layer = svg.selectAll(".layer")
            .data(layers)
            .enter().append("g")
            .attr("class", "layer")
            .style("fill", function(d) {
                return color(d.key);
            });

        var div = select("#chart").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        var bars = layer.selectAll("rect")
            .data(function(d) {
                return d;
            }, function(d) {
                return d.key;
            })
            .enter().append("rect")
            .attr("x", function(d) {
                return x(d.data.name);
            })
            .attr("y", height)
            .attr("width", x.bandwidth())
            .attr("height", 0)
            .attr("class", function(d) {
                return "bar " + d.lab_name.replace(" ", "_");
            })
            .on("mouseover", function(d) {
                div.transition()
                    .duration(200)
                    .style("opacity", 0.9);
                div.html("<span>" + d.lab_name + "</span><span>" + Math.round(d.data[d.lab_name]) + "</span>")
                    .style("left", (event.pageX) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function() {
                div.transition()
                    .duration(500)
                    .style("opacity", 0);
            });

        bars.transition()
            .delay(function(d, i) {
                return i * 10;
            })
            .attr("y", function(d) {
                return y(d[1]);
            })
            .attr("height", function(d) {
                return y(d[0]) - y(d[1]);
            });
        var x_axis = svg.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(axisBottom(x).tickSizeOuter(0));
        svg.append("g")
            .attr("class", "axis axis--y")
            .call(axisLeft(y).ticks(10))
            .append("text")
            .attr("x", 6)
            .attr("y", y(y.ticks(10).pop()))
            .attr("dy", "0.35em")
            .attr("text-anchor", "start")
            .attr("fill", "#000")
            .text(unit);

        x_axis.selectAll(".tick text")
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", "rotate(-65)")


        var legend_width = 200
        var legend_svg = select("#legend")
            .append("svg")
            .attr("width", legend_width)
            .attr("height", 20*lab_names.length);

        var legend = legend_svg.selectAll(".legend")
            .data(lab_names)
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) {
                return "translate(-150," + i * 20 + ")";
            });

        legend.append("rect")
            .attr("x", legend_width - 18)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", function(d) {
                return color(d);
            })
            .on("mouseover", function(d) {
                svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")")
                    .style('opacity', '0.25');
            })
            .on("mouseout", function(d) {
                svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")")
                    .style('opacity', '1');
            });


        legend.append("text")
            .attr("x", legend_width + 5)
            .attr("y", 9)
            .attr("dy", ".35em")
            .style("text-anchor", "start")
            .text(function(d) {
                return d;
            })
            .on("mouseover", function(d) {
                svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")")
                    .transition()
                    .duration(100)
                    .style('opacity', '0.25');
            })
            .on("mouseout", function(d) {
                svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")")
                    .transition()
                    .duration(500)
                    .style('opacity', '1');
            });
        //
        function transitionGrouped() {
            y.domain([0, yGroupMax]).nice();
            select(".axis--y").call(axisLeft(y).ticks(10))
                .transition()
                .duration(500);
            bars.transition()
                .duration(500)
                .delay(function(d, i) {
                    return i * 10;
                })
                .attr("x", function(d) {
                    // return x(d.data.name) + x.bandwidth() / n * j;
                    return x(d.data.name) + z(d.lab_name);
                })
                .attr("width", x.bandwidth() / m)
                .transition()
                .attr("y", function(d) {
                    return y(d.data[d.lab_name]);
                })
                .attr("height", function(d) {
                    return height - y(d.data[d.lab_name]);
                });
        }

        function transitionStacked() {
            y.domain([0, yStackMax]).nice();
            select(".axis--y").call(axisLeft(y).ticks(10))
                .transition()
                .duration(500);

            bars.transition()
                .duration(500)
                .delay(function(d, i) {
                    return i * 10;
                })
                .attr("y", function(d) {
                    return y(d[1]);
                })
                .attr("height", function(d) {
                    return y(d[0]) - y(d[1]);
                })
                .transition()
                .attr("x", function(d) {
                    return x(d.data.name);
                })
                .attr("width", x.bandwidth());
        }

        function change() {
            if (this.value === "grouped") {
                transitionGrouped();
            } else {
                transitionStacked();
            }
        }

        selectAll("input").on("change", change);
        select("input[value=\"stacked\"]").property("checked", true);

        function wrap(text, width) {
            text.each(function() {
                var text = select(this),
                    words = text.text().split(/\-+/).reverse(),
                    word,
                    line = [],
                    lineNumber = 0,
                    lineHeight = 1.1, // ems
                    y = text.attr("y"),
                    dy = parseFloat(text.attr("dy")),
                    tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
                while (word = words.pop()) {
                    line.push(word);
                    tspan.text(line.join("-"));
                    if (tspan.node().getComputedTextLength() > width) {
                        line.pop();
                        tspan.text(line.join("-"));
                        line = [word];
                        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text("-"+word);
                    }
                }
            });
        }
    }
    $("[name=show_seqs]")
        .on("click", function() {
            selectAll("svg").remove();
            $(".button_row .btn.active").removeClass('active');
            $(this).addClass('active');
            bar_plot(plot_data.sequences, 'Sequencing Runs');
        });
    $("[name=show_size]")
        .on("click", function() {
            selectAll("svg").remove();
            $(".button_row .btn.active").removeClass('active');
            $(this).addClass('active');
            bar_plot(plot_data.sizes, 'GB');
        });
    // $("[name=mode]").on("change", function() {
    //     $(".button_row .btn.active").trigger("click");
    // });
    // // bar_plot(plot_data.sizes, 'GB');
    // // bar_plot(plot_data.sequences, 'Sequencing Runs');

});
