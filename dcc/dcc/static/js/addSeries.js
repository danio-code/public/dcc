/* eslint-disable no-useless-escape, no-console */
/* global labs_json*/
$(document).ready(function() {
    'use strict';

    let linkingID  = {};
    const targetList = ['ChIP-seq', 'SELEX-seq', 'RIP-seq', 'PAR-Clip-seq',
        'iCLIP-seq', 'ChIP-exo-seq', 'Methyl-seq'
    ];
    const rnaList    = ['RNA-seq', 'short-RNA-seq','miRNA-seq', 'CAGE-seq', 'Bru-seq', 'PAS-seq','Ribo-seq', 'RIP-seq', 'PAR-Clip-seq', 'iCLIP-seq', 'GRO-seq', 'PAL-seq','3P-seq','SAPAS-seq'
  ];

    const csrftoken   = Cookies.get('csrftoken');

    // console.log('the cookie is:', csrftoken);

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });


    function integerToArray(x){
        if(x.length === undefined){
            x = [x];
        }
        return x;
    }

    function removePrefix(string, delimiter){
        string = string.substring(string.indexOf(delimiter), string.length);
        return string;
    }


    $('#biosample').hide();
    $('#assay').hide();
    $('#appliedAssay').hide();
    $('#sequencing').hide();
    $('#data').hide();
    $('#technicalReplicate').hide();

    $('.series_time_course').hide();
    $('.series_control_control').hide();
    $('#create_assay_button').hide();
    $('#create_appliedAssay_button').hide();
    $('#create_sequencing_button').hide();
    $('#create_data_button').hide();
    $('#submit_data_button').hide();
    $('.btn--apply').hide();
    $('.btn--edit').hide();
    $('.case_control').hide();
    $('#sequencing_strand_mode_select_parent').hide();

    // $('input[type=radio][name=series_type]').on('click', function() {
    // if (this.value == 'series_survey') {
    // $('.series_time_course').hide();
    // $('.series_control_control').hide();
    // $('.series_survey').show();
    // } else {
    //     $('.series_time_course').hide();
    //     $('.series_control_control').show();
    //     $('.series_survey').hide();
    // }
    // });

    // fillSelect('', '#series_lab_parent');
    loadTooltips();
    $('#sequencing_date_div_parent').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: moment('2000-01-01')
    });
    // $('select[data-subclass = 'autocomplete']').select2({
    //     placeholder: 'anatom. origin'
    // }); //use select2 library for anatomical terms
    // $('select[id='biosample_genetic_background_select_parent']').select2({
    //     formatLoadMore: 'Loading more...',
    //     ajax: {
    //         url: '/static/json/biosample_genetic_background.json',
    //         dataType: 'json',
    //         data = $.map(data,function(item) {
    //             return {
    //                 id: item.name,
    //                 text: item.name
    //             }
    //         })
    //     },
    //     data: data

    // });


    //The following function is used to create the biosample table

    $('#create_biosample_button').on('click', function() {
        let numSamples, numReplicates;
        if (validateSeries() === 0) {

            $('#series_div :input').prop('disabled', true);
            $('#series_div :input').addClass('disabled');
            $('#series_div').hide('slow');
            $('#create_biosample_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_biosample_button').hide();
                $('#biosample').toggle();
                $('#create_assay_button').show();
                $('#biosample_anatomical_term_parent').hide();
                $('#biosample_cell_line_type_parent').hide();
                $('#biosample_developmental_stage_select_parent').hide();
                $('#biosample_post_fertilization_select_parent').hide();
                // $('#series_edit_btn').show();
            });

            if ($('#series_survey').is(':checked')) {
                numSamples    = $('.survey .series_num_samples_input').val(); //get number of biosamples
                numReplicates = $('#series_num_replicates_input').val();
                $('#biosample_controlled_by_select_parent').hide();
                fillSelect('', '#biosample_select');
                let col = document.createElement('div');
                col.setAttribute('id', 'biosample_grid_subgrid_1');
                col.setAttribute('class', 'biosample_grid_subgrid');
                $('#biosample_grid').append(col);
                if (numSamples.indexOf(',') !== -1){
                    numSamples = numSamples.split(',').filter(Boolean);
                } else {
                    numSamples = [parseInt(numSamples, 10)];
                }
                createMultipleSampleCells(numReplicates, 1, numSamples);
                $('#biosample_grid').selectableScroll({
                    filter: '.biosample_cell',
                    stop: function() {
                        $('#biosample_selected_count').html(
                            `Selected Elements:  ${$('.ui-selected').length}`
                        );
                    },
                    unselected: function() {
                        $('#biosample_select [id$="_select_parent"]').prop('selectedIndex', 0);
                        $('#biosample_select select[data-subclass = "autocomplete"]').select2();
                    }
                });
            } else if ($('#series_case_control').is(':checked')) {
                numSamples    = $('.case_control .series_num_samples_input').val(); //get number of biosamples
                numReplicates = $('#series_num_replicates_input').val();
                let numControl    = $('.case_control .series_num_control_input').val(),
                    numGroups;
                $('.survey').hide();
                fillSelect('', '#biosample_select');
                numSamples = numSamples.split(',').filter(Boolean);
                numControl = numControl.split(',').filter(Boolean);
                numGroups = [numControl, numSamples];
                $(numGroups).each(function(index) {
                    let controlNumGroups = this;
                    let col = document.createElement('div');
                    col.setAttribute('id', `biosample_grid_subgrid_${index}`);
                    col.setAttribute('class', 'biosample_grid_subgrid');
                    if (index === 0) {
                        col.innerHTML = '<p class="biosample_subgrid_title">Control</p>';
                        col.setAttribute('name', 'control_subgrid');
                    }

                    $('#biosample_grid').append(col);
                    createMultipleSampleCells(numReplicates, index, controlNumGroups);
                });
                $('#biosample_grid').selectableScroll({
                    filter: '.biosample_cell',
                    stop: function() {
                        $('#biosample_selected_count').html(
                            `Selected Elements: ${$('.ui-selected').length}`
                        );
                    },
                    unselected: function() {
                        $('#biosample_select [id$="_select_parent"]')
                            .prop('selectedIndex', 0);
                        $('#biosample_select select[data-subclass = "autocomplete"]')
                            .select2();
                    }
                });
            }
            $('.biosample_biologicalReplicate').removeAttr('style');


            // $('[class$=_text] [name$=_lab]').text($('#series_lab_select_parent').val());
            let jsonData = parse2json('series_div', '', ''),
                data        = new FormData(),
                description;

            jsonData = JSON.stringify(jsonData);

            if ($('#description_file')[0]) {
                description = $('#description_file')[0].files[0];
            } else {
                description = '';
            }
            //        var description = $('#description_file').val();
            data.append('jsonData', jsonData);
            data.append('description_file', description);
            $.ajax({
                url: '/addSeries/',
                processData: false,
                contentType: false,
                type: 'POST',
                data: data,
                success: function(response) {
                    linkingID.series = response;
                    // console.log(response);
                }
            });
        } else {
            $('#create_biosample_button').blur();
        }
    });

    $('#create_assay_button').on('click', function() {
        if (validateBiosample() === 0) {
            $('#biosample_div').hide('slow');
            $('#create_assay_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_assay_button').hide();
                $('#assay').toggle();
                $('#create_appliedAssay_button').show();
                // $('#biosample_edit_btn').show();
            });
            $('.biosample_description').attr('disabled', 'disabled');
            let numAssays = $('#series_num_assay_input').val();

            for ( let i = 0; i < numAssays; i++) {
                let wrapper = document.createElement('div'); // create div element

                wrapper.setAttribute('class', 'assay_wrapper');
                wrapper.setAttribute('id', `assay_wrapper_${i}`);
                wrapper.innerHTML = /* @html */`<div class=assay_text>
                    <span>
                        <label for="assay_id_${i}_select" class="assay_available_assay_label_help">
                        Available Assays:
                        <span class="assay_available_assay_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top"></span>
                        </label>
                        <select name="" id="assay_id_${i}_select"class="select_assay" style="width:100%"></select>
                    </span>
                <div>
                    <span class = "sticky_label">
                        <label for="assay_lab_${i}_select" class = "assay_lab_label">Lab:
                            <span class="assay_lab_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top"></span>
                            <a target="_blank" href="/addLab">Add a lab</a>
                        </label>
                        <select id="assay_lab_${i}_select" class="form-control" data-filename="labs"></select>
                    </span>
                    <span class = "sticky_label"><label for="assay_type_${i}_select">
                            Type: <span class="assay_type_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top"></span>
                        </label>
                        <select id="assay_type_${i}_select" class="selectAutoFill form-control assay_type" data-filename="assay_types"></select>
                    </span>
            </div>
            <div>
            <div id="assay_main_target_div" class="assay_target" >
                <label for="assay_target_input" class="assay_target_label">Target:
                <span class="assay_target_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top">
                </label><input type="text" name="target" id="assay_target_input" maxlength="200"/>
            </div>
            <div id = "assay_control_target_div" class="assay_target" >
                <label for="assay_control_target_input" class = "assay_target_label">Control Target:
                <span class="assay_target_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top">
                </label>
                <input type="text" name="control_target" id="assay_control_target_input" maxlength="200"/>
            </div>
            <div class="assay_prep_div">
                <label for="assay_id_${i}_prep_select" class="assay_prep_label_help">
                Library preparation:
                <span class="assay_prep_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top"></span>
                </label>
                <select name="" id="assay_id_${i}_prep_select"class="assay_prep_select" style="width:100%">
                    <option></option>
                    <option value="poly(A)+">poly(A)+</option>
                    <option value="poly(A)-">poly(A)-</option>
                    <option value="rRNA depletion">rRNA depletion</option>
                </select>
            </div>
            </div>

            <span> <label for="assay_description_${i}" class = "assay_description_label">Description:
            <span class="assay_description_label_help fas fa-info-circle" data-toggle="tooltip" data-placement="top"></label>
                <textarea name = "assay_description" id = "assay_description_${i}"class = "assay_description" maxlength="2000" rows = 5 ></textarea>
            </span></div>`;
                $('#assay_grid').append(wrapper); // append div to the parent div
            }
            fillSelect('', '#assay_grid');
            fillAssays(assay_json);
            loadTooltips();
            $(".assay_prep_select").select2({
              tags: true,
              createTag: function (params) {
                return {
                  id: params.term,
                  text: params.term,
                  newOption: true
                }
              },
               templateResult: function (data) {
                var $result = $("<span></span>");

                $result.text(data.text);

                // if (data.newOption) {
                //   $result.append(" <em>(new)</em>");
                // }

                return $result;
              }
            }).on('select2:open', function() {
                $('.select2-search__field').attr('maxlength', 200);
            });

            $('.assay_target').hide();
            $('.assay_prep_div').hide();
            $('#assay_grid .selectAutoFill').prop('selectedIndex', -1); //set select fields to blank
            // $('#assay_div .select_assay').select2({
            //     placeholder: 'ID'
            // });
            let jsonData = parse2json('biosample_div', 'series', linkingID.series);

            jsonData = JSON.stringify(jsonData);
            $.post('/addBiosample/', jsonData, function(response) {
              console.log(response);
                linkingID.biosample = response;
                // console.log(response);
            }, 'json');
            loadTooltips();
        } else {
            $('#create_assay_button').blur();
        }

    });

    $('#create_appliedAssay_button').on('click', function() {
        if (validateAssay() === 0) {
            let grid;

            $('#create_appliedAssay_button').val('');

            $('#create_appliedAssay_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_appliedAssay_button').hide();
                $('#appliedAssay').show();
                $('#assay_div').hide('slow');
                $('#create_sequencing_button').show();
                // $('#assay_edit_btn').show();
            });

            $('.ui-selecting').removeClass('ui-selecting');
            $('.ui-selected').removeClass('ui-selected');
            $('.ui-selectee').removeClass('ui-selectee');


            $('#appliedAssay_select [id$="_parent"]').prop('checked', false);

            grid = $('#biosample_grid').children().clone();
            grid.each(function() {
                $(this).children().each(function() {
                    // this.id = 'appliedAssay' + this.id.substring(this.id.indexOf('_'), this.id.length);
                    this.className = `appliedAssay${removePrefix(this.className,'_')}`;
                    $(this).children().each(function() {
                        this.id = `appliedAssay${removePrefix(this.id,'_')}`;
                        this.className = `appliedAssay${removePrefix(this.className,'_')}`;
                        if (this.className === 'appliedAssay_biologicalReplicate') {
                            $(this).append(
                                /* @html */`<div class = appliedAssay_text>
                  <div name = "appliedAssay_assigned_assay"></div>
                                </div>`);
                        }
                    });
                });
                this.id = `appliedAssay${removePrefix(this.id,'_')}`;
                this.className = `appliedAssay${removePrefix(this.className,'_')}`;
                $('#appliedAssay_grid').append(this);
            });
            $('#appliedAssay_grid .appliedAssay_wrapper button').remove();
            $('#appliedAssay_grid').selectableScroll({
                filter: '.appliedAssay_biologicalReplicate',
                stop: function() {
                    $('#appliedAssay_selected_count').html(`Selected Elements:${$('.ui-selected').length}`);
                },
                unselected: function() {
                    $('#appliedAssay_select [id$="_parent"]').prop('checked', false);
                }
            });
            $('.assay_wrapper').each(function() {
                if ($.inArray($('.assay_type', this).val(), targetList) !== -1) {
                    let wrapperClone  = $(this).clone(),
                        controlTarget = $('#assay_control_target_input', this).val(),
                        lab            = $('[data-filename="labs"]', this).val(),
                        type           = $('.assay_type', this).val();

                    $(wrapperClone).addClass('wrapper_clone');
                    $('#assay_target_input', wrapperClone).val(controlTarget);
                    $('[data-filename="labs"]', wrapperClone).val(lab);
                    $('.assay_type', wrapperClone).val(type);
                    $(this).after(wrapperClone);
                }
            });
            $('.assay_wrapper').each(function(index) {
                $('#appliedAssay_assay_select_list').append(
                    /* @html */`<input
                    type="checkbox"
                    name="${$('.select_assay', this).val()}"
                    class="appliedAssay_selected_assay_checkbox"
                    id="assay_${index}_parent">

                    <span data-toggle="tooltip"
                          class="appliedAssay_selected_assay"
                          title="${$('.assay_description', this).val()}">
                            ${$('.select_assay', this).val()}
                    </span>
                    <br>`);
                $('#sequencing_select_buttons').append(
                    /* @html */`<input
                        type="button"
                        class="btn btn-outline-secondary btn--select_assay"
                        name="select_${index}"
                        value="Select ${$('.select_assay', this).val()}"
                        id="sequencing_${index}">`
                    );
                if ($('#assay_target_input', this).val().length > 0) {
                    if ($('#assay_target_input', this).val() === $('#assay_control_target_input', this).val()) {
                        $('#assay_target_input', this).val(`${$('#assay_target_input', this).val()}_control`);
                    }
                }
            });

            let jsonData = parse2json('assay_grid', '', '');

            jsonData = JSON.stringify(jsonData);
            $.post('/addAssay/', jsonData, function(response) {
                $(response).each(function(index) {
                    let assayTitle = this.assay_id.substr(0, this.assay_id.length - 2);
                    $($('.appliedAssay_selected_assay')[index]).text(assayTitle);
                    $($('.appliedAssay_selected_assay_checkbox')[index]).attr('name', assayTitle);
                    $($('.btn--select_assay')[index]).val(`Select ${assayTitle}`);
                });
                linkingID.assay = response;
                // console.log(response);
            }, 'json');
            loadTooltips();
            $('.wrapper_clone').remove();

        } else {
            $('#create_appliedAssay_button').blur();
        }
    });

    $('#create_sequencing_button').on('click', function() {
        if (validateAppliedAssay() === 0) {
            $('#create_sequencing_button').val('');
            $('#appliedAssay_div').hide('slow');
            $('#create_sequencing_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_sequencing_button').hide();
                $('#sequencing').show();
                $('#create_data_button').show();
                // $('#appliedAssay_edit_btn').show();
            });

            $('.ui-selecting', '#appliedAssay_div').removeClass('ui-selecting');
            $('.ui-selected', '#appliedAssay_div').removeClass('ui-selected');
            $('.ui-selectee', '#appliedAssay_div').removeClass('ui-selectee');

            let grid = $('#appliedAssay_grid').children().clone();
            grid.each(function() {
                let subgrid = $(this);

                this.id = `sequencing${removePrefix(this.id,'_')}`;
                this.className = `sequencing${removePrefix(this.className,'_')}`;
                subgrid.children().each(function() {
                    let wrapper = this;

                    if (this.className === 'appliedAssay_subgrid_title') {
                        this.className = 'sequencing_subgrid_title';
                        return true;
                    }
                    this.className = `sequencing${removePrefix(this.className,'_')}`;

                    $(wrapper).each(function(index) {
                        let wrapper = this,
                            wrapperClone = $(wrapper);

                        $(wrapperClone).children().each(function() {
                            this.id = `sequencing${removePrefix(this.id,'_')}_${index}`;
                            this.className = `sequencing${removePrefix(this.className,'_')}`;
                            if (this.className === 'sequencing_biologicalReplicate') {
                                $('.appliedAssay_assay_div', this).each(function() {
                                    let numTechRep,
                                        thisAppliedAssayAssayDiv = this;
                                    if ($('[name="appliedAssay_num_tech_replicates"]', this).val()) {
                                        numTechRep = $('[name="appliedAssay_num_tech_replicates"]', this).val();
                                    } else {
                                        numTechRep = $('[name="appliedAssay_num_tech_replicates"]', this).attr('placeholder');
                                    }
                                    for (let i = 0; i < numTechRep; i++) {
                                        $(thisAppliedAssayAssayDiv).append(
                                            /* @html */`<div class = 'technicalReplicate_div'>
                                                <span class=replicate_title>
                                                    <b>technical replicate${(i + 1)}</b>
                                                </span>
                                                <div class = sequencing_text>
                                                    <span name = "sequencing_platform"></span>
                                                    <span name = "sequencing_lab">
                                                        ${$('.biosample_text [name=biosample_lab]', wrapperClone)
                                                            .text()}
                                                    </span>
                                                    <span name = "sequencing_instrument"></span>
                                                    <span name = "sequencing_max_read_length"></span>
                                                    <span name = "sequencing_chem_version"></span>
                                                    <span name = "sequencing_paired_end"></span>
                                                    <span name = "sequencing_date"></span>
                                                </div>
                                            </div>`);
                                        if ($.inArray($('.appliedAssay_assay_row span', thisAppliedAssayAssayDiv).text().split('_')[0].trim(), rnaList) !== -1) {
                                            $('.sequencing_text', thisAppliedAssayAssayDiv).append(
                                                '<span name = "sequencing_strand_mode"></span>');
                                        }

                                    }
                                    $('[name="appliedAssay_num_tech_replicates"]', this).remove();
                                    $('label', this).remove();
                                    $(this).css('justify-content:space-around;');
                                });
                            }

                            // });
                            // });
                            // for (var i = 1; i < $(wrapper_clone).children().length; i++) {
                            //   $(wrapper_clone).children()[i].id = 'sequencing_replicate_' + i;
                            //   $(wrapper_clone).children()[i].className = 'sequencing_biologicalReplicate';
                            // }
                            subgrid.append(wrapperClone);
                        });
                    });
                    // this.remove();
                });
                $('#sequencing_grid').append(subgrid);
            });
            $('#sequencing_grid .sequencing_wrapper button').remove();
            $('#sequencing_grid').selectableScroll({
                filter: '.technicalReplicate_div',
                stop: function() {
                    $('#sequencing_selected_count').html(`Selected Elements:${$('.ui-selected').length}`);
                },
                unselected: function() {
                    $('#sequencing_select [id$="_parent"]').prop('checked', false);
                }
            });
            fillSelect('', '#sequencing_div');

            let jsonData = parse2json('appliedAssay_div', 'BiosampleAppliedAssay', linkingID);
            jsonData = JSON.stringify(jsonData);
            $.post('/addAppliedAssay/', jsonData, function(response) {
                linkingID.technicalReplicate = response;
                // // console.log(response);
            }, 'json');
        } else {
            $('#create_sequencing_button').blur();
        }
    });

    $('#create_data_button').on('click', function() {
        if (validateSequencing() === 0) {
            let subgrid;
            $('#sequencing_div').toggle('slow');
            $('#create_data_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_data_button').hide();
                $('#data_mapped_genome_parent').hide();
                $('#data_command_parent').hide();
                $('#data').toggle();
                $('#submit_data_button').show();
                // $('#sequencing_edit_btn').show();
            });


            $('#sequencing_div .ui-selecting').removeClass('ui-selecting');
            $('#sequencing_div .ui-selected').removeClass('ui-selected');
            $('#sequencing_div .ui-selectee').removeClass('ui-selectee');

            subgrid = $('#sequencing_grid').children().clone();
            subgrid.each(function() {
                this.id = `data${removePrefix(this.id,'_')}`;
                this.className = `data${removePrefix(this.className,'_')}`;
                $(this).children().each(function() {
                    // this.id = 'data' + this.id.substring(this.id.indexOf('_'), this.id.length);
                    this.className = `data${removePrefix(this.className,'_')}`;
                    $(this).children().each(function() {
                        this.id = `data${removePrefix(this.id,'_')}`;
                        this.className = `data${removePrefix(this.className,'_')}`;
                        if ($(this).attr('class') === 'data_biologicalReplicate') {
                            $('.technicalReplicate_div', this).each(function() {
                                $(this).append(/* @html */`<div class = data_text>
                                    <div id='data_primary_file_label'
                                         data-toggle='tooltip'
                                         data-placement='auto right'>
                                        <textarea class='url_text'
                                                  name='data_primary_file'
                                                  rows=2
                                                  cols=32
                                                  placeholder='Primary File'></textarea>
                                    </div>
                                    <div id='data_secondary_file_label'
                                         data-toggle='tooltip'
                                         data-placement='auto right'>
                                        <textarea class='url_text'
                                                  name='data_secondary_file'
                                                  rows=2
                                                  cols=32
                                                  placeholder='Secondary File'></textarea>
                                    </div>
                                </div>`);
                        });
                        }
                    });
                });
                $('#data_grid').append(this);
            });

            $('.technicalReplicate_div').each(function() {
                if ($('[name= sequencing_paired_end]', this).text() === 'single end') {
                    $('[name=data_secondary_file]', this).hide();
                }
            });

            let jsonData = parse2json('sequencing_div', 'technicalReplicate', linkingID.technicalReplicate);
            jsonData = JSON.stringify(jsonData);
            $.post('/addSequencing/', jsonData, function(response) {
                linkingID.sequencing = response;
                // // console.log(response);
            }, 'json');
            // $('#data_grid').selectableScroll({
            //     filter: '.data_wrapper',
            //     stop: function(event, ui) {
            //         $("#data_selected_count").html('Selected Elements: ' + $(".ui-selected").length)
            //     },
            //     unselected: function(event, ui) {
            //         // $("#data_select [id$='_parent']").prop('checked', false);
            //         // $("data_select select[data-subclass = 'autocomplete']").select2();
            //     }
            // });
        } else {
            $('#create_data_button').blur();
        }
    });

    $('#submit_data_button').on('click', function() {
        if (validateData() === 0) {
            $('#submit_data_button').html($(this).data('loading-text'));
            $('#data_div').hide('slow');
            let jsonData = parse2json('data_div', 'sequencing', linkingID.sequencing);
            jsonData = JSON.stringify(jsonData);
            $.ajax({
                type: 'POST',
                url: '/addData/',
                data: jsonData,
                dataType: 'json',
                success: function(response) {
                    let txt = `Successfully uploaded annotations.
                        <a href="/dcc/detailSeries/${response}/">
                            See details
                        <a>`;
                    $('#submit_data_button').hide();
                    $('#success_summary').html(txt);
                },
                error : function(xhr) {
                $('#success_summary').html(
                    /* @html */`<div class='alert-box alert radius' data-alert>
                        Error: ${xhr.responseText}
                        <a href='#' class='close'>&times;</a>
                    </div>`
                ); // add the error to the dom
                // console.log(`${xhr.status}: ${xhr.responseText}`); // provide a bit more info about the error to the console
                $('#submit_data_button').blur();
            }

            });
        } else {
            $('#submit_data_button').blur();
        }
    });

    $('[name=select_all]').on('click', function() {
        let parentId = $(this).closest('section')[0].id;
        //reseting all select boxes
        $(`#${parentId}_select [id$='_parent']`).prop('checked', false);
        $('[id$="_select_parent"]').prop('selectedIndex', 0);
        $('select[data-subclass = "autocomplete"]').select2();
        $(`.${parentId}_cell`).not('.ui-selected').addClass('ui-selecting');
        // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
        $(`#${parentId}_grid`).data('ui-selectableScroll')._mouseStop(null);
    });

    $('[name=select_all_bioReplicates]').on('click', function() {
        let parentId = $(this).parents('section')[0].id;
        //reseting all select boxes
        $(`#${parentId}_select [id$="_parent"]`).prop('checked', false);
        $('[id$="_select_parent"]').prop('selectedIndex', 0);
        $('select[data-subclass = "autocomplete"]').select2();
        $(`.${parentId}_biologicalReplicate`).not('.ui-selected').addClass('ui-selecting');
        // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
        $(`#${parentId}_grid`).data('ui-selectableScroll')._mouseStop(null);
    });
    $('[name=select_all_techReplicates]').on('click', function() {
        let parentId = $(this).parents('section')[0].id;
        //reseting all select boxes
        $(`#${parentId}_select [id$="_parent"]`).prop('checked', false);
        $('[id$="_select_parent"]').prop('selectedIndex', 0);
        $('select[data-subclass = "autocomplete"]').select2();
        $('.ui-selectee').not('.ui-selected').addClass('ui-selecting');
        // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
        $(`#${parentId}_grid`).data('ui-selectableScroll')._mouseStop(null);
    });

    $('#series_div input[type=radio]').on('click', function() {
        if (this.value === 'Survey') {
            $('.series_num_control_div').hide();
            $('.series_num_control_input').val();
        } else {
            $('.series_num_control_div').show();
        }
    });
    $('#sequencing').on('click', '[class$=btn--select_assay]', function() {
        //reseting all select boxes
        $('#sequencing_select [id$="_parent"]').prop('checked', false);
        $('#sequencing_select [id$="_select_parent"]').prop('selectedIndex', 0);
        $('select[data-subclass = "autocomplete"]').select2();
        if (event.shiftKey) {
            // console.log('Shift');
        } else {
            $('#sequencing_grid .ui-selected').removeClass('ui-selected').addClass('ui-unselecting');
        }
        let tmpAssay         = $(this).val().split(' ')[1],
            matchingAssayDiv = $(`#sequencing .appliedAssay_assay_div:contains("${tmpAssay}")`);
        $('.technicalReplicate_div', matchingAssayDiv).each(function() {
            $(this).addClass('ui-selecting');
        });
        // trigger the mouse stop event (this will select all .ui-selecting
        // elements, and deselect all .ui-unselecting elements)
        $('#sequencing_grid').data('ui-selectableScroll')._mouseStop(null);
    });

    $('#sequencing', '#appliedAssay').on('click', '[class$=btn--select_control]', function() {
        //reseting all select boxes
        $('#sequencing_select [id$="_parent"]').prop('checked', false);
        $('#sequencing_select [id$="_select_parent"]').prop('selectedIndex', 0);
        $('#appliedAssay_select [id$="_parent"]').prop('checked', false);
        $('#appliedAssay_select [id$="_select_parent"]').prop('selectedIndex', 0);

        if (event.shiftKey) {/*empty*/} else {
            $('#sequencing_grid .ui-selected').removeClass('ui-selected').addClass('ui-unselecting');
        }
        $(`.sequencing_subgrid:contains("${$(this).val().split(' ')[1]}")`).each(function() {
            $(this).addClass('ui-selecting');
        });
        // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
        $('#sequencing_grid').data('ui-selectableScroll')._mouseStop(null);
    });

    $('#biosample').on('click', '[name="cancel"]', function() {
        this.parentElement.innerHTML =
        /* @html */`<button type='button' class='x_btn' name='undo'>
            <img style='height:0.9em;' src='/static/images/undo.svg'>
            </button>\
            <span name='biosample_post_fertilization' data-subclass='col'></span>
            <span name='biosample_developmental_stage' data-subclass='col'></span>
            <span name='biosample_lab' data-subclass='row' ></span>
            <span name='biosample_genetic_background'data-subclass='row'></span>
            <span name='biosample_mutation_description' data-subclass='row'></span>
            <span name='biosample_anatomical_term' data-subclass='select'></span>
            <span name='biosample_sample_type' data-subclass='row'></span>
            <span name='biosample_cell_line_type' data-subclass='row'></span>
            <span name='biosample_treatment' data-subclass='row'></span>
            <span name='biosample_sex' data-subclass='row'></span>`;
    });

    $('#biosample').on('click', '[name="undo"]', function() {
        let divId = this.parentElement.id,
            tableId = `#${this.closest('table').id}`;

        $(`#${divId} button`).replaceWith(
            /* @html */`<button type="button" class="x_btn" name = "cancel">
                    <img style="width:0.5em;" src="/static/images/cancel.svg">
                </button>`);
        $(`#${divId} span`).each(function() {
            let elementName = $(this).attr('name'),
                elementClass = $(this).attr('data-subclass');
            if (elementClass === 'row') {
                $(this).text(
                    `$(${tableId} #${elementName}_${divId.split('_')[0]})
                        .find(":selected").text()}|`
                );
            } else if (elementClass === 'col') {
                $(this).text(`$(${tableId} #${elementName}_${divId.split('_')[1]}).val()}|`);
            } else if (elementClass === 'select') {
                $(`${tableId}#${elementName}_${divId.split('_')[0]}`)
                .children()
                .clone()
                .attr('id', 'biosample_anatomical_term_select_cell')
                .appendTo(`#${divId} span[name=${elementName}]`);

            }
        });

    });

    $('#biosample_save').on('click', function() {
        // parse2json();
    });

    $('h2').on('click', function() {
        let headerId,
            divId;

        headerId = this.id;
        divId = `${headerId.substring(0, headerId.length - 7)}_div`;
        $(`#${divId}`).toggle('fast');
    });

    $('#biosample').on('click', '[name="add_biologicalReplicate"]', function() {
        let wrapper, div, i;
        wrapper = $(this).closest('div [class^=biosample_wrapper]')[0];
        div = wrapper.children[0];
        i = wrapper.children.length - 1;

        createReplicateCell(wrapper, div.id, i);
        if (i === 0) {
            // $('#' + wrapper.children[0].id + ' > button').remove();
            // $(wrapper.children[1]).append("<button type='button' class='plus_btn' name = 'add_biologicalReplicate'><b>+</b></button>");
        }
    });

    $('#biosample').on('click', '[name="delete_biologicalReplicate"]', function() {
        let wrapper, i;
        wrapper = $(this).closest('div [class^=biosample_wrapper]')[0];
        i = wrapper.children.length;
        if (i === 2) {
            $(wrapper.children[0]).append(
                /* @html */`<button type="button" class="plus_btn singular"
                                    name = "add_biologicalReplicate"
                                    "style=margin:0; float:right">
                                        <b>&times;</b>
                                    </button>`
            );
        } else if (i === 1) {
            $(wrapper).remove();
        }
        wrapper.children[i - 1].remove();
    });

    // $('.container').on('click', '.btn--edit', function(event) {
    //     var parent_section = this.closest('section');
    //
    //     $(this).hide();
    //     $(parent_section).show();
    //     $('.btn--apply', parent_section).show();
    //
    // });

    $('.container').on('click', '#series_apply_btn', function() {


        if (validateSeries() === 0) {
            $('#series_div').hide('slow');
            $('#create_biosample_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_biosample_button').hide();
                $('#biosample').show();
                $('#biosample_anatomical_term_parent').hide();
                $('#biosample_cell_line_type_parent').hide();
                // $('#series_edit_btn').show();
                $('#series_apply_btn').hide();
            });
        }

        // $('[class$=_text] [name$=_lab]').text($('#series_lab_select_parent').val());
        let jsonData = parse2json('series_div', '', ''),
            data     = new FormData(),
            description;

        if ($('#description_file')[0]) {
            description = $('#description_file')[0].files[0];
        } else {
            description = '';
        }


        jsonData = addId(linkingID.series, jsonData, 'series');
        jsonData = JSON.stringify(jsonData);
        //        var description = $('#description_file').val();
        data.append('jsonData', jsonData);
        data.append('description_file', description);
        $.ajax({
            url: '/addSeries/',
            processData: false,
            contentType: false,
            type: 'POST',
            data: data,
            success: function(response) {
                linkingID.series = response;
                // console.log(response);
            },
            error : function(xhr) {
                $('#success_summary').html(
                    /* @html */`<div class='alert-box alert radius' data-alert>
                        Error: ${xhr.responseText}
                        There was information missing in your annotation.
                        Please reload this page.
                    </div>`
                ); // add the error to the dom
                // console.log(`${xhr.status}: ${xhr.responseText}`); // provide a bit more info about the error to the console
                $('#series_apply_btn').blur();
            }
        });

    });

    $('.container').on('click', '#biosample_apply_btn', function() {
        if (validateBiosample() === 0) {
            $('#biosample_div').hide('slow');
            $('#create_assay_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_assay_button').hide();
                $('#assay').show();
                // $('#biosample_edit_btn').show();
                $('#biosample_apply_btn').hide();
            });
            $('.ui-selecting').removeClass('ui-selecting');
            $('.ui-selected').removeClass('ui-selected');
            $('.ui-selectee').removeClass('ui-selectee');

            let jsonData = parse2json('biosample_div', 'series', linkingID.series);

            jsonData = addId(linkingID.biosample, jsonData, 'biosample');
            jsonData = JSON.stringify(jsonData);
            $.post('/addBiosample/', jsonData, function(response) {
                linkingID.biosample = response;
                // // console.log(response);
            }, 'json');
        }
    });

    $('.container').on('click', '#assay_apply_btn', function() {
        if (validateAssay() === 0) {
            $('#create_appliedAssay_button').val('');
            $('#assay_div').hide('slow');
            $('#create_appliedAssay_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_appliedAssay_button').hide();
                $('#appliedAssay').show();
                // $('#assay_edit_btn').show();
                $('#assay_apply_btn').hide();
            });


            $('#appliedAssay_select [id$="_parent"]').prop('checked', false);

            let jsonData = parse2json('assay_grid', '', '');
            jsonData = addId(linkingID.assay, jsonData, 'assay');
            jsonData = JSON.stringify(jsonData);
            $.post('/addAssay/', jsonData, function(response) {
                linkingID.appliedAssay = response;
                $(response).each(function(index) {
                    let assayTitle = this.assay_id.substr(0, this.assay_id.length - 2);
                    $($('.appliedAssay_selected_assay')[index]).text(assayTitle);
                    $($('.appliedAssay_selected_assay_checkbox')[index])
                    .attr('name', assayTitle);
                    $($('.btn--select_assay')[index]).val(`Select ${assayTitle}`);
                });
                // // console.log(response);
            }, 'json');
        }
    });

    $('.container').on('click', '#appliedAssay_apply_btn', function() {
        if (validateAppliedAssay() === 0) {

            $('#create_sequencing_button').val('');
            $('#appliedAssay_div').hide('slow');
            $('#create_sequencing_button').animate({
                width: '100%'
            }, 300, function() {
                $('#sequencing').show();
                // $('#appliedAssay_edit_btn').show();
                $('#appliedAssay_apply_btn').hide();
            });

            $('.appliedAssay_wrapper').removeClass('ui-selecting');
            $('.appliedAssay_wrapper').removeClass('ui-selected');
            $('.appliedAssay_wrapper').removeClass('ui-selectee');

            let jsonData = parse2json('appliedAssay_div', 'appliedAssay', linkingID);

            jsonData = addId(linkingID.appliedAssay, jsonData, 'appliedAssay');
            jsonData = JSON.stringify(jsonData);
            $.post('/addAppliedAssay/', jsonData, function(response) {
                linkingID.appliedAssay = response;
                // // console.log(response);
            }, 'json');
        }
    });

    $('.container').on('click', '#sequencing_apply_btn', function() {
        if (validateSequencing() === 0) {
            $('#sequencing_div').hide('slow');
            $('#create_data_button').animate({
                width: '100%'
            }, 300, function() {
                $('#create_data_button').hide();
                $('#data_mapped_genome_parent').hide();
                $('#data_command_parent').hide();
                $('#data').toggle();
                $('#submit_data_button').show();
                // $('#sequencing_edit_btn').show();
                $('#sequencing_apply_btn').hide();
            });

            $('.sequencing_wrapper').removeClass('ui-selecting');
            $('.sequencing_wrapper').removeClass('ui-selected');
            $('.sequencing_wrapper').removeClass('ui-selectee');

            let jsonData = parse2json('sequencing_div', 'appliedAssay', linkingID.appliedAssay);
            jsonData = addId(linkingID.sequencing, jsonData, 'sequencing');
            jsonData = JSON.stringify(jsonData);
            $.post('/addSequencing/', jsonData, function(response) {
                linkingID.sequencing = response;
                // // console.log(response);
            }, 'json');
        }
    });

    $('.container').on('click', '#data_apply_btn', function() {
        if (validateData() === 0) {
            $('#submit_data_button').hide('slow');
            $('#data_div').hide('slow');
            // $('#sdata_edit_btn').show();
            $('#data_apply_btn').hide();
            // data = JSON.stringify(data); //convert array into json object
            let jsonData = parse2json('data_div', 'sequencing', linkingID.sequencing);

            jsonData = addId(linkingID.sequencing, jsonData, 'sequencing');
            jsonData = JSON.stringify(jsonData);
            $.post('/addData/', jsonData, function(response) {
                linkingID.data = response;
                // // console.log(response);
            }, 'json');
        }
    });

    $('#series').on('keyup', '#series_title', function(){
        $('#series_header_title').text($(this).val());
    });

    $('#series #series_type_radio_buttons').on('change', function(){
        if ($('#series_case_control').is(':checked')) {
            $('.case_control').show();
            $('.survey').hide();
        } else if ($('#series_survey').is(':checked')){
            $('.case_control').hide();
            $('.survey').show();
        }
    });

    $('.series_num_samples_input').on('keyup',function() {
        if ($('.series_num_samples_input').val().indexOf(',')> -1) {
            $('#series_replicates_div').hide();
        }else{
            $('#series_replicates_div').show();
        }
});

    $('#biosample').on('change keyup', '[name$=parent]', function() {
        let parentId = this.id,
            groupName;

        groupName = parentId.substring(0, parentId.lastIndexOf('_'));
        groupName = groupName.substring(0, groupName.lastIndexOf('_'));
        if ($('.ui-selected').length === 0) {
            $('[id$="_select_parent"]').prop('selectedIndex', 0);
            $('select[data-subclass = "autocomplete"]').select2();
        }

        $('.ui-selected').each(function() {

            $(`[name=${groupName}]`, this).text($(`#${parentId}`).val());
            $(`[name=${groupName}]`, this).removeClass('error');
            if (groupName === 'biosample_developmental_stage') {
                $('.error[name=biosample_post_fertilization]', this).remove();
            }
            if ($('#biosample_sample_type_select_parent').val() === 'tissue') {
                $('#biosample_anatomical_term_parent').show();
            } else {
                $('#biosample_anatomical_term_parent').hide();
            }
            if ($('#biosample_sample_type_select_parent').val() === 'tissue' ||
                $('#biosample_sample_type_select_parent').val() === 'whole organism') {
                $('#biosample_developmental_stage_select_parent').show();
                $('#biosample_post_fertilization_select_parent').show();
            } else {
                $('#biosample_developmental_stage_select_parent').hide();
                $('#biosample_post_fertilization_select_parent').hide();
            }
            if ($('#biosample_sample_type_select_parent').val() === 'cell line') {
                $('#biosample_cell_line_type_parent').show();
            } else {
                $('#biosample_cell_line_type_parent').hide();
            }

        });
    });

    $('#appliedAssay').on('click', '[id$=parent]', function() {

        if ($('.ui-selected').length === 0) {
            $('#appliedAssay_select [id$="_parent"]').prop('checked', false);
        }

        $('.ui-selected').each(function() {
            let replicate = $(this);
            $('div[name$=assigned_assay]', replicate).html('');
            $('#appliedAssay_select input:checkbox:checked').each(function() {
                if ($(this).attr('data-control_for')) {
                    $('div[name=appliedAssay_assigned_assay]', replicate).append(
                    /* @html */`<div class="appliedAssay_assay_div">
                                    <div class="appliedAssay_assay_row">
                                        <span data-control_for=${$(this).attr('data-control_for')}>
                                            ${$(this).attr('name').replace(/_/g, '_<wbr/>')}
                                        </span>
                                        <label>Num. of technical replicates: </label>
                                        <span>
                                            <input type="number" class="number_input" min="1"
                                                default = "1" placeholder="1"
                                                name="appliedAssay_num_tech_replicates">
                                        </span>
                                    </div>
                                </div>`
                    );
                } else {
                    $('div[name=appliedAssay_assigned_assay]', replicate).append(
                    /* @html */`<div class="appliedAssay_assay_div">
                        <div class="appliedAssay_assay_row">
                            <span> ${$(this).attr('name').replace(/_/g, '_<wbr/>')}</span>
                            <label>Num. of technical replicates: </label>
                            <span>
                                <input type="number" class="number_input" min="1"
                                    default = "1" placeholder="1"
                                    name="appliedAssay_num_tech_replicates">
                            </span>
                        </div>
                    </div>`);
                }
                $('div[name=appliedAssay_assigned_assay]', replicate).removeClass('error');
                // replicate.parent().append(replicate_clone);
            });
            // $(this).remove();
        });
    });

    $('#sequencing').on('change keyup dp.change', '[id$=parent]', function() {
        let parentId = this.id,
            groupName;

        groupName = parentId.substring(0, parentId.lastIndexOf('_'));
        groupName = groupName.substring(0, groupName.lastIndexOf('_'));
        if ($('.ui-selected').length === 0) {
            $('#sequencing_select [id$="_parent"]').prop('checked', false);
        }
        $('.ui-selected .sequencing_text').each(function() {
            if ($(`#${parentId}`).attr('id') === 'sequencing_date_div_parent') {
                $(`[name=${groupName}]`, this).text(
                    $(`#${parentId} input`).val()
                );
                $(`[name=${groupName}]`, this).removeClass('error');
            } else {
                $(`[name=${groupName}]`, this).text($(`#${parentId}`).val());
                $(`[name=${groupName}]`, this).removeClass('error');
            }
        });
    });

    $('#sequencing').on('change', '[id=sequencing_paired_end]', function() {
        let parent = $(this);

        if ($('.ui-selected').length === 0) {
            $('#sequencing_select [id$="_parent"]').prop('checked', false);
        }
        $('.ui-selected .sequencing_text').each(function() {
            $($('[name=sequencing_paired_end]', this).text($('input[type=radio]:checked', parent).val())).removeClass('error');
        });
    });

    $('#data').on('change', '[id$=parent]', function() {
        let parentId = this.id,
            groupName;

        groupName = parentId.substring(0, parentId.lastIndexOf('_'));
        groupName = groupName.substring(0, groupName.lastIndexOf('_'));
        if ($('.ui-selected').length === 0) {
            $('#data_select [id$="_parent"]').prop('checked', false);
        }
        if ($('#data_file_type_select_parent').val() === '') {
            $('#data_mapped_genome_parent').hide();
            $('#data_command_parent').hide();
        } else if ($('#data_file_type_select_parent').val() !== 'FASTQ' || $($('.ui-selected span[name="data_file_type"]')[0]).text() !== 'FASTQ') {
            $('#data_mapped_genome_parent').show();
            $('#data_command_parent').show();
        } else {
            $('#data_mapped_genome_parent').hide();
            $('#data_command_parent').hide();
        }

        $('.ui-selected').each(function() {
            $(`[name=${groupName}]`, this).text($(`#${parentId}`).val());
            $(`[name=${groupName}]`, this).removeClass('error');
        });
    });

    $('#assay').on('change', '.selectAutoFill', function() {
        let parentDivId = $(this).closest('div [class=assay_wrapper]')[0].id;

        if ($.inArray($(`#${parentDivId} .assay_type`).val(), targetList) !== -1) {
            $(`#${parentDivId} .assay_target`).show();
            $(`#${parentDivId} .assay_prep_div`).hide();
        } else if ($.inArray($(`#${parentDivId} .assay_type`).val(), rnaList) !== -1) {
            $('#sequencing_strand_mode_select_parent').show();
            $(`#${parentDivId} .assay_prep_div`).show();
            $(`#${parentDivId} #assay_target_input`).val('');
            $(`#${parentDivId} .assay_target`).hide();
        } else {

            $(`#${parentDivId} .assay_prep_select`).val([]).trigger('change');
            $(`#${parentDivId} .assay_prep_div`).hide();
            $(`#${parentDivId} #assay_target_input`).val('');
            $(`#${parentDivId} .assay_target`).hide();
        }
    });
    $('[id^=assay_lab_]').on('change',function(){
      fillSelect('', '#assay_grid');
    })
    // $('#assay').on('change', '.select_assay', function(event) {
    //     var wrapper = $(this).closest('.assay_wrapper')[0].id;
    //     var id = $(this).val();
    //     var idx = wrapper.substring(wrapper.lastIndexOf('_') + 1, wrapper.length);

    //     var result = $.grep(assay_json, function(e) {
    //         return e.fields.assay_id === id + 'AS';
    //     });
    //     if (result.length === 1) {
    //         result = result[0].fields;
    //         $('#' + wrapper + ' #assay_lab_' + idx + '_select').val(result.assay_lab);
    //         $('#' + wrapper + ' #assay_type_' + idx + '_select').val(result.assay_type);
    //         $('#' + wrapper + ' #assay_type_' + idx + '_select').trigger('change');
    //         $('#' + wrapper + ' #sequencing_strand_mode_mode [value = "' + result.strand + '"]').prop('checked', true);
    //         $('#' + wrapper + ' #assay_target_input').val(result.target);
    //         $('#' + wrapper + ' #assay_description_' + idx).val(result.description);
    //     }
    // });

    $('#assay').on('change', '.select_assay', function() {
        let wrapper = $(this).closest('.assay_wrapper')[0].id,
            id      = $(this).val(),
            idx     = wrapper.substring(wrapper.lastIndexOf('_') + 1, wrapper.length);

        var result = $.grep(assay_json, function(e) {
            return e.assay_id === `${id}AS`;
        });
        if (result.length === 1) {
            result = result[0];
            $(`#${wrapper} #assay_lab_${idx}_select`).val(result.assay_lab);
            $(`#${wrapper} #assay_type_${idx}_select`).val(result.assay_type);
            $(`#${wrapper} #sequencing_strand_mode_mode [value = "${result.strand}"]`).prop('checked', true);
            $(`#${wrapper} #assay_target_input`).val(result.target);
            if ($(`#${wrapper} .assay_prep_select`).find("option[value=" + result.library_prep + "]").length) {
                $(`#${wrapper} .assay_prep_select`).val(result.library_prep).trigger("change");
            } else {
                // Create the DOM option that is pre-selected by default
                var new_option = new Option(result.library_prep, result.library_prep, true, true);
                // Append it to the select
                $(`#${wrapper} .assay_prep_select`).append(new_option).trigger('change');
            }
            $(`#${wrapper} #assay_description_${idx}`).val(result.description);


            // $('#' + parentDivId + ' .select_assay').select2();
            if ($.inArray(result.assay_type, targetList) !== -1) {
                $('#sequencing_strand_mode_select_parent').hide();
                $(`#${wrapper} .assay_prep_div`).hide();
                $(`#${wrapper} .assay_prep_select`).val([]).trigger('change');
                $(`#${wrapper} .assay_target`).show();
            } else if ($.inArray(result.assay_type, rnaList) !== -1) {
                $('#sequencing_strand_mode_select_parent').show();
                $(`#${wrapper} .assay_prep_div`).show();
                $(`#${wrapper} .assay_target`).hide();
            } else {
                $(`#${wrapper} .assay_target`).hide();
                $(`#${wrapper} .assay_prep_div`).hide();
                $(`#${wrapper} .assay_prep_select`).val([]).trigger('change');
                $(`#${wrapper} #assay_target_input`).val('');
            }
        }
    });


    $('#assay').on('change', '.form-control', function() {
        let wrapper = $(this).closest('.assay_wrapper')[0];
        if ($('.assay_description', wrapper).val() !== '') {
            $('.select_assay', wrapper).select2('val', '');
        }
    });

    $('#assay').on('keyup change', '.assay_description', function() {
        let wrapper = $(this).closest('.assay_wrapper')[0];
        $('.select_assay', wrapper).select2('val', '');
    });

    // Prevent submit when pressing enter
    $(document).on('keypress', ':input:not(textarea)', function(event) {
        return event.keyCode != 13;
    });


    // $('#sequencing').on('change', '[id$=parent]', function(event) {
    //     var parentId = this.id,
    //         groupName;

    //     if ($(".ui-selected").length === 0) {
    //         $("[id$='_select_parent']").prop('selectedIndex', 0);
    //         $('select[data-subclass = "autocomplete"]').select2();
    //     }

    //     $('.ui-selected').each(function() {
    //         groupName = parentId.substring(0, parentId.lastIndexOf('_'));
    //         groupName = groupName.substring(0, groupName.lastIndexOf('_'));
    //         $('#' + this.id + ' [name=' + groupName + ']').text($('#' + parentId).val());
    //     });
    // });

    // function updateCells(){
    //     $('.ui-selected').each(function() {
    //          $(this).children().each(function(){
    //             $(this).val($(this.getAttribute('name') + '_select_parent').val())
    //          })
    //     });
    // }




    //the following functions parses json files to autogenerate the options of a select element of table
    function createSampleCell(parent, iterator, replicates, title) {
        let wrapper = document.createElement('div'); // create div element
        let div = document.createElement('div'); // create div element
        wrapper.setAttribute('class', 'biosample_wrapper');
        div.innerHTML =
        /* @html */`<span name='biosample_title' ><b>${title} ${(iterator + 1)}</b><br></span>
                  <div class = biosample_text>
                      <span name='biosample_post_fertilization' data-subclass='col'></span>
                      <span name='biosample_developmental_stage' data-subclass='col'><br></span>
                      <span name='biosample_lab' data-subclass='row'></span>
                      <span name='biosample_source' data-subclass='row'></span>
                      <span name='biosample_genetic_background'data-subclass='row'><br></span>
                      <span name='biosample_sample_type' data-subclass='row'></span>
                      <span name='biosample_anatomical_term' data-subclass='select'><br></span>
                      <span name='biosample_mutation_description' data-subclass='row'></span>
                      <span name='biosample_cell_line_type' data-subclass='row'></span>
                      <span name='biosample_treatment' data-subclass='row'></span>
                      <span name='biosample_sex' data-subclass='row'></span>
                      <span name='biosample_controlled_by' data-subclass='row'></span><br>
                      <textarea class='biosample_description'
                                name='biosample_description'
                                maxlength="1000"
                                placeholder = 'Description'></textarea>
                  <div>
                  <button type='button' class='plus_btn' name = 'add_biologicalReplicate'>
                      <b>&times;</b>
                  </button>`;
        // < div > < span class = 'replicate_1_title' > < b > Replicate 1 < /b></span > < /div>\

        div.setAttribute('id', `biosample_cell_${iterator}`); //set id
        div.setAttribute('class', 'biosample_cell'); // set div class attribute
        wrapper.appendChild(div);
        for (let i = 0; i < parseInt(replicates); i++) {
            createReplicateCell(wrapper, div.id, i);
        }
        // $(wrapper.children[1]).append("<button type='button' class='plus_btn' name = 'add_biologicalReplicate'><b>+</b></button>");
        // append div to the wrapper div
        parent.appendChild(wrapper); // append div to the parent div
    }

    function createReplicateCell(wrapper, divId, i) {
        let rep = document.createElement('div');
        // rep.innerHTML = "<button type='button' class='x_btn' name = 'delete_biologicalReplicate'> \
        // <img style='height:0.7em;' src='/static/images/cancel.svg'></button>";
        rep.innerHTML = /* @html */`<div>
            <span class=replicate_title>
                <b> biological replicate ${(i + 1)}</b>
            </span>
            <button type='button' class='x_btn' name = 'delete_biologicalReplicate'>
            <b>&times;</b></button></div>`;
        $(`#${divId}`).id = `${divId}-${i}`; //set id
        rep.setAttribute('class', 'biosample_biologicalReplicate');
        rep.setAttribute('id', `biosample_replicate_${i}`);
        $(rep).hide().appendTo(wrapper).fadeIn(300);
    }

    function fillSelect(table, divId) {
        //looping through each select field with the added .selectAutoFill class
        fillLabs(labs_json);
        $(`${table}${divId} .selectAutoFill`).each(function() {
            let select     = $(this),
                fileName  = select.attr('data-filename'),
                groupName = this.id.substring(0, this.id.lastIndexOf('_'));

            groupName = groupName.substring(0, groupName.lastIndexOf('_'));

            //list of all json files in order as they appear in the header
            //request the json data and parse into the select element
            $.ajax({
                url: `/static/json/${fileName}.json`,
                dataType: 'json',
                success: function(data) {
                    //clear the current content of the select
                    select.html('');
                    let placeholder =
                    groupName.substring(groupName.indexOf('_') + 1, groupName.length)
                    .replace('_', ' ');
                    if (placeholder === 'developmental stage') {
                        select.append('<option value="" default selected>dev. stage</option>');
                    } else if (placeholder === 'post fertilization') {
                        select.append('<option value="" default selected>time post fert.</option>');
                    } else {
                        select.append(`<option value="" default selected>${placeholder}</option>`);
                    }
                    //iterate over the data and append a select option
                    $.each(data, function(key, val) {
                        select.append(`<option>${val.name}</option>`);
                    });

                },
                error: function() {
                    //if there is an error append an error option
                    select.html('<option id="-1">Error: term file missing</option>');
                }
            });
        });
    }

    function fillAssays(availableAssaysJson) {
        $('#assay_grid .select_assay').each(function() {
            var select = this,
                selectname = select.id.substring(0, select.id.lastIndexOf('_'));

            selectname = selectname.substring(0, selectname.lastIndexOf('_'));
            $(select).append('<option></option>');
            $.each(availableAssaysJson, function() {
                if (this[selectname].search('control_') === -1) {
                    $(select).append(
                        `<option>
                            ${this[selectname].substring(0, this[selectname].length - 2)}
                        </option>`);
                }
            });
        });


        $('#assay_grid .select_assay').select2({
            placeholder: 'Assay ID'
        });
    }

    function fillLabs(availableLabsJson) {
        $('[data-filename = "labs"]').each(function() {
            var select = this;
            $(select).html('');
            $(select).append('<option value="" default selected>lab</option>');
            $.each(availableLabsJson, function() {
                $(select).append(`<option>${this.name.replace('_', ' ')}</option>`);
            });
        });
    }

    function parse2json(parentDiv, section, id) {
        let counter = 0,
            json = [],
            parentName = parentDiv.substring(0, parentDiv.lastIndexOf('_'));

        $(`#${parentDiv}  [class^=${parentName}_text]`).each(function() {
            let text = this;

            if ($(`#${parentDiv} [name=biosample_title]`).length > 0) {
                let numberOfCells = $($(this).parents(`[class^=${parentName}_wrapper]`)[0])
                                .length;
                for (let i = 0; i < numberOfCells; i++) {
                    let item = {};

                    item['pk'] = counter;
                    item['model'] = parentDiv.substring(0, parentDiv.lastIndexOf('_'));
                    item['fields'] = {};
                    item['fields'][`${parentName}_id`] = '';
                    $('[data-filename]', text).each(function() {
                        item['fields'][this.id.split('_')[1]] = $(this).val().trim();
                    });
                    $('span', text).each(function() {
                        if ($(this).attr('name')) {
                            item['fields'][$(this).attr('name')] = $(this).text().trim();
                        } else {
                            return true;
                        }
                    });
                    $('textarea', text).each(function() {
                        item['fields'][$(this).attr('name')] = $(this).val().trim();
                    });
                    $('input', text).each(function() {
                        if ($(this).val()) {
                            item['fields'][$(this).attr('name')] = $(this).val().trim();
                        } else {
                            item['fields'][$(this).attr('name')] = $(this).attr('placeholder');
                        }
                    });

                    if (parentDiv.split('_')[0] === 'biosample') {
                        if ($('.biosample_grid_subgrid').length > 1) {
                            item.fields['control'] =
                                parseInt($('[name$=controlled_by]', text)
                                .text().split(' ')[1]) - 1;
                        } else {
                            item.fields['control'] = null;
                        }
                        item['fields'][`${section}_id`] = id.series_id;
                        item['fields']['num_replicates'] =
                            $($(this).parents(`[class^=${parentName}_wrapper]`)[0])
                                .children('[class$="_biologicalReplicate"]').length;
                    } else if (section === 'BiosampleAppliedAssay') {

                        item['fields']['biosamplereplicate_id'] =
                            id.biosample[counter].biosamplereplicate_id;

                        item['fields']['appliedAssay_id'] =
                            $('[name="appliedAssay_assigned_assay"] span', text)
                            .text().replace(/\s\s+/g, ' ').trim();

                        $('[data-controlled_by]', text).each(function() {
                            item['fields']['control'] =
                                $(`#${$(this).attr('data-controlled_by')}`)
                                .attr('name');
                        });
                    } else {
                        item['fields'][`${section}_id`] = id[counter][`${section}_id`];
                    }
                    counter++;
                    json.push(item);
                }

                // });
            } else {
                let item = {};

                item['pk'] = counter;
                item['model'] = parentDiv.substring(0, parentDiv.lastIndexOf('_'));
                item['fields'] = {};
                item['fields'][`${parentName}_id`] = '';
                $('[data-filename]', text).each(function() {
                    let id = this.id;
                    let name = id.substring(0, id.lastIndexOf('_'));
                    name = name.substring(0, name.lastIndexOf('_'));
                    item['fields'][name] = $(this).val();
                });
                $('span', text).each(function() {
                    if ($(this).attr('name')) {
                        item['fields'][$(this).attr('name')] = $(this).text().trim();
                    } else {
                        return true;
                    }
                });
                $('textarea', text).each(function() {
                    item['fields'][$(this).attr('name')] = $(this).val().trim();
                });
                $('input[type=radio]:checked', text).each(function() {
                    item['fields'][$(this).attr('name')] = $(this).val().trim();
                });
                $('input[type=text]', text).each(function() {
                    item['fields'][$(this).attr('name')] = $(this).val().trim();
                });
                $('.assay_prep_select', text).each(function(){
                    item['fields']['lib_prep'] = ''
                    if(this){
                    item['fields']['lib_prep'] = $(this).val().trim();
                }
                })
                counter++;
                json.push(item);
            }
        });

        // jsonData = JSON.stringify(json);
        //  console.log(jsonData);
        // return jsonData;

        console.log(json);

        return json;
    }

    function validateInput(section, fields) {
        let missingCounter = 0;
        $(`.${section}_text`).each(function() {
            let text = this;
            $(fields, text).each(function() {
                if ($(this, text).val() == '') {
                    $(this, text).addClass('missing');
                    // $(this, text).css('border', '1pt solid red');
                    missingCounter++;
                } else {
                    $(this, text).removeClass('missing');
                    // $(this, text).css('border', '1pt solid green');
                }
            });

        });
        return missingCounter;
    }

    function validateText(section, fields) {
        let missingCounter = 0;
        $(`.${section}`).each(function() {
            let wrapper = this;
            $.each(fields, function() {
                let groupName = this;
                // var groupName = this.substring(0, this.lastIndexOf('_'));
                let fieldName =
                    groupName.substring(groupName.indexOf('_') + 1, groupName.length)
                             .replace('_', ' ');
                if ($(`[name=${this}]`, wrapper).length > 0) {
                    let mistake = $(`[name=${this}]`, wrapper).text() === ''
                        || $(`[name=${this}]`, wrapper).hasClass('error');
                    if (mistake) {
                            $(`[name=${this}]`, wrapper).text(` ${fieldName} is missing`);
                            $(`[name=${this}]`, wrapper).addClass('error');
                            missingCounter++;
                    }
                }
            });

        });
        return missingCounter;
    }

    function validateSeries() {
        let missingCounter = 0;

        missingCounter += validateInput('series', ['#series_title',
            '#series_num_assay_input', '#series_description']);


        switch ($('input[name="series_type"]:checked', '#series').val()) {
            case 'Survey':
                $('#series_type_radio_buttons>span.error').remove();
                missingCounter += validateInput('series', ['.survey .series_num_samples_input']);
                break;
            case 'Case Control':
                missingCounter += validateInput('series',
                    ['.series_num_control_input',
                     '.case_control .series_num_samples_input']);
                break;
            default:
                if ($('#series_type_radio_buttons>span.error').length ===0){
                    let txt = /* @html */`<span class = error>
                                Select a series type
                               </span>`;
                    $('#series_type_radio_buttons').append(txt);
                }
                missingCounter++;
        }


        if (!$('input[name="series_is_public"]:checked', '#series')) {
            let txt = /* @html */`<span class = error>
                        Please state if this series is public or not
                       </span>`;
            $('#series_public_radio_buttons').append(txt);
            missingCounter++;
        }

        return missingCounter;
    }

    function validateBiosample() {
        let missingCounter = 0;

        missingCounter += validateText('biosample_wrapper',
                                     ['biosample_lab',
                                      'biosample_genetic_background',
                                      'biosample_sample_type']
                        );

        if ($('input[name="series_type"]:checked', '#series').val() === 'Case Control') {
            $('.biosample_wrapper', '#biosample_grid_subgrid_1').each(function() {
                let wrapper = this,
                    mistake = $('[name=biosample_controlled_by]', wrapper).text() === ''
                    || $('[name=biosample_controlled_by]', wrapper).hasClass('error');

                if (mistake) {
                    $('[name=biosample_controlled_by]', wrapper).text(' control misssing');
                    $('[name=biosample_controlled_by]', wrapper).addClass('error');
                    missingCounter++;
                }
            });
            // missingCounter += validateText('biosample', ['biosample_controlled_by']);
        }

        // $('.biosample_wrapper').each(function(index, el) {
        //   var wrapper = this;
        //   var cond1 = $('[name=biosample_post_fertilization]', wrapper).text() === "";
        //   var cond2 = $('[name=biosample_developmental_stage]', wrapper).text() === "";
        //   var cond3 = $('[name=biosample_post_fertilization]', wrapper).hasClass('error');
        //   // var cond4 = $('[name=biosample_developmental_stage]', wrapper).hasClass('error');
        //
        //   if ((cond1 || cond3) && (cond2 || cond3)) {
        //     $('[name=biosample_post_fertilization]', wrapper).text(' time point (post fertilization or developmental stage) is missing');
        //     $('[name=biosample_post_fertilization]', wrapper).addClass('error');
        //     missingCounter++;
        //   } else {
        //     $('[name=biosample_post_fertilization]', wrapper).removeClass('error');
        //   }
        // });

        $('span[name="biosample_sample_type"]:contains("tissue")').each(function() {
            let wrapper = $(this).parent(),
                cond1 = $('span[name="biosample_anatomical_term"]', wrapper).text() === '',
                cond2 = $('span[name="biosample_anatomical_term"]', wrapper).hasClass('error');
            if (cond1 || cond2) {
                $('span[name="biosample_anatomical_term"]', wrapper)
                    .text(' anatomical term is missing')
                    .addClass('error');
                missingCounter++;
            } else {
                $('span[name="biosample_anatomical_term"]', wrapper).removeClass('error');
            }

            cond1 = $('[name=biosample_developmental_stage]', wrapper).text() === '',
            cond2 = $('[name=biosample_developmental_stage]', wrapper).hasClass('error');
            if (cond1 || cond2) {
                $('[name=biosample_developmental_stage]', wrapper)
                    .text('developmental stage is missing');
                $('[name=biosample_developmental_stage]', wrapper).addClass('error');
                missingCounter++;
            } else {
                $('[name=biosample_developmental_stage]', wrapper).removeClass('error');
            }
            cond1 = $('[name=biosample_post_fertilization]', wrapper).text() === '',
            cond2 = $('[name=biosample_post_fertilization]', wrapper).hasClass('error');
            if (cond1 || cond2) {
                $('[name=biosample_post_fertilization]', wrapper)
                    .text('time post fertilization is missing');
                $('[name=biosample_post_fertilization]', wrapper).addClass('error');
                missingCounter++;
            } else {
                $('[name=biosample_post_fertilization]', wrapper).removeClass('error');
            }
        });
        $('span[name="biosample_sample_type"]:contains("whole organism")').each(function() {
            let wrapper = $(this).parent(),
                cond1 = $('[name=biosample_developmental_stage]', wrapper).text() === '',
                cond2 = $('[name=biosample_developmental_stage]', wrapper).hasClass('error');
            if (cond1 || cond2) {
                $('[name=biosample_developmental_stage]', wrapper)
                    .text('developmental stage is missing')
                    .addClass('error');
                missingCounter++;
            } else {
                $('[name=biosample_developmental_stage]', wrapper).removeClass('error');
            }

            cond1 = $('[name=biosample_post_fertilization]', wrapper).text() === '',
            cond2 = $('[name=biosample_post_fertilization]', wrapper).hasClass('error');
        if (cond1 || cond2) {
            $('[name=biosample_post_fertilization]', wrapper)
                .text('time post fertilization is missing')
                .addClass('error');
            missingCounter++;
        } else {
            $('[name=biosample_post_fertilization]', wrapper).removeClass('error');
        }
        });

        scroll2error('biosample');
        return missingCounter;
    }

    function validateAssay() {
        let missingCounter = 0;
        missingCounter += validateInput('assay',
                                      ['[id^=assay_lab_]',
                                       '[id^=assay_type_]',
                                       '[id^=assay_description_]']
                                      );

        $('.assay_text').each(function() {
            let text = this,
            target_mistake = $('.assay_target', text).is(':visible') &&
                $('#assay_target_input', text).val() === '';

            if (target_mistake) {
                $('#assay_target_input', text).addClass('missing');
                missingCounter++;
            } else if ($('.assay_target', text).is(':visible') &&
                ($('#assay_control_target_input', text).val() === '')) {
                $('#assay_control_target_input', text).addClass('missing');
                missingCounter++;
            } else if ($('.assay_target', text).is(':visible')) {
                $('#assay_target_input', text).removeClass('missing');
                $('#assay_control_target_input', text).removeClass('missing');
            }
            $('.assay_target', text).each(function() {
                if ($(this).is(':visible')) {
                    if ($('input', this).val().split(' ').length > 1) {
                        if ($('.error', this).length === 0) {
                            let txt = /* @html */`<br>
                                <span class = error>
                                    Please don't use any spaces in the target name
                                </span>`;
                            $(this, text).append(txt);
                        }
                        missingCounter++;
                    } else {
                        $('.error', this).remove();
                    }
                }
            });
            var prep_mistake = $('.assay_prep_div', text).is(':visible') &&
                !$('.assay_prep_select', text).val();
            if (prep_mistake) {
                $('.assay_prep_div .select2-container', '.assay_text').addClass('missing')
                missingCounter++;
            } else if ($('.assay_prep_div', text).is(':visible')) {
                $('.assay_prep_div .select2-container', '.assay_text').addClass('missing')
            }
        });
        scroll2error('assay');
        return missingCounter;
    }

    function validateAppliedAssay() {
        let missingCounter = 0;
        missingCounter += validateText('appliedAssay_wrapper', ['appliedAssay_assigned_assay']);
        scroll2error('appliedAssay');
        return missingCounter;
    }

    function validateSequencing() {
        let missingCounter = 0;
        missingCounter += validateText('sequencing_text',
                                     ['sequencing_platform',
                                      'sequencing_lab',
                                      'sequencing_instrument',
                                      'sequencing_paired_end']
                                     );
        let fieldName = 'sequencing date';
        $('.sequencing_text').each(function() {
            if ($('[name="sequencing_date"]', this).text() === '' || $('[name="sequencing_date"]', this).hasClass('error')) {
                $('[name="sequencing_date"]', this).text(` ${fieldName} is missing`);
                $('[name="sequencing_date"]', this).addClass('error');
                missingCounter++;
            } else if (!moment($('[name="sequencing_date"]', this).text(), 'YYYY-MM-DD').isValid()) {
                $('[name="sequencing_date"]', this).text('wrong date format');
                $('[name="sequencing_date"]', this).addClass('error');
                missingCounter++;
            }

        });

        scroll2error('sequencing');
        return missingCounter;
    }

    function validateData() {
        let missingCounter = 0;
        $('.data_text').each(function() {
            let wrapper = this,
                cond1 = $('[name=data_primary_file]', wrapper).val() === '',
                cond2 = $('[name=data_primary_file]', wrapper).hasClass('error');
            if (cond1) {
                $('[name=data_primary_file]', wrapper).val('file path is missing');
                $('[name=data_primary_file]', wrapper).addClass('error');
                missingCounter++;
            } else if (cond2) {
                missingCounter++;
            }
            });
        scroll2error('data');

        return missingCounter;
    }

    function loadTooltips() {
        fetch("/static/json/tooltips.json")
        .then(response => response.json())
        .then((json) => {
            json.terms.forEach((val) => {
          $(val.label).attr('title', val.text);
      })
        });
         $("body").tooltip({ selector: '[data-toggle=tooltip]', placement:'left' });
        // $.ajax({
        //     type:'GET',
        //     url: '/static/json/tooltips.json',
        //     dataType: 'json',
        //     async:false
        // }).done(function(data) {
        //     $.each(data.terms, function(index, val) {
        //         $(val.label).attr('title', val.text);
        //     });
        //     $('[data-toggle="tooltip"]').tooltip();
        // }).fail(function() {
        //     // console.log('failed');
        // });
    }

    function scroll2error(section) {
        let scrollPosition;
        if ($('.error').length > 0) {
            scrollPosition = $($($('.error')[0]).parent()).offset().top;
            $(`#${section}_grid`).scrollTop(scrollPosition - $(`#${section}_grid`)
                .offset().top);
        } else if ($('.missing').length > 0) {
            scrollPosition = $($($('.missing')[0]).parent()).offset().top;
            $(`#${section}_grid`).scrollTop(scrollPosition - $(`#${section}_grid`)
                .offset().top);
        } else if ($('.error').length > 0 && $('.missing').length > 0) {
            let errorOffset = $($($('.error')[0]).parent()).offset().top,
                missingOffset = $($($('.missing')[0]).parent()).offset().top;
            scrollPosition = Math.min(errorOffset, missingOffset);
            $(`#${section}_grid`).scrollTop(scrollPosition - $(`#${section}_grid`)
                .offset().top);
        }
    }

    function addId(id, data, section) {
        $(id).each(function(index) {
            data[index].fields[`${section}_id`] = this[`${section}_id`];
        });
        return data;
    }

    function createMultipleSampleCells(numReplicates = 1, index, numGroups) {
        let i;
        if (numReplicates === '' || numReplicates === 0) {
            numGroups = integerToArray(numGroups);
            for (i = 0; i < numGroups.length; i++) {
                if (index === 0) {
                    createSampleCell(
                        document.getElementById(`biosample_grid_subgrid_${index}`),
                        i,
                        parseInt(numGroups[i]),
                        'control '
                    );
                    $('#biosample_controlled_by_select_parent')
                        .append(`<option> Control ${ (i + 1)}</option>`);
                    $(`#biosample_cell_${i} [name='biosample_controlled_by']`)
                        .remove();
                } else {
                    createSampleCell(
                        document.getElementById(`biosample_grid_subgrid_${index}`),
                        i,
                        parseInt(numGroups[i]),
                        'biosample '
                    );
                }
            }
        } else {
            for (i = 0; i < numGroups; i++) {
                if (index === 0) {
                    createSampleCell(
                        document.getElementById(`biosample_grid_subgrid_${index}`),
                        i,
                        parseInt(numReplicates), 'control '
                    );
                    $('#biosample_controlled_by_select_parent')
                        .append(`<option> Control ${(i + 1)}</option>`);
                    $(`#biosample_cell_${i} [name='biosample_controlled_by']`)
                        .remove();
                } else {
                    createSampleCell(document.getElementById(
                        `biosample_grid_subgrid_${index}`),
                        i,
                        parseInt(numReplicates),
                        'biosample '
                    );
                }

            }
        }
    }


});
