$(document).ready(function() {
    /*jshint multistr: true */
    "use strict";

    var csrftoken = Cookies.get('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function humanFileSize(size) {
        var i = Math.floor(Math.log(size) / Math.log(1024));
        return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    }

    $('#btn-apply').hide();
    $('.editable').hide();
    $('#btn-really_delete').hide();
    $('[data-toggle="tooltip"]').tooltip();

   //  $('#btn-export_csv').on('click', function() {
   //      var id = $('[name="series_id"]').text();
   //      $(this).button('loading');
   //      $.ajax({
   //          type: "POST",
   //          url: "/exportcsv/",
   //          data: {
   //              id: id
   //          },
   //          success: function(data) {
   //             console.log(data);
   //              window.location = data.url;
   //          }
   //      });
   //      return false;
   // });


    $("#btn-edit").on('click', function() {
        $('#btn-edit').hide();
        $('#btn-apply').show();
        $('.description span').each(function() {
            if ($(this).attr('class') === 'not-editable') {
                return true;
            }
            $(this).hide().siblings(".editable").val($(this).text().trim()).show();
        });
    });
    $("#btn-apply").on('click', function() {
        var item = {},
            data = new FormData(),
            json_data = [];
        $('#btn-apply').hide();
        $('#btn-edit').show();

        item.fields = {};

        $('.not-editable').each(function() {
            item.fields[$(this).attr('name')] = $(this).text().trim();
        });

        $('.description .editable').each(function() {
            if ($(this).attr('class') === 'not-editable') {
                return true;
            }
            item.fields[$(this).attr('name')] = $(this).val().trim();
            $(this).hide().siblings("span").val($(this).text()).show();
        });
        json_data.push(item);
        json_data = JSON.stringify(json_data);
        data.append('json_data', json_data);
        $.ajax({
            url: '/addSeries/',
            processData: false,
            contentType: false,
            type: 'POST',
            data: data,
            success: function() {
                // linking_id.series = response;
                location.reload();
            }
        });
    });
    $('#btn-delete').on('click', function() {
        $('#btn-delete').hide();
        $('#btn-really_delete').show();
    });

    $('section').on('click', '#btn-really_delete', function() {
        var id = $('[name="series_id"]').text();
        // $(this).button('loading');
        $(this).html($(this).data('loading-text'));
        $.ajax({
            type: "POST",
            url: "/delete/",
            data: {
                id: id
            },
            success: function() {
              var prevPage = window.location.href;
              window.history.go(-1); //go back to previous page
              //if previous page doesn't exist go to main page
              setTimeout(function(){ if (window.location.href == prevPage) window.location.href = '/'; }, 3000);
            }
        });
        return false;
    });


    $('.file_size').each(function() {
        var file_size = parseInt($(this).text());
        $(this).text(humanFileSize(file_size));
    });

    $('.biosamplereplicate_length').each(function() {
        var count = $(this).text().split('appliedassay_children').length - 1;
        if (count === 1) {
            $(this).text(count + ' biological replicate');
        } else {
            $(this).text(count + ' biological replicates');
        }
    });

    $('.technicalreplicate_length').each(function() {
        var count = $(this).text().split('sequencing_children').length - 1;
        if (count === 1) {
            $(this).text(count + ' technical replicate');
        } else {
            $(this).text(count + ' technical replicates');
        }
    });

});
