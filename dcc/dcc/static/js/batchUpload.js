$(document).ready(function() {
    /*jshint multistr: true */
    "use strict";

    if($('#id_csvFile').length>0){
        $('.btn--submit').prop('disabled',true);
    }
    $('#id_csvFile').change(function(){
    if($('#id_csvFile').val()===""){
        $('.btn--submit').prop('disabled',true);
    }else{
        $('.btn--submit').prop('disabled',false);
    }
});
    $('.btn--submit').on('click', function() {
        $(this).html($(this).data('loading-text'));
    });

});
