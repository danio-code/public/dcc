import {selection, select, event as currentEvent, selectAll} from 'd3-selection';
import {scaleBand, scaleLinear, scaleOrdinal} from 'd3-scale';
import {keys} from 'd3-collection';
import {max} from 'd3-array';
import {axisBottom, axisLeft} from 'd3-axis';
import {stack} from 'd3-shape';
import 'd3-transition';

$(document).ready(function() {
  /* jshint undef:true */
  /*jshint devel:false */
  /*global d3, plot_data */
  'use strict';
  //     unit = "sequencing runs"
  // var data_input = ['lab1'];
  //
  let color = scaleOrdinal()
    .range(['#e74c3c', '#e67e22', '#40c057', '#27ae60', '#1098ad', '#0b7285']);

  graph(JSON.parse(plot_data.chip), color);
  function graph(data_input, color) {
    var margin = {
        top: 20,
        right: 10,
        bottom: 120,
        left: 25
      },
      height = 425 - margin.top - margin.bottom,
      width = height * 3.5 - margin.left - margin.right;
    var assay_types = data_input.assay_types;

    function arrayAwareInvert(obj) {
      var res = {};
      for (var p in obj) {
        var arr = obj[p],
          l = arr.length;
        for (var i = 0; i < l; i++) {
          res[arr[i]] = p;
        }
      }
      return res;
    }
    var assay_types_inv = arrayAwareInvert(assay_types);

    var assays = [],
      assay_groups = [];

    for (var i in assay_types) {
      assay_types[i].map(function(item) {
        assays.push(item);
      });
    }
    for (var i in assay_types_inv) {
      assay_groups.push(assay_types_inv[i]);
    }
    var assay_groups_uniq = Object.keys(assay_types);

    var x = scaleBand()
      .rangeRound([0, width])
      .padding(0.1);

    x.domain([
      '1-cell',
      '2-cell',
      '4-cell',
      '8-cell',
      '16-cell',
      '32-cell',
      '64-cell',
      '128-cell',
      '256-cell',
      '512-cell',
      '1k-cell',
      'High',
      'Oblong',
      'Sphere',
      'Dome',
      '30%-epiboly',
      '50%-epiboly',
      'Germ-ring',
      'Shield',
      '75%-epiboly',
      '90%-epiboly',
      'Bud',
      '1-4 somites',
      '5-9 somites',
      '10-13 somites',
      '14-19 somites',
      '20-25 somites',
      '26+ somites',
      'Prim-5',
      'Prim-15',
      'Prim-25',
      'High-pec',
      'Long-pec',
      'Pec-fin',
      'Protruding-mouth',
      'Day 4',
      'Day 5',
      'Day 6',
      'Days 7-13',
      'Days 14-20',
      'Days 21-29',
      'Days 30-44',
      'Days 45-89',
      '90 Days-2 Years'
    ]);

    color.domain(assays);

    var n = assay_groups_uniq.length, //number of groups
      lastRow = n - 1,
      spacing = {
        bottom: 15,
        right: 5
      },
      rowHeight = Math.floor((height - (n - 1) * spacing.bottom) / n);

    var y_split = scaleLinear().range([rowHeight, 0]);

    var y_stacked = scaleLinear().range([height, 0]);

    var z = scaleBand()
      .domain(assays)
      .rangeRound([0, x.bandwidth()]);

    var svg = select('#targets_chart')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    var stacked = stack().keys(assays),
      layers = stacked(data_input.data);

    layers.forEach(function(d, i) {
      d.forEach(function(dd) {
        dd.assay = assays[i];
        dd.assay_group = assay_groups[i];
      });
    });

    var ySplitMax = max(layers, function(layer) {
        return max(layer, function(d) {
          return d.data[d.assay];
        });
      }),
      yStackMax = max(layers, function(layer) {
        return max(layer, function(d) {
          return d[1];
        });
      });
    y_stacked.domain([0, yStackMax]).nice();
    y_split.domain([0, ySplitMax]).nice(5);

    // gridlines in y axis function
    function make_y_gridlines(scale, ticks) {
      return axisLeft(scale).ticks(ticks);
    }

    // add the Y gridlines
    svg
      .append('g')
      .attr('class', 'grid')
      .call(
        make_y_gridlines(y_stacked)
          .tickSize(-width)
          .tickFormat('')
      );

    var layer = svg
      .selectAll('.layer')
      .data(layers, function(d) {
        return assay_types_inv[d.key];
      })
      .enter()
      .append('g')
      .attr('class', 'layer')
      .attr('class', function(d) {
        return 'layer ' + assay_types_inv[d.key];
      });
    // .style("fill", function(d) {
    //     return color(d.key);
    // });

    assay_groups_uniq.forEach(function(d) {
      svg
        .select('.' + d)
        .append('g')
        .transition()
        .duration(300)
        .attr('class', 'grid grid--split')
        .call(
          make_y_gridlines(y_split, 4)
            .tickSize(-width)
            .tickFormat('')
        )
        .attr('opacity', 0);

      svg
        .select('.' + d)
        .append('g')
        .attr('class', 'axis--y')
        .attr('opacity', 0);
      svg
        .select('.' + d)
        .append('g')
        .attr('class', 'axis axes--x')
        .attr('transform', function() {
          return translate(0, rowHeight);
        })
        .attr('opacity', 0);
    });

    layer
      .append('g')
      .attr('class', 'axis--y')
      .attr('opacity', 0);
    layer
      .append('g')
      .attr('class', 'axis axes--x')
      .attr('transform', function() {
        return translate(0, rowHeight);
      })
      .attr('opacity', 0);

    var div = select('#targets_chart')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    var bars = layer
      .selectAll('rect')
      .data(
        function(d) {
          return d;
        },
        function(d) {
          return d.key;
        }
      )
      .enter()
      .append('rect')
      .style('fill', function(d) {
        return color(d.assay);
      })
      .attr('x', function(d) {
        x(d.data.stage);
        return x(d.data.stage);
      })
      .attr('y', function() {
        return height;
      })
      .attr('width', x.bandwidth())
      .attr('height', function() {
        return 0;
      })
      .attr('class', function(d) {
        return 'bar ' + d.assay;
      })
      .on('mouseover', function(d) {
        div
          .transition()
          .duration(200)
          .style('opacity', 0.9);
        div
          .html(
            '<span>' +
              d.assay +
              '</span><span>' +
              Math.round(d.data[d.assay]) +
              '</span>'
          )
          .style('left', event.pageX + 'px')
          .style('top', event.pageY - 28 + 'px');
      })
      .on('mouseout', function() {
        div
          .transition()
          .duration(500)
          .style('opacity', 0);
      });

    bars
      .transition()
      .delay(function(d, i) {
        return i * 10;
      })
      .attr('y', function(d) {
        return y_stacked(d[1]);
      })
      .attr('height', function(d) {
        return y_stacked(d[0]) - y_stacked(d[1]);
      });

    var legend_width = 250;
    var legend_svg = select('#targets_legend')
      .append('svg')
      .attr('width', legend_width)
      .attr('height', 20 * assays.length);

    var legend = legend_svg
      .selectAll('.legend')
      .data(assays)
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function(d, i) {
        return 'translate(-225,' + i * 20 + ')';
      });

    legend
      .append('rect')
      .attr('x', legend_width - 18)
      .attr('width', 18)
      .attr('height', 18)
      .style('fill', function(d) {
        return color(d);
      })
      .on('mouseover', function(d) {
        svg
          .selectAll('.bar:not(.' + d.replace(' ', '_') + ')')
          .style('opacity', '0.25');
      })
      .on('mouseout', function(d) {
        svg
          .selectAll('.bar:not(.' + d.replace(' ', '_') + ')')
          .style('opacity', '1');
      });

    legend
      .append('text')
      .attr('x', legend_width + 5)
      .attr('y', 9)
      .attr('dy', '.35em')
      .style('text-anchor', 'start')
      .text(function(d) {
        return d;
      })
      .on('mouseover', function(d) {
        svg
          .selectAll('.bar:not(.' + d.replace(' ', '_') + ')')
          .transition()
          .duration(100)
          .style('opacity', '0.25');
      })
      .on('mouseout', function(d) {
        svg
          .selectAll('.bar:not(.' + d.replace(' ', '_') + ')')
          .transition()
          .duration(500)
          .style('opacity', '1');
      });

    // var legend = svg.selectAll(".legend").data(assays).enter().append("g").attr("class", "legend").attr("transform", function(d, i) {
    //     return "translate(30," + i * 20 + ")";
    // });
    //
    // legend.append("rect").attr("x", width - 18).attr("width", 18).attr("height", 18).style("fill", function(d) {
    //     return color(d);
    // }).on("mouseover", function(d) {
    //     svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")").style('opacity', '0.1');
    // }).on("mouseout", function(d) {
    //     svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")").style('opacity', '1');
    // });
    //
    // legend.append("text").attr("x", width + 5).attr("y", 9).attr("dy", ".35em").style("text-anchor", "start").text(function(d) {
    //     return d;
    // }).on("mouseover", function(d) {
    //     svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")").transition().duration(100).style('opacity', '0.25');
    // }).on("mouseout", function(d) {
    //     svg.selectAll(".bar:not(." + d.replace(" ", "_") + ")").transition().duration(500).style('opacity', '1');
    // });

    svg
      .append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + height + ')')
      .call(axisBottom(x).tickSizeOuter(0))
      .selectAll('text')
      .attr('dx', '-0.35em')
      .attr('dy', '0.6em')
      .attr('transform', 'rotate(-45)')
      .style('text-anchor', 'end');
    svg
      .append('g')
      .attr('class', 'axis axis--y axis--stacked')
      .call(axisLeft(y_stacked).ticks(10))
      .append('text')
      .attr('x', 6)
      .attr('y', y_stacked(y_stacked.ticks(10).pop()))
      .attr('dy', '0.35em')
      .attr('text-anchor', 'start')
      .attr('fill', '#000')
      .text('sequencing runs');

    function translate(x, y) {
      return 'translate(' + x + ', ' + y + ')';
    }

    function rowPosition(d) {
      return assay_groups_uniq.indexOf(assay_types_inv[d.key]);
    }

    // setTimeout(function() {
    //     d3.select("input[value=\"split\"]").property("checked", true).each(change);
    // }, 1000);
  }
});
