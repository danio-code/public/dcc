$(document).ready(function() {
    /*jshint multistr: true */
    "use strict";
    $('.pagination').remove();
    $('.header').click(function(e) {
        if(e.target.tagName.toLowerCase() !== "a"){
        $(this).parent().children('.inner').toggle("fast");
        $(this).toggleClass('collapsed_header');
    }
    });
    $('.button_row .btn').click(function(){
        var title = $(this).val();
        title = title + '_item';
        $('.' + title + ' .header').each(function(){
            $(this).parent().children('.inner').toggle("fast");
            $(this).toggleClass('collapsed_header');
        });
    });
// $('[data-toggle="tooltip"]').tooltip();
$(document).on('click', '.btn--undo', function() {

    let button = this;
    let id = $(button).attr('data-id');
    if($(button).hasClass('really-delete')){
        $(this).html($(this).data('loading-text'));
    $.ajax({
        type: "POST",
        url: "/deleteFiles/",
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({
            "id": [
                {
                    'dataid': id
                }
            ]
        }),
        success: function() {
            $(button).append(`<span>deleted</span>`);

            $(button).closest('.data_item')[0].remove();
            // $(button).remove();
            // var prevPage = window.location.href;
            // window.history.go(-1); //go back to previous page
            // //if previous page doesn't exist go to main page
            // setTimeout(function() {
            //     if (window.location.href == prevPage)
            //         window.location.href = '/importDNANexus';
            //     }
            // , 3000);
        }
    });
    return false;
} else{
    $(button).removeClass('btn-light')
    $(button).addClass('really-delete btn-danger')
    $(button).text('really delete?')
    $(this).button('');
    $(this).html($(this).data('loading-text'));

}
});
});
