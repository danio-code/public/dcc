$(document).ready(function() {
    /*jshint multistr: true */
    'use strict';

    $('.clear_table').hide();
    $('.nav li').click(function() {
        $('.nav li').removeClass('active');
    });

    $('td.publication').each(function() {
        if ($.isNumeric($(this).text())) {
            let txt = `<a href="https://www.ncbi.nlm.nih.gov/pubmed/${$(this).text()}">${$(this).text()}</a>`;
            $(this).html(txt);
        }
    });

    $('table').DataTable({
        'dom': 't<"dataTables_navbar"lpi>r',
        'processing': true,
        'lengthMenu': [[10, 15, 25, 50, -1],[10, 15, 25, 50, 'All']],
        'paging': false,
        // 'pageLength':'15',
        'order': [
            [0, 'desc']
        ],
        'bProcessing': true
        // 'scrollX': true
    });
    let delayTimer;

    function fillBackground(table) {
        $(`#${table}_div .facet`).each(function() {
            let totalCount = $(`#${table}`).DataTable().page.info().recordsDisplay;
            $('.count', this).each(function() {
                let percentage = Math.floor(parseInt($(this).text().slice(1, -1)) / totalCount * 100);
                let cpercentage = 100 - percentage;
                $(this).parents('.facet_item').css('background', `linear-gradient(left, white ${cpercentage}%, #ECF0F1 ${cpercentage}%)`);
                $(this).parents('.facet_item').css('background', `-webkit-linear-gradient(left, white ${cpercentage}%, #ECF0F1 ${cpercentage}%)`);
                $(this).parents('.facet_item').css('background', `-moz-linear-gradient(left, white ${cpercentage}%, #ECF0F1 ${cpercentage}%)`);
                $(this).parents('.facet_item').css('background', `-o-linear-gradient(left, white ${cpercentage}%, #ECF0F1 ${cpercentage}%)`);

                // $(this).parents('.facet_item').children('.bar').width(percentage);

            });
        });
    }

    function updateCount(table) {
        $(`#${table}_div .facet_text`).each(function() {
            let term = $('.term', this).text().trim(),
                tableObj = $(`#${table}`).DataTable();
            $('.count', this).each(function() {
                let column = $(this).parents('.facet').children('label').text().replace(' ', '_'),
                    count;
                if (column.endsWith('_lab')){
                     count = tableObj.$(`td.${column}:contains("${term}")`, {'filter': 'applied'})
                                     .length;
                }else{
                     count = tableObj.$(`td.${column}`, {'filter': 'applied'})
                                    .filter(function(){
                                        return $(this).text().trim() === term;
                                    })
                                    .length;
                }
                $(this).text(`(${count})`);
            });
        });
        fillBackground(table);
        sortByCount(table);
    }
    function sortByCount(table) {
        $(`#${table}_div .facet`).each(function() {
            let items = $(this).children('.facet_item').sort(function(a, b) {
                let A = parseInt($('span.count', a).text().slice(1, -1));
                let B = parseInt($('span.count', b).text().slice(1, -1));
                return A < B ? 1
                    : A > B ? -1
                        : 0;
            });
            $(this).append(items);
        });
    }

    function searchTable(table) {
        // var sectionName = table.substring(0, table.indexOf('_'));
        // $(`.${sectionName}_filter_bar`).addClass('loading');
        let query = '';
        let tableObj = $(`#${table}`).DataTable();
        $(`#${table}_div .clear_table`).show();
        $(`#${table}_div .active_item`).each(function() {
            let column = $(this).parents('.facet').children('label').text().replace(' ', '_');
            if(column.endsWith('_lab')){
                query = `${$(this).text().trim().substring(0, $(this).text().trim().indexOf('(')).trim()}`;
                tableObj.column(`.${column}`).search(query);
            } else{
            // query = `${query} "${$(this).text().trim().substring(0, $(this).text().trim().indexOf('(')).trim()}"`;
            query = `${$(this).text().trim().substring(0, $(this).text().trim().indexOf('(')).trim()}`;
            tableObj.column(`.${column}`).search(`^${query}$`, true, false, true);}
        });
        tableObj.draw();
        let searchBoxQuery = $(`#${table}_div .search_box`).val();
        if ($(`#${table}_div .active_item`).length + searchBoxQuery.length === 0) {
            clearTable(table);
        }
        if (searchBoxQuery.length>0){
            tableObj.search(searchBoxQuery).draw();
        }

        // updateCount(table);
    }

    function clearTable(table){
        let tableObj = $(`#${table}`).DataTable();
        tableObj.search('').columns().search('').draw();
        $(`#${table}_div .clear_table`).hide();
        $(`#${table}_div .search_box`).val('');
    }

    function collapseFacet(table) {
        $(`\#${table}_div .facet`).each(function() {
            if ($(this).children('.facet_item').length > 5) {
                $('.facet_item:gt(4)', this).hide();
                $('.facet_item:nth-child(6)', this).after(
                /* @html */
                `<div class='more'>
                        <a href='#'>more</a>
                    </div>`);
            }
        });
        $(`\#${table}_div .facet`).on('click', 'div.more a', function() {

            $(this).parents('.facet').children('.facet_item').show();
            $(this).parents('.facet').children('.more').replaceWith(
            /* @html */`<div class='less'>
                    <a href='#'>less</a>
                </div>`);
        });
        $(`\#${table}_div .facet`).on('click', 'div.less a', function() {
            $(this).parents('.facet').children('.facet_item:gt(4)').hide();
            $(this).parents('.facet').children('.less').replaceWith(
            /* @html */`<div class='more'>
                    <a href='#'>more</a>
                </div>`);
        });
    }

    $('.replace_str').each(function() {
        $(this).text($(this).text().split('__')[0]);
        $(this).text($(this).text().replace('_', ' '));
    });

    $('.facet_item').on('click', function() {
        document.body.style.cursor = 'wait';
        $(this).toggleClass('active_item');
        let table = $(this).parents('[id$=_table_div]').attr('id');
        table = table.substring(0, table.lastIndexOf('_'));
        searchTable(table);
        document.body.style.cursor = 'default';
    });

    $('.search_box').on('keyup', function() {
        let searchBox = this;
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function() {
            let table = $(searchBox).parents('[id$=_table_div]').attr('id');
            table = table.substring(0, table.lastIndexOf('_'));
            searchTable(table);
        }, 500);
    });

    // making sure that tables in hidden tabs have full width
    $('a[data-toggle="tab"]').on('shown.bs.tab', function() {
        let table = $.fn.dataTable.fnTables(true);
        if (table.length > 0) {
            $(table).dataTable().fnAdjustColumnSizing();
        }
    });

    $('.facet_item').each(function() {
        if (!$('.term', this).text()) {
            $(this).hide();
        }
    });

    $('td.publication').each(function() {
        if ($.isNumeric($(this).text())) {
            let txt =
            /* @html */`<a href="https://www.ncbi.nlm.nih.gov/pubmed/${$(this).text()}">
                          ${$(this).text()}
                        </a>`;
            $(this).html(txt);
        }
    });

    if ($('.is_member')) {
        $('.btn--download').prop('disabled', true);
    }

    $(document).on('click','.clear_table',function(){
        let table = $(this).parents('[id$=_table_div]').attr('id');
        table = table.substring(0, table.lastIndexOf('_'));
        clearTable(table);
    });
    updateCount('series_table');
    collapseFacet('series_table');
    updateCount('biosample_table');
    collapseFacet('biosample_table');
    updateCount('assay_table');
    collapseFacet('assay_table');
});
