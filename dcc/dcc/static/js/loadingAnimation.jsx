import React from 'react';

class LoadingAnimation extends React.Component {
  render() {
    return (
      <div className="loader">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
        <div className="loading-text">
          <h4>
           This may take a few seconds.
          </h4>
          <span>Please reload this page after 15 seconds of inactivity. <br/>
            If the problem persists and you are using Firefox, please try a
            different browser.
          </span>
          </div>
        </div>
        );
  }
}

export default LoadingAnimation;
