$(document)
  .ready(function() {
    /*jshint multistr: true */
    "use strict";
    const csrftoken = Cookies.get('csrftoken');

    function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
    }
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader('X-CSRFToken', csrftoken);
        }
      }
    });
    $('.counts')
      .hide();
    $('#really_delete_button')
      .hide()
    $('#really_delete_files_button')
      .hide()

    function getLogFile() {
      let file = this.files[0]; //get the files
      let reader = new FileReader(); //initiate reader
      reader.onloadend = callbackFn; //set event handler
      reader.readAsText(file); //initiate reading of files
      if (this.id) { //only run if this is the input
        let id = this.id;
        this.outerHTML = this.outerHTML; //this resets the input
        $(id)
          .on('change', getLogFile); //reattach event handler
      }

      function callbackFn(e) {
        // if (e.target.result.substring(e.target.result.length-3,e.target.result.length-1)==='],]'){
        //     let pos = e.target.result.lastIndexOf(',');
        //     var json_string = e.target.result.substring(0, pos) + e.target.result.substring(pos + 1);
        // }
        // else {
        var json_string = e.target.result
        // }
        let data = $.parseJSON(json_string),
          parsedData = [];
        $('.counts')
          .show()
        $('.counts')
          .append(`<h4 class="data_ids card-header">Data IDs:</h4><div class="card-body"></div>`);
        if (data[0][3] && data[0][3].hasOwnProperty('workflow_name') && data[0][3].workflow_name.substring(0, 14) === 'DANIO-CODE_RNA') {
          console.log("parsing RNA-seq log");
          parsedData = parseRNALogFile(data);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('CAGE-seq') > 0) {
          console.log("parsing CAGE-seq log");
          parsedData = parseCAGELogFile(data);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('3P-seq') > 0) {
          console.log("parsing 3P-seq log");
          parsedData = parse3PLogFile(data);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('BS-seq') > 0) {
          console.log("parsing BS-seq log");
          parsedData = parseBSLogFile(data);
        } else if (data[data.length - 1].hasOwnProperty('workflow_name') && data[data.length - 1].workflow_name.search('HiC') > 0) {
          console.log("parsing HiC log");
          parsedData = parseHiCLogFile(data);
          console.log(parsedData);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('4C') > 0) {
          console.log("parsing 4C log");
          parsedData = parse4CLogFile(data);
          console.log(parsedData);
        } else if (data[data.length - 1].hasOwnProperty('workflow_name') && data[data.length - 1].workflow_name.search('ATAC') > 0) {
          console.log("parsing ATAC-seq log");
          parsedData = parseATACLogFile(data);
        } else if (data[data.length - 1].hasOwnProperty('workflow_name') && data[data.length - 1].workflow_name.search('ChIP-seq') > 0) {
          console.log("parsing ChIP-seq log");
          parsedData = parseChIPLogFile(data);
        }
        // parsedData = $.parseJSON(parsedData)
        let numFiles = parsedData.length;
        $('.counts')
          .after(`
              <div class="checking_urls d-flex flex-column">
              <h3>Checking URLs:</h3>
                <div class="progress">
                    <div class="validation-progress-bar progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="${numFiles}" style="width:0%">
                        0
                    </div>
                </div>
              </div>`)
        $('.counts .card-body')
          .append('<hr style="width:90%;">')
        $('.counts .card-body')
          .append(`<span>Number of Files: ${numFiles}</span>`);
        let step = 0;
        for (var i = 0; i < parsedData.length; i++) {
          let url = parsedData[i].primary_file;
          verifyUrl(url, `#url_${i}`, parsedData.length);
        }
        // $('.main').append(JSON.stringify(txt));
        if ($('.error')
          .length === 0) {
          $('#upload_button')
            .prop('disabled', false);
          $('.validation-progress-bar')
            .addClass('progress-bar-success')
        }
        localStorage.setItem("parsedData", JSON.stringify(parsedData));
      }
    }

    function getCustomLogFile() {
      let file = this.files[0]; //get the files
      let reader = new FileReader(); //initiate reader
      reader.onloadend = callbackFn; //set event handler
      reader.readAsText(file); //initiate reading of files
      if (this.id) { //only run if this is the input
        let id = this.id;
        this.outerHTML = this.outerHTML; //this resets the input
        $(id)
          .on('change', getCustomLogFile); //reattach event handler
      }

      function callbackFn(e) {
        let json_string = e.target.result,
          data = $.parseJSON(json_string),
          parsedData = [];

        $('.counts')
          .append(`<span class="data_ids">Data IDs:</span>`);
        if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.substring(0, 4) === 'CAGE') {
          parsedData = parseCustomCageLogFile(data);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('ATAC') > 0) {
          console.log("parsing custom ATAC-seq log", data[0].workflow_name);
          parsedData = parseCustomATACLogFile(data);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('ChIP') > 0) {
          console.log("parsing custom ChIP-seq log", data[0].workflow_name);
          parsedData = parseCustomChIPLogFile(data);
        } else if (data[0].hasOwnProperty('workflow_name') && data[0].workflow_name.search('MNase') > 0) {
          console.log("parsing custom MNase-seq log", data[0].workflow_name);
          parsedData = parsecustomMNaseLogFile(data);
        } else {
          parsedData = parseCustomRNALogFile(data);
        }
        // parsedData = $.parseJSON(parsedData)
        let numFiles = parsedData.length;
        $('.counts')
          .after(`<div class="progress">
                    <div class="validation-progress-bar progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="${numFiles}" style="width:0%">
                        0
                    </div>
                </div>`)
        $('.counts')
          .append('<hr>')
        $('.counts')
          .append(`<span>Number of Files: ${numFiles}</span>`);
        let step = 0;
        for (var i = 0; i < parsedData.length; i++) {
          let url = parsedData[i].primary_file;
          verifyUrl(url, `#url_${i}`, parsedData.length);
        }
        // $('.main').append(JSON.stringify(txt));
        if ($('.error')
          .length === 0) {
          $('#upload_button')
            .prop('disabled', false);
          $('.validation-progress-bar')
            .addClass('progress-bar-success')
        }
        localStorage.setItem("parsedData", JSON.stringify(parsedData));
      }
    }

    function updateLogFile() {
      console.log("updating");
      let file = this.files[0]; //get the files
      let reader = new FileReader(); //initiate reader
      reader.onloadend = callbackFn; //set event handler
      reader.readAsText(file); //initiate reading of files
      if (this.id) { //only run if this is the input
        let id = this.id;
        this.outerHTML = this.outerHTML; //this resets the input
        $(id)
          .on('change', getLogFile); //reattach event handler
      }

      function callbackFn(e) {
        if (e.target.result.substring(e.target.result.length - 3, e.target.result.length - 1) === '],]') {
          let pos = e.target.result.lastIndexOf(',');
          var json_string = e.target.result.substring(0, pos) + e.target.result.substring(pos + 1);
        } else {
          var json_string = e.target.result
        }
        let data = $.parseJSON(json_string),
          parsedData = [];
        $('.counts')
          .show();
        $('.counts')
          .append(`<h4 class="data_ids card-header">Data IDs:</h4><div class="card-body"></div>`);
        if (data[0][3] && data[0][3].workflow_name && data[0][3].workflow_name.substring(0, 14) === 'DANIO-CODE_RNA') {
          console.log("parsing RNA-seq log for updates");
          parsedData = parseRNALogFile(data);
        } else if (data[0][data[0].length - 1] && data[0][data[0].length - 1].workflow_name && data[0][data[0].length - 1].workflow_name.substring(0, 4) === 'CAGE') {
          console.log("parsing CAGE-seq log for updates");
          parsedData = parseCAGELogFile(data);
        } else if (data[0][4] && data[0][4].workflow_name && data[0][4].workflow_name.search('BS-seq') > 0) {
          console.log("parsing BS-seq log for updates");
          parsedData = parseBSLogFile(data);
        } else if (data[data.length - 1].workflow_name && data[data.length - 1].workflow_name.search('ATAC') > 0) {
          console.log("parsing ATAC-seq log for updates");
          parsedData = parseATACLogFile(data);
        } else if (data[0][5].workflow_name && data[0][5].workflow_name.search('ChIP-seq') > 0) {
          console.log("parsing ChIP-seq log for updates");
          parsedData = parseChIPLogFile(data);
        }
        // parsedData = $.parseJSON(parsedData)
        let numFiles = parsedData.length;
        // $('.counts').after(`<div class="progress">
        //         <div class="validation-progress-bar progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="${numFiles}" style="width:0%">
        //             0
        //         </div>
        //     </div>`)
        $('#update_button')
          .prop('disabled', false);
        $('.counts')
          .append('<hr>')
        $('.counts')
          .append(`<span>Number of Files: ${numFiles}</span>`);
        let step = 0;
        localStorage.setItem("parsedData", JSON.stringify(parsedData));
      }
    }

    function updateFile() {
      console.log("updating File");
      let file = this.files[0]; //get the files
      let reader = new FileReader(); //initiate reader
      reader.onloadend = callbackFn; //set event handler
      reader.readAsText(file); //initiate reading of files
      if (this.id) { //only run if this is the input
        let id = this.id;
        // this.outerHTML = this.outerHTML; //this resets the input
        $(id)
          .on('change', getLogFile); //reattach event handler
      }

      function callbackFn(e) {
        var json_string = e.target.result
        let data = $.parseJSON(json_string),
          parsedData = [];
        $('.counts')
          .show();
        $('.counts')
          .append(`<h4 class="data_ids card-header">Data IDs:</h4><div class="card-body"></div>`);
        for (var i = 0; i < data.length; i++) {
          parsedData.push({
            data_id: data[i].data_id,
            primary_file: data[i].primary_file,
            command: 'fileUpdate'
          })
        }
        let numFiles = parsedData.length;
        $('#update_button')
          .prop('disabled', false);
        $('.counts')
          .append('<hr>')
        $('.counts')
          .append(`<span>Number of Files: ${numFiles}</span>`);

        localStorage.setItem("parsedData", JSON.stringify(parsedData));
      }
    }

    function parseRNALogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed RNA-seq ids:</h4>`)

      for (var i = 0; i < data.length; i++) {
        if (!data[i][1]) {
          continue //check for null keys and skip if that is true
        }
        let dataId = data[i][0].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[i][3].workflow_name + "|" + data[i][0].command
        let genomeVersion = '';
        if (data[i][3].mapped_genome) {
          genomeVersion = data[i][3].mapped_genome
        }
        file = data[i][0].genome_bam.$dnanexus_link.split('|');
        let fileType = 'BAM';
        createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        file = data[i][0].log_file.$dnanexus_link.split('|');
        fileType = 'LOG';
        createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        file = data[i][0].splice_junctions.$dnanexus_link.split('|');
        fileType = 'TSV';
        createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        file = data[i][0].other_outputs[0].$dnanexus_link.split('|');
        fileType = 'TSV';
        createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        for (var j = 1; j < data[i][0].other_outputs.length; j++) {
          file = data[i][0].other_outputs[j].$dnanexus_link.split('|');
          fileType = file[0].split('.')
            .pop() === 'bg' ? 'BEDGRAPH' : 'BIGWIG';
          createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        }
        file = data[i][1].out[0].$dnanexus_link.split('|');
        fileType = 'PDF';
        createDBObject(data[i][1], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        file = data[i][1].out[1].$dnanexus_link.split('|');
        fileType = 'LOG';
        createDBObject(data[i][1], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        file = data[i][2].qualimap_rna_seq[0].$dnanexus_link.split('|');
        fileType = 'PDF';
        createDBObject(data[i][2], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        file = data[i][2].qualimap_rna_seq[1].$dnanexus_link.split('|');
        fileType = 'LOG';
        createDBObject(data[i][2], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
      }
      return objectArray;
    }

    function parseChIPLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed ChIP-seq ids:</h4>`)
      for (var i = 0; i < data.length - 1; i++) {
        let dataId = data[i].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[data.length - 1].workflow_name
        let genomeVersion = '';
        if (data[data.length - 1].mapped_genome) {
          genomeVersion = data[data.length - 1].mapped_genome
        }
        let types = {
          "bam": 'BAM',
          "tagAlign": 'BED',
          "narrowPeak": 'BIGNARROWPEAK',
          "bigBed": 'BIGNARROWPEAK',
          "bigWig": 'BIGWIG'
        }
        for (var k in types) {
          if (data[i].hasOwnProperty(k)) {
            let file = data[i][k].split('|');
            createDBObject(data[i][k], file, types[k], objectArray, workflow, dataId, [dataId], genomeVersion);
          }
        }
      }
      return objectArray;
    }

    function parseBSLogFile(data) {
      $('.counts .card-body')
        .append(`<h4>parsed BS-seq ids:</h4>`)
      let objectArray = []
      for (var i = 0; i < data.length; i++) {
        let file = '',
          databaseObject = {},
          workflow = data[i].workflow_name,
          genomeVersion = data[i].mapped_genome;
        if (data[i].data_id) {
          let dataId = data[i].data_id;
          $('.counts .card-body')
            .append(`<span><br>${dataId}</span>
                `);
          let file = ''
          let fileType = '';

          for (var j = 0; j < data[i].tracks.length; j++) {
            file = data[i].tracks[j].$url_path.split('|');
            fileType = file[0].split('.')
              .pop() === 'bigwig' ? 'BIGWIG' : 'BEDGRAPH';
            createDBObject(data[i], file, fileType, objectArray, workflow, dataId[0], dataId, genomeVersion);
          }
        } else {
          console.log('no data id found', data[i]);
        }
      }

      return objectArray;
    }

    function parseATACLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed ATAC-seq ids:</h4>`)
      for (var i = 0; i < data.length - 1; i++) {
        let dataId = data[i].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[data.length - 1].workflow_name
        let genomeVersion = '';
        if (data[data.length - 1].mapped_genome) {
          genomeVersion = data[data.length - 1].mapped_genome
        }
        let types = {
          "bam": 'BAM',
          "tagAlign": 'BED',
          "narrowPeak": 'BIGNARROWPEAK',
          "bigBed": 'BIGNARROWPEAK',
          "bigWig": 'BIGWIG'
        }
        for (var k in types) {
          if (data[i].hasOwnProperty(k)) {
            let file = data[i][k].$zfinpath.split('|');
            createDBObject(data[i][k], file, types[k], objectArray, workflow, dataId, [dataId], genomeVersion);
          }
        }
      }
      return objectArray;
    }

    function parseHiCLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed HiC ids:</h4>`)
      const workflow = data[data.length - 1].workflow_name
      let genomeVersion = '';
      if (data[data.length - 1].mapped_genome) {
        genomeVersion = data[data.length - 1].mapped_genome
      }

      for (var i = 0; i < data.length - 1; i++) {
        let dataIds = data[i].data_id
        $('.counts .card-body')
          .append(`<span><br>${dataIds}</span>`);
        let bigwigFiles = [
          "insulation_score_1MB",
          "insulation_score_500kb",
          "insulation_score_50kb",
          "directionality_index_1MB",
          "directionality_index_500kb",
          "directionality_index_50kb"
        ];
        for (let j = 0; j < bigwigFiles.length; j++) {
          let file = data[i][bigwigFiles[j]].$dnanexus_link.split('|');
          let fileType = 'BIGWIG';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataIds[0], dataIds, genomeVersion);
        }
        let file = data[i]["matrix_multiple_resolutions"].$dnanexus_link.split('|');
        let fileType = 'BIGWIG';
        createDBObject(data[i], file, fileType, objectArray, workflow, dataIds[0], dataIds, genomeVersion);

      }
      return objectArray;
    }

    function parse4CLogFile(data) {
      $('.counts .card-body')
        .append(`<h4>parsed 4C-seq ids:</h4>`)
      let objectArray = []
      for (var i = 0; i < data.length; i++) {
        let file = '',
          databaseObject = {},
          workflow = data[i].workflow_name,
          genomeVersion = data[i].mapped_genome;
        if (data[i].data_id) {
          let dataId = data[i].data_id;
          $('.counts .card-body')
            .append(`<span><br>${dataId}</span>`);
          let file = data[i].tracks.$zfin_path.split('|');
          let fileType = 'BIGWIG';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          }
      }

      return objectArray;
    }

    function parseCustomRNALogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed RNA-seq ids:</h4>`)
      for (var i = 0; i < data.length; i++) {
        let dataId = data[i][0].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[i][1].workflow_name
        let fileType = ''
        if (data[i][0].hasOwnProperty('genome_bam')) {
          file = data[i][0].genome_bam.$dnanexus_link.split('|');
          fileType = 'BAM';
          createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId);
        } else if (data[i][0].hasOwnProperty('first_strand_bw')) {
          file = data[i][0].first_strand_bw.$dnanexus_link.split('|');
          fileType = 'BIGWIG'
          createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId);
          if (data[i][0].hasOwnProperty('second_strand_bw')) {
            file = data[i][0].second_strand_bw.$dnanexus_link.split('|');
            fileType = 'BIGWIG'
            createDBObject(data[i][0], file, fileType, objectArray, workflow, dataId);
          }
        }

      }
      console.log(objectArray);
      return objectArray;
    }

    function parseCustomCageLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed CAGE-seq ids:</h4>`)
      for (var i = 0; i < data.length; i++) {
        let dataId = data[i].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[i].workflow_name;
        let fileType = ''
        file = data[i].bigwig.$dnanexus_link.split('|');
        fileType = 'BIGWIG'
        createDBObject(data[i], file, fileType, objectArray, workflow, dataId);
        file = data[i].bigbed.$dnanexus_link.split('|');
        fileType = 'BIGBED'
        createDBObject(data[i], file, fileType, objectArray, workflow, dataId);
      }
      console.log(objectArray);
      return objectArray;
    }

    function parseCustomATACLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed ATAC-seq ids:</h4>`)
      for (var i = 0; i < data.length; i++) {
        let dataId = data[i].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[i].workflow_name;
        let fileType = ''
        file = data[i].bigNarrowPeak.$dnanexus_link.split('|');
        fileType = 'BIGNARROWPEAK'
        createDBObject(data[i], file, fileType, objectArray, workflow, dataId);
      }
      console.log(objectArray);
      return objectArray;
    }

    function parseCustomChIPLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed ChIP-seq ids:</h4>`)
      for (var i = 0; i < data.length; i++) {
        let dataId = data[i].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[i].workflow_name
        let fileType = ''
        if (data[i].hasOwnProperty('bigNarrowPeak')) {
          file = data[i].bigNarrowPeak.$dnanexus_link.split('|');
          fileType = 'BIGNARROWPEAK';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId);
        } else if (data[i].hasOwnProperty('signal')) {
          file = data[i].signal.$dnanexus_link.split('|');
          fileType = 'BIGWIG'
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId);
        }
      }
      console.log(objectArray);
      return objectArray;
    }

    function parsecustomMNaseLogFile(data) {
      let objectArray = []
      $('.counts .card-body')
        .append(`<h4>parsed MNase-seq ids:</h4>`)
      for (var i = 0; i < data.length; i++) {
        let dataId = data[i].data_id;
        $('.counts .card-body')
          .append(`<span> ${dataId}</span>`);
        let file = '';
        let databaseObject = {};
        let workflow = data[i].workflow_name
        let fileType = ''
        file = data[i].signal.$dnanexus_link.split('|');
        fileType = 'BIGWIG'
        createDBObject(data[i], file, fileType, objectArray, workflow, dataId);
      }
      console.log(objectArray);
      return objectArray;
    }

    function parseCAGELogFile(data) {
      $('.counts .card-body')
        .append(`<h4>parsed CAGE-seq ids:</h4>`)
      let objectArray = []
      for (var i = 0; i < data.length; i++) {
        let file = '',
          databaseObject = {},
          workflow = data[i].workflow_name,
          genomeVersion = data[i].mapped_genome;
        if (data[i].data_id) {
          console.log("with data_id");
          let dataId = data[i].data_id;
          $('.counts .card-body')
            .append(`<span><br>${dataId}</span>
              `);
          let file = data[i]["fastqc.txt"].$dnanexus_link.split('|');
          let fileType = 'LOG';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i]["fastqc.html"].$dnanexus_link.split('|');
          fileType = 'HTML';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i]["4zenbu.bed"].$dnanexus_link.split('|');
          fileType = 'BED';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i].bed.$dnanexus_link.split('|');
          fileType = 'BED';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i].bam.$dnanexus_link.split('|');
          fileType = 'BAM';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i].bigwig.$dnanexus_link.split('|');
          fileType = 'BIGWIG';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i].bigbed.$dnanexus_link.split('|');
          fileType = 'BIGBED';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
        } else if (data[i].data_ids) {
          let dataIds = data[i].data_ids.replace(" ", "")
            .split(';');
          $('.counts .card-body')
            .append(`<span><br>${dataIds}</span>`);
          let file = data[i].counts.$dnanexus_link.split('|');
          let fileType = 'TSV';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataIds[0], dataIds, genomeVersion);
          var bedFiles = [
            "ctssall",
            "par_pos_30_2_200",
            "ctss_pos_4P",
            "ctss_pos_4Ps",
            "ctss_neg_4P",
            "ctss_neg_4Ps",
            "par_30_2_200",
            "par_pos_30",
            "par_neg_30_2_200",
            "par_30_2_200_bed",
            "par_neg_30"
          ];
          for (var j = 0; j < bedFiles.length; j++) {
            file = data[i][bedFiles[j]].$dnanexus_link.split('|');
            fileType = 'BED';
            createDBObject(data[i], file, fileType, objectArray, workflow, dataIds[0], dataIds, genomeVersion);
          }
        } else {
          console.log('no data id found', data[i]);
        }
      }
      return objectArray;
    }

    function parse3PLogFile(data) {
      $('.counts .card-body')
        .append(`<h4>parsed 3P-seq ids:</h4>`)
      let objectArray = []
      for (var i = 0; i < data.length; i++) {
        let file = '',
          databaseObject = {},
          workflow = data[i].workflow_name,
          genomeVersion = data[i].mapped_genome;
        if (data[i].data_id) {
          let dataId = data[i].data_id;
          $('.counts .card-body')
            .append(`<span><br>${dataId}</span>
              `);
          let file = data[i].align.$zfin_path.split('|');
          let fileType = 'BAM';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i].tes_tags.$zfin_path.split('|');
          fileType = 'BED';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          file = data[i].peaks.$zfin_path.split('|');
          fileType = 'BED';
          createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          for (var j = 0; j < data[i].tracks.length; j++) {
            file = data[i].tracks[j].$zfin_path.split('|');
            fileType = file[0].split('.')
              .pop() === 'bigbed' ? 'BIGBED' : 'BIGWIG';
            createDBObject(data[i], file, fileType, objectArray, workflow, dataId, [dataId], genomeVersion);
          }
        } else {
          console.log('no data id found', data[i]);
        }
      }
      return objectArray;
    }


    function verifyUrl(url, anchor, length) {
      if (url.substring(0, 3) === "http") {
        $.ajax({
          type: 'HEAD',
          url: url,
          success: function() {
            // $(anchor).before(`<span class='fa fa-ok success'></span>`);
            $(anchor)
              .remove();
            let step = parseFloat($('.validation-progress-bar')
              .attr('aria-valuenow'));
            let maxwidth = parseFloat($('.validation-progress-bar')
              .attr('aria-valuemax'));
            step = parseInt(step) + 1
            let width = step / maxwidth * 100
            $('.validation-progress-bar')
              .css('width', width + '%')
              .attr('aria-valuenow', step);
            $('.validation-progress-bar')
              .text(`${step}/${maxwidth}`)
          },
          error: function() {
            $('.counts')
              .append(`<div>
                    <span class='fas fa-times error'></span>
                    <a id="${anchor}">URL: ${url}</a>
                </div>`);
            $('.validation-progress-bar')
              .removeClass('progress-bar-success')
            $('.validation-progress-bar')
              .addClass('progress-bar-danger')
            $(anchor)
              .addClass('bg-danger');
          }
        });
      } else{
        $.ajax({
          type: 'HEAD',
          url: "https://danio-code.zfin.org/files/non_annotated_files/"+url,
          success: function() {
            // $(anchor).before(`<span class='fa fa-ok success'></span>`);
            $(anchor)
              .remove();
            let step = parseFloat($('.validation-progress-bar')
              .attr('aria-valuenow'));
            let maxwidth = parseFloat($('.validation-progress-bar')
              .attr('aria-valuemax'));
            step = parseInt(step) + 1
            let width = step / maxwidth * 100
            $('.validation-progress-bar')
              .css('width', width + '%')
              .attr('aria-valuenow', step);
            $('.validation-progress-bar')
              .text(`${step}/${maxwidth}`)
          },
          error: function() {
            $('.counts')
              .append(`<div>
                    <span class='fas fa-times error'></span>
                    <a id="${anchor}">URL: ${url}</a>
                </div>`);
            $('.validation-progress-bar')
              .removeClass('progress-bar-success')
            $('.validation-progress-bar')
              .addClass('progress-bar-danger')
            $(anchor)
              .addClass('bg-danger');
          }
        });
      }
    }

    function createDBObject(inputObject, inputFile, fileType, objectArray, workflow, dataId, dataIds = {}, genomeVersion = 'GRCz10') {
      let tmp_parsedJson = {};
      let primary_source = inputFile[1].trim()
      primary_source = primary_source.replace("10.0.3.1:8090", "dl.dnanex.us")
      tmp_parsedJson.derived_from = dataId;
      tmp_parsedJson.derived_from_data = dataIds;
      tmp_parsedJson.primary_file = primary_source;
      tmp_parsedJson.primary_source = inputFile[0].trim() + "|" + primary_source;
      tmp_parsedJson.secondary_file = '';
      tmp_parsedJson.secondary_source = '';
      tmp_parsedJson.mapped_genome = genomeVersion ? genomeVersion : 'GRCz10/danRer10';
      tmp_parsedJson.file_type = fileType;
      tmp_parsedJson.command = workflow;
      objectArray.push(tmp_parsedJson);
    }

    function post2DB(data) {
      $('.table')
        .after(`<div class="progress">
                <div class="upload-progress-bar progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="${data.length}" style="width:0%">
                    0
                </div>
            </div>`)
      console.log(data);
      for (var i = 0; i < data.length; i++) {
        $.ajax({
          url: '/addAnalyzedData/',
          processData: false,
          contentType: false,
          type: 'POST',
          data: JSON.stringify(data[i]),
          success: function(response) {
            let step = parseFloat($('.upload-progress-bar')
              .attr('aria-valuenow'));
            let maxwidth = data.length
            step = parseInt(step) + 1
            let width = step / maxwidth * 100
            $('.upload-progress-bar')
              .css('width', width + '%')
              .attr('aria-valuenow', step);
            $('.upload-progress-bar')
              .text(`${step}/${maxwidth}`)
            $('.table ')
              .append(`<tr><td>${response}<td></tr>`);
          },
          error: function(xhr, status, error) {
            var err = xhr.responseText;
            $('.upload-progress-bar')
              .addClass('progress-bar-danger')
            console.log(xhr, err, error);
          }
        });
      }
    }

    function postUpdate2DB(data, file) {

      var url = file ?
        '/updateFile/' :
        '/updateAnalyzedData/'
      $.ajax({
        url: url,
        processData: false,
        contentType: false,
        type: 'POST',
        data: JSON.stringify(data),
        success: function(response) {
          var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(response);
          var link = document.createElement('a');
          $('#update_button')
            .hide()
          $('#upload_button')
            .hide()
          $('.counts')
            .addClass('border-success')
          $(link)
            .attr({
              "href": csvData,
              "download": "updatedData" + data[0].command.split("|")[0] + ".csv"
            });
          link.click();
        },
        error: function(xhr, status, error) {
          var err = xhr.responseText;
          $('.counts')
            .addClass('border-danger')
          console.log(xhr, err, error);
        }
      });

    }
    $(document)
      .on('click', '.btn--undo', function() {
        // $(this).button('loading');
        $(this)
          .html($(this)
            .data('loading-text'));
        let button = this;
        let id = $(button)
          .attr('id')
        id = id.substring(($(button)
            .attr('id')
            .indexOf('_') + 1), $(button).attr('id').length)

        $.ajax({
          type: "POST",
          url: "/deleteFiles/",
          contentType:"application/json; charset=utf-8",
          dataType:"json",
          data:
            JSON.stringify({"id": [{"dataid": id}]}),
          success: function() {
            $(button)
              .append(`<span>deleted</span>`);
            $(button)
              .remove();
            $(button)
              .closest('.data_item')
              .remove();
          }
        });
        return false;
      });
    addEventListener('dragover', e => e.preventDefault());
    addEventListener('drop', function(e) {
      getLogFile.call(e.dataTransfer || e.clipboardData);
      e.preventDefault();
    });
    $('#logFileUpload')
      .on('change', getLogFile);
    $('#customLogFileUpload')
      .on('change', getCustomLogFile);
    $('#logFileUpdate')
      .on('change', updateLogFile);
    $('#FileUpdate')
      .on('change', updateFile);
    $('#upload_button')
      .on('click', function() {
        let parsedData = JSON.parse(localStorage.getItem("parsedData"));
        post2DB(parsedData);
      });
    $('#update_button')
      .on('click', function() {
        $(this)
          .html('<i class="fa fa-circle-notch fa-spin"></i> Updating');
        let parsedData = JSON.parse(localStorage.getItem("parsedData"));
        var file = $('#FileUpdate')
          .val() !== ''
        postUpdate2DB(parsedData, file);
      });
    $('#series_ids')
      .on('change keydown', function() {
        $('#path_update_button')
          .prop('disabled', false)
        $('#path_tmp_update_button')
          .prop('disabled', false)
      })
    $('.main')
      .on('click', '#path_update_button', function() {
        var button = this
        $(button)
          .html('<i class="fa fa-circle-notch fa-spin"></i>Processing');
        var series_array = $('#series_ids')
          .val()
          .split(',');
        var series_ids = []
        for (var i = 0; i < series_array.length; i++) {
          series_ids.push({
            series_id: series_array[i].trim()
          })
        }
        console.log(series_ids);
        $.ajax({
          type: "POST",
          url: "/updateFilePaths/",
          contentType: "application/json",
          data: JSON.stringify(series_ids),
          success: function(response) {
            var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(response);
            var link = document.createElement('a');
            $('#series_ids')
              .addClass('border-success')
            $(link)
              .attr({
                "href": csvData,
                "download": "updatedFilePaths-" + (new Date())
                  .toString()
                  .split(' ')
                  .splice(1, 4)
                  .join('_') + ".csv"
              });
            link.click();
            $(button)
              .remove()
          },
          error: function(xhr, status, error) {
            var err = xhr.responseText;
            $('.card-header')
              .parent('.card')
              .addClass('card-outline-danger')
            $(button)
              .html('Update Files')
              .prop('disabled', true);
            console.log(xhr, err, error);
          }
        });
        return false;
      });
    $('.main')
      .on('click', '#path_tmp_update_button', function() {
        var button = this
        $(button)
          .html('<i class="fa fa-circle-notch fa-spin"></i>Processing');
        var series_array = $('#series_ids')
          .val()
          .split(',');
        var series_ids = []
        for (var i = 0; i < series_array.length; i++) {
          series_ids.push({
            series_id: series_array[i].trim()
          })
        }
        console.log(series_ids);
        $.ajax({
          type: "POST",
          url: "/updateTemporaryFilePaths/",
          contentType: "application/json",
          data: JSON.stringify(series_ids),
          success: function(response) {
            $(button)
              .remove()
          },
          error: function(xhr, status, error) {
            var err = xhr.responseText;
            $('.card-header')
              .parent('.card')
              .addClass('card-outline-danger')
            $(button)
              .html('Remove TMP Files')
              .prop('disabled', true);
            console.log(xhr, err, error);
          }
        });
        return false;
      });
    $('#delete_data_ids')
      .on('change keydown', function() {
        $('#delete_button')
          .prop('disabled', false)
      })
    $('#delete_button')
      .on('click', function() {
        $('#really_delete_button')
          .show()
        $('#really_delete_button')
          .prop('disabled', false)
        $('#delete_button')
          .hide()
      });
    $('.main')
      .on('click', '#really_delete_button', function() {
        var button = this
        $(button)
          .html('<i class="fa fa-circle-notch fa-spin"></i>Processing');
        var data_array = $('#delete_data_ids')
          .val()
          .split(',');
        var data_ids = []
        for (var i = 0; i < data_array.length; i++) {
          data_ids.push({
            dataid: data_array[i].trim()
          })
        }
        console.log(data_ids);
        $.ajax({
          type: "POST",
          url: "/deleteFiles/",
          dataType: 'json',
          ContentType: "application/json",
          data: {
            id: JSON.stringify(data_ids)
          },
          success: function(response) {
            if (JSON.parse(response)
              .success) {
              button.remove()
              $('delete-ids')
                .parent('.card')
                .removeClass('bg-danger')
                .addClass('bg-success')
            }
          },
          error: function(xhr, status, error) {
            var err = xhr.responseText;
            $('delete-ids')
              .parent('.card')
              .addClass('border-danger')
            console.log(xhr, err, error);
          }
        });
        return false;
      });

    $('#delete_file_paths')
      .on('change keydown', function() {
        $('#delete_files_button')
          .prop('disabled', false)
      })
    $('#delete_files_button')
      .on('click', function() {
        $('#really_delete_files_button')
          .show()
        $('#really_delete_files_button')
          .prop('disabled', false)
        $('#delete_files_button')
          .hide()
      });
    $('.main')
      .on('click', '#really_delete_files_button', function() {
        var button = this
        $(button)
          .html('<i class="fa fa-circle-notch fa-spin"></i>Processing');
        var data_array = $('#delete_file_paths')
          .val()
          .split(',');
        var file_paths = []
        for (var i = 0; i < data_array.length; i++) {
          file_paths.push({
            path: data_array[i].trim()
          })
        }
        console.log(file_paths);
        $.ajax({
          type: "POST",
          url: "/deleteFilesByPath/",
          dataType: 'json',
          ContentType: "application/json",
          data: {
            id: JSON.stringify(file_paths)
          },
          success: function(response) {
            if (JSON.parse(response)
              .success) {
              button.remove()
              $('.delete-paths')
                .parent('.card')
                .removeClass('bg-danger')
                .addClass('bg-success')
            }
          },
          error: function(xhr, status, error) {
            var err = xhr.responseText;
            $('.delete-paths')
              .parent('.card')
              .addClass('bg-info')
            console.log(xhr, err, error);
          }
        });
        return false;
      });
    $('#freeze_data_ids')
      .on('change keydown', function() {
        $('#add_freeze_tag')
          .prop('disabled', false)
      })
    $('.main')
      .on('click', '#add_freeze_tag', function() {
        var button = this
        $(button)
          .html('<i class="fa fa-circle-notch fa-spin"></i>Processing');
        var data_array = $('#freeze_data_ids')
          .val()
          .split(',');
        var data_ids = []
        for (var i = 0; i < data_array.length; i++) {
          data_ids.push({
            dataid: data_array[i].trim(),
            freeze_tag: $('#freeze_tag')
              .val()
          })
        }
        console.log(data_ids);
        $.ajax({
          type: "POST",
          url: "/addFreezeTag/",
          dataType: 'json',
          ContentType: "application/json",
          data: {
            id: JSON.stringify(data_ids)
          },
          success: function(response) {
            if (JSON.parse(response)
              .success) {
              console.log('success');
              button.remove()
              $('.freeze-card')
                .addClass('border-success')
            }
          },
          error: function(xhr, status, error) {
            var err = xhr.responseText;
            $('.freeze-card')
              .addClass('border-danger')
            console.log(xhr, err, error);
          }
        });
        return false;
      });
  });
