import {
  selection,
  select,
  event as currentEvent,
  selectAll
} from 'd3-selection';
import { scaleLinear, scaleOrdinal, scaleTime } from 'd3-scale';
import { keys } from 'd3-collection';
import { max, extent } from 'd3-array';
import { axisBottom, axisLeft } from 'd3-axis';
import { area, stack } from 'd3-shape';
import { format } from 'd3-format';
import { timeParse, timeFormat } from 'd3-time-format';
import { schemeBlues, schemeReds, schemeGreens } from 'd3-scale-chromatic';
import { hsl } from 'd3-color';
import 'd3-transition';
import {
  annotationCalloutRect,
  annotationCustomType,
  annotation,
  annotationBadge
} from 'd3-svg-annotation';

$(document).ready(function() {
  /* jshint undef:true */
  /*jshint devel:false */
  /*global d3, plot_data */
  'use strict';

  const assay_types = JSON.parse(plot_data.assays).assay_types;
  const color = {
    Chromatin: scaleOrdinal()
      .range(schemeGreens[max([assay_types['Chromatin'].length,3])])
      .domain(assay_types['Chromatin']),
    RNA: scaleOrdinal()
      .range(schemeBlues[max([assay_types['RNA'].length,3])])
      .domain(assay_types['RNA']),
    Methylation: scaleOrdinal()
      .range(schemeReds[max([assay_types['Methylation'].length,3])])
      .domain(assay_types['Methylation'])
  };
  var assay_types_inv = {};
  for (var p in assay_types) {
    var arr = assay_types[p],
      l = arr.length;
    for (var i = 0; i < l; i++) {
      assay_types_inv[arr[i]] = p;
    }
  }
  graph(JSON.parse(plot_data.sizeDevelopment).sizes, color);
  function graph(data_input, color) {
    var margin = {
        top: 20,
        right: 60,
        bottom: 120,
        left: 25
      },
      height = 425 - margin.top - margin.bottom,
      width = height * 4 - margin.left - margin.right;
    var div = select('#sizes_chart')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);
    var parseDate = timeParse('%Y-%m-%d');

    data_input.forEach(function(d) {
      d.date = parseDate(d.date);
    });
    data_input.sort(function(a, b) {
      var x = a['date'];
      var y = b['date'];
      return x < y ? -1 : x > y ? 1 : 0;
    });
    var assay_names = Object.keys(assay_types_inv);
    // assay_names.splice(assay_names.indexOf('date'), 1);
    data_input.forEach(function(d, i) {
      if (i > 1) {
        assay_names.forEach(key => {
          data_input[i][key] += data_input[i - 1][key];
        });
      }
    });
    var stackFun = stack().keys(assay_names.reverse());
    var stacked = stackFun(data_input);
    var areaLayer = area()
      .x(function(d, i) {
        return x(d.data.date);
      })
      .y0(function(d) {
        return y(d[0]);
      })
      .y1(function(d) {
        return y(d[1]);
      });

    var x = scaleTime().range([0, width]),
      y = scaleLinear().range([height, 0]);
    x.domain(
      extent(data_input, function(d) {
        return d.date;
      })
    );
    // z.domain(assay_names);
    y.domain([0, max(stacked[stacked.length - 1].map(d => d[1]))]);

    // color.domain(assay_names);

    var svg = select('#sizes_chart')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom);

    var g = svg
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    var layer = g
      .selectAll('.layer')
      .data(stacked)
      .enter()
      .append('g')
      .attr('class', 'layer');

    layer
      .append('path')
      .attr('class', function(d) {
        return 'area assay_' + d.key;
      })
      .style('fill', function(d) {
        let type = assay_types_inv[d.key];
        return color[type](d.key);
      })
      .attr('d', areaLayer);
    layer
      .filter(function(d) {
        return d[d.length - 1][1] - d[d.length - 1][0] > 0.01;
      })
      .on('mouseover', function(d) {
        svg.selectAll('.assay_' + d.key).style('fill', function(d) {
          let type = assay_types_inv[d.key];

          return hsl(color[type](d.key)).brighter(0.5);
        });
        div
          .transition()
          .duration(200)
          .style('opacity', 0.9);
        div
          .html('<span>' + d.key + '</span>')
          .style('left', event.pageX + 'px')
          .style('top', event.pageY - 28 + 'px');
      })
      .on('mouseout', function(d) {
        svg.selectAll('.assay_' + d.key).style('fill', function(d) {
          let type = assay_types_inv[d.key];
          return color[type](d.key);
        });
        div
          .transition()
          .duration(500)
          .style('opacity', 0);
      })
      .append('text')
      .attr('x', width)
      .attr('y', function(d) {
        return y((d[d.length - 1][0] + d[d.length - 1][1]) / 2);
      })
      .attr('dy', '.35em')
      .style('font', '10px sans-serif')
      .style('text-anchor', 'start')
      .text(function(d) {
        return [
          'RNA-seq',
          'ChIP-seq',
          'MethylC-seq',
          'BS-seq',
          'ATAC-seq',
          'TAB-seq'
        ].indexOf(d.key) > -1
          ? d.key
          : '';
      });

    g
      .append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + height + ')')
      .call(axisBottom(x).tickFormat(timeFormat('%B, %Y')));

    g
      .append('g')
      .attr('class', 'axis axis--y')
      .call(
        axisLeft(y)
          .tickSizeOuter(0)
          .ticks(5)
          .tickFormat(format('.0s'))
      )
      .append('text')
      .attr('x', 6)
      .attr('y', y(y.ticks(10).pop()))
      .attr('dy', '0.35em')
      .attr('text-anchor', 'start')
      .attr('fill', '#000')
      .text('Bytes');

    const annotations = [

    ];
    const type = annotationCustomType(annotationBadge, {
      subject: { radius: 10 }
    });

    const makeAnnotations = annotation()
      // .type(type)
      .accessors({
        x: function(d) {
          return x(new Date(d.x));
        },
        y: function(d) {
          return y(d.y);
        }
      })
      .annotations(annotations);

    select('svg')
      .append('g')
      .attr('class', 'annotation-group')
      .call(makeAnnotations);
  }
});
