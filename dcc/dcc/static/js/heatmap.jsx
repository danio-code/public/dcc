import React, { Component } from 'react';
// import {
//   selection,
//   select,
//   event as currentEvent,
//   scalePoint,
//   scaleLinear,
//   nest,
//   extent,
//   axisBottom,
//   axisLeft
// } from 'd3';
import { selection, select, event as currentEvent } from 'd3-selection';
import { scaleBand, scaleLinear } from 'd3-scale';
import { nest } from 'd3-collection';
import { extent } from 'd3-array';
import { axisBottom, axisLeft } from 'd3-axis';
class HeatMap extends Component {
  constructor(props) {
    super(props);
    this.createHeatMap = this.createHeatMap.bind(this);
  }
  componentDidMount() {
    this.createHeatMap();
  }
  componentDidUpdate() {
    this.createHeatMap();
  }
  createHeatMap() {
    if (this.props.data.length > 0) {
      const node = select(this.node)

      var width = this.props.size[0] - this.props.margin.left - this.props.margin.right;
      var height = this.props.size[1] - this.props.margin.top - this.props.margin.bottom;
      let selectedOption = this.props.selectedOption;
      let targets = [];
      let stages = [];

      let x = scaleBand().range([0, width]);
      let y = scaleBand().range([height, 100]);
      const color = scaleLinear().range(['#EDEDED', '#1098ad']);
      // let input_data = this.props.data.filter(item => item.assay_type==='ChIP-seq');
      const nestedData = nest()
        .key(d => {
          return d.stage;
        })
        .key(d => {
          if(d[selectedOption].trim()!==''){
          targets.push(d[selectedOption]);
          return d[selectedOption];
        }
        })
        .rollup(leaves => {
          return leaves.length;
        })
        .entries(this.props.data);
      let data = [];
      nestedData.forEach(stage => {
        if (stage.key === 'no value') {
          return true;
        }
        stage.values.forEach(target => {
          if (target.key === 'no value') {
            return true;
          }
          targets.push(target.key);
          stages.push(stage.key);
          data.push({ stage: stage.key, target: target.key, value: target.value });
        });
      });
      targets = [...new Set(targets)];
      stages = [...new Set(stages)];
      let refStages = [
        '1-cell',
        '2-cell',
        '4-cell',
        '8-cell',
        '16-cell',
        '32-cell',
        '64-cell',
        '128-cell',
        '256-cell',
        '512-cell',
        '1k-cell',
        'High',
        'Oblong',
        'Sphere',
        'Dome',
        '30%-epiboly',
        '50%-epiboly',
        'Germ-ring',
        'Shield',
        '75%-epiboly',
        '90%-epiboly',
        'Bud',
        '1-4 somites',
        '5-9 somites',
        '10-13 somites',
        '14-19 somites',
        '20-25 somites',
        '26+ somites',
        'Prim-5',
        'Prim-15',
        'Prim-25',
        'High-pec',
        'Long-pec',
        'Pec-fin',
        'Protruding-mouth',
        'Day 4',
        'Day 5',
        'Day 6',
        'Days 7-13',
        'Days 14-20',
        'Days 21-29',
        'Days 30-44',
        'Days 45-89',
        '90 Days-2 Years'
      ];
      let refTypes =[
        'BS-seq',
        'MeDIP-seq',
        'MethylC-seq',
        'MethylCap-seq',
        'RRBS',
        'TAB-seq',
        '4C-seq',
        'ATAC-seq',
        'ChIP-seq',
        'DNAse-seq',
        'Hi-C',
        'MNase-seq',
        '3P-seq',
        'CAGE-seq',
        'mRNA-seq',
        'miRNA-seq',
        'PAL-seq',
        'Ribo-seq',
        'RNA-seq',
        'small-RNA-seq',
        'SAPAS',
      ]
      stages.sort(function(a, b) {
        return refStages.indexOf(a) < refStages.indexOf(b) ? -1 : 1;
      });
      if (this.props.selectedOption==="assay_type"){
      targets.sort(function(a, b) {
        return refTypes.indexOf(a) < refTypes.indexOf(b) ? 1 : -1;
      });
      }
      // stages.push(' ');
      x.domain(stages);
      targets.unshift(' ');
      targets = targets.filter(item => item.trim() !== '')
      y.domain(targets);
      color.domain(
        extent(data, d => {
          return d.value;
        })
      );
      var yStep = Math.abs(y(targets[1]) - y(targets[0]));
      var xStep = Math.abs(x(x.domain()[1]) - x(x.domain()[0]));



      var tile = node.selectAll('g.tile_group').data(data, d => {
        return d.stage + d.target;
      });

      tile.exit().remove();
      node.selectAll('g.axis').remove();

      var tileEnter = tile
        .enter()
        .append('g')
        .attr('class', 'tile_group');

      tileEnter.append('rect').attr('class', 'tile');

      tileEnter.append('text').attr('class', 'label');

      node.append('g').attr('class', 'axis x--axis');

      node.append('g').attr('class', 'axis y--axis');

      tile = tileEnter.merge(tile).on('click', d => {
        this.props.toggleView('table');
        let filters = {stage: d.stage}
        filters[this.props.selectedOption] = d.target
        this.props.filterFacet(filters);
      });

      tile
        .select('rect.tile')
        .attr('x', d => {
          if (d.target) {
          return x(d.stage);
        }
        })
        .attr('y', d => {
          if (d.target) {
            return y(d.target);
          }
        })
        .attr('width', d=>d.target?xStep:0)
        .attr('height',d=>d.target?yStep:0)
        .attr('fill', d => {
          return color(d.value);
        })
        .on('mouseover', function(d, i,j) {
          select('#colLabel_' + d.stage.replace("%","")).classed('hovered', true);
          select('#rowLabel_' + d.target).classed('hovered', true);
        })
        .on('mouseout', function(d, i) {
          select('#colLabel_' + d.stage.replace("%","")).classed('hovered', false);
          select('#rowLabel_' + d.target).classed('hovered', false);
        });

      tile
        .select('text.label')
        .attr('x', d => {
          return x(d.stage) + xStep / 2;
        })
        .attr('y', d => {
          return y(d.target) + yStep / 2;
        })
        .attr('dy', '.45em')
        // .attr("dx", "-0.35em")
        .text(function(d) {
          return d.value;
        })
        .style('text-anchor', 'middle')
        .on('mouseover', function(d, i,j) {
          select('#colLabel_' + d.stage.replace("%","")).classed('hovered', true);
          select('#rowLabel_' + d.target).classed('hovered', true);
        })
        .on('mouseout', function(d, i) {
          select('#colLabel_' + d.stage.replace("%","")).classed('hovered', false);
          select('#rowLabel_' + d.target).classed('hovered', false);
        });

      node
        .selectAll('g.x--axis')
        .attr('transform', 'translate(0,' + height + ')')
        .call(axisBottom(x).tickSizeOuter(0))
        .selectAll('text')
        .attr('id', function(d, i) {
          return 'colLabel_' + d.replace("%","");
        })
        .attr('transform', 'translate(-6,' + xStep / 8 + ')rotate(-45)')
        .style('text-anchor', 'end');

      node
        .selectAll('g.y--axis')
        .call(axisLeft(y).tickSizeOuter(0))
        .selectAll('text')
        // .attr('transform', 'translate(0,' + yStep / 2 + ')')
        .attr('id', function(d, i) {
          return 'rowLabel_' + d;
        });
    }
  }
  render() {
    return (
      <svg
        width={this.props.size[0] + this.props.margin.left + this.props.margin.right}
        height={this.props.size[1] + this.props.margin.top + this.props.margin.bottom}
        className="heatmap">
        <g
        ref={node => (this.node = node)}
        transform={`translate(${this.props.margin.left}, ${this.props.margin.top})`}/>
      </svg>
    );
  }
}
export default HeatMap;
