$(document).ready(function() {
    /*jshint multistr: true */
    'use strict';

$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      //save the latest tab using a cookie:
      Cookies.set('last_tab', $(e.target).attr('href'));
  });



  //activate latest tab, if it exists:
  let lastTab = Cookies.get('last_tab');
  if (lastTab) {
      $(`.nav-tabs a[href="${lastTab}"]`).tab('show');
  }
});
