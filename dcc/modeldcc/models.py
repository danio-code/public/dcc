from __future__ import unicode_literals
import os
import string
import re
import hashlib
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_delete
from django.dispatch import receiver

RNA_BASED_ASSAYS = (
    'RNA-seq', 'short-RNA-seq','miRNA-seq', 'CAGE-seq', 'Bru-seq', 'PAS-seq',
    'Ribo-seq', 'RIP-seq', 'PAR-Clip-seq', 'iCLIP-seq', 'GRO-seq', 'PAL-seq',
    '3P-seq','SAPAS-seq')

# define pre-filled autocomplete tables


class statuses(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "statuses"

    def __unicode__(self):
        return str(self.name)


class biosample_types(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "biosample types"

    def __unicode__(self):
        return str(self.name)


class labs(models.Model):
    lab_id = models.CharField(
        max_length=200, verbose_name="Lab ID")
    name = models.CharField(max_length=200, verbose_name="Lab Name")
    lab_pi = models.CharField(max_length=200, verbose_name="Principal Investigator",
        blank=True, null=True)
    lab_institution = models.CharField(max_length=200, verbose_name="Institution",
        blank=True, null=True)
    lab_country = models.CharField(max_length=200, verbose_name="Country",
        blank=True, null=True)
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)

    class Meta:
        verbose_name_plural = "labs"

    def __unicode__(self):
        if str(self.lab_institution).encode('utf-8') != "":
            name = str(self.name) +\
            ", " + str(self.lab_institution).encode('utf-8')
        else:
            name = str(self.name)
        return name

    def save(self, *args, **kwargs):
        # if the id has not been set yet then it is a new object
        # and needs a new lab_id
        if not self.id:
            super(labs, self).save(*args, **kwargs)
            self.lab_id = 'DCD' + \
                str(self.id).zfill(3) + 'LB'
        super(labs, self).save(*args, **kwargs)

    def to_json(self):
        return dict(
            lab_id=self.lab_id,
            name=string.replace(self.name, " ", "_"),
            lab_pi=self.lab_pi,
            lab_institution=self.lab_institution,
            lab_country=self.lab_country,
        )


class anatomical_terms(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "anatomical terms"

    def __unicode__(self):
        return str(self.name)


class genetic_backgrounds(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "genetic backgrounds"

    def __unicode__(self):
        return str(self.name)


class developmental_stages(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "developmental stages"

    def __unicode__(self):
        return str(self.name)


class sexes(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "sexes"

    def __unicode__(self):
        return str(self.name)


class assay_types(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "assay types"

    def __unicode__(self):
        return str(self.name)


class platforms(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "platforms"

    def __unicode__(self):
        return str(self.name)


class instruments(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "instruments"

    def __unicode__(self):
        return str(self.name)


class strand_modes(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "strand modes"

    def __unicode__(self):
        return str(self.name)


class file_types(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "file types"

    def __unicode__(self):
        return str(self.name)


class mapped_genomes(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "mapped genomes"

    def __unicode__(self):
        return str(self.name)


class organisms(models.Model):
    name = models.CharField(max_length=200, default='Danio rerio')

    class Meta:
        verbose_name_plural = "organisms"

    def __unicode__(self):
        return str(self.name)


class Versions(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "versions"

    def __unicode__(self):
        return str(self.name)

# define series, biosample, assay, sequencing and data models
class Series(models.Model):
    series_id = models.CharField(
        max_length=200, verbose_name='Series ID')

    title = models.CharField(max_length=1000, verbose_name='Title')
    series_type = models.CharField(
        max_length=100, verbose_name='Series Type')
    is_public = models.BooleanField(
        verbose_name='Is Public', default=True)
    description = models.TextField(
        max_length=10000, verbose_name='Series Description')
    status = models.ForeignKey(
        statuses, verbose_name='Status', default=0)
    publication = models.CharField(
        max_length=1000, verbose_name='Publication', blank=True)
    sra_geo_id = models.CharField(
        max_length=200, verbose_name='SRA/GEO Accession',
        blank=True)

    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    class Meta:
        verbose_name_plural = "series"

    def __unicode__(self):
        return unicode(self.series_id) + ": " + \
            unicode(self.series_type) + " titled " + \
            unicode(self.title)

    def save(self, *args, **kwargs):
        # if the id has not been set yet then it is a new object
        # and needs a new series_id
        if not self.id:
            super(Series, self).save(*args, **kwargs)
            self.series_id = 'DCD' + \
                str(self.id).zfill(6) + 'SR'
        super(Series, self).save(*args, **kwargs)

    @property
    def short_desc(self):
        words = self.description.split(' ')
        return ' '.join(words[:9]) + ' ...' if len(words) > 10 else \
            self.description


class Biosample(models.Model):
    biosample_id = models.CharField(
        max_length=200, verbose_name="Biosample ID")

    series = models.ForeignKey(
        Series, related_name='series_biosample', verbose_name="Series")

    biosample_type = models.ForeignKey(
        biosample_types, verbose_name='Biosample Type')
    biosample_lab = models.ForeignKey(labs, verbose_name='Biosample Lab')
    genetic_background = models.ForeignKey(
        genetic_backgrounds, verbose_name='Genetic Background')
    stage = models.ForeignKey(
        developmental_stages, verbose_name='Stage',
        blank=True, null=True)
    controlled_by = models.ForeignKey(
        'self', related_name='control_for',
        verbose_name="Controlled by", blank=True, null=True)
    anatomical_term = models.ForeignKey(
        anatomical_terms, verbose_name='Anatomical Term',
        blank=True, null=True)
    organism = models.ForeignKey(organisms, default='1')
    treatment = models.CharField(
        max_length=1000, verbose_name='Treatment', blank=True)
    post_fertilization = models.CharField(
        max_length=100, verbose_name='Hours post Fertilization',
        blank=True)
    source = models.CharField(
        max_length=200, verbose_name='Biosample Source', blank=True)
    sex = models.ForeignKey(
        sexes, verbose_name='Sex', blank=True, null=True)
    mutation_description = models.CharField(
        max_length=1000, verbose_name='Mutation Description', blank=True)
    cell_line_type = models.CharField(
        max_length=200, verbose_name='Cell Line Type', blank=True)
    description = models.TextField(
        max_length=1000, verbose_name='Biosample Description',
        blank=True)

    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        return str(self.biosample_id) + ": " + \
            str(self.biosample_type) + " from " + str(self.biosample_lab) +" at stage "+ str(self.stage)

    def save(self, *args, **kwargs):
        # if the id has not been set yet then it is a new object
        # and needs a new biosample_id
        if not self.id:
            super(Biosample, self).save(*args, **kwargs)
            self.biosample_id = 'DCD' + \
                str(self.id).zfill(6) + 'BS'
        super(Biosample, self).save(*args, **kwargs)

    @property
    def short_desc(self):
        words = self.description.split(' ')
        return ' '.join(words[:9]) + ' ...' if len(words) > 10 else \
            self.description


class BiosampleReplicate(models.Model):
    biosamplereplicate_id = models.CharField(
        max_length=200, verbose_name="Biosample Replicate ID")

    biosample = models.ForeignKey(
        Biosample, related_name='biosample_biorep')

    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        return str(self.biosamplereplicate_id)

    def save(self, *args, **kwargs):
        if not self.id:
            super(BiosampleReplicate, self).save(*args, **kwargs)
            self.biosamplereplicate_id = 'DCD' + \
                str(self.id).zfill(6) + 'BR'
        super(BiosampleReplicate, self).save(*args, **kwargs)


class Assay(models.Model):
    assay_id = models.CharField(
        max_length=200, verbose_name="Assay ID")

    assay_type = models.ForeignKey(
        assay_types, verbose_name='Assay Type')
    assay_lab = models.ForeignKey(labs, verbose_name='Assay Lab')
    description = models.CharField(
        max_length=2000, verbose_name='Assay Description')
    target = models.CharField(
        max_length=200, verbose_name='Target', blank=True)
    library_prep = models.CharField(
        max_length=200, verbose_name='RNA-library Preparation', blank=True,
        null=True)
    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        if str(self.assay_type) in RNA_BASED_ASSAYS:
            return str(self.assay_id) + ' : ' + str(self.assay_type) + \
            " from " + str(self.assay_lab) + " with "+ str(self.library_prep)
        else:
            return str(self.assay_id) + ' : ' + str(self.assay_type) + \
            " from " + str(self.assay_lab)

    def to_json(self):
        return dict(
            assay_id=self.assay_id,
            assay_lab=self.assay_lab.name,
            assay_type=self.assay_type.name,
            description=self.description,
            target=self.target,
        )

    # override save to give a unique formated ID
    def save(self, *args, **kwargs):
        if not self.id:
            super(Assay, self).save(*args, **kwargs)

            if self.target:
                target = "_" + self.target
            else:
                target = ""

            identifier = self.assay_type.name + "_" + \
                         string.replace(self.assay_lab.name, " ", "_") \
                         + target
            if self.target:
                no = Assay.objects.filter(
                    assay_type=self.assay_type).filter(
                        assay_lab=self.assay_lab).filter(
                            target=self.target).count()
            else:
                no = Assay.objects.filter(
                    assay_type=self.assay_type).filter(
                        assay_lab=self.assay_lab).count()
            self.assay_id = identifier + "_" + str(no).zfill(4) + 'AS'
        super(Assay, self).save(*args, **kwargs)

    @property
    def short_desc(self):
        words = self.description.split(' ')
        return ' '.join(words[:9]) + ' ...' if len(words) > 10 else \
            self.description


class AppliedAssay(models.Model):
    appliedassay_id = models.CharField(
        max_length=200, verbose_name="Applied Assay ID", null=True)

    biosamplereplicate = models.ForeignKey(
        BiosampleReplicate, related_name='biorep_appliedassay',
        verbose_name="Biosample Replicate")
    assay = models.ForeignKey(Assay, related_name='assay_appliedassay',
        verbose_name="Assay")

    controlled_by = models.ForeignKey(
        'self', related_name='control_for',
        verbose_name="Controlled By", blank=True, null=True)

    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        return str(self.appliedassay_id)

    def save(self, *args, **kwargs):
        if not self.id:
            super(AppliedAssay, self).save(*args, **kwargs)
            self.appliedassay_id = 'DCD' + \
                                   str(self.id).zfill(6) + 'AA'
        super(AppliedAssay, self).save(*args, **kwargs)


class TechnicalReplicate(models.Model):
    technicalreplicate_id = models.CharField(
        max_length=200, verbose_name="Technical Replicate ID")

    appliedassay = models.ForeignKey(AppliedAssay,
        related_name='appliedassay_technicalreplicate')

    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        return str(self.technicalreplicate_id)

    def save(self, *args, **kwargs):
        if not self.id:
            super(TechnicalReplicate, self).save(*args, **kwargs)
            self.technicalreplicate_id = 'DCD' + \
                str(self.id).zfill(6) + 'TR'
        super(TechnicalReplicate, self).save(*args, **kwargs)


class Sequencing(models.Model):
    sequencing_id = models.CharField(
        max_length=200, verbose_name="Sequencing ID")

    technicalreplicate = models.ForeignKey(
        TechnicalReplicate, verbose_name='Technical Replicate',
        related_name='sequenced_by')

    sequencing_lab = models.ForeignKey(
        labs, verbose_name='Sequencing Lab')
    platform = models.ForeignKey(platforms,  verbose_name='Platform')
    instrument = models.ForeignKey(
        instruments, verbose_name='Instrument')
    chemistry_version = models.CharField(
        max_length=200, verbose_name='Chemistry Version', blank=True)
    paired_end = models.BooleanField(
        verbose_name='Paired End', default=False)
    strand_mode = models.ForeignKey(
        strand_modes, max_length=200, verbose_name='Strand',
        blank=True, null=True)
    max_read_length = models.IntegerField(
        verbose_name='Maximum Read Length', blank=True, null=True)
    sequencing_date = models.DateField(
        'Sequencing Date', default="2000-01-01")

    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        return str(self.sequencing_id) + ' : ' + str(self.platform) + \
            str(self.instrument) + " from " + str(self.sequencing_lab)

    # override save to give a unique formated ID
    def save(self, *args, **kwargs):
        if not self.id:
            super(Sequencing, self).save(*args, **kwargs)
            self.sequencing_id = 'DCD' + \
                str(self.id).zfill(6) + 'SQ'
        super(Sequencing, self).save(*args, **kwargs)


# get file name for Data instance reads (first and possibly second reads)
def get_data_fn(inst, read_number):
    return os.path.join(
        str(inst.sequencing.technicalreplicate.appliedassay.assay.assay_type),
        str(inst.sequencing.technicalreplicate.appliedassay.biosamplereplicate.biosample.biosample_id),
        '.'.join(map(str, (
            inst.sequencing.technicalreplicate.appliedassay.assay.assay_id,
            inst.sequencing.sequencing_id,
            "USER" + str(inst.sequencing.user.username),
            read_number, inst.file_type.name.lower(), 'gz')))
        )


def get_file_path(inst, file_name):
    return os.path.join(
        str(inst.sequencing.technicalreplicate.appliedassay.assay.assay_type),
        str(inst.sequencing.technicalreplicate.appliedassay.biosamplereplicate.biosample.biosample_id),
        file_name)


def get_primary_data_fn(inst, filename):
    if inst.file_type.name == 'FASTQ':
        return get_data_fn(inst, 'R1')
    else:
        file_name = inst.primary_source.rsplit('|', 1)[0]
        mapped_genome = unicode(inst.mapped_genome).split('/')[1]
        if file_name.find(mapped_genome) == -1:
            if re.match(r'.*\d*SQ\..*', file_name):
                file_name = re.sub(r'(\d*SQ\.)', r'\1'+mapped_genome+'.', file_name)
            else:
                file_name = mapped_genome + '.' + file_name
        return get_file_path(inst, file_name)


def get_secondary_data_fn(inst, filename):
    if inst.file_type.name == 'FASTQ':
        return get_data_fn(inst, 'R2')
    else:
        file_name = inst.secondary_source.rsplit('|', 1)[0]
        mapped_genome = inst.mapped_genome.name.split('/')[1]
        if file_name.find(mapped_genome) > -1:
            file_name = re.sub(r'(\d*SQ\.)', r'\1'+mapped_genome+'.', file_name)
        return get_file_path(inst, file_name)


def generate_sha(files):
    sha = hashlib.sha1()
    for file in files:
        file.seek(0)
        while True:
            buf = file.read(104857600)
            if not buf:
                break
            sha.update(buf)
        sha1 = sha.hexdigest()
        file.seek(0)

    return sha1

class Data(models.Model):
    data_id = models.CharField(
        max_length=200, verbose_name="Data ID", null=True)

    sequencing = models.ForeignKey(
        Sequencing, verbose_name='Sequencing',
        related_name='sequencing_data')
    primary_file = models.FileField(
        upload_to=get_primary_data_fn, verbose_name="Primary File", max_length=255)
    secondary_file = models.FileField(
        upload_to=get_secondary_data_fn, verbose_name="Secondary File",
        null=True, max_length=255)
    derived_from = models.ForeignKey(
        AppliedAssay, related_name='source_of',
        verbose_name="Derived from", blank=True, null=True)
    derived_from_data = models.ManyToManyField(
        'self', related_name='sources_of',
        verbose_name="Derived from", blank=True)
    # hard coded FASTQ as the first key in the json file so this
    # should be stable since when the database is instantiated
    # we can't have the filetypes pre-loaded
    file_type = models.ForeignKey(
        file_types, verbose_name="File Type", default=0)
    command = models.CharField(
        max_length=2000, verbose_name='Command', blank=True)
    mapped_genome = models.ForeignKey(
        mapped_genomes, verbose_name='Mapped Genome',
        blank=True, null=True)

    primary_source = models.CharField(
        max_length=1000, verbose_name='Primary File Source', null=True )
    secondary_source = models.CharField(
        max_length=1000, verbose_name='Secondary File Source', null=True,
        blank=True)
    sha1 = models.CharField(
        null=True,
        # editable=False,
        max_length=40)
    version = models.ForeignKey(Versions, verbose_name='Version/Release', null=True,
    blank=True)
    user = models.ForeignKey(User, verbose_name='Added by User')
    date_added = models.DateTimeField('Date Entered', auto_now_add=True)
    date_edited = models.DateTimeField('Date Entered', auto_now=True)

    def __unicode__(self):
        return str(self.data_id) + ' : ' + str(self.file_type)

    # override save to give a unique formated ID
    def save(self, *args, **kwargs):
        files = [self.primary_file]
        if self.secondary_file and hasattr(self.secondary_file, 'name'):
            files.append(self.secondary_file)
        self.sha1 = generate_sha(files)
        if not self.id:
            super(Data, self).save(*args, **kwargs)
            self.data_id = 'DCD' + \
                str(self.id).zfill(6) + 'DT'
        super(Data, self).save(*args, **kwargs)


@receiver(post_delete, sender=Data)
def submission_delete(sender, instance, **kwargs):
    if instance.primary_file.name:
        if os.path.isfile(instance.primary_file.path):
            os.remove(instance.primary_file.path)
    if instance.secondary_file.name:
        if os.path.isfile(instance.secondary_file.path):
            os.remove(instance.secondary_file.path)


# @receiver(models.signals.pre_save, sender=Data)
# def auto_delete_file_on_change(sender, instance, **kwargs):
#     """
#     Deletes old file from filesystem
#     when corresponding `Data` object is updated
#     with new file.
#     """
#     if not instance.pk:
#         return False
#
#     try:
#         old_file = Data.objects.get(pk=instance.pk).primary_file.file
#     except Data.DoesNotExist:
#         return False
#
#     new_file = instance.primary_file.file
#     if not old_file == new_file:
#         if os.path.isfile(old_file.path):
#             os.remove(old_file.path)


#####end of alpha view       #####
#####continue to batch upload#####

def get_batch_name(inst, filename):
    return os.path.join('batch_upload_csv/', '.'.join((
            str(inst.user), str(inst.isValid), filename, 'csv')))
class BatchUploads(models.Model):
    csvFile = models.FileField(
        upload_to=get_batch_name, verbose_name="CSV File",
        max_length=1000)
    user = models.ForeignKey(User, verbose_name='Added by User')
    isValid = models.BooleanField()
    committed = models.BooleanField()


class BatchUploadDetails(models.Model):
    # batch upload fields
    batchUpload = models.ForeignKey(BatchUploads)
    errors = models.CharField(
        max_length=10000, null=True, blank=True)
    row_type = models.CharField(
        max_length=50, null=True, blank=True)


    # series fields
    series00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SR Internal ID')
    series00series_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SR Series ID')
    series00title = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='SR Title')
    series00series_type = models.CharField(
        max_length=100, null=True, blank=True,
        verbose_name='SR Series Type')
    series00is_public = models.CharField(
        max_length=100, null=True, blank=True,
        verbose_name='SR Is Public')
    series00description = models.CharField(
        max_length=10000, null=True, blank=True,
        verbose_name='SR Description')
    series00publication = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='SR Publication')
    series00sra_geo_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SR SRA/GEO ID')


    # biosample fields
    biosample00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Internal ID')
    biosample00biosample_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Biosample ID')
    biosample00biosample_type = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Biosample Type')
    biosample00biosample_lab = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Lab')
    biosample00genetic_background = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Genetic Background')
    biosample00stage = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Stage')
    biosample00controlled_by = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Controlled By')
    biosample00anatomical_term = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Anatomical Term')
    biosample00organism = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Organism')
    biosample00treatment = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Treatment')
    biosample00post_fertilization = models.CharField(
        max_length=100, null=True, blank=True,
        verbose_name='BS Hours Post Fertilization')
    biosample00source = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Source')
    biosample00sex = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Sex')
    biosample00mutation_description = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='BS Mutation Description')
    biosample00cell_line_type = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BS Cell Line Type')
    biosample00description = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='BS Description')


    # biosamplereplicate fields
    biosamplereplicate00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BR Internal ID')
    biosamplereplicate00biosamplereplicate_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='BR BiosampleReplicate ID')


    # assay fields
    assay00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AS Internal ID')
    assay00assay_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AS Assay ID')
    assay00assay_type = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AS Assay Type')
    assay00assay_lab = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AS Lab')
    assay00description = models.CharField(
        max_length=2000, null=True, blank=True,
        verbose_name='AS Description')
    assay00target = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AS Target')
    assay00library_prep = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AS Library Preparation')


    # appliedassay fields
    appliedassay00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AA Internal ID')
    appliedassay00appliedassay_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AA Appliedassay ID')
    appliedassay00controlled_by = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='AA Controlled By')


    # technicalreplicate fields
    technicalreplicate00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='TR Internal ID')
    technicalreplicate00technicalreplicate_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='TR TechnicalReplicate ID')


    # sequencing fields
    sequencing00internal_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Internal ID')
    sequencing00sequencing_id = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Sequencing ID')
    sequencing00platform = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Platform')
    sequencing00sequencing_lab = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Lab')
    sequencing00instrument = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Instrument')
    sequencing00chemistry_version = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Chemistry Version')
    sequencing00paired_end = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Paired End')
    sequencing00strand_mode = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Strand Mode')
    sequencing00sequencing_date = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name  = 'SQ Date')
    sequencing00max_read_length = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name='SQ Max Read Length')



    # data fields
    data00url = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='DT URL')
    data00local_path = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='DT Local Path')
    data00secondary_url = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='DT 2nd URL')
    data00secondary_local_path = models.CharField(
        max_length=1000, null=True, blank=True,
        verbose_name='DT 2nd Local Path')
