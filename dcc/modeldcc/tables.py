import django_tables2 as tables

from django.utils.safestring import mark_safe

from .models import Series, Biosample, BiosampleReplicate, Assay, AppliedAssay,\
TechnicalReplicate, Sequencing, Data, BatchUploadDetails, statuses,\
biosample_types,labs, anatomical_terms, genetic_backgrounds,\
developmental_stages, sexes, assay_types, platforms, instruments, strand_modes,\
file_types, mapped_genomes


## define tables to show data
class SeriesTable(tables.Table):
#    short_desc = tables.Column(verbose_name='Description')

    short_desc = tables.TemplateColumn("{{ record.short_desc }}", verbose_name = "Series Description", orderable=False)
    series_id = tables.TemplateColumn(
        "<a href='/dcc/detailSeries/{{ record.id }}'>" + \
        "{{record.series_id}}</a>")
    user = tables.Column('user', order_by='user__username')
    stages = tables.Column(accessor = 'biosample.series_biosample')
    # download = tables.TemplateColumn(
        # "<button id='btn-export_csv' class='btn btn--download btn-secondary'>\
        # <a href={% url 'csv_export' %}?series_id={{record.series_id}}>Download</a>\
    # </button>", verbose_name = "Download Annotations", orderable=False)

    class Meta:
        model = Series
        attrs = {"class": "table series_table", "id":"series_table",
                 'disp_name':'Series'}
        orderable = False
        # fields = ("series_id", "title", "keywords", "series_lab", "publication", "series_type", "description", "sra_geo_id", "is_public", "status", "date_edited" )
        exclude = ['date_edited', 'user_edited', 'description', 'id','is_public', 'user_edited', 'sra_geo_id']

class BioTable(tables.Table):
#    short_desc = tables.Column(verbose_name='Description')
    series = tables.TemplateColumn(
        "<a href='/dcc/detailSeries/{{ record.series_id }}'>" + \
        "{{record.series.series_id}}</a>")
    controlled_by = tables.TemplateColumn(
        "<a href='/dcc/detailBiosample/{{ record.controlled_by_id }}'>" + \
        "{{ record.controlled_by.biosample_id }}</a>", orderable=False)
    biosample_id = tables.TemplateColumn(
        "<a href='/dcc/detailBiosample/{{ record.id }}'>" + \
        "{{record.biosample_id}}</a>")
    biosample_type = tables.Column('biosample_type', order_by='biosample_type__name')
    biosample_lab = tables.Column('biosample_lab', order_by='biosample_lab__name')
    genetic_background = tables.Column('genetic_background', order_by='genetic_background__name')
    stage = tables.Column('stage', order_by='stage__name')
    # anatomical_term = tables.Column('anatomical_term', order_by='anatomical_term__name')
    user = tables.Column('user', order_by='user__username')

    class Meta:
        model = Biosample
        attrs = {"class": "table  biosample_table",
                 "id":"biosample_table",
                 'disp_name':'Biosamples'}
        orderable = False
        exclude = ['id', 'description', 'date_added', 'user_edited', 'source',
                   'sex', 'cell_line_type','organism', 'date_edited','user']
        # fields = ("series", "biosample_id", "organism", "biosample_type", \
        #           "source", "biosample_lab", "cell_type", "biological_background", "sex", \
        #           "genotype", "developmental_stage", "post_fertilization", \
        #           "anatomy_term", "user_added", "date_added", "controlled_by")

class AssayTable(tables.Table):
    short_desc = tables.Column(verbose_name='Description', orderable=False)
    assay_id = tables.TemplateColumn(
        "<a href='/dcc/detailAssay/{{ record.id }}'>" + \
        "{{record.assay_id}}</a>", orderable=False)

    assay_type = tables.Column('assay_type', order_by='assay_type__name')
    assay_lab = tables.Column('assay_lab', order_by='assay_lab__name')

    user = tables.Column('user', order_by='user__username')

    class Meta:
        model = Assay
        orderable = False
        attrs = {"class": "table  assay_table", "id":"assay_table",
                 'disp_name':'Assays'}
        exclude = ['id', 'date_added',
                   'user_edited', 'details', 'date_edited', 'description']

class AppliedAssayTable(tables.Table):
    # short_desc = tables.Column(verbose_name='Description')
    biosamplereplicate = tables.TemplateColumn(
        "<a href='/dcc/detailBiosample/{{ record.biosamplereplicate.biosample_id }}'>" + \
        "{{ record.biosamplereplicate.biosamplereplicate_id }}</a>")
    controlled_by = tables.TemplateColumn(
        "<a href='/dcc/detailAppliedAssay/{{ record.controlled_by_id }}'>" + \
        "{{ record.controlled_by.appliedassay_id }}</a>", orderable=False)
    assay = tables.TemplateColumn(
        "<a href='/dcc/detailAssay/{{ record.assay_id }}'>" + \
        "{{ record.assay.assay_id }}</a>")
    # appliedassay_id = tables.TemplateColumn(
    #     "<a href='/dcc/detailAppliedAssay/{{ record.id }}'>" + \
    #     "{{record.appliedassay_id}}</a>", orderable=False)
    # details = tables.TemplateColumn(
    #     "<a href='/dcc/detailAppliedAssay/{{ record.id }}'>" + \
    #     "Details</a>", orderable=False)
    class Meta:
        model = AppliedAssay
        attrs = {"class": "table  applied_assay_table", 'disp_name':'Applied Assay'}
        exclude = ['id', 'date_added',
                   'user_edited', 'details', 'date_edited', 'date_added', 'user'
                   ,'description']

class SequencingTable(tables.Table):
    sequencing_id = tables.TemplateColumn(
        "<a href='/dcc/detailSequencing/{{ record.id }}'>" + \
        "{{record.sequencing_id}}</a>", orderable=False)
    class Meta:
        model = Sequencing
        attrs = {"class": "table  sequencing_table", 'disp_name':'Sequencings'}
        exclude = ['id', 'description', 'date_added',
                    'user_edited', 'date_edited', 'date_added', 'user',
                    'description']

class DataTable(tables.Table):
    short_desc = tables.Column(verbose_name='Description')
    details = tables.TemplateColumn(
        "<a href='/dcc/detailData/{{ record.id }}'>" + \
        "Details</a>", orderable=False)
    class Meta:
        model = Data
        attrs = {"class": "table  data_table", 'disp_name':'Data'}
        exclude = ['id', 'description', 'date_added',
                    'user_edited', 'primary_source', 'secondary_source',
                    'command', 'mapped_genome']

class ErrorColumn(tables.Column):
    def __init__(self, classname=None, *args, **kwargs):
        self.classname=classname
        super(ErrorColumn, self).__init__(*args, **kwargs)

    def render(self, value):
        return mark_safe("<div class='" + self.classname + "' >" +value+"</div>")

class BatchUploadTable(tables.Table):
    errors = ErrorColumn(classname='error_column')
    class Meta:
        model = BatchUploadDetails
        attrs = {"class": "table ",
                 'disp_name':'Batch Upload'}
        exclude = ['batchUpload']
        empty_text = 'None Submitted'

class StatusesTable(tables.Table):
    class Meta:
        model = statuses
        attrs = {"class": "table  series_table", 'disp_name':'status'}
        exclude = {'id'}

class BiosampleTypesTable(tables.Table):
    class Meta:
        model = biosample_types
        attrs = {"class": "table  biosample_table", 'disp_name':'biosample type'}
        exclude = {'id'}

class LabsTable(tables.Table):
    class Meta:
        model = labs
        attrs = {"class": "table  labs_table", 'disp_name':'labs'}
        exclude = {'id', 'lab_id', 'date_added'}

class AnatomicalTermsTable(tables.Table):
    class Meta:
        model = anatomical_terms
        attrs = {"class": "table  biosample_table", 'disp_name':'anatomical terms'}
        exclude = {'id'}

class GeneticBackgroundsTable(tables.Table):
    class Meta:
        model = genetic_backgrounds
        attrs = {"class": "table  biosample_table", 'disp_name':'genetic backgrounds'}
        exclude = {'id'}

class DevelopmentalStagesTable(tables.Table):
    class Meta:
        model = developmental_stages
        attrs = {"class": "table   biosample_table", 'disp_name':'developmental stages'}
        exclude = {'id'}

class SexesTable(tables.Table):
    class Meta:
        model = sexes
        attrs = {"class": "table  biosample_table", 'disp_name':'sexes'}
        exclude = {'id'}

class AssayTypesTable(tables.Table):
    class Meta:
        model = assay_types
        attrs = {"class": "table  assay_table", 'disp_name':'assay types'}
        exclude = {'id'}

class PlatformsTable(tables.Table):
    class Meta:
        model = platforms
        attrs = {"class": "table  sequencing_table", 'disp_name':'platforms'}
        exclude = {'id'}

class InstrumentsTable(tables.Table):
    class Meta:
        model = instruments
        attrs = {"class": "table  sequencing_table", 'disp_name':'instruments'}
        exclude = {'id'}

class StrandModesTable(tables.Table):
    class Meta:
        model = strand_modes
        attrs = {"class": "table  sequencing_table", 'disp_name':'strand modes'}
        exclude = {'id'}
