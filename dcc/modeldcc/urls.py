import os
from django.conf.urls import url, include
from django.contrib.auth.views import password_reset, password_reset_done, \
    password_change, password_reset_complete,\
    password_reset_confirm, logout
from django.conf import settings
from django.views.static import serve
from .views import index, visualization, SeriesDetail, BiosampleDetail, \
    CombinedView, AssayDetail, AppliedAssayDetail, SequencingDetail,\
    DataDetail, filePaths, addLab, addSeries, addBiosample, addAssay, \
    addAppliedAssay, addSequencing, addData, addDataPage, \
    announcements, csv_export, batchUpload, commitBatch,\
    dataExport, createDataExportJSON, createindexJSON,\
    delete, deleteFiles, deleteFilesByPath, updateFilePaths, \
    updateTemporaryFilePaths, addAnalyzedData,\
    updateAnalyzedData, updateFile, importDNANexus, getZippedFiles, termsPage, \
    helpPage, getURLs, login_interface, addFreezeTag, policyPage
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^dcc/$', index, name='index'),
    # url(r'viewBiosamples/$', BiosampleView, name='viewBio'),
    # url(r'viewSeries/$', SeriesView, name='viewSeries'),
    url(r'visualization/$', visualization, name='visualization'),
    url(r'detailSeries/(?P<series_id>[0-9]+)/$',
        SeriesDetail, name='detailSeries'),
    url(r'detailBiosample/(?P<biosample_id>[0-9]+)/$',
        BiosampleDetail, name='detailBio'),
    url(r'viewCombined/$', CombinedView, name='viewCombined'),
    url(r'detailAssay/(?P<assay_id>[0-9]+)/$',
        AssayDetail, name='detailAssay'),
    url(r'detailAppliedAssay/(?P<appliedassay_id>[0-9]+)/$',
        AppliedAssayDetail, name='detailAppliedAssay'),
    url(r'detailSequencing/(?P<sequencing_id>[0-9]+)/$',
        SequencingDetail, name='detailSequencing'),
    url(r'detailData/(?P<data_id>[0-9]+)/$',
        DataDetail, name='detailData'),
    url(r'filepaths/$', filePaths, name='filePaths'),
    url(r'announcements/$', announcements, name='announcements'),
    url(r'addLab/$', addLab, name='addLab'),
    url(r'addSeries/$', addSeries, name='addSeries'),
    url(r'addBiosample/$', addBiosample, name='addBio'),
    url(r'addAssay/$', addAssay, name='addAssay'),
    url(r'addAppliedAssay/$', addAppliedAssay, name='addAppliedAssay'),
    url(r'addSequencing/$', addSequencing, name='addSequencing'),
    url(r'addData/$', addData, name='addData'),
    url(r'addDataPage/$', addDataPage, name='addDataPage'),
    url(r'addAssay/(?P<biosample_pk>[0-9]+)/$',
        addAssay, name='addAssay'),
    url(r'dataExport/$', dataExport, name='dataExport'),
    url(r'createDataExportJSON/$', createDataExportJSON, name='createDataExportJSON'),
    url(r'createindexJSON/$', createindexJSON, name='createindexJSON'),
    url(r'help/', helpPage, name='help'),
    url(r'terms/', termsPage, name='terms'),
    url(r'login_interface/', login_interface),
    url(r'policy/', policyPage, name='policy'),
    url(r'^users/', include('registration.backends.hmac.urls')),
    url(r'^password/reset/$', password_reset,
        {'post_reset_redirect': '/password/reset/complete/'}),
    url(r'^password/reset/complete/$', password_reset_done),
    url(r'^password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', password_reset_confirm,
        {'post_reset_redirect': '/password/complete/'}),
    url(r'^password/change/$', password_change),
    url(r'^password/complete/$', password_reset_complete),
    url(r'^logout/$', logout,{'next_page': '/dcc/'}),
    url(
        r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('images/favicons/favicon-32x32.png'),
            permanent=False),
        name="favicon"),
    # ...

    #    url(r'addDocument/(?P<experiment_pk>[0-9]+)/$',
    #        addDoc, name='addDoc'),
    #    url(r'addFile/(?P<bioexper_pk>[0-9]+)/$',
    #        addFile, name='addFile'),
    #    url(r'autocomplete/', include('autocomplete_light.urls')),
    url(r'^delete/$', delete),
    url(r'^deleteFiles/$', deleteFiles),
    url(r'^deleteFilesByPath/$', deleteFilesByPath),
    url(r'^addFreezeTag/$', addFreezeTag),
    url(r'^updateFilePaths/$', updateFilePaths),
    url(r'^updateTemporaryFilePaths/$', updateTemporaryFilePaths),
    url(r'^exportcsv/', csv_export, name ='csv_export'),
    url(r'^addAnalyzedData/$', addAnalyzedData, name='addAnalyzedData'),
    url(r'^updateAnalyzedData/$', updateAnalyzedData, name='updateAnalyzedData'),
    url(r'^updateFile/$', updateFile, name='updateFile'),
    url(r'^importDNANexus/$', importDNANexus, name='importDNANexus'),
    url(r'^getZippedFiles/$', getZippedFiles, name='getZippedFiles'),
    url(r'^getURLs/?$', getURLs, name='getURLs'),
    url(r'batchUpload/', batchUpload, name='batchUpload'),
    url(r'commitBatch/(?P<batch_pk>[0-9]+)/$',
        commitBatch, name='commitBatch'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT,
    }),
    url(r'^trackhub/(?P<path>.*)$', serve, {
        'document_root': os.path.join(settings.MEDIA_ROOT_DCC,'trackhub'),
    }),
    url(r'^freezer/(?P<path>.*)$', serve, {
        'document_root': os.path.join(settings.MEDIA_ROOT_DCC,'freezer'),
    }),
]
