from django.contrib import admin
# from django.contrib.admin import ModelAdmin
# from django.contrib.admin import SimpleListFilter

from .models import Series, Biosample, BiosampleReplicate, Assay, \
    AppliedAssay, TechnicalReplicate, Sequencing, Data, BatchUploads, \
    BatchUploadDetails, statuses, biosample_types,labs, \
    anatomical_terms, genetic_backgrounds, developmental_stages, \
    sexes, assay_types, platforms, instruments, strand_modes, \
    file_types, mapped_genomes, Versions



class BiosampleAdmin(admin.ModelAdmin):
    list_filter = ('biosample_lab__name','stage','post_fertilization')
class AssayAdmin(admin.ModelAdmin):
    list_filter = ('assay_type','assay_lab__name')
admin.site.register(Series)
admin.site.register(Biosample, BiosampleAdmin)
admin.site.register(BiosampleReplicate)
admin.site.register(Assay, AssayAdmin)
admin.site.register(AppliedAssay)
admin.site.register(TechnicalReplicate)
admin.site.register(Sequencing)
admin.site.register(Data)
admin.site.register(BatchUploads)
admin.site.register(BatchUploadDetails)
admin.site.register(statuses)
admin.site.register(biosample_types)
admin.site.register(labs)
admin.site.register(anatomical_terms)
admin.site.register(genetic_backgrounds)
admin.site.register(developmental_stages)
admin.site.register(sexes)
admin.site.register(assay_types)
admin.site.register(platforms)
admin.site.register(instruments)
admin.site.register(strand_modes)
admin.site.register(file_types)
admin.site.register(mapped_genomes)
admin.site.register(Versions)
