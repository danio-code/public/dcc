# -*- coding: utf-8 -*-
from django.core import mail
from django.contrib.auth.models import AnonymousUser, User, Permission, Group
from django.test import RequestFactory, TestCase, Client
from django.urls import reverse
from django.contrib.staticfiles import finders
from django.core.cache import cache
from django.conf import settings

from model_mommy import mommy

import json
import os
import csv
import io
import re

from modeldcc.views import addSeries, index, createDataExportJSON, \
    createindexJSON, CombinedView
from modeldcc.models import Series, Biosample, BiosampleReplicate, Assay, \
    AppliedAssay, TechnicalReplicate, Sequencing, Data, BatchUploads, \
    BatchUploadDetails, statuses, biosample_types, labs, \
    anatomical_terms, genetic_backgrounds, developmental_stages, \
    sexes, assay_types, platforms, instruments, strand_modes, \
    file_types, mapped_genomes, Versions, generate_sha


class ViewsTest(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        cache.clear()
        group_name = "test"
        self.group = Group.objects.get(name=group_name)
        self.c = Client()
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test", email="test@test.com", password="test")
        self.user.groups.add(self.group)
        self.user.save()
        self.c.login(username='test', password='test')

    def test_viewHelpPage(self):

        response = self.c.get('/help/')
        self.assertEqual(response.status_code, 200)

    def test_viewTermsPage(self):

        response = self.c.get('/terms/')
        self.assertEqual(response.status_code, 200)

    def test_addSeries_not_signed_in(self):
        request = self.factory.get('/dcc/addSeries')

        request.user = AnonymousUser()

        response = addSeries(request)
        self.assertEqual(response.status_code, 302)

    def test_addSeries_signed_in(self):
        response = self.c.get('/dcc/addSeries/')
        self.assertEqual(response.status_code, 200)

    def test_filePaths(self):
        response = self.c.get('/filepaths/')
        self.assertEqual(response.status_code, 200)

    def test_viewpolicy(self):
        response = self.c.get('/policy/')
        self.assertEqual(response.status_code, 200)

    def test_announcements(self):
        response = self.c.get('/announcements/')
        self.assertEqual(response.status_code, 200)

    def test_viewCombined(self):
        response = self.c.get('/viewCombined/')
        self.assertEqual(response.status_code, 200)

    def test_viewCombined_not_signed_in(self):
        request = self.factory.get('/viewCombined/')

        request.user = AnonymousUser()

        response = CombinedView(request)
        self.assertEqual(response.status_code, 302)

    def test_index(self):
        response = self.c.get('/dcc/')
        self.assertEqual(response.status_code, 200)


class DetailsViewsTest(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        cache.clear()
        group_name = "test"
        self.group = Group.objects.get(name=group_name)
        self.c = Client()
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test", email="test@test.com", password="test")
        self.user.groups.add(self.group)
        self.user.save()
        self.c.login(username='test', password='test')

    def test_detailSeriesView(self):
        pk = str(Series.objects.all()[0].pk)
        response = self.c.get('/dcc/detailSeries/' + pk + '/')
        self.assertEqual(response.status_code, 200)

    def test_detailBiosampleView(self):
        pk = str(Biosample.objects.all()[0].pk)
        response = self.c.get('/dcc/detailBiosample/' + pk + '/')
        self.assertEqual(response.status_code, 200)

    def test_detailAssayView(self):
        pk = str(Assay.objects.all()[0].pk)
        response = self.c.get('/dcc/detailAssay/' + pk + '/')
        self.assertEqual(response.status_code, 200)

    def test_detailAppliedAssayView(self):
        pk = str(AppliedAssay.objects.all()[0].pk)
        response = self.c.get('/dcc/detailAppliedAssay/' + pk + '/')
        self.assertEqual(response.status_code, 200)

    def test_detailSequencingView(self):
        pk = str(Sequencing.objects.all()[0].pk)
        response = self.c.get('/dcc/detailSequencing/' + pk + '/')
        self.assertEqual(response.status_code, 200)

    def test_detailDataView(self):
        pk = str(Data.objects.all()[0].pk)
        response = self.c.get('/dcc/detailData/' + pk + '/')
        self.assertEqual(response.status_code, 200)


class APItest(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        group_name = "test"
        self.group = Group.objects.get(name=group_name)
        self.c = Client()
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test", email="test@test.com", password="test")
        self.user.groups.add(self.group)
        self.user.save()

    def test_login_interface(self):
        response = self.c.post(
            '/login_interface/', {
                "username": "test",
                "password": "test",
            })
        self.assertEqual(response.content, "fine")

    def test_csv_export(self):
        id = Series.objects.all()[0].series_id
        self.c.login(username='test', password='test')

        response = self.c.get('/exportcsv?series_id=' + str(id), follow=True)
        content = response.content.decode('utf-8')

        cvs_reader = csv.reader(io.StringIO(content))
        body = list(cvs_reader)[1]
        self.assertTrue(id in body)

    def test_zip_export(self):
        primary_url = Data.objects.all()[0].primary_file.url
        secondary_url = Data.objects.all()[0].secondary_file.url

        path = os.path.join(settings.MEDIA_URL, primary_url[7:])
        path = path + "," + os.path.join(settings.MEDIA_URL, secondary_url[7:])
        response = self.c.get(
            '/getZippedFiles/?filepaths=' + path)

        self.assertEqual(
            response['Content-Disposition'],
            "attachment; filename=dcc_data_export.tar.gz"
        )

    def test_URL_export(self):
        primary_url = Data.objects.all()[0].primary_file.url
        secondary_url = Data.objects.all()[0].secondary_file.url

        list = {"filepaths": [primary_url, secondary_url]}
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/getURLs/', jsonFile, content_type="application/json")

        self.assertEquals(
            response['Content-Disposition'],
            "attachment; filename=dcc_url_export.txt"
        )


class AddAnnotationTest(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        group_name = "test"
        self.group = Group.objects.get(name=group_name)
        self.c = Client()
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test", email="test@test.com", password="test")
        self.user.groups.add(self.group)
        self.user.save()
        self.c.login(username='test', password='test')

    def test_addSeries(self):
        list = [{"fields": {
            "series_id": "",
            "series_title": "test",
            "series_publication": "test",
            "series_type": "test",
            "series_description": "test",
            "series_is_public": "True",
            "series_geo_id": "test"
        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addSeries/', jsonFile, content_type="application/json")
        self.assertTrue("series_id" in json.loads(response.content))

    def test_addBiosample(self):
        series_id = Series.objects.all()[0].series_id
        list = [{"fields": {
            "series_id": series_id,
            "biosample_id": "",
            "biosample_lab": "test lab",
            "biosample_sample_type": "cell line",
            "biosample_genetic_background": "AB",
            "biosample_mutation_description": "",
            "biosample_anatomical_term": "",
            "biosample_sex": "",
            "biosample_developmental_stage": "",
            "biosample_treatment": "",
            "biosample_cell_line_type": "",
            "biosample_description": "test",
            "biosample_post_fertilization": "",
            "biosample_source": "",
            "num_replicates": 2,
            "control": None
        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addBiosample/', jsonFile, content_type="application/json")
        self.assertTrue("biosamplereplicate_id" in json.loads(
            response.content)[0])

    def test_addAssay_known_lab_and_assay(self):
        list = [{"fields": {
            "assay_type": "RNA-seq",
            "assay_lab": "test lab",
            "lib_prep": "",
            "assay_description": "test",
            "target": "",
        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addAssay/', jsonFile, content_type="application/json")
        self.assertTrue("assay_id" in json.loads(response.content)[0])

    def test_addAssay_known_lab(self):
        list = [{"fields": {
            "assay_type": "ATAC-seq",
            "assay_lab": "test lab",
            "lib_prep": "",
            "assay_description": "test",
            "target": "",
        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addAssay/', jsonFile, content_type="application/json")
        self.assertTrue("assay_id" in json.loads(response.content)[0])

    def test_addAssay_update_description(self):
        list = [{"fields": {
            "assay_type": "ATAC-seq",
            "assay_lab": "test lab",
            "lib_prep": "",
            "assay_description": "test test",
            "target": "",
        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addAssay/', jsonFile, content_type="application/json")
        self.assertTrue("assay_id" in json.loads(response.content)[0])

    def test_addAppliedAssay(self):
        assay = Assay.objects.all()[0].assay_id.replace("AS", "")
        biosamplerep = BiosampleReplicate.objects.all()[
            0].biosamplereplicate_id
        list = [{"fields": {
            "appliedAssay_id": assay,
            "biosamplereplicate_id": biosamplerep,
            "appliedAssay_num_tech_replicates": 2,

        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addAppliedAssay/', jsonFile, content_type="application/json")
        self.assertTrue("technicalReplicate_id" in json.loads(
            response.content)[0])

    def test_addSequencing(self):
        technicalrep = TechnicalReplicate.objects.all()[
            0].technicalreplicate_id
        list = [{"fields": {
            "technicalReplicate_id": technicalrep,
            "sequencing_lab": "test lab",
            "sequencing_platform": "Illumina",
            "sequencing_instrument": "HiSeq 2000",
            "sequencing_strand_mode": "forward",
            "sequencing_max_read_length": "",
            "sequencing_chem_version": "",
            "sequencing_paired_end": "paired end",
            "sequencing_date": "2000-01-01"

        }}]
        jsonFile = json.dumps(list)
        response = self.c.post(
            '/addSequencing/', jsonFile, content_type="application/json")
        self.assertTrue("sequencing_id" in json.loads(
            response.content)[0])

    def test_addData_local_file(self):
        series_pk = Series.objects.all()[0].id
        sequencing = mommy.make('Sequencing',
            technicalreplicate=TechnicalReplicate.objects.all()[0]).sequencing_id
        data_primary_file = "test/test_lab.02SQ.test.R1.fastq.gz"
        data_secondary_file = "test/test_lab.02SQ.test.R2.fastq.gz"
        list = [{"fields": {
            "sequencing_id": sequencing,
            "data_primary_file": data_primary_file,
            "data_secondary_file": data_secondary_file,
        }}]
        jsonFile = json.dumps(list)
        response = self.c.post('/addData/', jsonFile, content_type="application/json")
        # delete files created for this test
        data_ids = Data.objects.filter(sequencing__sequencing_id=sequencing)
        files = {"id": []}
        for data_id in data_ids:
            files["id"].append({"dataid": data_id.data_id})
        self.c.post(
            '/deleteFiles/', json.dumps(files), content_type="application/json")

        self.assertEqual(response.content, str(series_pk))

    def test_addLab(self):
        response = self.c.post(
            '/addLab/',
            {
                "lab_name": "test",
                "lab_pi": "test PI",
                "lab_institution": "test institute",
                "lab_country": "test country"

            })
        self.assertEqual(response.context["error"], None)

    def test_addLab_with_known_lab(self):
        response = self.c.post(
            '/addLab/',
            {"lab_name": "test lab"}
        )

        self.assertEqual(response.context["error"], "Lab name exists already")

    def test_addFreezeTag(self):
        id = Data.objects.all()[0].data_id
        response = self.c.post(
            '/addFreezeTag/',
            json.dumps({'id': [{'dataid': id, 'freeze_tag': 'test'}, ]}),
            content_type="application/json"
        )
        self.assertJSONEqual(json.loads(response.content), {"success": True})

    def test_seriesDelete(self):
        id = Series.objects.all()[0].series_id
        response = self.c.post(
            '/delete/',
            {"id": id}
        )
        self.assertJSONEqual(json.loads(response.content), {"success": True})

    def test_updateFilePaths(self):
        id = Series.objects.all()[0].series_id
        id_list = [{"series_id": id}, ]
        response = self.c.post('/updateFilePaths/', json.dumps(id_list),
                               content_type="application/json")
        self.c.post('/updateTemporaryFilePaths/', json.dumps(id_list),
                    content_type="application/json")
        self.assertEqual(
            response.get('Content-Disposition'),
            'attachment; filename="updatedData.csv"'
        )

    def test_dataDelete(self):
        self.data = mommy.make('Data', _create_files=True,
                               sequencing=Sequencing.objects.all()[0])
        id = self.data.data_id
        file = {"id": [{"dataid": id}, ]}
        response = self.c.post(
            '/deleteFiles/', json.dumps(file), content_type="application/json")

        self.assertJSONEqual(json.loads(response.content), {"success": True})

    def test_dataDelete_by_Path(self):
        self.data = mommy.make('Data', _create_files=True,
                               sequencing=Sequencing.objects.all()[0])
        primary_path = self.data.primary_file.path
        file = {"id": [{"path": primary_path}, ]}
        response = self.c.post(
            '/deleteFilesByPath/', json.dumps(file), content_type="application/json")

        self.assertJSONEqual(json.loads(response.content), {"success": True})

    def test_addAnalyzedData(self):
        # self.data = mommy.make('Data', _create_files=True,
        #                        sequencing=Sequencing.objects.all()[0])
        path = "test/test_lab.02SQ.test.R1.fastq.gz"
        derived_from = Data.objects.all()[0].data_id
        file_type = file_types.objects.all()[1].name
        genome = mapped_genomes.objects.all()[0].name
        file = {"primary_source": path,
                "primary_file": path,
                "secondary_source": "",
                "secondary_file": "",
                "derived_from": derived_from,
                "file_type": file_type,
                "mapped_genome": genome,
                "derived_from_data": [derived_from, ],
                "command": "test"}
        response = self.c.post(
            '/addAnalyzedData/', json.dumps(file), content_type="application/json")
        self.assertTemplateUsed(
            response, 'modeldcc/importDNANexusSuccess.html')
        self.assertEqual(response.context["already_exists"], False)
        id = response.context["datum"].data_id
        file = {"id": [{"dataid": id}, ]}
        response = self.c.post(
            '/deleteFiles/', json.dumps(file), content_type="application/json")


    def test_addAnalyzedData_already_exists(self):
        self.data = mommy.make('Data',  _create_files=True,
                               sequencing=Sequencing.objects.all()[0])
        path = self.data.primary_file.name
        source = self.data.primary_source
        derived_from = Data.objects.all()[0].data_id
        file_type = file_types.objects.all()[0].name
        genome = mapped_genomes.objects.all()[0].name
        file = {"primary_source": source,
                "primary_file": path,
                "secondary_source": "",
                "secondary_file": "",
                "derived_from": derived_from,
                "file_type": file_type,
                "mapped_genome": genome,
                "derived_from_data": [derived_from, ],
                "command": "test"}
        response = self.c.post(
            '/addAnalyzedData/', json.dumps(file), content_type="application/json")
        self.assertTemplateUsed(
            response, 'modeldcc/importDNANexusSuccess.html')
        self.assertEqual(response.context["already_exists"], True)
        id = response.context["datum"].data_id
        file = {"id": [{"dataid": id}, ]}
        response = self.c.post(
            '/deleteFiles/', json.dumps(file), content_type="application/json")
        # file = {"id": [{"path": path},]}
        # self.c.post(
        #     '/deleteFilesByPath/', json.dumps(file), content_type="application/json")

    def test_updateAnalyzedData(self):
        self.data = Data.objects.all()[0]
        path = self.data.primary_file.url[6:]
        source = self.data.primary_source
        derived_from = Data.objects.all()[0].data_id
        file_type = file_types.objects.all()[0].name
        file = [{"primary_source": source,
                 "primary_file": path,
                 "secondary_source": path,
                 "secondary_file": path,
                 "derived_from": derived_from,
                 "file_type": file_type,
                 "mapped_genome_id": 1,
                 "derived_from_data": [derived_from, ],
                 "command": "test"}, ]
        response = self.c.post(
            '/updateAnalyzedData/', json.dumps(file), content_type="application/json")
        self.assertEquals(
            response.get('Content-Disposition'),
            'attachment; filename="updatedData.csv"'
        )

    def test_updateFile(self):
        self.data = Data.objects.all()[0]
        primary_path = self.data.primary_file.name
        secondary_path = self.data.secondary_file.name
        data_id = Data.objects.all()[0].data_id
        file = [{"primary_file": primary_path,
                 "secondary_file": secondary_path,
                 "data_id": data_id}, ]
        response = self.c.post(
            '/updateFile/', json.dumps(file), content_type="application/json")
        self.assertEquals(
            response.get('Content-Disposition'),
            'attachment; filename="updatedData.csv"'
        )
        content = response.content.decode('utf-8')

        cvs_reader = csv.reader(io.StringIO(content))
        body = list(cvs_reader)[0]
        path = body[1]
        file = {"id": [{"path": path}, ]}
        self.c.post(
            '/deleteFilesByPath/', json.dumps(file), content_type="application/json")


class dccBatchUpload(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        group_name = "test"
        self.group = Group.objects.get(name=group_name)
        self.c = Client()
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test", email="test@test.com", password="test")
        self.user.groups.add(self.group)
        self.user.save()
        self.c.login(username='test', password='test')

    def test_batchUpload(self):
        file = finders.find('Sample_Batch_Upload.csv')
        # with open(file,'rb') as fp:
        #     response = self.c.post('/batchUpload/', {'file': fp}, format='multipart')
        myfile = open(file, 'r')
        response = self.c.post('/batchUpload/', {'csvFile': myfile})
        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadSuccess.html')
        myfile.close()
        os.remove(
            "dcc/modeldcc/media_root/batch_upload_csv/test.True.Sample_Batch_Upload.csv.csv")



    def test_invalid_batchUpload(self):
        file = finders.find('Invalid_Sample_Batch_Upload.csv')
        # with open(file,'rb') as fp:
        #     response = self.c.post('/batchUpload/', {'file': fp}, format='multipart')
        myfile = open(file, 'r')
        response = self.c.post('/batchUpload/', {'csvFile': myfile})
        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadFail.html')
        myfile.close()
        os.remove(
            "dcc/modeldcc/media_root/batch_upload_csv/test.False.Invalid_Sample_Batch_Upload.csv.csv")


    def test_commitBatch(self):
        file = finders.find('Sample_Batch_Upload.csv')
        # with open(file,'rb') as fp:
        #     response = self.c.post('/batchUpload/', {'file': fp}, format='multipart')
        myfile = open(file, 'r')
        myfile.seek(0)
        response = self.c.post('/batchUpload/', {'csvFile': myfile})
        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadSuccess.html')
        url = re.findall("commitBatch.*'\"", response.content)

        batch_id = url[0].split("/")[1].replace("'\"", "")
        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadSuccess.html')
        response = self.c.post('/commitBatch/' + batch_id + '/')
        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadCommit.html')
        os.remove(
            "dcc/modeldcc/media_root/batch_upload_csv/test.True.Sample_Batch_Upload.csv.csv")

        series_id = response.context['added_series'][0][0]
        data_ids = Data.objects.filter(
            sequencing__technicalreplicate__appliedassay__biosamplereplicate__biosample__series__series_id=series_id)
        file = {"id": []}
        for data_id in data_ids:
            file["id"].append({"dataid": data_id.data_id})
        self.c.post(
            '/deleteFiles/', json.dumps(file), content_type="application/json")

    def test_url_batchUpload(self):
        file = finders.find('Sample_Batch_URL_Upload.csv')
        # with open(file,'rb') as fp:
        #     response = self.c.post('/batchUpload/', {'file': fp}, format='multipart')
        myfile = open(file, 'r')
        myfile.seek(0)
        response = self.c.post('/batchUpload/', {'csvFile': myfile})

        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadSuccess.html')
        url = re.findall("commitBatch.*'\"", response.content)
        batch_id = url[0].split("/")[1].replace("'\"", "")

        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadSuccess.html')
        response = self.c.post('/commitBatch/' + batch_id + '/')
        self.assertTemplateUsed(
            response, 'modeldcc/batchUploadCommit.html')

        series_id = response.context['added_series'][0][0]
        data_ids = Data.objects.filter(
            sequencing__technicalreplicate__appliedassay__biosamplereplicate__biosample__series__series_id=series_id)
        file = {"id": []}
        for data_id in data_ids:
            file["id"].append({"dataid": data_id.data_id})
        self.c.post(
            '/deleteFiles/', json.dumps(file), content_type="application/json")
        myfile.close()
        os.remove(
            "dcc/modeldcc/media_root/batch_upload_csv/test.True.Sample_Batch_URL_Upload.csv.csv")

class dccPlotDataTestCase(TestCase):
    fixtures = ['initial_data.yaml']

    def setUp(self):
        self.c = Client()

    def test_visualization(self):
        response = self.c.get('/visualization/')
        self.assertEqual(response.status_code, 200)


class EmailTest(TestCase):
    def test_send_email(self):
        mail.send_mail('test email subject', 'test message.',
                       'no-reply@dcc.org', [
                           'm.hortenhuber@ki.se'],
                       fail_silently=False)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'test email subject')
