You have just created an account on the DCC website.

To activate your account, please click the following link within {{ expiration_days }} day.

http://{{site.domain}}{% url 'registration_activate' activation_key %}

To get the permission to upload annotations, please contact the DCC admins directly.

Sincerely,
the DCC admins
