from django import template
from django.conf import settings
from django.contrib.auth.models import Group

register = template.Library()

@register.filter(name='has_group')
def has_group(user):
    return user.groups.filter(name=settings.GROUP).exists()
