import json

from django import forms
from django.db import models
from django.contrib.auth.models import User

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Div, HTML

from .models import BatchUploads

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class BatchUploadForm(forms.ModelForm):
    class Meta:
        model = BatchUploads
        exclude = ['user', 'isValid']

    def __init__(self, *args, **kwargs):
        super(BatchUploadForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.label_class = 'col-form-label'
        self.helper.field_class = ''
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Div(                    Fieldset(
                    'CSV file',
                    'csvFile'),

                css_class='card card-body')
        )
