.. _views:

Views
======

The models are located at ``dcc/modeldcc/views.py``.

.. automodule:: modeldcc.views
    :members:
    :exclude-members: ID_FIELD
