Data Export
============

The data export page is a react app, with the following classes and methods, located at ``/dcc/dcc/static/js/dataExport.js``.

.. js:autofunction:: heatMapOptions
.. js:autofunction:: csrfSafeMethod
.. js:autofunction:: sortByKey
.. js:autofunction:: sortByNestedKey
.. js:autoclass:: FileTable
.. js:autofunction:: loadTableFromServer
.. js:autofunction:: updatePageCount
.. js:autofunction:: handlePageChange
.. js:autofunction:: createSeriesElements
.. js:autofunction:: clearFilter
.. js:autofunction:: searchUpdated
.. js:autofunction:: deleteItem
.. js:autofunction:: afterSelect
.. js:autofunction:: selectHeatMapOption
.. js:autofunction:: filterData
.. js:autofunction:: filterFacet
.. js:autofunction:: heatMap2SideBar
.. js:autofunction:: toggleView
.. js:autofunction:: downloadZip
.. js:autofunction:: downloadURL
.. js:autofunction:: selectAll
.. js:autofunction:: showTitleOn
.. js:autofunction:: showTitleOff
.. js:autoclass:: HeatMapSelect
.. js:autoclass:: Facet
.. js:autoclass:: FacetElements
.. js:autofunction:: toggleMore
.. js:autofunction:: handleClick
.. js:autoclass:: Biosamples
