.. dcc documentation master file, created by
   sphinx-quickstart on Fri Apr 12 09:42:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##################################
Welcome to \*-DCC's documentation!
##################################

****
Aim
****

We present here an annotation structure and user-hosted platform for sequencing experiment data, suitable for lab-internal documentation, collaborations and large-scale annotation efforts.

******************************
Implementations of \*-DCC
******************************

- The annotation structure and web platform is currently used for the DANIO-CODE project to annotate functional elements of the zebrafish genome: danio-code.zfin.org_
  |daniocode_logo|_


- An extended version of the annotation structure and the web platform is also used for the dog genome annotation project: doggenomeannotation.org_

  |doga_logo|_

.. _danio-code.zfin.org: https://danio-code.zfin.org
.. |daniocode_logo| image:: images/daniocode_logo.png
  :width: 400
.. _daniocode_logo: https://danio-code.zfin.org
.. _doggenomeannotation.org: https://www.doggenomeannotation.org/
.. |doga_logo| image:: images/doga_logo.png
  :width: 250
.. _doga_logo: https://www.doggenomeannotation.org/

******
Demos
******

Demo Instance
==============

An example implementation is available at dcc-demo.daublab.org_.

.. _dcc-demo.daublab.org: http://dcc-demo.daublab.org/

Demo Videos
===========

.. raw:: html

  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/pPVS6X6_f-0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-3WE2kYHDj8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/or3YqiVtGzk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. toctree::
   :maxdepth: 3
   :caption: Getting Started:

   installation

.. toctree::
   :maxdepth: 2
   :caption: Model & Method Definitions:

   views
   models
   dataExport


..
