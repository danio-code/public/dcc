.. _models:

Models
======

The models are located at ``dcc/modeldcc/models.py``.

.. automodule:: modeldcc.models
    :members:
