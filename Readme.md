## Documentation
You can find the documentation for this project including install instructions at [dcc.readthedocs.io](https://dcc.readthedocs.io).
## Demo Videos
View and export data and annotation:

[![](http://img.youtube.com/vi/pPVS6X6_f-0/mqdefault.jpg)](http://www.youtube.com/watch?v=pPVS6X6_f-0 "*-DCC: View and Export Annotations and Data")

Upload data and annotations with a csv-file:

[![](http://img.youtube.com/vi/-3WE2kYHDj8/mqdefault.jpg)](http://www.youtube.com/watch?v=-3WE2kYHDj8 "*-DCC: csv-File Upload of Annotation and Data")

Upload data and annotations with a web-form:

[![](http://img.youtube.com/vi/or3YqiVtGzk/mqdefault.jpg)](http://www.youtube.com/watch?v=or3YqiVtGzk "*-DCC: Web-form Upload of Annotation and Data")

## Funding
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No 643062.
