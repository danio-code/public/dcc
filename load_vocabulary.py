#!/usr/bin/python

import psycopg2
import sys
import json

con = None

try:
    con = psycopg2.connect(database='testdata', user='test-user', password='password')
    cur = con.cursor()

    names = [
        'anatomical_terms','biosample_types', 'developmental_stages',
        'file_types', 'assay_types', 'genetic_backgrounds',
        'instruments', 'mapped_genomes', 'platforms',
        'sexes', 'statuses', 'strand_modes', 'organisms']
    # import pdb; pdb.set_trace()
    for name in names:
        cur.execute("DELETE FROM modeldcc_" + name)
        with open('dcc/dcc/static/json/' + name + '.json') \
             as json_file:
            json_data = json.load(json_file)
        idx = 0
        for item in json_data:
            cur.execute(
                "INSERT INTO modeldcc_" +
                name + "(id, name) VALUES (%s, %s)",(idx, item['name']))
            idx+=1
        con.commit()

        cur.execute("SELECT * FROM modeldcc_" + name)

        print(cur.fetchone())

except psycopg2.DatabaseError, e:
    if con:
        con.rollback()

    print 'Error %s' % e
    sys.exit(1)

finally:
    if con:
        con.close()
