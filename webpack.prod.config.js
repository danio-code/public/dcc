const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const config = require('./webpack.base.config.js');
const TerserPlugin = require('terser-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

// config.output.publicPath = require('path').resolve(
//   './dcc/static/js/bundles/'
// );
console.log("-------production environment-------")
config.output.path = require('path')
  .resolve('./dcc/dcc/static/js/bundles/');
// config.output.filename = '[name]-[hash].js';
config.plugins = config.plugins.concat([
  new BundleTracker({
    filename: './webpack-stats-prod.json'
  }),

  // removes a lot of debugging code in React
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': '"production"'
  }),
  // keeps hashes consistent between compilations
  new webpack.optimize.OccurrenceOrderPlugin(),

  // helps optimizing bundle size
  // new BundleAnalyzerPlugin(),

  new WebpackCleanupPlugin()
]);
// config.optimization.minimize = true
config.optimization.minimizer = [
  new TerserPlugin({
        parallel: true,
        sourceMap: true
      })
]
// Add a loader for JSX files
config.module.rules.push({
  test: /\.jsx?$/,
  exclude: /node_modules/,
  loader: 'babel-loader',
  query: {
    //specify that we will be dealing with React code
    presets: [
      ['env', {
        modules: false
      }], 'react'
    ]
  }
});
config.mode = 'production'
module.exports = config;
