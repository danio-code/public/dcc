import gulp from 'gulp'
import Browser from 'browser-sync'
import webpack from 'webpack'
import WebpackDevServer from 'webpack-dev-server'
import webpackDevMiddleware from 'webpack-dev-middleware'

const webpackConfig = require(process.env.NODE_ENV === 'development' ? './webpack.local.config' : './webpack.prod.config');
const bundler = webpack(webpackConfig)

export const browser = Browser.create()
export function server() {

    let config = {
        // server: 'site',
        // files: 'dcc/static/css/*.css',
        proxy: '127.0.0.1:8000',
        open:false,
        middleware: [
            webpackDevMiddleware(bundler, {
            publicPath: webpackConfig.output.publicPath,
            stats: { colors: true, mode:process.env.NODE_ENV  }
          })],
    }

    browser.init(config)
}
