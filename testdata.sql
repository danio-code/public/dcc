--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO "test-user";

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO "test-user";

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO "test-user";

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO "test-user";

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO "test-user";

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO "test-user";

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO "test-user";

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO "test-user";

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO "test-user";

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO "test-user";

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO "test-user";

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO "test-user";

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: modeldcc_anatomical_terms; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_anatomical_terms (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_anatomical_terms OWNER TO "test-user";

--
-- Name: modeldcc_anatomical_terms_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_anatomical_terms_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_anatomical_terms_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_anatomical_terms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_anatomical_terms_id_seq OWNED BY public.modeldcc_anatomical_terms.id;


--
-- Name: modeldcc_appliedassay; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_appliedassay (
    id integer NOT NULL,
    appliedassay_id character varying(200),
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    assay_id integer NOT NULL,
    biosamplereplicate_id integer NOT NULL,
    controlled_by_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_appliedassay OWNER TO "test-user";

--
-- Name: modeldcc_appliedassay_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_appliedassay_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_appliedassay_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_appliedassay_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_appliedassay_id_seq OWNED BY public.modeldcc_appliedassay.id;


--
-- Name: modeldcc_assay; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_assay (
    id integer NOT NULL,
    assay_id character varying(200) NOT NULL,
    description character varying(2000) NOT NULL,
    target character varying(200) NOT NULL,
    library_prep character varying(200),
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    assay_lab_id integer NOT NULL,
    assay_type_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_assay OWNER TO "test-user";

--
-- Name: modeldcc_assay_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_assay_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_assay_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_assay_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_assay_id_seq OWNED BY public.modeldcc_assay.id;


--
-- Name: modeldcc_assay_types; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_assay_types (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_assay_types OWNER TO "test-user";

--
-- Name: modeldcc_assay_types_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_assay_types_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_assay_types_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_assay_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_assay_types_id_seq OWNED BY public.modeldcc_assay_types.id;


--
-- Name: modeldcc_batchuploaddetails; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_batchuploaddetails (
    id integer NOT NULL,
    errors character varying(10000),
    row_type character varying(50),
    series00internal_id character varying(200),
    series00series_id character varying(200),
    series00title character varying(1000),
    series00series_type character varying(100),
    series00is_public character varying(100),
    series00description character varying(10000),
    series00publication character varying(1000),
    series00sra_geo_id character varying(200),
    biosample00internal_id character varying(200),
    biosample00biosample_id character varying(200),
    biosample00biosample_type character varying(200),
    biosample00biosample_lab character varying(200),
    biosample00genetic_background character varying(200),
    biosample00stage character varying(200),
    biosample00controlled_by character varying(200),
    biosample00anatomical_term character varying(200),
    biosample00organism character varying(200),
    biosample00treatment character varying(200),
    biosample00post_fertilization character varying(100),
    biosample00source character varying(200),
    biosample00sex character varying(200),
    biosample00mutation_description character varying(1000),
    biosample00cell_line_type character varying(200),
    biosample00description character varying(1000),
    biosamplereplicate00internal_id character varying(200),
    biosamplereplicate00biosamplereplicate_id character varying(200),
    assay00internal_id character varying(200),
    assay00assay_id character varying(200),
    assay00assay_type character varying(200),
    assay00assay_lab character varying(200),
    assay00description character varying(2000),
    assay00target character varying(200),
    assay00library_prep character varying(200),
    appliedassay00internal_id character varying(200),
    appliedassay00appliedassay_id character varying(200),
    appliedassay00controlled_by character varying(200),
    technicalreplicate00internal_id character varying(200),
    technicalreplicate00technicalreplicate_id character varying(200),
    sequencing00internal_id character varying(200),
    sequencing00sequencing_id character varying(200),
    sequencing00platform character varying(200),
    sequencing00sequencing_lab character varying(200),
    sequencing00instrument character varying(200),
    sequencing00chemistry_version character varying(200),
    sequencing00paired_end character varying(200),
    sequencing00strand_mode character varying(200),
    sequencing00sequencing_date character varying(200),
    sequencing00max_read_length character varying(200),
    data00url character varying(1000),
    data00local_path character varying(1000),
    data00secondary_url character varying(1000),
    data00secondary_local_path character varying(1000),
    "batchUpload_id" integer NOT NULL
);


ALTER TABLE public.modeldcc_batchuploaddetails OWNER TO "test-user";

--
-- Name: modeldcc_batchuploaddetails_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_batchuploaddetails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_batchuploaddetails_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_batchuploaddetails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_batchuploaddetails_id_seq OWNED BY public.modeldcc_batchuploaddetails.id;


--
-- Name: modeldcc_batchuploads; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_batchuploads (
    id integer NOT NULL,
    "csvFile" character varying(1000) NOT NULL,
    "isValid" boolean NOT NULL,
    committed boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_batchuploads OWNER TO "test-user";

--
-- Name: modeldcc_batchuploads_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_batchuploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_batchuploads_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_batchuploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_batchuploads_id_seq OWNED BY public.modeldcc_batchuploads.id;


--
-- Name: modeldcc_biosample; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_biosample (
    id integer NOT NULL,
    biosample_id character varying(200) NOT NULL,
    treatment character varying(1000) NOT NULL,
    post_fertilization character varying(100) NOT NULL,
    source character varying(200) NOT NULL,
    mutation_description character varying(1000) NOT NULL,
    cell_line_type character varying(200) NOT NULL,
    description text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    anatomical_term_id integer,
    biosample_lab_id integer NOT NULL,
    biosample_type_id integer NOT NULL,
    controlled_by_id integer,
    genetic_background_id integer NOT NULL,
    organism_id integer NOT NULL,
    series_id integer NOT NULL,
    sex_id integer,
    stage_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_biosample OWNER TO "test-user";

--
-- Name: modeldcc_biosample_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_biosample_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_biosample_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_biosample_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_biosample_id_seq OWNED BY public.modeldcc_biosample.id;


--
-- Name: modeldcc_biosample_types; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_biosample_types (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_biosample_types OWNER TO "test-user";

--
-- Name: modeldcc_biosample_types_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_biosample_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_biosample_types_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_biosample_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_biosample_types_id_seq OWNED BY public.modeldcc_biosample_types.id;


--
-- Name: modeldcc_biosamplereplicate; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_biosamplereplicate (
    id integer NOT NULL,
    biosamplereplicate_id character varying(200) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    biosample_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_biosamplereplicate OWNER TO "test-user";

--
-- Name: modeldcc_biosamplereplicate_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_biosamplereplicate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_biosamplereplicate_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_biosamplereplicate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_biosamplereplicate_id_seq OWNED BY public.modeldcc_biosamplereplicate.id;


--
-- Name: modeldcc_data; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_data (
    id integer NOT NULL,
    data_id character varying(200),
    primary_file character varying(255) NOT NULL,
    secondary_file character varying(255),
    command character varying(2000) NOT NULL,
    primary_source character varying(1000),
    secondary_source character varying(1000),
    sha1 character varying(40),
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    derived_from_id integer,
    file_type_id integer NOT NULL,
    mapped_genome_id integer,
    sequencing_id integer NOT NULL,
    user_id integer NOT NULL,
    version_id integer
);


ALTER TABLE public.modeldcc_data OWNER TO "test-user";

--
-- Name: modeldcc_data_derived_from_data; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_data_derived_from_data (
    id integer NOT NULL,
    from_data_id integer NOT NULL,
    to_data_id integer NOT NULL
);


ALTER TABLE public.modeldcc_data_derived_from_data OWNER TO "test-user";

--
-- Name: modeldcc_data_derived_from_data_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_data_derived_from_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_data_derived_from_data_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_data_derived_from_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_data_derived_from_data_id_seq OWNED BY public.modeldcc_data_derived_from_data.id;


--
-- Name: modeldcc_data_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_data_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_data_id_seq OWNED BY public.modeldcc_data.id;


--
-- Name: modeldcc_developmental_stages; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_developmental_stages (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_developmental_stages OWNER TO "test-user";

--
-- Name: modeldcc_developmental_stages_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_developmental_stages_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_developmental_stages_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_developmental_stages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_developmental_stages_id_seq OWNED BY public.modeldcc_developmental_stages.id;


--
-- Name: modeldcc_file_types; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_file_types (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_file_types OWNER TO "test-user";

--
-- Name: modeldcc_file_types_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_file_types_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_file_types_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_file_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_file_types_id_seq OWNED BY public.modeldcc_file_types.id;


--
-- Name: modeldcc_genetic_backgrounds; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_genetic_backgrounds (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_genetic_backgrounds OWNER TO "test-user";

--
-- Name: modeldcc_genetic_backgrounds_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_genetic_backgrounds_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_genetic_backgrounds_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_genetic_backgrounds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_genetic_backgrounds_id_seq OWNED BY public.modeldcc_genetic_backgrounds.id;


--
-- Name: modeldcc_instruments; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_instruments (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_instruments OWNER TO "test-user";

--
-- Name: modeldcc_instruments_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_instruments_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_instruments_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_instruments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_instruments_id_seq OWNED BY public.modeldcc_instruments.id;


--
-- Name: modeldcc_labs; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_labs (
    id integer NOT NULL,
    lab_id character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    lab_pi character varying(200),
    lab_institution character varying(200),
    lab_country character varying(200),
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.modeldcc_labs OWNER TO "test-user";

--
-- Name: modeldcc_labs_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_labs_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_labs_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_labs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_labs_id_seq OWNED BY public.modeldcc_labs.id;


--
-- Name: modeldcc_mapped_genomes; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_mapped_genomes (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_mapped_genomes OWNER TO "test-user";

--
-- Name: modeldcc_mapped_genomes_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_mapped_genomes_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_mapped_genomes_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_mapped_genomes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_mapped_genomes_id_seq OWNED BY public.modeldcc_mapped_genomes.id;


--
-- Name: modeldcc_organisms; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_organisms (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_organisms OWNER TO "test-user";

--
-- Name: modeldcc_organisms_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_organisms_id_seq
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_organisms_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_organisms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_organisms_id_seq OWNED BY public.modeldcc_organisms.id;


--
-- Name: modeldcc_platforms; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_platforms (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_platforms OWNER TO "test-user";

--
-- Name: modeldcc_platforms_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_platforms_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_platforms_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_platforms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_platforms_id_seq OWNED BY public.modeldcc_platforms.id;


--
-- Name: modeldcc_sequencing; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_sequencing (
    id integer NOT NULL,
    sequencing_id character varying(200) NOT NULL,
    chemistry_version character varying(200) NOT NULL,
    paired_end boolean NOT NULL,
    max_read_length integer,
    sequencing_date date NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    instrument_id integer NOT NULL,
    platform_id integer NOT NULL,
    sequencing_lab_id integer NOT NULL,
    strand_mode_id integer,
    technicalreplicate_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_sequencing OWNER TO "test-user";

--
-- Name: modeldcc_sequencing_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_sequencing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_sequencing_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_sequencing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_sequencing_id_seq OWNED BY public.modeldcc_sequencing.id;


--
-- Name: modeldcc_series; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_series (
    id integer NOT NULL,
    series_id character varying(200) NOT NULL,
    title character varying(1000) NOT NULL,
    series_type character varying(100) NOT NULL,
    is_public boolean NOT NULL,
    description text NOT NULL,
    publication character varying(1000) NOT NULL,
    sra_geo_id character varying(200) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    status_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_series OWNER TO "test-user";

--
-- Name: modeldcc_series_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_series_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_series_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_series_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_series_id_seq OWNED BY public.modeldcc_series.id;


--
-- Name: modeldcc_sexes; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_sexes (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_sexes OWNER TO "test-user";

--
-- Name: modeldcc_sexes_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_sexes_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_sexes_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_sexes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_sexes_id_seq OWNED BY public.modeldcc_sexes.id;


--
-- Name: modeldcc_statuses; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_statuses (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_statuses OWNER TO "test-user";

--
-- Name: modeldcc_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_statuses_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_statuses_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_statuses_id_seq OWNED BY public.modeldcc_statuses.id;


--
-- Name: modeldcc_strand_modes; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_strand_modes (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_strand_modes OWNER TO "test-user";

--
-- Name: modeldcc_strand_modes_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_strand_modes_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_strand_modes_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_strand_modes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_strand_modes_id_seq OWNED BY public.modeldcc_strand_modes.id;


--
-- Name: modeldcc_technicalreplicate; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_technicalreplicate (
    id integer NOT NULL,
    technicalreplicate_id character varying(200) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_edited timestamp with time zone NOT NULL,
    appliedassay_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.modeldcc_technicalreplicate OWNER TO "test-user";

--
-- Name: modeldcc_technicalreplicate_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_technicalreplicate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_technicalreplicate_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_technicalreplicate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_technicalreplicate_id_seq OWNED BY public.modeldcc_technicalreplicate.id;


--
-- Name: modeldcc_versions; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.modeldcc_versions (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.modeldcc_versions OWNER TO "test-user";

--
-- Name: modeldcc_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.modeldcc_versions_id_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modeldcc_versions_id_seq OWNER TO "test-user";

--
-- Name: modeldcc_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.modeldcc_versions_id_seq OWNED BY public.modeldcc_versions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO "test-user";

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO "test-user";

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO "test-user";

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO "test-user";

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO "test-user";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: test-user
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO "test-user";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test-user
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: test-user
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO "test-user";

--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: modeldcc_anatomical_terms id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_anatomical_terms ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_anatomical_terms_id_seq'::regclass);


--
-- Name: modeldcc_appliedassay id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_appliedassay ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_appliedassay_id_seq'::regclass);


--
-- Name: modeldcc_assay id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_assay_id_seq'::regclass);


--
-- Name: modeldcc_assay_types id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay_types ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_assay_types_id_seq'::regclass);


--
-- Name: modeldcc_batchuploaddetails id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_batchuploaddetails ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_batchuploaddetails_id_seq'::regclass);


--
-- Name: modeldcc_batchuploads id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_batchuploads ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_batchuploads_id_seq'::regclass);


--
-- Name: modeldcc_biosample id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_biosample_id_seq'::regclass);


--
-- Name: modeldcc_biosample_types id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample_types ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_biosample_types_id_seq'::regclass);


--
-- Name: modeldcc_biosamplereplicate id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosamplereplicate ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_biosamplereplicate_id_seq'::regclass);


--
-- Name: modeldcc_data id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_data_id_seq'::regclass);


--
-- Name: modeldcc_data_derived_from_data id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data_derived_from_data ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_data_derived_from_data_id_seq'::regclass);


--
-- Name: modeldcc_developmental_stages id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_developmental_stages ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_developmental_stages_id_seq'::regclass);


--
-- Name: modeldcc_file_types id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_file_types ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_file_types_id_seq'::regclass);


--
-- Name: modeldcc_genetic_backgrounds id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_genetic_backgrounds ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_genetic_backgrounds_id_seq'::regclass);


--
-- Name: modeldcc_instruments id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_instruments ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_instruments_id_seq'::regclass);


--
-- Name: modeldcc_labs id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_labs ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_labs_id_seq'::regclass);


--
-- Name: modeldcc_mapped_genomes id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_mapped_genomes ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_mapped_genomes_id_seq'::regclass);


--
-- Name: modeldcc_organisms id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_organisms ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_organisms_id_seq'::regclass);


--
-- Name: modeldcc_platforms id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_platforms ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_platforms_id_seq'::regclass);


--
-- Name: modeldcc_sequencing id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_sequencing_id_seq'::regclass);


--
-- Name: modeldcc_series id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_series ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_series_id_seq'::regclass);


--
-- Name: modeldcc_sexes id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sexes ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_sexes_id_seq'::regclass);


--
-- Name: modeldcc_statuses id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_statuses ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_statuses_id_seq'::regclass);


--
-- Name: modeldcc_strand_modes id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_strand_modes ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_strand_modes_id_seq'::regclass);


--
-- Name: modeldcc_technicalreplicate id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_technicalreplicate ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_technicalreplicate_id_seq'::regclass);


--
-- Name: modeldcc_versions id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_versions ALTER COLUMN id SET DEFAULT nextval('public.modeldcc_versions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.auth_group (id, name) FROM stdin;
1	test
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add group	1	add_group
2	Can change group	1	change_group
3	Can delete group	1	delete_group
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add biosample_types	6	add_biosample_types
17	Can change biosample_types	6	change_biosample_types
18	Can delete biosample_types	6	delete_biosample_types
19	Can add genetic_backgrounds	7	add_genetic_backgrounds
20	Can change genetic_backgrounds	7	change_genetic_backgrounds
21	Can delete genetic_backgrounds	7	delete_genetic_backgrounds
22	Can add strand_modes	8	add_strand_modes
23	Can change strand_modes	8	change_strand_modes
24	Can delete strand_modes	8	delete_strand_modes
25	Can add file_types	9	add_file_types
26	Can change file_types	9	change_file_types
27	Can delete file_types	9	delete_file_types
28	Can add batch uploads	10	add_batchuploads
29	Can change batch uploads	10	change_batchuploads
30	Can delete batch uploads	10	delete_batchuploads
31	Can add series	11	add_series
32	Can change series	11	change_series
33	Can delete series	11	delete_series
34	Can add assay	12	add_assay
35	Can change assay	12	change_assay
36	Can delete assay	12	delete_assay
37	Can add instruments	13	add_instruments
38	Can change instruments	13	change_instruments
39	Can delete instruments	13	delete_instruments
40	Can add sexes	14	add_sexes
41	Can change sexes	14	change_sexes
42	Can delete sexes	14	delete_sexes
43	Can add mapped_genomes	15	add_mapped_genomes
44	Can change mapped_genomes	15	change_mapped_genomes
45	Can delete mapped_genomes	15	delete_mapped_genomes
46	Can add versions	16	add_versions
47	Can change versions	16	change_versions
48	Can delete versions	16	delete_versions
49	Can add applied assay	17	add_appliedassay
50	Can change applied assay	17	change_appliedassay
51	Can delete applied assay	17	delete_appliedassay
52	Can add platforms	18	add_platforms
53	Can change platforms	18	change_platforms
54	Can delete platforms	18	delete_platforms
55	Can add biosample replicate	19	add_biosamplereplicate
56	Can change biosample replicate	19	change_biosamplereplicate
57	Can delete biosample replicate	19	delete_biosamplereplicate
58	Can add organisms	20	add_organisms
59	Can change organisms	20	change_organisms
60	Can delete organisms	20	delete_organisms
61	Can add labs	21	add_labs
62	Can change labs	21	change_labs
63	Can delete labs	21	delete_labs
64	Can add batch upload details	22	add_batchuploaddetails
65	Can change batch upload details	22	change_batchuploaddetails
66	Can delete batch upload details	22	delete_batchuploaddetails
67	Can add biosample	23	add_biosample
68	Can change biosample	23	change_biosample
69	Can delete biosample	23	delete_biosample
70	Can add anatomical_terms	24	add_anatomical_terms
71	Can change anatomical_terms	24	change_anatomical_terms
72	Can delete anatomical_terms	24	delete_anatomical_terms
73	Can add developmental_stages	25	add_developmental_stages
74	Can change developmental_stages	25	change_developmental_stages
75	Can delete developmental_stages	25	delete_developmental_stages
76	Can add technical replicate	26	add_technicalreplicate
77	Can change technical replicate	26	change_technicalreplicate
78	Can delete technical replicate	26	delete_technicalreplicate
79	Can add assay_types	27	add_assay_types
80	Can change assay_types	27	change_assay_types
81	Can delete assay_types	27	delete_assay_types
82	Can add sequencing	28	add_sequencing
83	Can change sequencing	28	change_sequencing
84	Can delete sequencing	28	delete_sequencing
85	Can add statuses	29	add_statuses
86	Can change statuses	29	change_statuses
87	Can delete statuses	29	delete_statuses
88	Can add data	30	add_data
89	Can change data	30	change_data
90	Can delete data	30	delete_data
91	Can add log entry	31	add_logentry
92	Can change log entry	31	change_logentry
93	Can delete log entry	31	delete_logentry
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$36000$B5IYyDlDldvJ$2+wwPAiWpl8WSXY6jk4QV29dkPYhfBXbUhI0si6KTrI=	2018-08-17 09:51:22.317829+00	t	test-user				t	t	2018-05-18 09:50:22+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	1	1
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: modeldcc_anatomical_terms; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_anatomical_terms (id, name) FROM stdin;
0	abdominal musculature
1	abducens motor nucleus
2	abductor hyohyoid
3	abductor muscle
4	abductor profundus
5	absorptive cell
6	accessory chamber of the maxillary blood sinus
7	accessory pretectal nucleus
8	acellular anatomical structure
9	acid secreting cell
10	acinar cell
11	actinotrichium
12	adaxial cell
13	adductor
14	adductor arcus palatini
15	adductor hyohyoid
16	adductor hyomandibulae
17	adductor mandibulae
18	adductor mandibulae complex
19	adductor muscle
20	adductor operculi
21	adductor profundus
22	adenohypophyseal placode
23	adenohypophysis
24	adipose tissue
25	adrenal medulla cell
26	adrenergic neuron
27	adrenocorticotropic hormone secreting cell
28	afferent branchial artery
29	afferent filamental artery
30	afferent glomerular arteriole
31	afferent lamellar arteriole
32	afferent neuron
33	alar plate midbrain region
34	alkali secreting cell
35	alpha-beta T cell
36	amacrine cell
37	ameloblast
38	amelocyte
39	ampullary nerve
40	anal depressor
41	anal erector
42	anal fin
43	anal fin actinotrichium
44	anal fin distal radial
45	anal fin lepidotrichium
46	anal fin lepidotrichium 1
47	anal fin lepidotrichium 2
48	anal fin lepidotrichium 3
49	anal fin lepidotrichium 4
50	anal fin lepidotrichium 5
51	anal fin lepidotrichium 6
52	anal fin lepidotrichium 7
53	anal fin musculature
54	anal fin proximal radial
55	anal fin pterygiophore
56	anal fin radial
57	anal fin skeleton
58	anal inclinator
59	anatomical cluster
60	anatomical group
61	anatomical line
62	anatomical space
63	anatomical structure
64	anatomical surface
65	anatomical system
66	androgen secreting cell
67	angioblastic mesenchymal cell
68	angiogenic sprout
69	angular bone
70	anguloarticular
71	anguloarticular-retroarticular joint
72	annular ligament
73	ansulate commissure
74	anterior axial hypoblast
75	anterior cardinal vein
76	anterior catecholaminergic tract
77	anterior cerebral vein
78	anterior chamber swim bladder
79	anterior commissure
80	anterior crista
81	anterior crista primordium
82	anterior dorsomedial process of autopalatine
83	anterior lateral line
84	anterior lateral line ganglion
85	anterior lateral line nerve
86	anterior lateral line neuromast
87	anterior lateral line placode
88	anterior lateral line primordium
89	anterior lateral line system
90	anterior lateral mesoderm
91	anterior lateral plate mesoderm
92	anterior lateral process of autopalatine
93	anterior macula
94	anterior mesencephalic central artery
95	anterior mesenteric artery
96	anterior myodome
97	anterior naris
98	anterior nasal barbel
99	anterior neural keel
100	anterior neural plate
101	anterior neural rod
102	anterior neural tube
103	anterior pancreatic bud
104	anterior presumptive neural plate
105	anterior sclerotic bone
106	anterior segment eye
107	anterior semicircular canal
108	anterior swim bladder bud
109	anterior ventromedial process of autopalatine
110	anteromedial zone olfactory bulb
111	aortic arch
112	aortic arch 1
113	aortic arch 2
114	aortic arch 3
115	aortic arch 4
116	aortic arch 5
117	aortic arch 6
118	apical ectodermal ridge
119	apical ectodermal ridge dorsal fin
120	apical ectodermal ridge median fin fold
121	apical ectodermal ridge pectoral fin bud
122	apical ectodermal ridge pelvic fin
123	apical ectodermal ridge pelvic fin bud
124	apical epidermal cap
125	aponeurosis
126	aqueous humor
127	area postrema
128	arrector muscle
129	arteriole
130	artery
131	articular cartilage
132	articular fossa of opercle
133	asteriscus
134	astrocyte
135	atrial endocardium
136	atrial epicardium
137	atrial myocardium
138	atrioventricular canal
139	atrioventricular canal endocardium
140	atrioventricular node
141	atrioventricular ring
142	atrioventricular valve
143	atrium
144	atrium of sinus impar
145	atypical epithelium
146	auditory capsule
147	auditory epithelial support cell
148	auditory fenestra
149	auditory foramen
150	auditory receptor cell
151	autonomic nervous system
152	autonomic neuron
153	autopalatine
154	autopalatine-lateral ethmoid joint
155	autopalatine-maxillary joint
156	autopalatine-preethmoid joint
157	autopalatine-vomer joint
158	axial blood vessel
159	axial chorda mesoderm
160	axial fin skeleton
161	axial hypoblast
162	axial lymph vessel
163	axial mesoderm
164	axial vasculature
165	axis
166	B cell
167	ball
168	band form neutrophil
169	barbel
170	barbel primordium
171	barrier cell
172	barrier epithelial cell
173	basal communicating artery
174	basal lamina
175	basal layer breeding tubercle
176	basal plate cartilage
177	basal plate midbrain region
178	basal regeneration epithelium
179	basibranchial
180	basibranchial 1
181	basibranchial 2
182	basibranchial 3
183	basibranchial 4
184	basicapsular fenestra
185	basidorsal
186	basihyal bone
187	basihyal cartilage
188	basilar artery
189	basioccipital
190	basioccipital posterodorsal region
191	basioccipital-exoccipital joint
192	basioccipital-prootic joint
193	basipterygium
194	basipterygium cartilage
195	basiventral
196	basket cell
197	basophilic erythroblast
198	Bergmann glial cell
199	bifurcate interneuron
200	bile canaliculus
201	bile ductule
202	biogenic amine secreting cell
203	bipolar neuron
204	blastema
205	blastemal cell
206	blastoderm
207	blastoderm cell
208	blastodisc
209	blastomere
210	blood
211	blood cell
212	blood island
213	blood plasma
214	blood sinus
215	blood sinus cavity
216	blood vasculature
217	blood vessel
218	blood vessel endothelial cell
219	blood vessel endothelium
220	blue sensitive photoreceptor cell
221	bone element
222	bone tissue
223	bony plate
224	bony projection
225	bony shelf above orbit
226	Bowman's layer
227	Brachet's cleft
228	brain
229	brain nucleus
230	brain vasculature
231	brainstem
232	brainstem and spinal white matter
233	branched anal fin ray
234	branched caudal fin ray
235	branched dorsal fin ray
236	branchial mesenchyme
237	branchial muscle
238	branchiomotor neuron
239	branchiostegal membrane
240	branchiostegal ray
241	branchiostegal ray 1
242	branchiostegal ray 2
243	branchiostegal ray 3
244	breeding tubercle
245	Bruch's membrane
246	brush border epithelial cell
247	bulbo-spinal tract
248	bulbus arteriosus
249	bulbus arteriosus inner layer
250	bulbus arteriosus middle layer
251	bulbus arteriosus outer layer
252	CaD
253	Cajal-Retzius cell
254	calcitonin secreting cell
255	CaP motoneuron
256	capillary
257	capillary loop nephron
258	cardiac conduction system
259	cardiac jelly
260	cardiac mesenchymal cell
261	cardiac muscle
262	cardiac muscle cell
263	cardiac muscle myoblast
264	cardiac neural crest
265	cardiac ventricle
266	cardinal system
267	cardiovascular system
268	carotid artery
269	cartilage element
270	cartilage tissue
271	cartilaginous joint
272	caudal artery
273	caudal commissure
274	caudal division of the internal carotid artery
275	caudal fin
276	caudal fin actinotrichium
277	caudal fin blood vessel
278	caudal fin dorsal procurrent ray
279	caudal fin fat
280	caudal fin lepidotrichium
281	caudal fin lower lobe
282	caudal fin lymph vessel
283	caudal fin musculature
284	caudal fin principal ray
285	caudal fin principal ray 1
286	caudal fin principal ray 10
287	caudal fin principal ray 11
288	caudal fin principal ray 12
289	caudal fin principal ray 13
290	caudal fin principal ray 14
291	caudal fin principal ray 15
292	caudal fin principal ray 16
293	caudal fin principal ray 17
294	caudal fin principal ray 18
295	caudal fin principal ray 19
296	caudal fin principal ray 2
297	caudal fin principal ray 3
298	caudal fin principal ray 4
299	caudal fin principal ray 5
300	caudal fin principal ray 6
301	caudal fin principal ray 7
302	caudal fin principal ray 8
303	caudal fin principal ray 9
304	caudal fin procurrent ray
305	caudal fin skeleton
306	caudal fin upper lobe
307	caudal fin vasculature
308	caudal fin ventral procurrent ray
309	caudal hypothalamic zone
310	caudal levator
311	caudal mesencephalo-cerebellar tract
312	caudal motor nucleus
313	caudal oblique
314	caudal octaval nerve motor nucleus
315	caudal octaval nucleus
316	caudal octavolateralis nucleus
317	caudal parvocellular preoptic nucleus
318	caudal peduncle
319	caudal periventricular hypothalamus
320	caudal preglomerular nucleus
321	caudal pretectal nucleus
322	caudal root of abducens nerve
323	caudal thalamic nucleus
324	caudal tuberal nucleus
325	caudal tuberculum
326	caudal vein
327	caudal vein plexus
328	caudal vertebra
329	caudal zone of dorsal telencephalon
330	CaV
331	cavitated compound organ
332	celiacomesenteric artery
333	cell
334	cementoblast
335	cementocyte
336	central artery
337	central canal
338	central cardiac conduction system
339	central caudal thalamic nucleus
340	central chamber of the maxillary blood sinus
341	central nervous system
342	central nucleus inferior lobe
343	central nucleus of ventral telencephalon
344	central nucleus torus semicircularis
345	central pretectal nucleus
346	central pretectum
347	central ray artery
348	central vein
349	central zone of dorsal telencephalon
350	central zone of the optic tectum
351	central zone olfactory bulb
352	central zone protoglomerulus
353	central zone protoglomerulus 1
354	central zone protoglomerulus 2
355	central zone protoglomerulus 3
356	central zone protoglomerulus 4
357	central zone protoglomerulus 5
358	centrum
359	cephalic flexure
360	cephalic musculature
361	ceratobranchial 1 bone
362	ceratobranchial 1 cartilage
363	ceratobranchial 2 bone
364	ceratobranchial 2 cartilage
365	ceratobranchial 3 bone
366	ceratobranchial 3 cartilage
367	ceratobranchial 4 bone
368	ceratobranchial 4 cartilage
369	ceratobranchial 5 bone
370	ceratobranchial 5 cartilage
371	ceratobranchial 5 ligament
372	ceratobranchial 5 tooth
373	ceratobranchial bone
374	ceratobranchial cartilage
375	ceratohyal bone
376	ceratohyal cartilage
377	ceratohyal-branchiostegal ray joint
378	ceratohyal-dorsal hypohyal joint
379	ceratohyal-ventral hypohyal joint
380	cerebellar central artery
381	cerebellar crest
382	cerebellar granule cell
383	cerebellar white matter
384	cerebellovestibular tract
385	cerebellum
386	cerebral spinal fluid
387	cerebrospinal fluid secreting cell
388	chemoreceptor cell
389	cholangiocyte
390	cholinergic neuron
391	chondral bone
392	chondroblast
393	chondrocranium
394	chondrocranium cartilage
395	chondrocyte
396	chordo neural hinge
397	chorion
398	choroid plexus
399	choroid plexus epithelial cell
400	choroid plexus fourth ventricle
401	choroid plexus tectal ventricle
402	choroid plexus telencephalic ventricle
403	choroid plexus third ventricle
404	choroid plexus vascular circuit
405	choroidal cell of the eye
406	choroidal fissure
407	chromaffin cell
408	CiA
409	CiD
410	ciliary marginal zone
411	ciliary zone
412	ciliated cell
413	ciliated epithelial cell
414	ciliated olfactory receptor neuron
415	circulating cell
416	circulus
417	claustrum bone
418	claustrum cartilage
419	cleft cell
420	cleithrum
421	cleithrum-vertebral ligament
422	climbing fiber
423	cloaca
424	cloacal chamber
425	cloacal epithelium
426	CNS interneuron
427	CNS long range interneuron
428	CNS neuron (sensu Vertebrata)
429	CNS short range interneuron
430	CoB
431	CoBL
432	coelom
433	CoLA
434	collagen secreting cell
435	collagenous dermal stroma
436	collecting duct
437	columnar chondrocyte
438	columnar/cuboidal epithelial cell
439	comma-shaped body
440	commissura cerebelli
441	commissura rostral, pars dorsalis
442	commissura rostral, pars ventralis
443	commissure infima of Haller
444	commissure of the caudal tuberculum
445	commissure of the secondary gustatory nucleus
446	common bile duct
447	common cardinal vein
448	common crus
449	common lymphoid progenitor
450	common myeloid progenitor
451	communicating vessel palatocerebral artery
452	compact layer of ventricle
453	compound organ
454	compound organ component
455	concurrent branch afferent branchial artery
456	cone retinal bipolar cell
457	connective tissue cell
458	constrictor dorsalis
459	continuous blood vessel endothelium
460	CoPA
461	copula
462	copula 1
463	copula 2
464	coracoid
465	coracoid foramen
466	coracoradialis
467	cornea
468	corneal endothelial cell
469	corneal endothelium
470	corneal epithelial cell
471	corneal epithelium
472	corneal primordium
473	corneal stroma
474	corneo-scleral junction
475	coronary artery
476	coronary capillary
477	coronary vein
478	coronomeckelian
479	corpus cerebelli
480	corpus mamillare
481	corpuscles of Stannius
482	CoSA
483	cranial blood vessel
484	cranial cartilage
485	cranial division of the internal carotid artery
486	cranial fat
487	cranial ganglion
488	cranial lymph vessel
489	cranial motor neuron
490	cranial nerve
491	cranial nerve I
492	cranial nerve II
493	cranial nerve III
494	cranial nerve IV
495	cranial nerve IX
496	cranial nerve root
497	cranial nerve V
498	cranial nerve VI
499	cranial nerve VII
500	cranial nerve VIII
501	cranial nerve X
502	cranial neural crest
503	cranial neural crest cell
504	cranial vasculature
505	cranial vault
506	cranium
507	crossed tecto-bulbar tract
508	crypt neuron olfactory support cell
509	crypt olfactory receptor neuron
510	cupula
511	cyanoblast
512	cyanophore
513	cystic duct
514	D cell
515	decussation of medial funicular nucleus
516	decussation of the medial octavolateralis nucleus
517	deep blastomere
518	deep caudal fin musculature
519	deep lateral dorsal muscle
520	deep lateral ventral muscle
521	deep white zone
522	DEL
523	dendritic cell
524	dental epithelium
525	dental mesenchyme
526	dental organ
527	dental papilla
528	dental papilla cell
529	dentary
530	dentary foramen
531	dentary-anguloarticular joint
532	dentine
533	depressor muscle
534	dermal bone
535	dermal deep region
536	dermal superficial region
537	dermatocranium
538	dermis
539	dermomyotome
540	Descemet's membrane
541	descending octaval nucleus
542	descending trigeminal root
543	developing mesonephric distal tubule
544	developing mesonephric nephron
545	developing mesonephric proximal tubule
546	dG1
547	diencephalic efferent neurons to the lateral line
548	diencephalic nucleus
549	diencephalic white matter
550	diencephalon
551	diffuse nucleus inferior lobe
552	digestive enzyme secreting cell
553	digestive system
554	digestive system duct
555	dilator operculi
556	distal bulb of the maxillary blood sinus
557	distal early tubule
558	distal epidermal cap
559	distal late tubule
560	dlG1
561	dlG2
562	dlG3
563	dlG4
564	dlG5
565	DoLA
566	dopaminergic neuron
567	dorsal accessory optic nucleus
568	dorsal actinotrichium
569	dorsal anterior lateral line ganglion
570	dorsal anterior lateral line nerve
571	dorsal aorta
572	dorsal arrector
573	dorsal axial hypoblast
574	dorsal branch nasal ciliary artery
575	dorsal caudal thalamic nucleus
576	dorsal ciliary vein
577	dorsal depressor
578	dorsal entopeduncular nucleus of ventral telencephalon
579	dorsal erector
580	dorsal fin
581	dorsal fin distal radial
582	dorsal fin distal radial 1
583	dorsal fin distal radial 2
584	dorsal fin distal radial 3
585	dorsal fin distal radial 4
586	dorsal fin distal radial 5
587	dorsal fin distal radial 6
588	dorsal fin distal radial 7
589	dorsal fin distal radial 8
590	dorsal fin fold
591	dorsal fin lepidotrichium
592	dorsal fin lepidotrichium 1
593	dorsal fin lepidotrichium 2
594	dorsal fin lepidotrichium 3
595	dorsal fin lepidotrichium 4
596	dorsal fin lepidotrichium 5
597	dorsal fin lepidotrichium 6
598	dorsal fin lepidotrichium 7
599	dorsal fin musculature
600	dorsal fin protractor
601	dorsal fin proximal radial
602	dorsal fin proximal radial 1
603	dorsal fin proximal radial 2
604	dorsal fin proximal radial 3
605	dorsal fin proximal radial 4
606	dorsal fin proximal radial 5
607	dorsal fin proximal radial 6
608	dorsal fin proximal radial 7
609	dorsal fin proximal radial 8
610	dorsal fin pterygiophore
611	dorsal fin pterygiophore 1
612	dorsal fin pterygiophore 2
613	dorsal fin pterygiophore 3
614	dorsal fin pterygiophore 4
615	dorsal fin pterygiophore 5
616	dorsal fin pterygiophore 6
617	dorsal fin pterygiophore 7
618	dorsal fin pterygiophore 8
619	dorsal fin radial
620	dorsal fin retractor
621	dorsal fin skeleton
622	dorsal funiculus
623	dorsal habenular nucleus
624	dorsal horn spinal cord
625	dorsal hyoid arch
626	dorsal hypohyal bone
627	dorsal hypohyal-urohyal joint
628	dorsal hypohyal-ventral hypohyal joint
629	dorsal hypothalamic zone
630	dorsal inclinator
631	dorsal interfilamental caudal muscle
632	dorsal larval melanophore stripe
633	dorsal lateral line
634	dorsal lateral line neuromast
635	dorsal longitudinal anastomotic vessel
636	dorsal longitudinal fasciculus
637	dorsal longitudinal lymphatic vessel
638	dorsal longitudinal vein
639	dorsal mesentery
640	dorsal motor nucleus trigeminal nerve
641	dorsal motor root of V
642	dorsal nucleus of ventral telencephalon
643	dorsal oblique branchial muscle
644	dorsal oblique extraocular muscle
645	dorsal pelvic arrector
646	dorsal periventricular hypothalamus
647	dorsal proneural cluster
648	dorsal rectus
649	dorsal retractor
650	dorsal root
651	dorsal root ganglion
652	dorsal spinal nerve
653	dorsal tegmental nucleus
654	dorsal telencephalon
655	dorsal thalamus
656	dorsal tooth row
657	dorsal transverse
658	dorsal zone of dorsal telencephalon
659	dorsal zone olfactory bulb
660	dorso-rostral cluster
661	dorsolateral field
662	dorsolateral motor nucleus of vagal nerve
663	dorsolateral placode
664	dorsolateral septum
665	dorsolateral zone olfactory bulb
666	dorsomedial optic tract
667	dorsoventral diencephalic tract
668	duct
669	duct epithelial cell
670	ductus communicans
671	E-YSL
672	early embryonic cell
673	ecto-epithelial cell
674	ectoderm
675	ectodermal cell
676	ectomesenchyme
677	ectopterygoid
678	ectopterygoid-entopterygoid joint
679	ectopterygoid-quadrate joint
680	efferent branchial artery
681	efferent filamental artery
682	efferent glomerular arteriole
683	efferent lamellar arteriole
684	efferent neuron
685	electrically active cell
686	electrically responsive cell
687	electrically signaling cell
688	embryonic blood vessel endothelial progenitor cell
689	embryonic structure
690	eminentia thalami
691	enameloid
692	endo-epithelial cell
693	endocardial cushion
694	endocardial precursor
695	endocardial ring
696	endocardium
697	endochondral bone
698	endochondral element
699	endocrine cell
700	endocrine pancreas
701	endocrine system
702	endoderm
703	endodermal cell
704	endohypothalamic tract
705	endolymph
706	endolymphatic duct
707	endorphin secreting cell
708	endothelial cell
709	endothelial tip cell
710	enkephalin secreting cell
711	enteric circular muscle
712	enteric longitudinal muscle
713	enteric musculature
714	enteric nervous system
715	enteric neuron
716	enterocyte
717	enteroendocrine cell
718	entopterygoid
719	entopterygoid-metapterygoid joint
720	epaxial myotome region
721	epaxial region somite 1
722	epaxial region somite 10
723	epaxial region somite 11
724	epaxial region somite 12
725	epaxial region somite 13
726	epaxial region somite 14
727	epaxial region somite 15
728	epaxial region somite 16
729	epaxial region somite 17
730	epaxial region somite 18
731	epaxial region somite 19
732	epaxial region somite 2
733	epaxial region somite 20
734	epaxial region somite 21
735	epaxial region somite 22
736	epaxial region somite 23
737	epaxial region somite 24
738	epaxial region somite 25
739	epaxial region somite 26
740	epaxial region somite 27
741	epaxial region somite 28
742	epaxial region somite 29
743	epaxial region somite 3
744	epaxial region somite 30
745	epaxial region somite 4
746	epaxial region somite 5
747	epaxial region somite 6
748	epaxial region somite 7
749	epaxial region somite 8
750	epaxial region somite 9
751	epaxialis
752	ependymal cell
753	ependymoglial cell
754	epiblast
755	epibranchial 1 bone
756	epibranchial 1 cartilage
757	epibranchial 2 bone
758	epibranchial 2 cartilage
759	epibranchial 3 bone
760	epibranchial 3 cartilage
761	epibranchial 4 bone
762	epibranchial 4 cartilage
763	epibranchial 5
764	epibranchial bone
765	epibranchial cartilage
766	epibranchial field
767	epibranchial ganglion
768	epibranchial placode
769	epibranchial uncinate process
770	epicardium
771	epidermal basal stratum
772	epidermal cell
773	epidermal intermediate stratum
774	epidermal placode
775	epidermal superficial stratum
776	epidermis
777	epihyal
778	epihyal-branchiostegal ray joint
779	epihyal-ceratohyal joint
780	epinephrin secreting cell
781	epineural
782	epiotic
783	epiphyseal bar
784	epiphyseal cluster
785	epiphyseal stalk
786	epiphysis
787	epipleural
788	epithalamus
789	epithelial cell
790	epithelial cell of esophagus
791	epithelial cell of pancreas
792	epithelial mesenchymal boundary
793	epithelium
794	epural
795	erector muscle
796	erythroblast
797	erythroid lineage cell
798	erythroid progenitor cell
799	erythrophore
800	esophageal epithelium
801	esophageal fat
802	esophageal sphincter
803	esophageal striated muscle
804	esophagus
805	estradiol secreting cell
806	ethmoid cartilage
807	eurydendroid cell
808	EVL
809	exoccipital
810	exoccipital posteroventral region
811	exocrine cell
812	exocrine pancreas
813	extension
814	external cellular layer
815	external lateral dorsal muscle
816	external lateral ventral muscle
817	external levatores
818	external pharyngoclavicularis
819	extracellular lipid lamellae
820	extracellular matrix secreting cell
821	extraembryonic cell
822	extraembryonic structure
823	extrahepatic duct
824	extramedullary cell
825	extraocular musculature
826	extrapancreatic duct
827	extrascapula
828	eye
829	eye photoreceptor cell
830	facial ganglion
831	facial lobe
832	facial lymphatic network
833	facial lymphatic sprout
834	facial lymphatic vessel
835	facial nerve motor nucleus
836	facial neural crest
837	facial placode
838	facio-acoustic neural crest
839	fasciculus retroflexus
840	fast muscle cell
841	fast muscle cell somite 1
842	fast muscle cell somite 10
843	fast muscle cell somite 11
844	fast muscle cell somite 12
845	fast muscle cell somite 13
846	fast muscle cell somite 14
847	fast muscle cell somite 15
848	fast muscle cell somite 16
849	fast muscle cell somite 17
850	fast muscle cell somite 18
851	fast muscle cell somite 19
852	fast muscle cell somite 2
853	fast muscle cell somite 20
854	fast muscle cell somite 21
855	fast muscle cell somite 22
856	fast muscle cell somite 23
857	fast muscle cell somite 24
858	fast muscle cell somite 25
859	fast muscle cell somite 26
860	fast muscle cell somite 27
861	fast muscle cell somite 28
862	fast muscle cell somite 29
863	fast muscle cell somite 3
864	fast muscle cell somite 30
865	fast muscle cell somite 4
866	fast muscle cell somite 5
867	fast muscle cell somite 6
868	fast muscle cell somite 7
869	fast muscle cell somite 8
870	fast muscle cell somite 9
871	fast muscle myoblast
872	fat cell
873	female organism
874	fenestrated blood vessel endothelium
875	fenestrated cell
876	fertilized egg
877	fibroblast
878	fibrous joint
879	filamental artery
880	fin
881	fin blood vessel
882	fin bud
883	fin fold actinotrichium
884	fin fold pectoral fin bud
885	fin lymph vessel
886	fin musculature
887	fin vasculature
888	flexor muscle
889	flexural organ
890	floor plate
891	floor plate diencephalic region
892	floor plate midbrain region
893	floor plate neural rod
894	floor plate neural tube
895	floor plate rhombomere 1
896	floor plate rhombomere 2
897	floor plate rhombomere 3
898	floor plate rhombomere 4
899	floor plate rhombomere 5
900	floor plate rhombomere 6
901	floor plate rhombomere 7
902	floor plate rhombomere 8
903	floor plate rhombomere region
904	floor plate spinal cord region
905	floor plate telencephalic region
906	follicle stimulating hormone secreting cell
907	folliculostellate cell
908	fontanel
909	foramen
910	foramen magnum
911	forebrain
912	forebrain midbrain boundary
913	forebrain midbrain boundary neural keel
914	forebrain midbrain boundary neural plate
915	forebrain midbrain boundary neural rod
916	forebrain midbrain boundary neural tube
917	forebrain neural keel
918	forebrain neural plate
919	forebrain neural rod
920	forebrain neural tube
921	forebrain ventricle
922	forerunner cell group
923	fossa
924	fourth ventricle
925	frontal bone
926	frontal-parietal joint
927	frontal-pterotic joint
928	GABAergic neuron
929	GAG secreting cell
930	gall bladder
931	gamete
932	gamma-delta intraepithelial T cell
933	gamma-delta T cell
934	ganglion
935	gastrula cell
936	germ line cell
937	germ ring
938	ghrelin secreting cell
939	gigantocellular part of magnocellular preoptic nucleus
940	gill
941	gill filament
942	gill ionocyte
943	gill lamella
944	gill opening
945	gill raker
946	gill ray
947	glial cell
948	glial cell (sensu Vertebrata)
949	glioblast
950	glioblast (sensu Vertebrata)
951	glomerular basement membrane
952	glomerular capillary
953	glomerular layer
954	glossopharyngeal ganglion
955	glossopharyngeal lobe
956	glossopharyngeal neural crest
957	glossopharyngeal placode
958	glucagon secreting cell
959	glucocorticoid secreting cell
960	glutamatergic neuron
961	glycinergic neuron
962	goblet cell
963	gold iridophore
964	Golgi cell
965	gonad
966	gonad primordium
967	granular eminence
968	granular layer corpus cerebelli
969	granular layer valvula cerebelli
970	granule cell
971	granulocyte
972	granulocyte monocyte progenitor cell
973	granulosa cell
974	granulosa cell layer
975	green sensitive photoreceptor cell
976	grey matter
977	griseum centrale
978	groove
979	gustatory system
980	gut
981	gut absorptive cell
982	gut endothelial cell
983	gut epithelium
984	habenula
985	habenular commissure
986	hair cell
987	hair cell anterior macula
988	hair cell posterior macula
989	hatching gland
990	hatching gland cell
991	head
992	head kidney
993	head mesenchyme
994	head muscle
995	head sensory canal system
996	heart
997	heart primordium
998	heart rudiment
999	heart tube
1000	heart valve
1001	heart vasculature
1002	hemal arch
1003	hemal postzygapophysis
1004	hemal prezygapophysis
1005	hemal spine
1006	hematopoietic cell
1007	hematopoietic multipotent progenitor cell
1008	hematopoietic stem cell
1009	hematopoietic system
1010	hepatic artery
1011	hepatic duct
1012	hepatic portal vein
1013	hepatic sinusoid
1014	hepatic vein
1015	hepatoblast
1016	hepatocyte
1017	hepatopancreatic ampulla
1018	hindbrain
1019	hindbrain commissure
1020	hindbrain interneuron
1021	hindbrain neural keel
1022	hindbrain neural plate
1023	hindbrain neural rod
1024	hindbrain neural tube
1025	hindbrain nucleus
1026	horizontal cell
1027	horizontal commissure
1028	horizontal myoseptum
1029	hyaloid artery
1030	hyaloid vein
1031	hyaloid vessel
1032	hyohyoideus
1033	hyoid muscle
1034	hyoid neural crest
1035	hyoideomandibular nerve
1036	hyomandibula
1037	hyomandibula-metapterygoid joint
1038	hyomandibula-opercle joint
1039	hyomandibula-pterotic joint
1040	hyomandibular foramen
1041	hyomandibular-otic region joint
1042	hyosymplectic cartilage
1043	hypaxial myotome region
1044	hypaxial region somite 1
1045	hypaxial region somite 10
1046	hypaxial region somite 11
1047	hypaxial region somite 12
1048	hypaxial region somite 13
1049	hypaxial region somite 14
1050	hypaxial region somite 15
1051	hypaxial region somite 16
1052	hypaxial region somite 17
1053	hypaxial region somite 18
1054	hypaxial region somite 19
1055	hypaxial region somite 2
1056	hypaxial region somite 20
1057	hypaxial region somite 21
1058	hypaxial region somite 22
1059	hypaxial region somite 23
1060	hypaxial region somite 24
1061	hypaxial region somite 25
1062	hypaxial region somite 26
1063	hypaxial region somite 27
1064	hypaxial region somite 28
1065	hypaxial region somite 29
1066	hypaxial region somite 3
1067	hypaxial region somite 30
1068	hypaxial region somite 4
1069	hypaxial region somite 5
1070	hypaxial region somite 6
1071	hypaxial region somite 7
1072	hypaxial region somite 8
1073	hypaxial region somite 9
1074	hypaxialis
1075	hypertrophic chondrocyte
1076	hypoblast
1077	hypobranchial 1 bone
1078	hypobranchial 1 cartilage
1079	hypobranchial 2 bone
1080	hypobranchial 2 cartilage
1081	hypobranchial 3 bone
1082	hypobranchial 3 cartilage
1083	hypobranchial 4 bone
1084	hypobranchial 4 cartilage
1085	hypobranchial artery
1086	hypobranchial bone
1087	hypobranchial cartilage
1088	hypochord
1089	hypocretin-secreting neuron
1090	hypodermal cell
1091	hypodermis
1092	hypohyal bone
1093	hypophyseal artery
1094	hypophyseal capillary
1095	hypophyseal fenestra
1096	hypophyseal vein
1097	hypophysis
1098	hypothalamus
1099	hypural
1100	hypural 1
1101	hypural 2
1102	hypural 3
1103	hypural 4
1104	hypural 5
1105	hypural muscle
1106	hypurapophysis
1107	I-YSL
1108	immature anterior macula
1109	immature B cell
1110	immature eye
1111	immature gamma-delta T cell
1112	immature gonad
1113	immature hair cell anterior macula
1114	immature hair cell posterior macula
1115	immature macula
1116	immature natural killer cell
1117	immature neutrophil
1118	immature posterior macula
1119	immature Schwann cell
1120	immature thymic epithelium
1121	immune system
1122	inclinator muscle
1123	inferior caudal dorsal flexor
1124	inferior caudal ventral flexor
1125	inferior hyohyoid
1126	inferior lobe
1127	inferior olive
1128	inferior raphe nucleus
1129	inferior reticular formation
1130	infracarinalis
1131	infraorbital
1132	infraorbital 1
1133	infraorbital 2
1134	infraorbital 3
1135	infraorbital 4
1136	infraorbital 5
1137	infraorbital lateral line
1138	infraorbital lateral line neuromast
1139	infraorbital sensory canal
1140	infraorbital series
1141	infundibulum
1142	inhibitory interneuron
1143	inner dental epithelium
1144	inner ear
1145	inner ear foramen
1146	inner limiting membrane
1147	inner mental barbel
1148	inner optic circle
1149	insulating cell
1150	insulin secreting cell
1151	integument
1152	integument ionocyte
1153	inter-basipterygium joint
1154	inter-coracoid joint
1155	inter-frontal joint
1156	inter-hypobranchial 3 joint
1157	inter-parietal joint
1158	inter-premaxillary joint
1159	inter-ventral hypohyal joint
1160	intercalar
1161	intercalarium
1162	intercalarium articulating process
1163	intercalarium ascending process
1164	intercalated duct pancreas
1165	intercostal ligament
1166	interhyal cartilage
1167	interhyal-epihyal joint
1168	interhyoideus
1169	intermandibularis
1170	intermediate cell mass of mesoderm
1171	intermediate hypothalamus
1172	intermediate mesenchyme
1173	intermediate mesoderm
1174	intermediate nucleus
1175	intermediate reticular formation
1176	intermediate thalamic nucleus
1177	intermuscular bone
1178	internal carotid artery
1179	internal cellular layer
1180	internal gill bud
1181	internal levator
1182	internal pharyngoclavicularis
1183	interneuromast cell
1184	interneuron
1185	interopercle
1186	interopercular-mandibular ligament
1187	interossicular ligament
1188	interpeduncular nucleus medulla oblongata
1189	interpeduncular nucleus tegmentum
1190	interradialis caudalis
1191	interray vessel
1192	interrenal angiogenic sprout
1193	interrenal gland
1194	interrenal primordium
1195	interrenal vessel
1196	intersegmental artery
1197	intersegmental lymph vessel
1198	intersegmental vein
1199	intersegmental vessel
1200	intertectal commissure
1201	intervening zone
1202	intervessel commissure
1203	intervillus pockets
1204	intestinal bulb
1205	intestinal bulb epithelium
1206	intestinal bulb primordium
1207	intestinal epithelial cell
1208	intestinal epithelium
1209	intestinal interlymphatic vessel
1210	intestinal lamina propria mucosa
1211	intestinal lymphatic network
1212	intestinal mucosa
1213	intestinal mucosal muscle
1214	intestinal opening
1215	intestinal rod
1216	intestinal villus
1217	intestine
1218	intestine lumen
1219	intrahepatic bile duct
1220	intrahepatic bile duct epithelial cell
1221	intramembranous bone
1222	intrapancreatic duct
1223	ionocyte
1224	iridoblast
1225	iridophore
1226	iris
1227	iris blood vessels
1228	iris stroma
1229	islet
1230	isthmic primary nucleus
1231	ito cell
1232	jaw flap
1233	jaw flap breeding tubercle
1234	jaw row breeding tubercle
1235	joint
1236	jugular lymphatic vessel
1237	juxtaglomerular cell
1238	kappe olfactory receptor neuron
1239	keratin accumulating cell
1240	keratinocyte
1241	kidney
1242	kidney blood vessel
1243	kidney cell
1244	kidney epithelial cell
1245	kidney interstitial cell
1246	kidney medulla cell
1247	kidney vasculature
1248	kinethmoid bone
1249	kinethmoid cartilage
1250	Kolmer-Agduhr neuron
1251	Kupffer's vesicle
1252	labial cavity
1253	lacuna
1254	lacunocanalicular canal
1255	lacunocanalicular system
1256	lagena
1257	lagenar capsule
1258	lamina densa
1259	lamina orbitonasalis
1260	lamina rara externa
1261	lamina rara interna
1262	lapillus
1263	larval melanophore stripe
1264	lateral column
1265	lateral crista
1266	lateral crista primordium
1267	lateral dorsal aorta
1268	lateral ethmoid
1269	lateral ethmoid-autopalatine ligament
1270	lateral ethmoid-ectopterygoid ligament
1271	lateral ethmoid-frontal joint
1272	lateral ethmoid-supraethmoid
1273	lateral facial lymph vessel
1274	lateral floor plate
1275	lateral fontanel of frontal
1276	lateral forebrain bundle
1277	lateral forebrain bundle diencephalon
1278	lateral forebrain bundle telencephalon
1279	lateral funiculus
1280	lateral hypothalamic nucleus
1281	lateral larval melanophore stripe
1282	lateral lemniscus nucleus
1283	lateral line
1284	lateral line ganglion
1285	lateral line nerve
1286	lateral line primordium
1287	lateral line scale
1288	lateral line sensory nucleus
1289	lateral line system
1290	lateral longitudinal fasciculus
1291	lateral mesenchyme derived from mesoderm
1292	lateral mesoderm
1293	lateral migration pathway mesenchyme
1294	lateral nucleus of ventral telencephalon
1295	lateral olfactory tract
1296	lateral plate mesoderm
1297	lateral preglomerular nucleus
1298	lateral protoglomerulus
1299	lateral protoglomerulus 1
1300	lateral protoglomerulus 2
1301	lateral protoglomerulus 3
1302	lateral protoglomerulus 4
1303	lateral recess
1304	lateral rectus
1305	lateral reticular nucleus
1306	lateral semicircular canal
1307	lateral spinal nerve
1308	lateral valvula cerebelli
1309	lateral wall diencephalic region
1310	lateral wall midbrain region
1311	lateral wall neural rod
1312	lateral wall neural tube
1313	lateral wall rhombomere 1
1314	lateral wall rhombomere 2
1315	lateral wall rhombomere 3
1316	lateral wall rhombomere 4
1317	lateral wall rhombomere 5
1318	lateral wall rhombomere 6
1319	lateral wall rhombomere 7
1320	lateral wall rhombomere 8
1321	lateral wall spinal cord
1322	lateral wall telencephalic region
1323	lateral zone of dorsal telencephalon
1324	lateral zone olfactory bulb
1325	left intestinal lymphatics
1326	left liver lobe
1327	lens
1328	lens capsule
1329	lens epithelium
1330	lens placode
1331	lepidotrichium
1332	lepidotrichium joint
1333	lepidotrichium segment
1334	leptomeningeal cell
1335	leucoblast
1336	leucophore
1337	leukocyte
1338	levator arcus palatini
1339	levator operculi
1340	Leydig cell
1341	lG1
1342	lG2
1343	lG3
1344	lG4
1345	lG5
1346	lG6
1347	lGx
1348	ligament
1349	ligament cell
1350	lining cell
1351	lip
1352	lip epithelium
1353	liver
1354	liver and biliary system
1355	locus coeruleus
1356	longitudinal hypochordal
1357	longitudinal lateral lymphatic vessel
1358	lower left intestinal lymph vessel
1359	lower lip
1360	lower oral valve
1361	lower rhombic lip
1362	lower right intestinal lymph vessel
1363	luteinizing hormone secreting cell
1364	lymph
1365	lymph vasculature
1366	lymph vessel
1367	lymph vessel endothelium
1368	lymphangioblast
1369	lymphangioblast cord
1370	lymphangiogenic sprout
1371	lymphatic branchial arch 1
1372	lymphatic branchial arch 2
1373	lymphatic branchial arch 3
1374	lymphatic branchial arch 4
1375	lymphatic capillary
1376	lymphatic system
1377	lymphocyte
1378	lymphoid progenitor cell
1379	M cell
1380	macroglial cell
1381	macrophage
1382	macula
1383	macula communis
1384	macula lagena
1385	macula neglecta
1386	macula saccule
1387	macula utricle
1388	maG1
1389	magnocellular octaval nucleus
1390	magnocellular preoptic nucleus
1391	magnocellular superficial pretectal nucleus
1392	maGx
1393	main intrapancreatic duct
1394	male organism
1395	mandibular arch skeleton
1396	mandibular fat
1397	mandibular lateral line
1398	mandibular lateral line neuromast
1399	mandibular muscle
1400	mandibular neural crest
1401	mandibular sensory canal
1402	mandibular symphysis
1403	manubrium
1404	margin
1405	marginal blastomere
1406	mature B cell
1407	mature gamma-delta T cell
1408	mature natural killer cell
1409	mature neutrophil
1410	Mauthner neuron
1411	maxilla
1412	maxillary barbel
1413	maxillary barbel blood sinus
1414	maxillary barbel blood vessel
1415	maxillary barbel lymph vessel
1416	maxillary barbel proximal plexus
1417	maxillary barbel vasculature
1418	maxillo-mandibular ligament
1419	maxillo-rostroid ligament
1420	MCoD
1421	mdG1
1422	mdG2
1423	mdG3
1424	mdG4
1425	mdG5
1426	mdG6
1427	mechanoreceptor cell
1428	Meckel's cartilage
1429	medial caudal lobe
1430	medial column
1431	medial facial lymph vessel
1432	medial floor plate
1433	medial forebrain bundle
1434	medial forebrain bundle diencephalon
1435	medial forebrain bundle telencephalon
1436	medial funicular nucleus medulla oblongata
1437	medial funicular nucleus trigeminal nucleus
1438	medial longitudinal catecholaminergic tract
1439	medial longitudinal fasciculus
1440	medial migration pathway mesenchyme
1441	medial motor nucleus of vagal nerve
1442	medial octavolateralis nucleus
1443	medial olfactory tract
1444	medial preglomerular nucleus
1445	medial protoglomerulus
1446	medial protoglomerulus 1
1447	medial protoglomerulus 2
1448	medial protoglomerulus 3
1449	medial protoglomerulus 4
1450	medial rectus
1451	medial valvula cerebelli
1452	medial zone of dorsal telencephalon
1453	medial zone olfactory bulb
1454	median axial vein
1455	median fin
1456	median fin cartilage
1457	median fin fold
1458	median palatocerebral vein
1459	median tuberal portion
1460	mediodorsal tooth row
1461	mediodorsal zone of the olfactory bulb
1462	medulla oblongata
1463	megakaryocyte erythroid progenitor cell
1464	MeL
1465	melanoblast
1466	melanocyte
1467	melanocyte stimulating hormone secreting cell
1468	melanophore stripe
1469	MeLc
1470	MeLm
1471	MeLr
1472	MeM
1473	MeM1
1474	membrane bone
1475	memory B cell
1476	memory T cell
1477	mental barbel
1478	Merkel cell
1479	mesangial cell
1480	mesencephalic artery
1481	mesencephalic nucleus of trigeminal nerve
1482	mesencephalic vein
1483	mesenchymal cell
1484	mesenchymal lymphangioblast
1485	mesenchyme
1486	mesenchyme condensation cell
1487	mesenchyme derived from head mesoderm
1488	mesenchyme derived from head neural crest
1489	mesenchyme derived from trunk neural crest
1490	mesenchyme dorsal fin
1491	mesenchyme median fin fold
1492	mesenchyme pectoral fin
1493	mesenchyme pelvic fin
1494	mesentery
1495	mesethmoid bone
1496	mesethmoid-lateral ethmoid joint
1497	mesethmoid-vomer joint
1498	meso-epithelial cell
1499	mesocoracoid bone
1500	mesocoracoid cartilage
1501	mesoderm
1502	mesoderm pectoral fin bud
1503	mesoderm pelvic fin bud
1504	mesodermal cell
1505	mesonephric duct
1506	mesonephric nephron
1507	mesonephric podocyte
1508	mesothelial cell
1509	mesovarium
1510	metapterygoid
1511	metapterygoid-symplectic fenestra
1512	metencephalic artery
1513	microcirculatory vessel
1514	microglial cell
1515	micropylar cell
1516	microvillous olfactory receptor neuron
1517	mid cerebral vein
1518	mid diencephalic organizer
1519	mid intestine
1520	mid intestine epithelium
1521	MiD2cl
1522	MiD2cm
1523	MiD2i
1524	MiD3cl
1525	MiD3cm
1526	MiD3i
1527	midbrain
1528	midbrain hindbrain boundary
1529	midbrain hindbrain boundary neural keel
1530	midbrain hindbrain boundary neural plate
1531	midbrain hindbrain boundary neural rod
1532	midbrain hindbrain boundary neural tube
1533	midbrain interneuron
1534	midbrain neural keel
1535	midbrain neural plate
1536	midbrain neural rod
1537	midbrain neural tube
1538	midbrain nucleus
1539	middle lateral line
1540	middle lateral line ganglion
1541	middle lateral line nerve
1542	middle lateral line neuromast
1543	middle lateral line placode
1544	middle lateral line primordium
1545	middle lateral line system
1546	middle mesencephalic central artery
1547	midline column
1548	migratory slow muscle precursor cell
1549	MiM1
1550	MiP motor neuron
1551	MiR1
1552	MiR2
1553	MiV1
1554	MiV2
1555	molecular layer corpus cerebelli
1556	molecular layer valvula cerebelli
1557	monoblast
1558	monocyte
1559	mononuclear cell
1560	mononuclear odontoclast
1561	mononuclear osteoclast
1562	mononuclear phagocyte
1563	mossy fiber
1564	motile cell
1565	motor neuron
1566	motor nucleus of vagal nerve
1567	mouth
1568	mucus secreting cell
1569	Muller cell
1570	multi fate stem cell
1571	multi-ciliated epithelial cell
1572	multi-tissue structure
1573	multilaminar epithelium
1574	multipolar neuron
1575	muscle
1576	muscle cell
1577	muscle pioneer
1578	muscle pioneer somite 1
1579	muscle pioneer somite 10
1580	muscle pioneer somite 11
1581	muscle pioneer somite 12
1582	muscle pioneer somite 13
1583	muscle pioneer somite 14
1584	muscle pioneer somite 15
1585	muscle pioneer somite 16
1586	muscle pioneer somite 17
1587	muscle pioneer somite 18
1588	muscle pioneer somite 19
1589	muscle pioneer somite 2
1590	muscle pioneer somite 20
1591	muscle pioneer somite 21
1592	muscle pioneer somite 22
1593	muscle pioneer somite 23
1594	muscle pioneer somite 24
1595	muscle pioneer somite 25
1596	muscle pioneer somite 26
1597	muscle pioneer somite 27
1598	muscle pioneer somite 28
1599	muscle pioneer somite 29
1600	muscle pioneer somite 3
1601	muscle pioneer somite 30
1602	muscle pioneer somite 4
1603	muscle pioneer somite 5
1604	muscle pioneer somite 6
1605	muscle pioneer somite 7
1606	muscle pioneer somite 8
1607	muscle pioneer somite 9
1608	muscle precursor cell
1609	muscle stem cell
1610	musculature system
1611	myelin accumulating cell
1612	myelinating Schwann cell
1613	myeloblast
1614	myeloid cell
1615	myeloid leukocyte
1616	myeloid lineage restricted progenitor cell
1617	myoblast
1618	myocardial precursor
1619	myocardium
1620	myoepithelial cell
1621	myoseptum
1622	myotome
1623	myotome somite 1
1624	myotome somite 10
1625	myotome somite 11
1626	myotome somite 12
1627	myotome somite 13
1628	myotome somite 14
1629	myotome somite 15
1630	myotome somite 16
1631	myotome somite 17
1632	myotome somite 18
1633	myotome somite 19
1634	myotome somite 2
1635	myotome somite 20
1636	myotome somite 21
1637	myotome somite 22
1638	myotome somite 23
1639	myotome somite 24
1640	myotome somite 25
1641	myotome somite 26
1642	myotome somite 27
1643	myotome somite 28
1644	myotome somite 29
1645	myotome somite 3
1646	myotome somite 30
1647	myotome somite 4
1648	myotome somite 5
1649	myotome somite 6
1650	myotome somite 7
1651	myotome somite 8
1652	myotome somite 9
1653	naive B cell
1654	NaK ionocyte
1655	naris
1656	nasal artery
1657	nasal bone
1658	nasal capsule
1659	nasal ciliary artery
1660	nasal vein
1661	natural killer cell
1662	NCC ionocyte
1663	nephrogenic mesenchyme stem cell
1664	nephron
1665	nephron progenitor
1666	nerve
1667	nervous system
1668	neural arch
1669	neural arch 3
1670	neural arch 4
1671	neural complex
1672	neural crest
1673	neural crest cell
1674	neural crest diencephalon
1675	neural crest hindbrain
1676	neural crest midbrain
1677	neural crest telencephalon
1678	neural keel
1679	neural plate
1680	neural postzygapophysis
1681	neural prezygapophysis
1682	neural rod
1683	neural spine
1684	neural spine 4
1685	neural tube
1686	neuraxis flexure
1687	neurecto-epithelial cell
1688	neurectodermal cell
1689	neuroblast
1690	neuroblast (sensu Vertebrata)
1691	neurocranial trabecula
1692	neurocranium
1693	neuroectoderm
1694	neuroendocrine cell
1695	neuroepithelial cell
1696	neurogenic field
1697	neurogenic placode
1698	neuroglioform cell
1699	neurohypophysis
1700	neuromast
1701	neuromast foramen
1702	neuromast hair cell
1703	neuromast mantle cell
1704	neuromast support cell
1705	neuromere
1706	neuron
1707	neuron neural crest derived
1708	neuronal stem cell
1709	neuroplacodal cell
1710	neurosecretory neuron
1711	neutrophil
1712	neutrophil progenitor cell
1713	neutrophilic metamyelocyte
1714	neutrophilic myeloblast
1715	neutrophilic myelocyte
1716	neutrophilic promyelocyte
1717	nevus
1718	nitrergic neuron
1719	non neural ectoderm
1720	non-Weberian precaudal vertebra
1721	noninvoluting endocytic marginal cell cluster
1722	norepinephrin secreting cell
1723	notochord
1724	notochord inner cell
1725	notochord outer sheath cell
1726	notochord posterior region
1727	notochordal ossification
1728	nucleate erythrocyte
1729	nucleus Edinger-Westphal
1730	nucleus isthmi
1731	nucleus lateralis valvulae
1732	nucleus of the descending root
1733	nucleus of the lateral recess
1734	nucleus of the medial longitudinal fasciculus medulla oblongata
1735	nucleus of the medial longitudinal fasciculus synencephalon
1736	nucleus of the posterior recess
1737	nucleus of the tract of the anterior commissure
1738	nucleus of the tract of the postoptic commissure
1739	nucleus ruber
1740	nucleus subglomerulosis
1741	nucleus taeniae
1742	occipital arch cartilage
1743	occipital lateral line
1744	occipital lateral line neuromast
1745	occipital region
1746	octaval nerve motor nucleus
1747	octaval nerve sensory nucleus
1748	ocular blood vessel
1749	oculomotor nucleus
1750	odontoblast
1751	odontoclast
1752	odontocyte
1753	odontoid tissue
1754	OFF-bipolar cell
1755	olfactory bulb
1756	olfactory bulb glomerulus
1757	olfactory bulb protoglomerulus
1758	olfactory epithelial support cell
1759	olfactory epithelium
1760	olfactory field
1761	olfactory granule cell
1762	olfactory nerve foramen
1763	olfactory pit
1764	olfactory placode
1765	olfactory receptor cell
1766	olfactory region
1767	olfactory rosette
1768	olfactory support cell
1769	olfactory system
1770	oligodendrocyte
1771	ON-bipolar cell
1772	oocyte
1773	oocyte stage I
1774	oocyte stage II
1775	oocyte stage III
1776	oocyte stage IV
1777	oocyte stage V
1778	opercle
1779	opercle-interopercle joint
1780	opercular artery
1781	opercular cavity
1782	opercular flap
1783	opercular lateral line
1784	opercular lateral line neuromast
1785	optic artery
1786	optic chiasm
1787	optic choroid
1788	optic choroid vascular plexus
1789	optic cup
1790	optic fiber layer
1791	optic fissure
1792	optic foramen
1793	optic furrow
1794	optic nerve head
1795	optic primordium
1796	optic recess
1797	optic stalk
1798	optic tectum
1799	optic tract
1800	optic vein
1801	optic vesicle
1802	oral cavity
1803	oral ectoderm
1804	oral epithelium
1805	oral region
1806	orbit
1807	orbital foramen
1808	orbital region
1809	orbitosphenoid
1810	orbitosphenoid-lateral ethmoid joint
1811	orbitosphenoid-prootic joint
1812	organism subdivision
1813	organizer inducing center
1814	os suspensorium
1815	os suspensorium medial flange
1816	osteoblast
1817	osteoclast
1818	osteocyte
1819	osteoprogenitor cell
1820	otic duct
1821	otic epithelium
1822	otic lateral line
1823	otic lateral line neuromast
1824	otic placode
1825	otic region
1826	otic sensory canal
1827	otic sensory epithelium
1828	otic squamous epithelium
1829	otic vesicle
1830	otic vesicle anterior protrusion
1831	otic vesicle lateral protrusion
1832	otic vesicle posterior protrusion
1833	otic vesicle protrusion
1834	otic vesicle ventral protrusion
1835	otolith
1836	otolith organ
1837	otolithic lymph vessel
1838	outer dental epithelium
1839	outer limiting membrane
1840	ovarian follicle
1841	ovarian follicle stage I
1842	ovarian follicle stage II
1843	ovarian follicle stage III
1844	ovarian follicle stage IV
1845	ovary
1846	oviduct
1847	oxygen accumulating cell
1848	pain receptor cell
1849	paired fin
1850	paired fin cartilage
1851	paired fin skeleton
1852	palate
1853	palatocerebral artery
1854	palatocerebral vein
1855	palatoquadrate arch
1856	palatoquadrate cartilage
1857	pancreas
1858	pancreas primordium
1859	pancreatic A cell
1860	pancreatic acinar cell
1861	pancreatic acinar gland
1862	pancreatic B cell
1863	pancreatic bud
1864	pancreatic centroacinar cell
1865	pancreatic D cell
1866	pancreatic duct
1867	pancreatic ductal cell
1868	pancreatic epsilon cell
1869	pancreatic fat
1870	pancreatic interlobular duct
1871	pancreatic intralobular duct
1872	pancreatic lobule
1873	pancreatic PP cell
1874	pancreatic system
1875	parachordal cartilage
1876	parachordal vessel
1877	paracommissural nucleus
1878	paracrine cell
1879	parafollicular cell
1880	parallel fiber
1881	parapineal organ
1882	parapophysis
1883	parapophysis 1
1884	parapophysis 2
1885	parapophysis/rib
1886	parasphenoid
1887	parasphenoid-basioccipital joint
1888	parasympathetic nervous system
1889	parasympathetic neuron
1890	parathyroid hormone secreting cell
1891	paraventricular organ
1892	paraxial mesoderm
1893	parhypural
1894	parietal bone
1895	parietal peritoneum
1896	pars anterior
1897	pars inferior ear
1898	pars intermedia
1899	pars subcommissuralis of ventral telencephalon
1900	pars superior ear
1901	parvocellular preoptic nucleus
1902	parvocellular superficial pretectal nucleus
1903	pectinate muscle
1904	pectoral artery
1905	pectoral fin
1906	pectoral fin actinotrichium
1907	pectoral fin blood vessel
1908	pectoral fin breeding tubercle
1909	pectoral fin bud
1910	pectoral fin cartilage
1911	pectoral fin distal radial
1912	pectoral fin endoskeletal disc
1913	pectoral fin field
1914	pectoral fin fold
1915	pectoral fin lepidotrichium
1916	pectoral fin lepidotrichium 1
1917	pectoral fin lepidotrichium 2
1918	pectoral fin lepidotrichium 3
1919	pectoral fin lepidotrichium 4
1920	pectoral fin lepidotrichium 5
1921	pectoral fin lepidotrichium 6
1922	pectoral fin lepidotrichium 7
1923	pectoral fin lymph vessel
1924	pectoral fin motor nerve
1925	pectoral fin motor nerve 1
1926	pectoral fin motor nerve 2
1927	pectoral fin motor nerve 3
1928	pectoral fin motor nerve 4
1929	pectoral fin musculature
1930	pectoral fin nerve
1931	pectoral fin plate fat
1932	pectoral fin proximal radial
1933	pectoral fin radial
1934	pectoral fin sensory nerve
1935	pectoral fin skeleton
1936	pectoral fin vasculature
1937	pectoral girdle
1938	pectoral lymphatic vessel
1939	pectoral vein
1940	pelvic abductor profundus
1941	pelvic adductor profundus
1942	pelvic fin
1943	pelvic fin actinotrichium
1944	pelvic fin bud
1945	pelvic fin cartilage
1946	pelvic fin field
1947	pelvic fin lepidotrichium
1948	pelvic fin lepidotrichium 1
1949	pelvic fin lepidotrichium 2
1950	pelvic fin lepidotrichium 3
1951	pelvic fin lepidotrichium 4
1952	pelvic fin musculature
1953	pelvic fin skeleton
1954	pelvic girdle
1955	pelvic radial
1956	pelvic radial 1
1957	pelvic radial 1 cartilage
1958	pelvic radial 2
1959	pelvic radial 2 cartilage
1960	pelvic radial 3
1961	pelvic radial 3 cartilage
1962	pelvic radial cartilage
1963	peptide hormone secreting cell
1964	peptidergic neuron
1965	periarticular chondrocyte
1966	pericardial cavity
1967	pericardial fat
1968	pericardial muscle
1969	pericardium
1970	perichondral bone
1971	perichondrium
1972	perichordal bone
1973	perichordal connective tissue
1974	pericyte
1975	periderm
1976	peridermal cell
1977	perijunctional fibroblast
1978	perilymph
1979	perilymphatic duct
1980	perilymphatic space
1981	perineuronal satellite cell
1982	periocular mesenchyme
1983	periorbital fat
1984	peripheral cardiac conduction system
1985	peripheral nervous system
1986	peripheral neuron
1987	peripheral nucleus of ventral telencephalon
1988	peripheral olfactory organ
1989	peritoneal macrophage
1990	peritoneum
1991	periventricular grey zone
1992	periventricular nucleus
1993	periventricular nucleus of caudal tuberculum
1994	periventricular nucleus of ventral telencephalon
1995	phagocyte
1996	pharyngeal arch
1997	pharyngeal arch 1
1998	pharyngeal arch 2
1999	pharyngeal arch 2 skeleton
2000	pharyngeal arch 3
2001	pharyngeal arch 3 skeleton
2002	pharyngeal arch 3-7
2003	pharyngeal arch 3-7 skeleton
2004	pharyngeal arch 4
2005	pharyngeal arch 4 skeleton
2006	pharyngeal arch 5
2007	pharyngeal arch 5 skeleton
2008	pharyngeal arch 6
2009	pharyngeal arch 6 skeleton
2010	pharyngeal arch 7
2011	pharyngeal arch 7 skeleton
2012	pharyngeal arch cartilage
2013	pharyngeal ectoderm
2014	pharyngeal endoderm
2015	pharyngeal epithelium
2016	pharyngeal mesoderm
2017	pharyngeal musculature
2018	pharyngeal pouch
2019	pharyngeal pouch 1
2020	pharyngeal pouch 2
2021	pharyngeal pouch 3
2022	pharyngeal pouch 4
2023	pharyngeal pouch 5
2024	pharyngeal pouch 6
2025	pharyngeal pouches 2-6
2026	pharyngeal vasculature
2027	pharyngobranchial 2 bone
2028	pharyngobranchial 2 cartilage
2029	pharyngobranchial 3 bone
2030	pharyngobranchial 3 cartilage
2031	pharyngobranchial 4 cartilage
2032	pharyngobranchial bone
2033	pharyngobranchial cartilage
2034	pharyngohyoid
2035	pharynx
2036	photopic photoreceptor cell
2037	photoreceptor cell
2038	photoreceptor inner segment layer
2039	photoreceptor outer segment layer
2040	pigment cell
2041	pigment cell (sensu Vertebrata)
2042	pigment erythroblast
2043	pigmented epithelial cell
2044	pillar of the anterior semicircular canal
2045	pillar of the lateral semicircular canal
2046	pillar of the posterior semicircular canal
2047	pillar of the semicircular canal
2048	pineal complex
2049	pinealocyte
2050	pioneer neuron
2051	plasma cell
2052	pleuroperitoneal cavity
2053	pneumatic duct
2054	podocyte
2055	polster
2056	polychromatophilic erythroblast
2057	polymodal neuron
2058	pore
2059	portion of connective tissue
2060	portion of organism substance
2061	portion of tissue
2062	post-otic sensory canal
2063	post-vent region
2064	post-vent vasculature
2065	post-Weberian supraneural
2066	postcleithrum
2067	postcommissural nucleus of ventral telencephalon
2068	postcranial axial cartilage
2069	postcranial axial skeleton
2070	posterior cardinal vein
2071	posterior caudal vein
2072	posterior cerebral vein
2073	posterior chamber swim bladder
2074	posterior communicating artery
2075	posterior crista
2076	posterior crista primordium
2077	posterior intestine
2078	posterior intestine epithelium
2079	posterior kidney
2080	posterior lateral line
2081	posterior lateral line ganglion
2082	posterior lateral line nerve
2083	posterior lateral line neuromast
2084	posterior lateral line placode
2085	posterior lateral line primordium
2086	posterior lateral line system
2087	posterior lateral mesoderm
2088	posterior lateral plate mesoderm
2089	posterior macula
2090	posterior mesencephalic central artery
2091	posterior mesenteric artery
2092	posterior naris
2093	posterior neural keel
2094	posterior neural plate
2095	posterior neural rod
2096	posterior neural tube
2097	posterior pancreatic bud
2098	posterior presumptive neural plate
2099	posterior pronephric duct
2100	posterior recess
2101	posterior sclerotic bone
2102	posterior segment eye
2103	posterior semicircular canal
2104	posteromedial glomerulus
2105	postoptic commissure
2106	postovulatory follicle
2107	posttemporal
2108	posttemporal fossa
2109	precaudal vertebra
2110	prechordal plate
2111	precursor B cell
2112	predentine
2113	preethmoid bone
2114	preglomerular nucleus
2115	premaxilla
2116	premaxilla ascending process
2117	premaxillary-maxillary ligament
2118	premaxillo-rostroid ligament
2119	preopercle
2120	preopercle horizontal limb
2121	preopercle horizontal limb-symplectic joint
2122	preopercle vertical limb
2123	preopercle vertical limb-hyomandibula joint
2124	preopercle-retroarticular ligament
2125	preopercular sensory canal
2126	preoperculo-mandibular sensory canal
2127	preoptic area
2128	preopticohypophyseal tract
2129	preopticohypothalamic tract
2130	preplacodal ectoderm
2131	pressoreceptor cell
2132	presumptive atrioventricular canal
2133	presumptive atrium heart tube
2134	presumptive atrium primitive heart tube
2135	presumptive blood
2136	presumptive brain
2137	presumptive bulbus arteriosus
2138	presumptive cardiac ventricle heart tube
2139	presumptive cardiac ventricle primitive heart tube
2140	presumptive cephalic mesoderm
2141	presumptive diencephalon
2142	presumptive dorsal fin fold
2143	presumptive dorsal mesoderm
2144	presumptive ectoderm
2145	presumptive endocardium
2146	presumptive endoderm
2147	presumptive enteric nervous system
2148	presumptive floor plate
2149	presumptive forebrain
2150	presumptive hindbrain
2151	presumptive hypochord
2152	presumptive intervening zone
2153	presumptive median fin fold
2154	presumptive mesoderm
2155	presumptive midbrain
2156	presumptive neural plate
2157	presumptive neural retina
2158	presumptive paraxial mesoderm
2159	presumptive pronephric mesoderm
2160	presumptive retinal pigmented epithelium
2161	presumptive rhombomere 1
2162	presumptive rhombomere 2
2163	presumptive rhombomere 3
2164	presumptive rhombomere 4
2165	presumptive rhombomere 5
2166	presumptive rhombomere 6
2167	presumptive rhombomere 7
2168	presumptive rhombomere 8
2169	presumptive segmental plate
2170	presumptive shield
2171	presumptive sinus venosus
2172	presumptive spinal cord
2173	presumptive structure
2174	presumptive swim bladder
2175	presumptive telencephalon
2176	presumptive ventral fin fold
2177	presumptive ventral mesoderm
2178	pretectal periventricular nucleus
2179	pretecto-mammillary tract
2180	pretectum
2181	preural 1 vertebra
2182	preural 2 vertebra
2183	preural centrum 1+ ural centrum 1
2184	preural vertebra
2185	prevomer
2186	primary dental epithelium
2187	primary germ layer
2188	primary head sinus
2189	primary interneuron
2190	primary islet
2191	primary motor neuron
2192	primary neuron
2193	primary neuron hindbrain
2194	primary olfactory fiber layer
2195	primary posterior lateral line primordium
2196	primitive heart tube
2197	primitive internal carotid artery
2198	primitive meninx
2199	primitive mesencephalic artery
2200	primitive olfactory epithelium
2201	primitive pectoral fin abductor
2202	primitive pectoral fin adductor
2203	primitive prosencephalic artery
2204	primordial germ cell
2205	primordial hindbrain channel
2206	primordial ligament
2207	primordial midbrain channel
2208	primordial vasculature
2209	pro-B cell
2210	pro-NK cell
2211	pro-T cell
2212	proctodeum
2213	proepicardial cluster
2214	professional antigen presenting cell
2215	progesterone secreting cell
2216	prolactin secreting cell
2217	proliferative region
2218	promonocyte
2219	pronephric capsular space
2220	pronephric distal early tubule
2221	pronephric distal late tubule
2222	pronephric duct
2223	pronephric duct opening
2224	pronephric glomerular basement membrane
2225	pronephric glomerular capillary
2226	pronephric glomerular capsule
2227	pronephric glomerular capsule epithelium
2228	pronephric glomerulus
2229	pronephric mesoderm
2230	pronephric podocyte
2231	pronephric proximal convoluted tubule
2232	pronephric proximal straight tubule
2233	pronephric tubule
2234	pronephros
2235	proneural cluster
2236	prootic
2237	prootic bulla
2238	prootic depression
2239	prootic foramen
2240	prootic-exoccipital joint
2241	prootic-pterosphenoid joint
2242	propterygium
2243	prosencephalic artery
2244	protoneuromast
2245	protractor hyoidei
2246	proximal convoluted tubule
2247	proximal pars anterior
2248	proximal straight tubule
2249	pseudobranchial artery
2250	pseudounipolar neuron
2251	pterosphenoid
2252	pterosphenoid-orbitosphenoid joint
2253	pterotic
2254	pterotic fossa
2255	pupil
2256	Purkinje cell
2257	Purkinje cell layer corpus cerebelli
2258	Purkinje cell layer valvula cerebelli
2259	pyramidal cell
2260	quadrate
2261	quadrate ventral process
2262	quadrate-anguloarticular joint
2263	quadrate-entopterygoid joint
2264	quadrate-hyomandibula joint
2265	quadrate-metapterygoid joint
2266	radial
2267	radial glial cell
2268	raphe nucleus
2269	ray vein
2270	receptor cell (sensu Animalia)
2271	recessus lateralis
2272	recurrent branch afferent branchial artery
2273	red sensitive photoreceptor cell
2274	regenerating fin
2275	regenerating tissue
2276	regeneration epithelium
2277	regeneration fibroblast
2278	renal alpha-intercalated cell
2279	renal artery
2280	renal capsular space
2281	renal corpuscle
2282	renal duct
2283	renal glomerular capsule
2284	renal glomerular capsule epithelium
2285	renal glomerulus
2286	renal intercalated cell
2287	renal portal vein
2288	renal principal cell
2289	renal system
2290	renal tubule
2291	renal vesicle
2292	replacement bone
2293	replacement element
2294	reproductive system
2295	respiratory system
2296	reticular cell
2297	reticular formation
2298	reticulocyte
2299	retina
2300	retinal bipolar neuron
2301	retinal cone cell
2302	retinal ganglion cell
2303	retinal ganglion cell layer
2304	retinal inner nuclear layer
2305	retinal inner plexiform layer
2306	retinal neural layer
2307	retinal outer nuclear layer
2308	retinal outer plexiform layer
2309	retinal photoreceptor layer
2310	retinal pigmented epithelium
2311	retinal rod cell
2312	retroarticular
2313	rhombencephalic efferent neurons to the lateral line
2314	rhombencephalic octavola-teral efferent neuron
2315	rhombic lip
2316	rhombomere
2317	rhombomere 1
2318	rhombomere 2
2319	rhombomere 3
2320	rhombomere 4
2321	rhombomere 5
2322	rhombomere 6
2323	rhombomere 7
2324	rhombomere 8
2325	rib
2326	rib of vertebra 1
2327	rib of vertebra 2
2328	rib of vertebra 3
2329	rib of vertebra 4
2330	rib of vertebra 5
2331	rib of vertebra 6
2332	right intestinal lymphatics
2333	right liver lobe
2334	rod bipolar cell
2335	Rohon-Beard neuron
2336	RoI2C
2337	RoI2R
2338	RoL1
2339	RoL2
2340	RoL2c
2341	RoL2r
2342	RoL3
2343	RoM1c
2344	RoM1r
2345	RoM2l
2346	RoM2m
2347	RoM3l
2348	RoM3m
2349	roof plate
2350	roof plate diencephalic region
2351	roof plate midbrain region
2352	roof plate neural tube region
2353	roof plate rhombomere 1
2354	roof plate rhombomere 2
2355	roof plate rhombomere 3
2356	roof plate rhombomere 4
2357	roof plate rhombomere 5
2358	roof plate rhombomere 6
2359	roof plate rhombomere 7
2360	roof plate rhombomere 8
2361	roof plate rhombomere region
2362	roof plate spinal cord region
2363	roofing cartilage
2364	RoP motor neuron
2365	rostral blood island
2366	rostral cerebellar tract
2367	rostral mesencephalo-cerebellar tract
2368	rostral motor nucleus
2369	rostral octaval nerve motor nucleus
2370	rostral octaval nucleus
2371	rostral pars anterior
2372	rostral parvocellular preoptic nucleus
2373	rostral preglomerular nucleus
2374	rostral root of abducens nerve
2375	rostral tegmental nucleus
2376	rostral thalamic nucleus
2377	rostral tuberal nucleus
2378	rostrolateral thalamic nucleus of Butler & Saidel
2379	RoV3
2380	rudimentary neural arch
2381	s-shaped body
2382	saccule
2383	sacculoagenar foramen
2384	saccus dorsalis
2385	sagitta
2386	scale
2387	scale primordium
2388	scaphium
2389	scapula
2390	scapular foramen
2391	scapulo-vertebral ligament
2392	scapulocoracoid
2393	Schlemm's canal
2394	sclera
2395	scleral cell
2396	sclerotic bone
2397	sclerotic cartilage
2398	sclerotome
2399	sclerotome somite 1
2400	sclerotome somite 10
2401	sclerotome somite 11
2402	sclerotome somite 12
2403	sclerotome somite 13
2404	sclerotome somite 14
2405	sclerotome somite 15
2406	sclerotome somite 16
2407	sclerotome somite 17
2408	sclerotome somite 18
2409	sclerotome somite 19
2410	sclerotome somite 2
2411	sclerotome somite 20
2412	sclerotome somite 21
2413	sclerotome somite 22
2414	sclerotome somite 23
2415	sclerotome somite 24
2416	sclerotome somite 25
2417	sclerotome somite 26
2418	sclerotome somite 27
2419	sclerotome somite 28
2420	sclerotome somite 29
2421	sclerotome somite 3
2422	sclerotome somite 30
2423	sclerotome somite 4
2424	sclerotome somite 5
2425	sclerotome somite 6
2426	sclerotome somite 7
2427	sclerotome somite 8
2428	sclerotome somite 9
2429	second tier layer of breeding tubercle
2430	secondary gustatory nucleus medulla oblongata
2580	sternohyoid
2431	secondary gustatory nucleus trigeminal nucleus
2432	secondary gustatory tract
2433	secondary islet
2434	secondary motor neuron
2435	secondary neuron
2436	secondary posterior lateral line primordium
2437	segmental intercostal artery
2438	segmental plate
2439	semicircular canal
2440	seminal fluid secreting cell
2441	sensory canal
2442	sensory canal tubular ossicle
2443	sensory canal tubule
2444	sensory epithelial cell
2445	sensory neuron
2446	sensory processing neuron
2447	sensory root of facial nerve
2448	sensory system
2449	sensory trigeminal nucleus
2450	serotonergic neuron
2451	serotonin secreting cell
2452	serous membrane
2453	Sertoli cell
2454	silver iridophore
2455	simple columnar epithelial cell
2456	simple columnar epithelium
2457	simple cuboidal epithelium
2458	simple organ
2459	simple squamous epithelium
2460	single ciliated epithelial cell
2461	single fate stem cell
2462	sinoatrial node
2463	sinoatrial region
2464	sinoatrial ring
2465	sinoatrial valve
2466	sinus impar
2467	sinus venosus
2468	sinusoidal blood vessel endothelium
2469	skeletal element
2470	skeletal muscle
2471	skeletal muscle cell
2472	skeletal muscle myoblast
2473	skeletal muscle satellite cell
2474	skeletal system
2475	skeletal tissue
2476	skin flap
2477	slow muscle cell
2478	slow muscle cell somite 1
2479	slow muscle cell somite 10
2480	slow muscle cell somite 11
2481	slow muscle cell somite 12
2482	slow muscle cell somite 13
2483	slow muscle cell somite 14
2484	slow muscle cell somite 15
2485	slow muscle cell somite 16
2486	slow muscle cell somite 17
2487	slow muscle cell somite 18
2488	slow muscle cell somite 19
2489	slow muscle cell somite 2
2490	slow muscle cell somite 20
2491	slow muscle cell somite 21
2492	slow muscle cell somite 22
2493	slow muscle cell somite 23
2494	slow muscle cell somite 24
2495	slow muscle cell somite 25
2496	slow muscle cell somite 26
2497	slow muscle cell somite 27
2498	slow muscle cell somite 28
2499	slow muscle cell somite 29
2500	slow muscle cell somite 3
2501	slow muscle cell somite 30
2502	slow muscle cell somite 4
2503	slow muscle cell somite 5
2504	slow muscle cell somite 6
2505	slow muscle cell somite 7
2506	slow muscle cell somite 8
2507	slow muscle cell somite 9
2508	slow muscle myoblast
2509	smooth muscle
2510	smooth muscle cell
2511	smooth muscle myoblast
2512	snout
2513	solid compound organ
2514	solid lens vesicle
2515	somatic cell
2516	somatic stem cell
2517	somatomotor neuron
2518	somatostatin secreting cell
2519	somatotropin secreting cell
2520	somite
2521	somite 1
2522	somite 10
2523	somite 11
2524	somite 12
2525	somite 13
2526	somite 14
2527	somite 15
2528	somite 16
2529	somite 17
2530	somite 18
2531	somite 19
2532	somite 2
2533	somite 20
2534	somite 21
2535	somite 22
2536	somite 23
2537	somite 24
2538	somite 25
2539	somite 26
2540	somite 27
2541	somite 28
2542	somite 29
2543	somite 3
2544	somite 30
2545	somite 4
2546	somite 5
2547	somite 6
2548	somite 7
2549	somite 8
2550	somite 9
2551	somite border
2552	specialized hemal arch and spine
2553	sperm
2554	sperm duct
2555	spermatid
2556	spermatocyte
2557	spermatogonium
2558	sphenoid region
2559	sphenotic
2560	sphenotic-prootic fossa
2561	spinal accessory motor neuron
2562	spinal artery
2563	spinal cord
2564	spinal cord interneuron
2565	spinal cord neural keel
2566	spinal cord neural plate
2567	spinal cord neural rod
2568	spinal cord neural tube
2569	spinal nerve
2570	spinal nerve root
2571	spinal neuromere
2572	spinous layer of breeding tubercle
2573	splanchnocranium
2574	spleen
2575	squamous epithelial cell
2576	statoacoustic (VIII) ganglion
2577	statoacoustic (VIII) nucleus
2578	stellate cell
2579	stellate interneuron
2581	steroid hormone secreting cell
2582	stomodeum
2583	stratified cuboidal epithelial cell
2584	stratified epithelial cell
2585	stratified epithelial stem cell
2586	stratified keratinized epithelial stem cell
2587	stratified squamous epithelial cell
2588	stratum album centrale
2589	stratum fibrosum et griseum superficiale
2590	stratum griseum centrale
2591	stratum marginale
2592	stratum opticum
2593	stratum periventriculare
2594	stretch receptor cell
2595	striated muscle cell
2596	stromal cell
2597	structural cell
2598	subcommissural organ
2599	subcutaneous fat
2600	subintestinal vein
2601	subopercle
2602	substance P secreting cell
2603	subtemporal fossa
2604	sulcus ypsiloniformis
2605	superficial abductor
2606	superficial adductor
2607	superficial blastomere
2608	superficial caudal fin musculature
2609	superficial grey and white zone
2610	superficial lateralis
2611	superficial layer of breeding tubercle
2612	superficial ophthalmic nerve foramen
2613	superficial pelvic abductor
2614	superficial pelvic adductor
2615	superficial pretectum
2616	superior caudal dorsal flexor
2617	superior caudal ventral flexor
2618	superior cervical ganglion
2619	superior raphe nucleus
2620	superior reticular formation medial column
2621	superior reticular formation tegmentum
2622	supportive cell
2623	supracarinalis
2624	suprachiasmatic nucleus
2625	supracleithrum
2626	supracleithrum-intercalar ligament
2627	supracleithrum-vertebral ligament
2628	supracommissural nucleus of ventral telencephalon
2629	supradorsal
2630	supraethmoid
2631	supraintestinal artery
2632	supraintestinal lymphatic vessel
2633	supraintestinal vein
2634	supraneural
2635	supraneural 10
2636	supraneural 2
2637	supraneural 3
2638	supraneural 5
2639	supraneural 6
2640	supraneural 7
2641	supraneural 8
2642	supraneural 9
2643	supraoccipital
2644	supraoccipital-parietal joint
2645	supraoptic commissure
2646	supraoptic tract
2647	supraorbital bone
2648	supraorbital lateral line
2649	supraorbital lateral line neuromast
2650	supraorbital sensory canal
2651	supratemporal sensory canal
2652	surface structure
2653	suspensorium
2654	sustentacular cell
2655	swim bladder
2656	swim bladder artery
2657	swim bladder bud
2658	sympathetic chain ganglion
2659	sympathetic nervous system
2660	sympathetic neuron
2661	symplectic
2662	synencephalon
2663	synostosis
2664	synovial cell
2665	synovial fluid
2666	synovial joint
2667	T cell
2668	T interneuron
2669	t-interneuron
2670	taenia marginalis anterior
2671	taenia marginalis posterior
2672	tail bud
2673	tangential nucleus
2674	taste bud
2675	taste receptor cell
2676	tectal ventricle
2677	tecto-bulbar tract
2678	tectum synoticum
2679	tegmental nucleus
2680	tegmentum
2681	tela chorioidea
2682	tela chorioidea fourth ventricle
2683	tela chorioidea tectal ventricle
2684	tela chorioidea telencephalic ventricle
2685	tela chorioidea third ventricle
2686	telencephalic nucleus
2687	telencephalic ventricle
2688	telencephalic white matter
2689	telencephalon
2690	telencephalon diencephalon boundary
2691	tendon
2692	tendon cell
2693	terminal nerve
2694	terminal Schwann cell
2695	tertiary gustatory nucleus
2696	testis
2697	testosterone secreting cell
2698	tether cell
2699	thalamus
2700	theca cell
2701	thecal cell layer
2702	thermoreceptor cell
2703	third ventricle
2704	thoracic duct
2705	thromboblast
2706	thrombocyte
2707	thymic epithelium
2708	thymus
2709	thymus primordium
2710	thyroid follicle
2711	thyroid hormone secreting cell
2712	thyroid primordium
2713	thyroid stimulating hormone secreting cell
2714	tongue
2715	tooth 1D
2716	tooth 1MD
2717	tooth 1V
2718	tooth 2D
2719	tooth 2MD
2720	tooth 2V
2721	tooth 3MD
2722	tooth 3V
2723	tooth 4MD
2724	tooth 4V
2725	tooth 5V
2726	tooth cusp
2727	tooth placode
2728	tooth pulp
2729	tooth row
2730	torus lateralis
2731	torus longitudinalis
2732	torus semicircularis
2733	totipotent stem cell
2734	trabecula communis
2735	trabecula cranii
2736	trabecular layer
2737	trabecular layer of the atrium
2738	trabecular layer of ventricle
2739	tract of the caudal commissure
2740	tract of the postoptic commissure
2741	trans-choroid plexus branch
2742	transitional epithelial cell
2743	transitional stage B cell
2744	transverse band
2745	transverse canal
2746	tri-tipped trabecula
2747	trigeminal ganglion
2748	trigeminal motor nucleus
2749	trigeminal neural crest
2750	trigeminal placode
2751	trigeminal sensory neuron
2752	tripus
2753	trochlear motor nucleus
2754	trunk
2755	trunk ganglion
2756	trunk mesenchyme
2757	trunk musculature
2758	trunk neural crest
2759	trunk neural crest cell
2760	trunk sensory canal
2761	trunk vasculature
2762	tunica externa
2763	tunica interna
2764	UCoD
2765	ultimobranchial body
2766	uncrossed tecto-bulbar tract
2767	unfertilized egg
2768	unilaminar epithelium
2769	unipolar neuron
2770	unspecified
2771	upper left intestinal lymph vessel
2772	upper lip
2773	upper oral valve
2774	upper rhombic lip
2775	upper right intestinal lymph vessel
2776	ural centrum 2
2777	ural vertebra 2
2778	urogenital papilla
2779	urohyal
2780	uroneural
2781	urostyle
2782	urothelial cell
2783	utricle
2784	utriculosaccular foramen
2785	UV sensitive photoreceptor cell
2786	vagal ganglion
2787	vagal ganglion 1
2788	vagal ganglion 2
2789	vagal ganglion 3
2790	vagal ganglion 4
2791	vagal lobe
2792	vagal neural crest
2793	vagal placode 1
2794	vagal placode 2
2795	vagal placode 3
2796	vagal placode 4
2797	vagal root
2798	vagal sensory zone
2799	valvula cerebelli
2800	VaP motor neuron
2801	vascular associated smooth muscle cell
2802	vascular cord
2803	vascular endothelium
2804	vascular lymphangioblast
2805	vascular smooth muscle
2806	vascular sprouts
2807	vasculature
2808	vein
2809	VeLD
2810	vent
2811	ventral accessory optic nucleus
2812	ventral actinotrichium
2813	ventral anterior lateral line ganglion
2814	ventral anterior lateral line nerve
2815	ventral aorta
2816	ventral arrector
2817	ventral branch nasal ciliary artery
2818	ventral canal eye
2819	ventral caudal adductor
2820	ventral entopeduncular nucleus of ventral telencephalon
2821	ventral fin fold
2822	ventral funiculus
2823	ventral habenular nucleus
2824	ventral horn spinal cord
2825	ventral hyoid arch
2826	ventral hypohyal bone
2827	ventral hypohyal-urohyal joint
2828	ventral hypothalamic zone
2829	ventral interfilamental caudal muscle
2830	ventral intermandibularis anterior
2831	ventral intermandibularis posterior
2832	ventral larval melanophore stripe
2833	ventral lateral mesoderm
2834	ventral liver lobe
2835	ventral mandibular arch
2836	ventral mesenchyme
2837	ventral mesentery
2838	ventral mesoderm
2839	ventral motor nucleus trigeminal nerve
2840	ventral nucleus of ventral telencephalon
2841	ventral oblique branchial muscle
2842	ventral oblique extraocular muscle
2843	ventral pelvic arrector
2844	ventral posterior glomerulus
2845	ventral proneural cluster
2846	ventral rectus
2847	ventral rhombencephalic commissure
2848	ventral rhombencephalic commissure medulla oblongata
2849	ventral root
2850	ventral spinal nerve
2851	ventral spinal nerve median branch
2852	ventral spinal nerve septal branch
2853	ventral sulcus
2854	ventral telencephalon
2855	ventral thalamus
2856	ventral thalamus nucleus
2857	ventral tooth row
2858	ventral transverse
2859	ventral wall of dorsal aorta
2860	ventral zone olfactory bulb
2861	ventricular endocardium
2862	ventricular epicardium
2863	ventricular myocardium
2864	ventricular system
2865	ventricular zone
2866	ventriculo bulbo valve
2867	ventro-caudal cluster
2868	ventro-rostral cluster
2869	ventroanterior zone of olfactory bulb
2870	ventrolateral nucleus
2871	ventrolateral optic tract
2872	ventrolateral thalamic nucleus
2873	ventromedial thalamic nucleus
2874	ventromedial zone olfactory bulb
2875	ventroposterior zone olfactory bulb
2876	venule
2877	vertebra
2878	vertebra 1
2879	vertebra 10
2880	vertebra 11
2881	vertebra 12
2882	vertebra 2
2883	vertebra 3
2884	vertebra 4
2885	vertebra 4-vertebra 5 joint
2886	vertebra 5
2887	vertebra 5-vertebra 6 joint
2888	vertebra 6
2889	vertebra 6 - vertebra 7 joint
2890	vertebra 7
2891	vertebra 8
2892	vertebra 9
2893	vertebral artery
2894	vertebral column
2895	vertical myoseptum
2896	vestibulo-spinal tract
2897	vestibuloauditory system
2898	vestibulolateralis lobe
2899	vH ionocyte
2900	visceral fat
2901	visceral peritoneum
2902	visceromotor column
2903	visceromotor neuron
2904	viscerosensory commissural nucleus of Cajal
2905	visible light photoreceptor cell
2906	visual pigment cell (sensu Vertebrata)
2907	visual system
2908	vitreous
2909	vmG1
2910	vmG2
2911	vmG3
2912	vmG4
2913	vmG5
2914	vmG6
2915	vmG7
2916	vmGx
2917	vmGy
2918	vpG1
2919	vpG2
2920	Weberian apparatus
2921	Weberian ossicle
2922	Weberian vertebra
2923	white matter
2924	whole organism
2925	xanthoblast
2926	xanthophore
2927	yolk
2928	yolk larval melanophore stripe
2929	YSL
2930	zona limitans intrathalamica
2931	zona radiata
2932	zone of polarizing activity pectoral fin bud
2933	zone of polarizing activity pelvic fin bud
2934	zonule
\.


--
-- Data for Name: modeldcc_appliedassay; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_appliedassay (id, appliedassay_id, date_added, date_edited, assay_id, biosamplereplicate_id, controlled_by_id, user_id) FROM stdin;
1	DCD000001AA	2018-08-14 07:17:30.713547+00	2018-08-14 07:17:30.752209+00	2	1	\N	1
\.


--
-- Data for Name: modeldcc_assay; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_assay (id, assay_id, description, target, library_prep, date_added, date_edited, assay_lab_id, assay_type_id, user_id) FROM stdin;
1	RNA-seq_test_lab_0001AS	test		\N	2018-08-14 07:16:05.582553+00	2018-08-14 07:16:05.59349+00	1	0	1
2	RNA-seq_test_lab_0002AS	test		null	2018-08-14 07:17:22.792674+00	2018-08-14 07:17:22.8342+00	1	0	1
\.


--
-- Data for Name: modeldcc_assay_types; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_assay_types (id, name) FROM stdin;
0	RNA-seq
1	short-RNA-seq
2	miRNA-seq
3	ChIP-seq
4	DNAse-seq
5	Hi-C-seq
6	ChIA-PET-seq
7	FAIRE-seq
8	ATAC-seq
9	CAGE-seq
10	Bru-seq
11	PAS-seq
12	Ribo-seq
13	SELEX-seq
14	STARR-seq
15	Capture-C-seq
16	RIP-seq
17	PAR-Clip-seq
18	iCLIP-seq
19	ChIP-exo-seq
20	BS-seq
21	RRBS
22	TAB-seq
23	GRO-seq
24	3P-seq
25	PAL-seq
26	SAPAS-seq
27	MeDIP-seq
28	4C-seq
29	MNase-seq
30	MethylC-seq
31	MethylCap-seq
\.


--
-- Data for Name: modeldcc_batchuploaddetails; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_batchuploaddetails (id, errors, row_type, series00internal_id, series00series_id, series00title, series00series_type, series00is_public, series00description, series00publication, series00sra_geo_id, biosample00internal_id, biosample00biosample_id, biosample00biosample_type, biosample00biosample_lab, biosample00genetic_background, biosample00stage, biosample00controlled_by, biosample00anatomical_term, biosample00organism, biosample00treatment, biosample00post_fertilization, biosample00source, biosample00sex, biosample00mutation_description, biosample00cell_line_type, biosample00description, biosamplereplicate00internal_id, biosamplereplicate00biosamplereplicate_id, assay00internal_id, assay00assay_id, assay00assay_type, assay00assay_lab, assay00description, assay00target, assay00library_prep, appliedassay00internal_id, appliedassay00appliedassay_id, appliedassay00controlled_by, technicalreplicate00internal_id, technicalreplicate00technicalreplicate_id, sequencing00internal_id, sequencing00sequencing_id, sequencing00platform, sequencing00sequencing_lab, sequencing00instrument, sequencing00chemistry_version, sequencing00paired_end, sequencing00strand_mode, sequencing00sequencing_date, sequencing00max_read_length, data00url, data00local_path, data00secondary_url, data00secondary_local_path, "batchUpload_id") FROM stdin;
1	<span class="fas fa-asterisk" aria-hidden="true"></span>Error 1: Controlled vocabulary field biosample_lab contains invalid value.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 2: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 3: Invalid biosample model instance (most likely missing required fields or mis-formated data type). Possible fields: series, biosample_lab.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 4: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 5: Invalid biosamplereplicate model instance (most likely missing required fields or mis-formated data type). Possible fields: biosample.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 6: Controlled vocabulary field assay_lab contains invalid value.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 7: Invalid assay model instance (most likely missing required fields or mis-formated data type). Possible fields: assay_lab.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 8: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 9: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 10: Invalid appliedassay model instance (most likely missing required fields or mis-formated data type). Possible fields: biosamplereplicate, assay.	\N	1		Comprehensive identification of long non-coding RNAs expressed during zebrafish embryogenesis [ChIP_	Survey	TRUE	Long non-coding RNAs (lncRNAs) comprise a diverse class of transcripts that structurally resemble mR	Pauli A et al., "Systematic identification of long noncoding RNAs expressed during zebrafish embryog	PRJNA154403	1		whole organism	Mueller lab	WT	Shield		early embryonic cell			6						1		1		ChIP-seq	Mueller lab	ChIPSeq_WholeCellExtract	Mock		1			1		1		Illumina	Mueller lab	HiSeq 3000		yes	unstranded	2000-01-01	101		upload/ChIPSeq_WholeCellExtract_shield.R1.fastq		upload/ChIPSeq_WholeCellExtract_shield.R2.fastq	1
2	<span class="fas fa-asterisk" aria-hidden="true"></span>Error 1: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 2: Invalid biosample model instance (most likely missing required fields or mis-formated data type). Possible fields: series, biosample_type, genetic_background, biosample_lab.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 3: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 4: Invalid biosamplereplicate model instance (most likely missing required fields or mis-formated data type). Possible fields: biosample.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 5: Controlled vocabulary field assay_lab contains invalid value.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 6: Invalid assay model instance (most likely missing required fields or mis-formated data type). Possible fields: assay_lab.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 7: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 8: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 9: Invalid appliedassay model instance (most likely missing required fields or mis-formated data type). Possible fields: biosamplereplicate, assay.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 10: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.	\N	1								1																1		2		ChIP-seq	Mueller lab	ChIPSeq_WholeCellExtract	H3K27me3		2		1	2		2		Illumina	Mueller lab	HiSeq 3000		yes	unstranded	2000-01-01	101		upload/ChIPSeq_H3K27me3_shield.R1.fastq		upload/ChIPSeq_H3K27me3_shield.R2.fastq	1
3	<span class="fas fa-asterisk" aria-hidden="true"></span>Error 1: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 2: Invalid biosample model instance (most likely missing required fields or mis-formated data type). Possible fields: series, biosample_type, genetic_background, biosample_lab.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 3: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 4: Invalid biosamplereplicate model instance (most likely missing required fields or mis-formated data type). Possible fields: biosample.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 5: Controlled vocabulary field assay_lab contains invalid value.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 6: Invalid assay model instance (most likely missing required fields or mis-formated data type). Possible fields: assay_lab.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 7: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 8: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 9: Invalid appliedassay model instance (most likely missing required fields or mis-formated data type). Possible fields: biosamplereplicate, assay.<br><span class="fas fa-asterisk" aria-hidden="true"></span>Error 10: Batch upload cannot be the first data entered. If data exists in the data base please contact the administrators.	\N	1								1																1		3		ChIP-seq	Mueller lab	ChIPSeq_WholeCellExtract	H3K4me3		3		1	3		3		Illumina	Mueller lab	HiSeq 3000		yes	unstranded	2000-01-01	101		upload/ChIPSeq_H3K4me3_shield.R1.fastq		upload/ChIPSeq_H3K4me3_shield.R2.fastq	1
\.


--
-- Data for Name: modeldcc_batchuploads; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_batchuploads (id, "csvFile", "isValid", committed, user_id) FROM stdin;
1	batch_upload_csv/test-user.False.Sample_Batch_Upload_6.csv.csv	f	f	1
\.


--
-- Data for Name: modeldcc_biosample; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_biosample (id, biosample_id, treatment, post_fertilization, source, mutation_description, cell_line_type, description, date_added, date_edited, anatomical_term_id, biosample_lab_id, biosample_type_id, controlled_by_id, genetic_background_id, organism_id, series_id, sex_id, stage_id, user_id) FROM stdin;
1	DCD000001BS		1					2018-08-14 07:17:15.664293+00	2018-08-14 07:17:15.681992+00	\N	1	0	\N	0	1	5	\N	0	1
\.


--
-- Data for Name: modeldcc_biosample_types; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_biosample_types (id, name) FROM stdin;
0	whole organism
1	tissue
2	stem cells
3	cell line
\.


--
-- Data for Name: modeldcc_biosamplereplicate; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_biosamplereplicate (id, biosamplereplicate_id, date_added, date_edited, biosample_id, user_id) FROM stdin;
1	DCD000001BR	2018-08-14 07:17:15.740993+00	2018-08-14 07:17:15.751351+00	1	1
\.


--
-- Data for Name: modeldcc_data; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_data (id, data_id, primary_file, secondary_file, command, primary_source, secondary_source, sha1, date_added, date_edited, derived_from_id, file_type_id, mapped_genome_id, sequencing_id, user_id, version_id) FROM stdin;
1	DCD000001DT	RNA-seq/DCD000001BS/RNA-seq_test_lab_0002AS.DCD000001SQ.USERtest-user.R1.fastq.gz	RNA-seq/DCD000001BS/RNA-seq_test_lab_0002AS.DCD000001SQ.USERtest-user.R2.fastq.gz		test	\N	4e1243bd22c66e76c2ba9eddc1f91394e57f9f83	2018-08-14 07:19:48.983303+00	2018-08-14 07:19:48.990731+00	\N	0	\N	1	1	\N
\.


--
-- Data for Name: modeldcc_data_derived_from_data; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_data_derived_from_data (id, from_data_id, to_data_id) FROM stdin;
\.


--
-- Data for Name: modeldcc_developmental_stages; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_developmental_stages (id, name) FROM stdin;
0	1-cell
1	2-cell
2	4-cell
3	8-cell
4	16-cell
5	32-cell
6	64-cell
7	128-cell
8	256-cell
9	512-cell
10	1k-cell
11	High
12	Oblong
13	Sphere
14	Dome
15	30%-epiboly
16	50%-epiboly
17	Germ-ring
18	Shield
19	75%-epiboly
20	90%-epiboly
21	Bud
22	1-4 somites
23	5-9 somites
24	10-13 somites
25	14-19 somites
26	20-25 somites
27	26+ somites
28	Prim-5
29	Prim-15
30	Prim-25
31	High-pec
32	Long-pec
33	Pec-fin
34	Protruding-mouth
35	Day 4
36	Day 5
37	Day 6
38	Days 7-13
39	Days 14-20
40	Days 21-29
41	Days 30-44
42	Days 45-89
43	90 Days-2 Years
\.


--
-- Data for Name: modeldcc_file_types; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_file_types (id, name) FROM stdin;
0	FASTQ
1	BAM
2	BED
3	BEDGRAPH
4	WIGGLE
5	BIGBED
6	BIGWIG
7	GTF
8	GFF
9	VCF
10	TSV
11	LOG
\.


--
-- Data for Name: modeldcc_genetic_backgrounds; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_genetic_backgrounds (id, name) FROM stdin;
0	AB
1	AB/C32
2	AB/EKW
3	AB/TL
4	AB/TU
5	C32
6	KOLN
7	DAR
8	EKW
9	HK/AB
10	HK/SING
11	HK
12	IND
13	INDO
14	SPF 5-D
15	SPF AB
16	NA
17	NHGR-1
18	RW
19	SAT
20	SING
21	SJA
22	SJD
23	SJD/C32
24	TU
25	TL
26	TLN
27	WIK
28	WIK/AB
29	WT
\.


--
-- Data for Name: modeldcc_instruments; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_instruments (id, name) FROM stdin;
0	AB SOLiD System
1	ABI 377 automated sequencer
2	Genome Sequence 20
3	Genome Sequence FLX+ / FLX
4	HiSeq X Ten
5	HiSeq 2000
6	HiSeq 2500
7	HiSeq 3000
8	HiSeq 4000
9	HiSeq 5000
10	NextSeq 500 High-Output
11	NextSeq 500 Mid-Output
12	HiSeq High-Output v4
13	HiSeq High-Output v3
14	HiSeq Rapid Run
15	HiScanSQ
16	GAIIx
17	Li-Cor 4300 DNA Analysis System
18	MiSeq v3
\.


--
-- Data for Name: modeldcc_labs; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_labs (id, lab_id, name, lab_pi, lab_institution, lab_country, date_added) FROM stdin;
1	DCD001LB	test lab	PI	Institute	Atlantis	2018-05-22 11:11:10.987119+00
\.


--
-- Data for Name: modeldcc_mapped_genomes; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_mapped_genomes (id, name) FROM stdin;
0	Zv7
1	Zv9
2	GRCz10
\.


--
-- Data for Name: modeldcc_organisms; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_organisms (id, name) FROM stdin;
1	Danio rerio
\.


--
-- Data for Name: modeldcc_platforms; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_platforms (id, name) FROM stdin;
0	Illumina
1	Ion
2	PacBio
3	Roche 454
4	SOLiD
\.


--
-- Data for Name: modeldcc_sequencing; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_sequencing (id, sequencing_id, chemistry_version, paired_end, max_read_length, sequencing_date, date_added, date_edited, instrument_id, platform_id, sequencing_lab_id, strand_mode_id, technicalreplicate_id, user_id) FROM stdin;
1	DCD000001SQ		f	\N	1999-12-29	2018-08-14 07:18:02.214782+00	2018-08-14 07:18:02.239242+00	0	0	1	1	1	1
\.


--
-- Data for Name: modeldcc_series; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_series (id, series_id, title, series_type, is_public, description, publication, sra_geo_id, date_added, date_edited, status_id, user_id) FROM stdin;
5	DCD000005SR	test	Survey	t	test			2018-08-14 07:17:03.825341+00	2018-08-14 07:21:51.414551+00	1	1
\.


--
-- Data for Name: modeldcc_sexes; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_sexes (id, name) FROM stdin;
0	m
1	f
\.


--
-- Data for Name: modeldcc_statuses; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_statuses (id, name) FROM stdin;
0	initial entry
1	complete upload
2	with associated files
\.


--
-- Data for Name: modeldcc_strand_modes; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_strand_modes (id, name) FROM stdin;
0	unstranded
1	forward
2	reverse
\.


--
-- Data for Name: modeldcc_technicalreplicate; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_technicalreplicate (id, technicalreplicate_id, date_added, date_edited, appliedassay_id, user_id) FROM stdin;
1	DCD000001TR	2018-08-14 07:17:30.769368+00	2018-08-14 07:17:30.787025+00	1	1
\.


--
-- Data for Name: modeldcc_versions; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.modeldcc_versions (id, name) FROM stdin;
\.




--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	auth	group
2	auth	permission
3	auth	user
4	contenttypes	contenttype
5	sessions	session
6	modeldcc	biosample_types
7	modeldcc	genetic_backgrounds
8	modeldcc	strand_modes
9	modeldcc	file_types
10	modeldcc	batchuploads
11	modeldcc	series
12	modeldcc	assay
13	modeldcc	instruments
14	modeldcc	sexes
15	modeldcc	mapped_genomes
16	modeldcc	versions
17	modeldcc	appliedassay
18	modeldcc	platforms
19	modeldcc	biosamplereplicate
20	modeldcc	organisms
21	modeldcc	labs
22	modeldcc	batchuploaddetails
23	modeldcc	biosample
24	modeldcc	anatomical_terms
25	modeldcc	developmental_stages
26	modeldcc	technicalreplicate
27	modeldcc	assay_types
28	modeldcc	sequencing
29	modeldcc	statuses
30	modeldcc	data
31	admin	logentry
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-05-18 09:41:53.02265+00
2	auth	0001_initial	2018-05-18 09:41:53.273425+00
3	admin	0001_initial	2018-05-18 09:41:53.408414+00
4	admin	0002_logentry_remove_auto_add	2018-05-18 09:41:53.452259+00
5	contenttypes	0002_remove_content_type_name	2018-05-18 09:41:53.532142+00
6	auth	0002_alter_permission_name_max_length	2018-05-18 09:41:53.559523+00
7	auth	0003_alter_user_email_max_length	2018-05-18 09:41:53.598737+00
8	auth	0004_alter_user_username_opts	2018-05-18 09:41:53.625452+00
9	auth	0005_alter_user_last_login_null	2018-05-18 09:41:53.682611+00
10	auth	0006_require_contenttypes_0002	2018-05-18 09:41:53.686882+00
11	auth	0007_alter_validators_add_error_messages	2018-05-18 09:41:53.717643+00
12	auth	0008_alter_user_username_max_length	2018-05-18 09:41:53.808319+00
13	modeldcc	0001_initial	2018-05-18 09:41:56.484649+00
14	sessions	0001_initial	2018-05-18 09:41:56.52327+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: test-user
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
mz3kqjvj55l4wlkktkop7t5ndwzykdsa	N2NmOTc0MjQ1NDcyNmRjMGRmODQ4N2M5N2QyOWI5MmNkZWVhYTYzMDp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1OGI2YjA3Zjg0MGJiYmI3MGNhMjcxYmZkMmE5NzQ0NjAwZTBjNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-06-01 09:51:07.6546+00
96gi42zqbwlht1r8javm7nqsqaiozvqv	N2NmOTc0MjQ1NDcyNmRjMGRmODQ4N2M5N2QyOWI5MmNkZWVhYTYzMDp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1OGI2YjA3Zjg0MGJiYmI3MGNhMjcxYmZkMmE5NzQ0NjAwZTBjNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-08-28 07:03:32.120703+00
f9kxw4jhirgi6m073iprhqixm3v8uad7	N2NmOTc0MjQ1NDcyNmRjMGRmODQ4N2M5N2QyOWI5MmNkZWVhYTYzMDp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1OGI2YjA3Zjg0MGJiYmI3MGNhMjcxYmZkMmE5NzQ0NjAwZTBjNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-08-31 09:47:39.736358+00
ngyy0y2c2lavtbo1csoazvfxr53iio6y	N2NmOTc0MjQ1NDcyNmRjMGRmODQ4N2M5N2QyOWI5MmNkZWVhYTYzMDp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1OGI2YjA3Zjg0MGJiYmI3MGNhMjcxYmZkMmE5NzQ0NjAwZTBjNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-08-31 09:47:42.488365+00
2v6sihcmtnz02qtoq7dro39vvx3ie2nn	N2NmOTc0MjQ1NDcyNmRjMGRmODQ4N2M5N2QyOWI5MmNkZWVhYTYzMDp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1OGI2YjA3Zjg0MGJiYmI3MGNhMjcxYmZkMmE5NzQ0NjAwZTBjNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-08-31 09:51:17.936485+00
e9mfr5vxtglrhn0tg9y095vdd91i5fgk	N2NmOTc0MjQ1NDcyNmRjMGRmODQ4N2M5N2QyOWI5MmNkZWVhYTYzMDp7Il9hdXRoX3VzZXJfaGFzaCI6ImU1OGI2YjA3Zjg0MGJiYmI3MGNhMjcxYmZkMmE5NzQ0NjAwZTBjNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-08-31 09:51:22.54005+00
knc7z8nm0f87bam3kmw0i0tbrh1aum0f	ZjBjNTUxNzZkZDU3Yjc3ZWYxNWZlZTc2NDUxYjFiN2QwZjM2M2Q1MDp7Il9hdXRoX3VzZXJfaGFzaCI6IjJiMmVkNjg4YWM3MTRiYzg4ZTkzNjM2Njc1MDJmNDc1NTA5OWQwYWEiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2018-10-11 12:54:17.622314+00
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 93, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: modeldcc_anatomical_terms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_anatomical_terms_id_seq', 1, false);


--
-- Name: modeldcc_appliedassay_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_appliedassay_id_seq', 1, true);


--
-- Name: modeldcc_assay_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_assay_id_seq', 2, true);


--
-- Name: modeldcc_assay_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_assay_types_id_seq', 1, false);


--
-- Name: modeldcc_batchuploaddetails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_batchuploaddetails_id_seq', 3, true);


--
-- Name: modeldcc_batchuploads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_batchuploads_id_seq', 1, true);


--
-- Name: modeldcc_biosample_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_biosample_id_seq', 1, true);


--
-- Name: modeldcc_biosample_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_biosample_types_id_seq', 1, false);


--
-- Name: modeldcc_biosamplereplicate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_biosamplereplicate_id_seq', 1, true);


--
-- Name: modeldcc_data_derived_from_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_data_derived_from_data_id_seq', 1, false);


--
-- Name: modeldcc_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_data_id_seq', 1, true);


--
-- Name: modeldcc_developmental_stages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_developmental_stages_id_seq', 44, true);


--
-- Name: modeldcc_file_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_file_types_id_seq', 1, false);


--
-- Name: modeldcc_genetic_backgrounds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_genetic_backgrounds_id_seq', 1, false);


--
-- Name: modeldcc_instruments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_instruments_id_seq', 1, false);


--
-- Name: modeldcc_labs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_labs_id_seq', 1, true);


--
-- Name: modeldcc_mapped_genomes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_mapped_genomes_id_seq', 1, false);


--
-- Name: modeldcc_organisms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_organisms_id_seq', 1, false);


--
-- Name: modeldcc_platforms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_platforms_id_seq', 1, false);


--
-- Name: modeldcc_sequencing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_sequencing_id_seq', 1, true);


--
-- Name: modeldcc_series_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_series_id_seq', 5, true);


--
-- Name: modeldcc_sexes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_sexes_id_seq', 1, false);


--
-- Name: modeldcc_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_statuses_id_seq', 1, false);


--
-- Name: modeldcc_strand_modes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_strand_modes_id_seq', 1, false);


--
-- Name: modeldcc_technicalreplicate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_technicalreplicate_id_seq', 1, true);


--
-- Name: modeldcc_versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.modeldcc_versions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 13, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 31, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test-user
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 14, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: modeldcc_anatomical_terms modeldcc_anatomical_terms_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_anatomical_terms
    ADD CONSTRAINT modeldcc_anatomical_terms_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_appliedassay modeldcc_appliedassay_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_appliedassay
    ADD CONSTRAINT modeldcc_appliedassay_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_assay modeldcc_assay_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay
    ADD CONSTRAINT modeldcc_assay_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_assay_types modeldcc_assay_types_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay_types
    ADD CONSTRAINT modeldcc_assay_types_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_batchuploaddetails modeldcc_batchuploaddetails_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_batchuploaddetails
    ADD CONSTRAINT modeldcc_batchuploaddetails_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_batchuploads modeldcc_batchuploads_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_batchuploads
    ADD CONSTRAINT modeldcc_batchuploads_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_biosample modeldcc_biosample_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosample_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_biosample_types modeldcc_biosample_types_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample_types
    ADD CONSTRAINT modeldcc_biosample_types_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_biosamplereplicate modeldcc_biosamplereplicate_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosamplereplicate
    ADD CONSTRAINT modeldcc_biosamplereplicate_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_data_derived_from_data modeldcc_data_derive_from_data_id_to_data_id_e2a1e389_uniq; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data_derived_from_data
    ADD CONSTRAINT modeldcc_data_derive_from_data_id_to_data_id_e2a1e389_uniq UNIQUE (from_data_id, to_data_id);


--
-- Name: modeldcc_data_derived_from_data modeldcc_data_derived_from_data_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data_derived_from_data
    ADD CONSTRAINT modeldcc_data_derived_from_data_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_data modeldcc_data_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_developmental_stages modeldcc_developmental_stages_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_developmental_stages
    ADD CONSTRAINT modeldcc_developmental_stages_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_file_types modeldcc_file_types_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_file_types
    ADD CONSTRAINT modeldcc_file_types_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_genetic_backgrounds modeldcc_genetic_backgrounds_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_genetic_backgrounds
    ADD CONSTRAINT modeldcc_genetic_backgrounds_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_instruments modeldcc_instruments_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_instruments
    ADD CONSTRAINT modeldcc_instruments_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_labs modeldcc_labs_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_labs
    ADD CONSTRAINT modeldcc_labs_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_mapped_genomes modeldcc_mapped_genomes_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_mapped_genomes
    ADD CONSTRAINT modeldcc_mapped_genomes_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_organisms modeldcc_organisms_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_organisms
    ADD CONSTRAINT modeldcc_organisms_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_platforms modeldcc_platforms_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_platforms
    ADD CONSTRAINT modeldcc_platforms_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_sequencing modeldcc_sequencing_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequencing_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_series modeldcc_series_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_series
    ADD CONSTRAINT modeldcc_series_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_sexes modeldcc_sexes_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sexes
    ADD CONSTRAINT modeldcc_sexes_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_statuses modeldcc_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_statuses
    ADD CONSTRAINT modeldcc_statuses_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_strand_modes modeldcc_strand_modes_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_strand_modes
    ADD CONSTRAINT modeldcc_strand_modes_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_technicalreplicate modeldcc_technicalreplicate_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_technicalreplicate
    ADD CONSTRAINT modeldcc_technicalreplicate_pkey PRIMARY KEY (id);


--
-- Name: modeldcc_versions modeldcc_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_versions
    ADD CONSTRAINT modeldcc_versions_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: modeldcc_appliedassay_assay_id_285b9012; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_appliedassay_assay_id_285b9012 ON public.modeldcc_appliedassay USING btree (assay_id);


--
-- Name: modeldcc_appliedassay_biosamplereplicate_id_b785e701; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_appliedassay_biosamplereplicate_id_b785e701 ON public.modeldcc_appliedassay USING btree (biosamplereplicate_id);


--
-- Name: modeldcc_appliedassay_controlled_by_id_ec0795dd; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_appliedassay_controlled_by_id_ec0795dd ON public.modeldcc_appliedassay USING btree (controlled_by_id);


--
-- Name: modeldcc_appliedassay_user_id_f2bfdcdb; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_appliedassay_user_id_f2bfdcdb ON public.modeldcc_appliedassay USING btree (user_id);


--
-- Name: modeldcc_assay_assay_lab_id_8b385b45; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_assay_assay_lab_id_8b385b45 ON public.modeldcc_assay USING btree (assay_lab_id);


--
-- Name: modeldcc_assay_assay_type_id_10875859; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_assay_assay_type_id_10875859 ON public.modeldcc_assay USING btree (assay_type_id);


--
-- Name: modeldcc_assay_user_id_4b5e9df8; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_assay_user_id_4b5e9df8 ON public.modeldcc_assay USING btree (user_id);


--
-- Name: modeldcc_batchuploaddetails_batchUpload_id_579a900e; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX "modeldcc_batchuploaddetails_batchUpload_id_579a900e" ON public.modeldcc_batchuploaddetails USING btree ("batchUpload_id");


--
-- Name: modeldcc_batchuploads_user_id_5edbaa85; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_batchuploads_user_id_5edbaa85 ON public.modeldcc_batchuploads USING btree (user_id);


--
-- Name: modeldcc_biosample_anatomical_term_id_a79e5ee5; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_anatomical_term_id_a79e5ee5 ON public.modeldcc_biosample USING btree (anatomical_term_id);


--
-- Name: modeldcc_biosample_biosample_lab_id_bc645f02; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_biosample_lab_id_bc645f02 ON public.modeldcc_biosample USING btree (biosample_lab_id);


--
-- Name: modeldcc_biosample_biosample_type_id_52819550; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_biosample_type_id_52819550 ON public.modeldcc_biosample USING btree (biosample_type_id);


--
-- Name: modeldcc_biosample_controlled_by_id_59366641; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_controlled_by_id_59366641 ON public.modeldcc_biosample USING btree (controlled_by_id);


--
-- Name: modeldcc_biosample_genetic_background_id_95e59492; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_genetic_background_id_95e59492 ON public.modeldcc_biosample USING btree (genetic_background_id);


--
-- Name: modeldcc_biosample_organism_id_76d7abf7; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_organism_id_76d7abf7 ON public.modeldcc_biosample USING btree (organism_id);


--
-- Name: modeldcc_biosample_series_id_d143cd33; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_series_id_d143cd33 ON public.modeldcc_biosample USING btree (series_id);


--
-- Name: modeldcc_biosample_sex_id_702c1dd9; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_sex_id_702c1dd9 ON public.modeldcc_biosample USING btree (sex_id);


--
-- Name: modeldcc_biosample_stage_id_a29a517a; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_stage_id_a29a517a ON public.modeldcc_biosample USING btree (stage_id);


--
-- Name: modeldcc_biosample_user_id_3009f447; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosample_user_id_3009f447 ON public.modeldcc_biosample USING btree (user_id);


--
-- Name: modeldcc_biosamplereplicate_biosample_id_9dd0c851; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosamplereplicate_biosample_id_9dd0c851 ON public.modeldcc_biosamplereplicate USING btree (biosample_id);


--
-- Name: modeldcc_biosamplereplicate_user_id_eb2ba878; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_biosamplereplicate_user_id_eb2ba878 ON public.modeldcc_biosamplereplicate USING btree (user_id);


--
-- Name: modeldcc_data_derived_from_data_from_data_id_db251d76; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_derived_from_data_from_data_id_db251d76 ON public.modeldcc_data_derived_from_data USING btree (from_data_id);


--
-- Name: modeldcc_data_derived_from_data_to_data_id_2a35e706; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_derived_from_data_to_data_id_2a35e706 ON public.modeldcc_data_derived_from_data USING btree (to_data_id);


--
-- Name: modeldcc_data_derived_from_id_5c1428db; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_derived_from_id_5c1428db ON public.modeldcc_data USING btree (derived_from_id);


--
-- Name: modeldcc_data_file_type_id_1c384091; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_file_type_id_1c384091 ON public.modeldcc_data USING btree (file_type_id);


--
-- Name: modeldcc_data_mapped_genome_id_23c03676; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_mapped_genome_id_23c03676 ON public.modeldcc_data USING btree (mapped_genome_id);


--
-- Name: modeldcc_data_sequencing_id_a2c5d33b; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_sequencing_id_a2c5d33b ON public.modeldcc_data USING btree (sequencing_id);


--
-- Name: modeldcc_data_user_id_20a5f6e8; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_user_id_20a5f6e8 ON public.modeldcc_data USING btree (user_id);


--
-- Name: modeldcc_data_version_id_4e22d359; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_data_version_id_4e22d359 ON public.modeldcc_data USING btree (version_id);


--
-- Name: modeldcc_sequencing_instrument_id_121aab77; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_sequencing_instrument_id_121aab77 ON public.modeldcc_sequencing USING btree (instrument_id);


--
-- Name: modeldcc_sequencing_platform_id_590cdf8c; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_sequencing_platform_id_590cdf8c ON public.modeldcc_sequencing USING btree (platform_id);


--
-- Name: modeldcc_sequencing_sequencing_lab_id_da8dbd73; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_sequencing_sequencing_lab_id_da8dbd73 ON public.modeldcc_sequencing USING btree (sequencing_lab_id);


--
-- Name: modeldcc_sequencing_strand_mode_id_dd6d88df; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_sequencing_strand_mode_id_dd6d88df ON public.modeldcc_sequencing USING btree (strand_mode_id);


--
-- Name: modeldcc_sequencing_technicalreplicate_id_bdc27ff4; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_sequencing_technicalreplicate_id_bdc27ff4 ON public.modeldcc_sequencing USING btree (technicalreplicate_id);


--
-- Name: modeldcc_sequencing_user_id_22d0158e; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_sequencing_user_id_22d0158e ON public.modeldcc_sequencing USING btree (user_id);


--
-- Name: modeldcc_series_status_id_77fe06d7; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_series_status_id_77fe06d7 ON public.modeldcc_series USING btree (status_id);


--
-- Name: modeldcc_series_user_id_25477691; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_series_user_id_25477691 ON public.modeldcc_series USING btree (user_id);


--
-- Name: modeldcc_technicalreplicate_appliedassay_id_5616f8f4; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_technicalreplicate_appliedassay_id_5616f8f4 ON public.modeldcc_technicalreplicate USING btree (appliedassay_id);


--
-- Name: modeldcc_technicalreplicate_user_id_144d63a6; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX modeldcc_technicalreplicate_user_id_144d63a6 ON public.modeldcc_technicalreplicate USING btree (user_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: test-user
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_appliedassay modeldcc_applied_assay_id_285b9012_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_appliedassay
    ADD CONSTRAINT modeldcc_applied_assay_id_285b9012_fk_dcc FOREIGN KEY (assay_id) REFERENCES public.modeldcc_assay(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_appliedassay modeldcc_applied_biosamplereplicate_i_b785e701_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_appliedassay
    ADD CONSTRAINT modeldcc_applied_biosamplereplicate_i_b785e701_fk_dcc FOREIGN KEY (biosamplereplicate_id) REFERENCES public.modeldcc_biosamplereplicate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_appliedassay modeldcc_applied_controlled_by_id_ec0795dd_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_appliedassay
    ADD CONSTRAINT modeldcc_applied_controlled_by_id_ec0795dd_fk_dcc FOREIGN KEY (controlled_by_id) REFERENCES public.modeldcc_appliedassay(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_appliedassay modeldcc_appliedassay_user_id_f2bfdcdb_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_appliedassay
    ADD CONSTRAINT modeldcc_appliedassay_user_id_f2bfdcdb_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_assay modeldcc_assay_assay_lab_id_8b385b45_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay
    ADD CONSTRAINT modeldcc_assay_assay_lab_id_8b385b45_fk_dcc FOREIGN KEY (assay_lab_id) REFERENCES public.modeldcc_labs(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_assay modeldcc_assay_assay_type_id_10875859_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay
    ADD CONSTRAINT modeldcc_assay_assay_type_id_10875859_fk_dcc FOREIGN KEY (assay_type_id) REFERENCES public.modeldcc_assay_types(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_assay modeldcc_assay_user_id_4b5e9df8_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_assay
    ADD CONSTRAINT modeldcc_assay_user_id_4b5e9df8_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_batchuploaddetails modeldcc_batchup_batchUpload_id_579a900e_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_batchuploaddetails
    ADD CONSTRAINT "modeldcc_batchup_batchUpload_id_579a900e_fk_dcc" FOREIGN KEY ("batchUpload_id") REFERENCES public.modeldcc_batchuploads(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_batchuploads modeldcc_batchuploads_user_id_5edbaa85_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_batchuploads
    ADD CONSTRAINT modeldcc_batchuploads_user_id_5edbaa85_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_anatomical_term_id_a79e5ee5_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_anatomical_term_id_a79e5ee5_fk_dcc FOREIGN KEY (anatomical_term_id) REFERENCES public.modeldcc_anatomical_terms(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosamplereplicate modeldcc_biosamp_biosample_id_9dd0c851_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosamplereplicate
    ADD CONSTRAINT modeldcc_biosamp_biosample_id_9dd0c851_fk_dcc FOREIGN KEY (biosample_id) REFERENCES public.modeldcc_biosample(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_biosample_lab_id_bc645f02_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_biosample_lab_id_bc645f02_fk_dcc FOREIGN KEY (biosample_lab_id) REFERENCES public.modeldcc_labs(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_biosample_type_id_52819550_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_biosample_type_id_52819550_fk_dcc FOREIGN KEY (biosample_type_id) REFERENCES public.modeldcc_biosample_types(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_controlled_by_id_59366641_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_controlled_by_id_59366641_fk_dcc FOREIGN KEY (controlled_by_id) REFERENCES public.modeldcc_biosample(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_genetic_background_i_95e59492_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_genetic_background_i_95e59492_fk_dcc FOREIGN KEY (genetic_background_id) REFERENCES public.modeldcc_genetic_backgrounds(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_organism_id_76d7abf7_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_organism_id_76d7abf7_fk_dcc FOREIGN KEY (organism_id) REFERENCES public.modeldcc_organisms(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_series_id_d143cd33_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_series_id_d143cd33_fk_dcc FOREIGN KEY (series_id) REFERENCES public.modeldcc_series(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosamp_stage_id_a29a517a_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosamp_stage_id_a29a517a_fk_dcc FOREIGN KEY (stage_id) REFERENCES public.modeldcc_developmental_stages(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosamplereplicate modeldcc_biosamp_user_id_eb2ba878_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosamplereplicate
    ADD CONSTRAINT modeldcc_biosamp_user_id_eb2ba878_fk_auth_user FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosample_sex_id_702c1dd9_fk_modeldcc_sexes_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosample_sex_id_702c1dd9_fk_modeldcc_sexes_id FOREIGN KEY (sex_id) REFERENCES public.modeldcc_sexes(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_biosample modeldcc_biosample_user_id_3009f447_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_biosample
    ADD CONSTRAINT modeldcc_biosample_user_id_3009f447_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data_derived_from_data modeldcc_data_de_from_data_id_db251d76_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data_derived_from_data
    ADD CONSTRAINT modeldcc_data_de_from_data_id_db251d76_fk_dcc FOREIGN KEY (from_data_id) REFERENCES public.modeldcc_data(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data_derived_from_data modeldcc_data_de_to_data_id_2a35e706_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data_derived_from_data
    ADD CONSTRAINT modeldcc_data_de_to_data_id_2a35e706_fk_dcc FOREIGN KEY (to_data_id) REFERENCES public.modeldcc_data(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data modeldcc_data_derived_from_id_5c1428db_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_derived_from_id_5c1428db_fk_dcc FOREIGN KEY (derived_from_id) REFERENCES public.modeldcc_appliedassay(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data modeldcc_data_file_type_id_1c384091_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_file_type_id_1c384091_fk_dcc FOREIGN KEY (file_type_id) REFERENCES public.modeldcc_file_types(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data modeldcc_data_mapped_genome_id_23c03676_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_mapped_genome_id_23c03676_fk_dcc FOREIGN KEY (mapped_genome_id) REFERENCES public.modeldcc_mapped_genomes(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data modeldcc_data_sequencing_id_a2c5d33b_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_sequencing_id_a2c5d33b_fk_dcc FOREIGN KEY (sequencing_id) REFERENCES public.modeldcc_sequencing(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data modeldcc_data_user_id_20a5f6e8_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_user_id_20a5f6e8_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_data modeldcc_data_version_id_4e22d359_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_data
    ADD CONSTRAINT modeldcc_data_version_id_4e22d359_fk_dcc FOREIGN KEY (version_id) REFERENCES public.modeldcc_versions(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_sequencing modeldcc_sequenc_instrument_id_121aab77_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequenc_instrument_id_121aab77_fk_dcc FOREIGN KEY (instrument_id) REFERENCES public.modeldcc_instruments(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_sequencing modeldcc_sequenc_platform_id_590cdf8c_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequenc_platform_id_590cdf8c_fk_dcc FOREIGN KEY (platform_id) REFERENCES public.modeldcc_platforms(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_sequencing modeldcc_sequenc_sequencing_lab_id_da8dbd73_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequenc_sequencing_lab_id_da8dbd73_fk_dcc FOREIGN KEY (sequencing_lab_id) REFERENCES public.modeldcc_labs(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_sequencing modeldcc_sequenc_strand_mode_id_dd6d88df_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequenc_strand_mode_id_dd6d88df_fk_dcc FOREIGN KEY (strand_mode_id) REFERENCES public.modeldcc_strand_modes(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_sequencing modeldcc_sequenc_technicalreplicate_i_bdc27ff4_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequenc_technicalreplicate_i_bdc27ff4_fk_dcc FOREIGN KEY (technicalreplicate_id) REFERENCES public.modeldcc_technicalreplicate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_sequencing modeldcc_sequencing_user_id_22d0158e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_sequencing
    ADD CONSTRAINT modeldcc_sequencing_user_id_22d0158e_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_series modeldcc_series_status_id_77fe06d7_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_series
    ADD CONSTRAINT modeldcc_series_status_id_77fe06d7_fk_dcc FOREIGN KEY (status_id) REFERENCES public.modeldcc_statuses(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_series modeldcc_series_user_id_25477691_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_series
    ADD CONSTRAINT modeldcc_series_user_id_25477691_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_technicalreplicate modeldcc_technic_appliedassay_id_5616f8f4_fk_dcc; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_technicalreplicate
    ADD CONSTRAINT modeldcc_technic_appliedassay_id_5616f8f4_fk_dcc FOREIGN KEY (appliedassay_id) REFERENCES public.modeldcc_appliedassay(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: modeldcc_technicalreplicate modeldcc_technic_user_id_144d63a6_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.modeldcc_technicalreplicate
    ADD CONSTRAINT modeldcc_technic_user_id_144d63a6_fk_auth_user FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: test-user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;


--
-- PostgreSQL database dump complete
--
