describe('addSeries test', function() {
  it('tests if the web form for the annotation upload works', function() {
    // destructuring assignment of the this.currentUser object
    cy.visit('/');
    // programmatically log us in without needing the UI
    cy.get("input[name='csrfmiddlewaretoken']").then($input => {
      const hidden_token = $input.val();
      cy.request({
        method: 'POST',
        form: true,
        url: '/login_interface/',
        body: {
          username: 'test-user',
          password: 'password',
          csrfmiddlewaretoken: hidden_token
        }
      });
    });

    // now that we're logged in, we can visit
    // any kind of restricted route!
    cy.visit('/dcc/addSeries/');
    cy.server()
    cy.route('POST', '**/addSeries').as('postSeries')

    cy.get('#series_title').type('test');
    cy.get('#series_survey').click();
    cy.get('#series_description').type('test');
    cy.get('.survey > .series_num_samples_div > input[name="series_num_samples"].series_num_samples_input').type('2,1')
    cy.get('#series_num_assay_input').type('{selectall}1')
    cy.get("#create_biosample_button").click()
    cy.wait("@postSeries")
    cy.route('POST', '**/addBiosample').as('postBiosample')
    cy.get('#biosample_select_all_button').click()
    cy.get('#biosample_lab_select_parent').select('test lab')
    cy.get('#biosample_genetic_background_select_parent').select('AB',{force: true})
    cy.get('#biosample_sample_type_select_parent').select('tissue',{force: true})
    cy.get('#biosample_post_fertilization_select_parent').type("3")
    cy.get('#biosample_developmental_stage_select_parent').select('1-cell',{force: true})
    cy.get('#biosample_anatomical_term_select_parent').select('adductor',{force: true})
    cy.get("#create_assay_button").click()
    cy.wait("@postBiosample")
    cy.route('POST', '**/addAssay').as('postAssay')
    cy.get("#assay_lab_0_select").select("test lab")
    cy.get("#assay_type_0_select").select("ChIP-seq")
    cy.get("#assay_target_input").type("test")
    cy.get("#assay_control_target_input").type("input")
    cy.get("#assay_description_0").type("test")
    cy.get("#create_appliedAssay_button").click()

    cy.wait("@postAssay")
    cy.route('POST', '**/addAppliedAssay').as('postAppliedAssay')
    cy.get("#appliedAssay_cell_0").siblings("#appliedAssay_replicate_0").click()
    cy.get("#assay_0_parent").click()
    cy.get("#appliedAssay_cell_0").siblings("#appliedAssay_replicate_1").click()
    cy.get("#assay_0_parent").click()
    cy.get("#appliedAssay_cell_1").siblings("#appliedAssay_replicate_0").click()
    cy.get("#assay_1_parent").click()
    cy.get("#appliedAssay_cell_1").siblings("#appliedAssay_replicate_0").within(()=>{
      cy.get("[name='appliedAssay_num_tech_replicates']").type('{selectall}2')
    })
    cy.get("#create_sequencing_button").click()
    cy.wait("@postAppliedAssay")
    cy.route('POST', '**/addSequencing').as('postSequencing')
    cy.get('#sequencing_select_all_button').click()
    cy.get('#sequencing_platform_select_parent').select('Illumina',{force: true})
    cy.get('#biosample_lab_select_parent').select('test lab',{force: true})
    cy.get('#sequencing_instrument_select_parent').select('HiSeq X Ten',{force: true})
    cy.get('#sequencing_mode_paired').click()
    cy.get('.input-group-addon').click()
    cy.get('[data-day="01/02/2000"]').click()
    cy.get("#create_data_button").click()
    cy.wait("@postSequencing")
    cy.route('POST', '**/addData').as('postData')
    cy.get("#data_primary_file_label").get("[name='data_primary_file']").eq(0).type("test/test_lab.02SQ.test.R1.fastq.gz")
    cy.get("#data_secondary_file_label").get("[name='data_secondary_file']").eq(0).type("test/test_lab.02SQ.test.R2.fastq.gz")
    cy.get("#data_primary_file_label").get("[name='data_primary_file']").eq(1).type("test/test_lab.02SQ.test.R1.fastq.gz")
    cy.get("#data_secondary_file_label").get("[name='data_secondary_file']").eq(1).type("test/test_lab.02SQ.test.R2.fastq.gz")
    cy.get("#data_primary_file_label").get("[name='data_primary_file']").eq(2).type("test/test_lab.02SQ.test.R1.fastq.gz")
    cy.get("#data_secondary_file_label").get("[name='data_secondary_file']").eq(2).type("test/test_lab.02SQ.test.R2.fastq.gz")
    cy.get("#data_primary_file_label").get("[name='data_primary_file']").eq(3).type("test/test_lab.02SQ.test.R1.fastq.gz")
    cy.get("#data_secondary_file_label").get("[name='data_secondary_file']").eq(3).type("test/test_lab.02SQ.test.R2.fastq.gz")
    cy.get("#submit_data_button").click()
    cy.wait('@postData')
    cy.contains("See details").click()
    cy.get(".btn--undo").each(($el)=>{cy.get($el).click()})
    cy.get(".really-delete").each(($el)=>{cy.get($el).click()})
    cy.get("#btn-delete").click()
    cy.get("#btn-really_delete").click()
  });
});
