describe('Login test', function() {
  it('clicks the "Register or Login" button and enters credentials', function() {
    cy.visit('/');

    cy.contains('Register or Login').click();
    cy.url().should('include', '/users/login');

    cy
      .get('#id_username')
      .type('test-user')
      .should('have.value', 'test-user');
    cy
      .get('#id_password')
      .type('password')
      .should('have.value', 'password');

    cy.get('#login_btn').click();
  });
});
