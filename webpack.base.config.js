var path = require('path');
var webpack = require('webpack');
// var BundleTracker = require('webpack-bundle-tracker');
var glob = require("glob");
// var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
// const ShakePlugin = require('webpack-common-shake').Plugin;

let config = {
  context: __dirname,
  mode: process.env.NODE_ENV,
  entry: {
    dataExport: './dcc/dcc/static/js/dataExport.jsx',
    index: './dcc/dcc/static/js/_index.js',
    visualization: glob.sync('./dcc/dcc/static/js/_stacked*.js')
  },
  output: {
    path: path.resolve('./dcc/dcc/static/js/bundles/'),
    filename: '[name]-[hash].js'
  },

  plugins: [
    // new HardSourceWebpackPlugin()
    // new ShakePlugin()
    // new webpack.DefinePlugin({
    //    'process.env.NODE_ENV': JSON.stringify('production')
    //  })
  ], // add all common plugins here

  module: {
    rules: [] // add all common loaders here
  },

  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.jsx']
  },

  optimization: {
    namedModules: true, // NamedModulesPlugin()
    // splitChunks: {
    //   // CommonsChunkPlugin()
    //   chunks: 'initial'
    // },
    noEmitOnErrors: true, // NoEmitOnErrorsPlugin
    concatenateModules: true //ModuleConcatenationPlugin
  }
};
// process.traceDeprecation = true
module.exports = config;
